{-# LANGUAGE TypeApplications #-}

module Regtool.Costs.ConfigSpec
  ( spec
  ) where

import TestImport

import Regtool.Gen ()

import Regtool.Utils.Costs.Config

spec :: Spec
spec = do
  genValidSpec @CostsConfig
  -- shrinkValidSpec @CostsConfig
  -- doesNotShrinkToItselfSpec @CostsConfig
