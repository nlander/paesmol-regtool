{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Regtool.Webdriver.Utils
  ( waitABit
  , clickButton
  , fill
  , wit
  , renderTestRoute
  , checkpoint
  , startRegtool
  , stopRegtoolIfRunning
  ) where

import TestImport

import Control.Monad.Logger
import Looper

import qualified Data.Text as T

import Web.Stripe (StripeConfig(..), StripeKey(..))

import Yesod.Core

import Test.Hspec
import Test.Hspec.WebDriver

import Regtool
import Regtool.Application ()
import Regtool.Core.Foundation
import Regtool.Data
import Regtool.OptParse
import Regtool.OptParse.Types

import Regtool.Webdriver.Constants

waitABit :: WD ()
waitABit = when webdriverDevelopment $ liftIO $ threadDelay $ round $ (0.5 :: Double) * 1000 * 1000

clickButton :: Test.Hspec.WebDriver.Selector -> WD ()
clickButton s = do
  button <- findElem s
  click button
  waitABit

fill :: Text -> Text -> WD ()
fill name input = do
  nameField <- findElem $ ByName name
  click nameField
  sendKeys input nameField
  waitABit

wit :: String -> WD () -> SpecWith (Arg (WdExample ()))
wit s f = it s $ runWD f

renderTestRoute :: Route Regtool -> String
renderTestRoute r =
  let (comps, _) = renderRoute r
      burl = "http://localhost:" ++ show testPort
   in case comps of
        [] -> burl
        _ -> T.unpack $ T.concat [T.pack burl, "/", T.intercalate "/" comps]

checkpoint :: FilePath -> MVar (Async ()) -> WD () -> WD ()
checkpoint dbPath mv func = do
  testDBFile <- liftIO $ resolveFile' testDbPath
  dbfile <- liftIO $ resolveCheckpointPath dbPath
  exists <- liftIO $ doesFileExist dbfile
  if exists
    then liftIO $ do
           putStrLn $ unwords ["Skipping through to end of checkpoint:", dbPath]
           stopRegtoolIfRunning mv
           putStrLn $ unwords ["Copying", fromAbsFile dbfile, "to", fromAbsFile testDBFile]
           ignoringAbsence $ copyFile dbfile testDBFile
           startRegtool mv
    else do
      liftIO $ startRegtoolIfNotRunning mv
      func
      liftIO $ do
        stopRegtool mv -- Make sure writes are persistent
        copyFile testDBFile dbfile

resolveCheckpointPath :: FilePath -> IO (Path Abs File)
resolveCheckpointPath fp = do
  f <- resolveFile' $ "/tmp/regtool-webdriver-checkpoints/" ++ fp
  ensureDir $ parent f
  pure f

startRegtool :: MVar (Async ()) -> IO ()
startRegtool mv = asyncRegtool >>= putMVar mv

startRegtoolIfNotRunning :: MVar (Async ()) -> IO ()
startRegtoolIfNotRunning mv = do
  ma <- tryReadMVar mv
  case ma of
    Nothing -> asyncRegtool >>= putMVar mv
    Just _ -> pure ()

asyncRegtool :: IO (Async ())
asyncRegtool = do
  testDBFile <- resolveFile' testDbPath
  a <-
    async $
    Regtool.run
      RunSettings
        { runSetsEnv = EnvManualTesting
        , runSetsMaintenance = False
        , runSetsDbFile = testDBFile
        , runSetsPort = testPort
        , runSetsHost = "localhost"
        , runSetsLogLevel = LevelDebug
        , runSetsEmailAddress = unsafeEmailAddress "registration" "paesmol.eu"
        , runSetsLoopers =
            LoopersSettings
              { loopersSetsEmailer =
                  LooperSettings
                    {looperSetEnabled = False, looperSetPhase = 0, looperSetPeriod = seconds 5}
              , loopersSetsBackuper =
                  LooperSettings
                    {looperSetEnabled = False, looperSetPhase = 0, looperSetPeriod = hours 1}
              }
        , runSetsStripeSettings =
            Just
              StripeSettings
                { stripeSetsConfig =
                    StripeConfig {secretKey = StripeKey "sk_test_6sSh4b8d0dZgVg0yJGX8hrZu"}
                , stripeSetsPublishableKey = "pk_test_nKdAP8BsvEq0QR3PTQ2NpzAg"
                }
        , runSetsVerification = DoNotRunVerification
        }
  threadDelay 2000000
  pure a

stopRegtool :: MVar (Async ()) -> IO ()
stopRegtool mv = takeMVar mv >>= cancel

stopRegtoolIfRunning :: MVar (Async ()) -> IO ()
stopRegtoolIfRunning mv = do
  ma <- tryTakeMVar mv
  case ma of
    Nothing -> pure ()
    Just a -> cancel a
