ChoicesCache
  account AccountId
  json ByteString

  UniqueChoices account

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable
