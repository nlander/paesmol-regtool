{-# LANGUAGE OverloadedStrings #-}

module Regtool.LanguageSection.Utils
  ( getLanguageSection
  ) where

import TestImport

import Database.Persist

import Regtool.Data

import Regtool.Application ()
import Regtool.Gen ()
import Regtool.TestUtils

getLanguageSection :: RegtoolExample (Entity LanguageSection)
getLanguageSection = do
  ml <- runDB $ selectFirst ([] :: [Filter LanguageSection]) []
  case ml of
    Nothing -> failure "Should have had a language section"
    Just els -> pure els
