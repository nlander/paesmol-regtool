{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Core.Persist
  ( runDB
  ) where

import Database.Persist.Sqlite

import Yesod.Core
import Yesod.Persist.Core

import Regtool.Core.Foundation.Regtool

instance YesodPersist Regtool where
  type YesodPersistBackend Regtool = SqlBackend
  runDB action = do
    Regtool {regtoolConnectionPool = pool} <- getYesod
    runSqlPool action pool
