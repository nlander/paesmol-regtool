{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

module Regtool.Data.Gen.Company where

import Import

import Regtool.Data

instance GenUnchecked Company

instance GenValid Company

genSimpleCompany :: Gen Company
genSimpleCompany = genValid
