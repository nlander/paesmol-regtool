{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Gen.Costs.Checkout where

import GenImport

import Regtool.Data.Gen ()

import Regtool.Utils.Costs.Checkout

instance GenValid CheckoutsOverview where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid CheckoutOverview where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
