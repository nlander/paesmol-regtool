{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.PaymentReceived
  ( paymentReceivedEmailsDirective
  , paymentReceivedEmailTextContent
  , paymentReceivedEmailHtmlContent
  ) where

import Import

import qualified Data.Map as M
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB

import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet
import Text.Shakespeare.Text

import Regtool.Core.Foundation
import Regtool.Data

import Regtool.Utils.Costs.Checkout
import Regtool.Utils.Costs.Input

import Regtool.Looper.Emailer.Types
import Regtool.Looper.Types
import Regtool.Looper.Utils

paymentReceivedEmailsDirective :: EmailCreationDirective PaymentReceivedEmail
paymentReceivedEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = PaymentReceivedEmailEmail
    , emailTimestampSelector = PaymentReceivedEmailTimestamp
    , makeEmail = makePaymentReceivedEmail
    }

makePaymentReceivedEmail :: PaymentReceivedEmail -> Renderer -> Looper Email
makePaymentReceivedEmail pre@PaymentReceivedEmail {..} render = do
  now <- liftIO getCurrentTime
  mcoo <-
    looperDB $ do
      co <- deriveCheckoutsOverview <$> getCostsInputForAccount paymentReceivedEmailAccount
      pure $ M.lookup paymentReceivedEmailCheckout $ checkoutsOverviewCheckouts co
  ea <- asks looperEmailAddress
  pure $
    case mcoo of
      Just coo ->
        Email
          { emailTo = paymentReceivedEmailTo
          , emailFrom = ea
          , emailFromName = "Regtool Payments"
          , emailSubject = "Payment Received"
          , emailTextContent = paymentReceivedEmailTextContent coo pre render
          , emailHtmlContent = paymentReceivedEmailHtmlContent coo pre render
          , emailStatus = unsent
          , emailError = Nothing
          , emailSesId = Nothing
          , emailAddedTimestamp = now
          }
      Nothing ->
        Email
          { emailTo = unsafeEmailAddress "admin" "paesmol.eu"
          , emailFrom = unsafeEmailAddress "payments" "paesmol.eu"
          , emailFromName = "Regtool Payments"
          , emailSubject = "Payment Received: error while building email"
          , emailTextContent =
              "Something went wrong while sending an email with a payment overview: the checkout was not found."
          , emailHtmlContent =
              "Something went wrong while sending an email with a payment overview: the checkout was not found."
          , emailStatus = unsent
          , emailError = Nothing
          , emailSesId = Nothing
          , emailAddedTimestamp = now
          }

paymentReceivedEmailTextContent :: CheckoutOverview -> PaymentReceivedEmail -> Renderer -> Text
paymentReceivedEmailTextContent coo PaymentReceivedEmail {..} render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/payment-received-email.txt") render

paymentReceivedEmailHtmlContent :: CheckoutOverview -> PaymentReceivedEmail -> Renderer -> Text
paymentReceivedEmailHtmlContent coo PaymentReceivedEmail {..} render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/payment-received-email.hamlet") render
