{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}

module Regtool.Looper.Types where

import Import

import Database.Persist.Sqlite (ConnectionPool)

import Yesod.Core.Types

import Regtool.Core.Foundation
import Regtool.Data

data LooperEnv =
  LooperEnv
    { looperDatabaseFilename :: !(Path Abs File)
    , looperConnectionPool :: !ConnectionPool
    , looperRegtool :: !Regtool
    , looperApproot :: !ResolvedApproot
    , looperEmailAddress :: !EmailAddress
    }

type Looper = ReaderT LooperEnv IO
