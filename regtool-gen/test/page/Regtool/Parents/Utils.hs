{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Parents.Utils
  ( RegisterParent(..)
  , registerParent
  , genSimpleRegisterParent
  , verifyParentRegistered
  ) where

import TestImport

import qualified Data.Text as T
import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types

import Yesod.Form
import Yesod.Test

import Regtool.Data

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Handler.Parents
import Regtool.TestUtils

registerParent :: RegisterParent -> RegtoolExample ()
registerParent RegisterParent {..} = do
  get RegisterParentR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl RegisterParentR
    addToken
    addPostParam "f1" registerParentFirstName
    addPostParam "f2" registerParentLastName
    addPostParam "f3" registerParentAddressLine1
    addPostParamMaybe "f4" registerParentAddressLine2
    addPostParam "f5" registerParentPostalCode
    addPostParam "f6" registerParentCity
    addPostParamStandardOrOther "f7" "f8" registerParentCountry
    addPostParam "f9" registerParentPhoneNumber1
    addPostParamMaybe "f10" registerParentPhoneNumber2
    addPostParam "f11" $ emailAddressText registerParentEmail1
    addPostParamMaybe "f12" $ emailAddressText <$> registerParentEmail2
    addPostParam "f13" $
      T.pack $
      show $ fromJust $ lookup registerParentCompany (zip [minBound .. maxBound] [(1 :: Int) ..])
    addPostParamStandardOrOther "f14" "f15" registerParentRelationShip
    addPostParamMaybe "f16" $ unTextarea <$> registerParentComments
  shouldRedirectTo ParentsR

verifyParentRegistered :: AccountId -> RegisterParent -> RegtoolExample ()
verifyParentRegistered aid RegisterParent {..} = do
  parents <- runDB $ selectList [ParentAccount ==. aid] []
  (Entity _ p) <-
    case parents of
      [] -> failure "Should have found one parent, got empty list instead."
      [p] -> pure p
      ps -> failure $ unlines $ "Should have found one parent, got these instead: " : map show ps
  liftIO $ do
    parentAccount p `shouldBe` aid
    parentFirstName p `shouldBe` registerParentFirstName
    parentLastName p `shouldBe` registerParentLastName
    parentAddressLine1 p `shouldBe` registerParentAddressLine1
    parentAddressLine2 p `shouldBe` registerParentAddressLine2
    parentPostalCode p `shouldBe` registerParentPostalCode
    parentCity p `shouldBe` registerParentCity
    parentCountry p `shouldBe` registerParentCountry
    parentPhoneNumber1 p `shouldBe` registerParentPhoneNumber1
    parentPhoneNumber2 p `shouldBe` registerParentPhoneNumber2
    parentEmail1 p `shouldBe` registerParentEmail1
    parentEmail2 p `shouldBe` registerParentEmail2
    parentCompany p `shouldBe` registerParentCompany
    parentComments p `shouldBe` registerParentComments
