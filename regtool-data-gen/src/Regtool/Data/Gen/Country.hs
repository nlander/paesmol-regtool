{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

module Regtool.Data.Gen.Country where

import Import

import Regtool.Data

import Regtool.Data.Gen.StandardOrRaw ()

instance GenUnchecked StandardCountry

instance GenValid StandardCountry

genSimpleCountry :: Gen Country
genSimpleCountry = genValid
