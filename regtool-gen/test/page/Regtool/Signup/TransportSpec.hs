{-# LANGUAGE OverloadedStrings #-}

module Regtool.Signup.TransportSpec
  ( spec
  ) where

import TestImport

import Database.Persist

import Yesod.Test

import Regtool.Data

import Regtool.Application ()
import Regtool.Buslines.Utils
import Regtool.Children.Utils
import Regtool.Core.Foundation.Regtool
import Regtool.Core.Migrations
import Regtool.Doctors.Utils
import Regtool.Gen ()
import Regtool.LanguageSection.Utils
import Regtool.Parents.Utils
import Regtool.Signup.Transport.Utils
import Regtool.TestUtils

spec :: RegtoolSpec
spec =
  ydescribe "SignupTransport" $ do
    needsLoginSpec GET SignupTransportR
    needsLoginSpec POST SignupTransportR
    multiYit "Signs up a child succesfully" $ \i -> do
      let rp = generateDeterministicallyWithSeed i genSimpleRegisterParent
          rd = generateDeterministicallyWithSeed i genSimpleRegisterDoctor
      withAnyVerifiedAccountLogin $ \(Entity aid _) -> do
        registerParent rp
        registerDoctor rd
        runDB setupLanguageSections
        (Entity did _) <- getDoctorByRegister aid rd
        (Entity lsid _) <- getLanguageSection
        let rc =
              generateDeterministicallyWithSeed i $
              genSimpleRegisterChild did lsid
        registerChild rc
        (Entity cid _) <- getChildByRegister aid rc
        csy <- liftIO getCurrentSchoolyear
        runDB setupBusLines
        (Entity bsid _) <- getBusStop
        let sct =
              SignupChildTransport
                { signupChildTransportChild = cid
                , signupChildTransportBusStop = bsid
                , signupChildTransportSchoolYear = csy
                , signupChildTransportInstalments = 5
                }
        signupChildTransport sct
        verifyChildSignedUpForTransport sct
