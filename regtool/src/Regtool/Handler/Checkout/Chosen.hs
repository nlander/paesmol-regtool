{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Checkout.Chosen
  ( postCheckoutChooseR
  ) where

import Import

import Database.Persist.Sql

import Text.Printf

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

import Regtool.Utils.Stripe

import Regtool.Handler.Admin.Config

import Regtool.Utils.Costs.Abstract
import Regtool.Utils.Costs.Cache
import Regtool.Utils.Costs.Calculate
import Regtool.Utils.Consequences
import Regtool.Utils.Costs.Choice
import Regtool.Utils.Costs.Input
import Regtool.Utils.Costs.Invoice

import Regtool.Handler.Checkout.Form
import Regtool.Handler.Checkout.Page

postCheckoutChooseR :: Handler Html
postCheckoutChooseR = do
  aid <- requireAuthId
  env <- regtoolEnvironment <$> getYesod
  ci <- getCostsInput
  let ac = makeAbstractCosts ci
  conf <- confCostsConfig <$> getConfiguration
  let cc = calculateCosts conf ac
  let choices = deriveChoices cc
  ((result, _), _) <- runFormPost $ chooseForm choices
  now <- liftIO getCurrentTime
  let proposedConsequences -- Only for debugging
       = paymentConsequences (costsInputAccount ci) (toSqlKey (-1)) now choices
  case result of
    FormSuccess chosenChoices -> do
      let i = deriveInvoice chosenChoices
          total = invoiceTotal i
      setChoicesCache aid chosenChoices
      checkoutForm <- makeCheckoutForm total
      let invoiceWidget = $(widgetFile "invoice")
      withNavBar $ mconcat [$(widgetFile "checkout-chosen"), $(widgetFile "checkout-chosen-debug")]
    fr -> getCheckoutChoosePage $ Just fr

amountStripeString :: Amount -> String
amountStripeString = printf "%d" . amountCents

stripeDescription :: Text
stripeDescription = "Services"

stripeDataLabel :: Text
stripeDataLabel = "Pay"

makeCheckoutForm :: Amount -> Handler Widget
makeCheckoutForm amountToBePaid = do
  aid <- requireAuthId
  Account {..} <- runDB $ get404 aid
  token <- genToken
  if amountToBePaid <= zeroAmount
    then pure $(widgetFile "confirm-form")
    else do
      mspk <- (fmap stripeSetsPublishableKey . regtoolStripeSettings) <$> getYesod
      pure $(widgetFile "stripe-form")
