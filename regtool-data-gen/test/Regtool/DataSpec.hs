{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

module Regtool.DataSpec
  ( spec
  ) where

import TestImport

import Data.Aeson

import Database.Persist
import Web.Stripe.Types (ChargeId(..), CustomerId(..))

import Regtool.Data
import Regtool.Data.Gen

spec :: Spec
spec = do
  describe "Helper generators" $ do
    describe "genSeperate" $ do
      it "generates values that are seperate" $
        forAll (genSeperate genUnchecked) $ \ls -> distinct (ls :: [Double])
      it "generates values that are seperate" $
        forAll (genSeperate genValid) $ \ls -> distinct (ls :: [Double])
    describe "genSeperateFor" $ do
      it "generates values that are seperate" $
        forAllValid $ \ls ->
          forAll (genSeperateFor genUnchecked ls) $ \tups -> distinct (tups :: [(Double, Double)])
      it "generates values that are seperate" $
        forAllValid $ \ls ->
          forAll (genSeperateFor genValid ls) $ \tups -> distinct (tups :: [(Double, Double)])
    describe "genValidSeperateFor" $
      it "generates values that are seperate" $
      forAllValid $ \ls ->
        forAll (genValidSeperateFor ls) $ \tups -> distinct (tups :: [(Double, Double)])
    describe "genSeperateIds" $
      it "generates values with seperate ids" $
      forAll genSeperateIds $ \is -> distinct (is :: [Key Account])
    describe "genSeperateIdsFor" $
      it "generates values with seperate ids" $
      forAll genValid $ \as ->
        forAll (genSeperateIdsFor as) $ \es -> distinct $ map entityKey (es :: [Entity Account])
    describe "genValidsWithSeperateIds" $ do
      it "generates values with seperate ids" $
        forAll (genValidsWithSeperateIDs genValid) $ \es ->
          distinct $ map entityKey (es :: [Entity Account])
      it "generates values with seperate ids" $
        forAll (genValidsWithSeperateIDs genValid) $ \es ->
          distinct $ map entityKey (es :: [Entity Account])
    describe "validsWithSeperateIDs" $
      it "generates values with seperate ids" $
      forAll validsWithSeperateIDs $ \es -> distinct $ map entityKey (es :: [Entity Account])
  describe "Dependency types" $ do
    helperTypeSpec @ChargeId
    helperTypeSpec @CustomerId
  describe "Helper types" $ do
    helperTypeSpec @Company
    helperTypeSpec @Nationality
    helperTypeSpec @RelationShip
    helperTypeSpec @SchoolLevel
    helperTypeSpec @SchoolYear
  describe "Database types" $ do
    dbTypeSpec @Account
    dbTypeSpec @BusLine
    dbTypeSpec @BusLineStop
    dbTypeSpec @BusStop
    dbTypeSpec @Checkout
    dbTypeSpec @Child
    dbTypeSpec @ChildOf
    dbTypeSpec @DaycareActivity
    dbTypeSpec @DaycareActivityPayment
    dbTypeSpec @DaycareActivitySignup
    dbTypeSpec @DaycarePayment
    dbTypeSpec @DaycareSemestre
    dbTypeSpec @DaycareSignup
    dbTypeSpec @DaycareTimeslot
    dbTypeSpec @Discount
    dbTypeSpec @Discount
    dbTypeSpec @Doctor
    dbTypeSpec @Email
    dbTypeSpec @LanguageSection
    dbTypeSpec @OccasionalDaycarePayment
    dbTypeSpec @OccasionalDaycareSignup
    dbTypeSpec @OccasionalTransportPayment
    dbTypeSpec @OccasionalTransportSignup
    dbTypeSpec @Parent
    dbTypeSpec @PasswordResetEmail
    dbTypeSpec @PaymentReceivedEmail
    dbTypeSpec @Semestre
    dbTypeSpec @StripeCustomer
    dbTypeSpec @StripePayment
    dbTypeSpec @TransportEnrollment
    dbTypeSpec @TransportPayment
    dbTypeSpec @TransportPaymentPlan
    dbTypeSpec @TransportSignup
    dbTypeSpec @VerificationEmail
    dbTypeSpec @YearlyFeePayment

helperTypeSpec ::
     forall a. (Show a, Eq a, Ord a, Typeable a, GenValid a)
  => Spec
helperTypeSpec = do
  eqSpecOnValid @a
  ordSpecOnValid @a
  genValidSpec @a

dbTypeSpec ::
     forall a. (Show a, Eq a, Ord a, GenValid a, Typeable a, FromJSON a, ToJSON a, PersistField a)
  => Spec
dbTypeSpec = do
  eqSpecOnValid @a
  ordSpecOnValid @a
  genValidSpec @a
  jsonSpecOnValid @a
  persistSpecOnValid @a
