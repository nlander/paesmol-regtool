{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Discount
  ( getAdminAddDiscountR
  , postAdminAddDiscountR
  ) where

import Import

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

data RegisterDiscount =
  RegisterDiscount
    { registerDiscountAmount :: Amount
    , registerDiscountReason :: Maybe Text
    }
  deriving (Show, Eq)

registerDiscountForm :: AForm Handler RegisterDiscount
registerDiscountForm =
  RegisterDiscount <$> areq amountField (bfs ("Amount" :: Text)) Nothing <*>
  aopt textField (bfs ("Reason" :: Text)) Nothing

getAddDiscountPage :: Maybe (FormResult a) -> AccountId -> Handler Html
getAddDiscountPage mfr aid = do
  account <- runDB $ get404 aid
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm registerDiscountForm
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "admin/discount")

getAdminAddDiscountR :: AccountId -> Handler Html
getAdminAddDiscountR = getAddDiscountPage Nothing

postAdminAddDiscountR :: AccountId -> Handler Html
postAdminAddDiscountR aid = do
  ((result, _), _) <-
    runFormPost $ renderCustomForm BootstrapBasicForm registerDiscountForm
  case result of
    FormSuccess RegisterDiscount {..} -> do
      now <- liftIO getCurrentTime
      let discount =
            Discount
              { discountAccount = aid
              , discountAmount = registerDiscountAmount
              , discountReason = registerDiscountReason
              , discountCheckout = Nothing -- Not used yet.
              , discountTimestamp = now
              }
      runDB $ insertValid_ discount
      redirect $ AdminR $ AdminAccountR aid
    fr -> getAddDiscountPage (Just fr) aid
