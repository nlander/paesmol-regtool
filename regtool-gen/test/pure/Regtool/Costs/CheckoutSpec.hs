{-# LANGUAGE TypeApplications #-}

module Regtool.Costs.CheckoutSpec
  ( spec
  ) where

import TestImport

import Regtool.Gen ()

import Regtool.Utils.Costs.Checkout

spec :: Spec
spec = do
  genValidSpec @CheckoutsOverview
  -- shrinkValidSpec @CheckoutsOverview
  -- doesNotShrinkToItselfSpec @CheckoutsOverview
  genValidSpec @CheckoutOverview
  -- shrinkValidSpec @CheckoutOverview
  -- doesNotShrinkToItselfSpec @CheckoutOverview
  describe "deriveCheckoutsOverview" $
    it "produces valid cost checkouts overviews" $
    producesValidsOnValids deriveCheckoutsOverview
