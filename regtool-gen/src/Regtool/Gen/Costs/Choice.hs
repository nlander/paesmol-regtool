{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Gen.Costs.Choice where

import GenImport

import Database.Persist (Entity(..))

import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M
import Data.Map (Map)

import Regtool.Data
import Regtool.Data.Gen

import Regtool.Utils.Costs.Choice

instance GenValid Choices where
  genValid =
    Choices <$> genNonNullMap <*> genValid <*> genValid <*>
    (modEntL (\d -> d {discountCheckout = Nothing}) <$> genValid)
  shrinkValid = shrinkValidStructurally

instance GenValid YearlyChoices where
  genValid = YearlyChoices <$> mgen strictlyPositiveValidAmount <*> genValid <*> genNonNullMap
  shrinkValid = shrinkValidStructurally

genNonNullMap :: (GenValid a, Ord a, GenValid m, Eq m, Monoid m) => Gen (Map a m)
genNonNullMap =
  fmap M.fromList $
  genListOf $ do
    key <- genValid
    value <-
      let go = do
            v <- genValid
            if v == mempty
              then scale (+ 1) go
              else pure v
       in go
    pure (key, value)

shrinkNonNullMap :: (GenValid a, Ord a, GenValid m, Eq m, Monoid m) => Map a m -> [Map a m]
shrinkNonNullMap = filter (notElem mempty) . shrinkValid

instance GenValid SemestrelyChoices where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid TransportInstallmentChoices where
  genValid =
    TransportInstallmentChoices <$> suchThatMap (genListOf strictlyPositiveValidAmount) NE.nonEmpty <*>
    genValid <*>
    genValid
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalTransportChoice where
  genValid = do
    ec <- genValid
    eots <- modEnt (\ots -> ots {occasionalTransportSignupChild = entityKey ec}) <$> genValid
    a <- strictlyPositiveValidAmount
    pure $ OccasionalTransportChoice ec eots a
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareChoice where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareActivityChoice where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalDaycareChoice where
  genValid = do
    ec <- genValid
    eots <- modEnt (\ots -> ots {occasionalDaycareSignupChild = entityKey ec}) <$> genValid
    a <- strictlyPositiveValidAmount
    pure $ OccasionalDaycareChoice ec eots a
  shrinkValid = shrinkValidStructurally
