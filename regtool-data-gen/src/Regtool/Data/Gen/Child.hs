{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Child where

import Import

import Regtool.Data

import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Nationality ()
import Regtool.Data.Gen.SchoolLevel ()
import Regtool.Data.Gen.StandardOrRaw ()
import Regtool.Data.Gen.Utils ()

instance GenValid Child where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
