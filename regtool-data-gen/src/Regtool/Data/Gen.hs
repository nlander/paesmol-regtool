{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-dodgy-exports #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}

module Regtool.Data.Gen
  ( module Regtool.Data.Gen
  , module Regtool.Data.Gen.Account
  , module Regtool.Data.Gen.Amount
  , module Regtool.Data.Gen.BusDirection
  , module Regtool.Data.Gen.BusLine
  , module Regtool.Data.Gen.BusLineStop
  , module Regtool.Data.Gen.BusStop
  , module Regtool.Data.Gen.Checkout
  , module Regtool.Data.Gen.Child
  , module Regtool.Data.Gen.Country
  , module Regtool.Data.Gen.Company
  , module Regtool.Data.Gen.DaycareActivity
  , module Regtool.Data.Gen.DaycareActivityPayment
  , module Regtool.Data.Gen.DaycareActivitySignup
  , module Regtool.Data.Gen.DaycarePayment
  , module Regtool.Data.Gen.DaycareSemestre
  , module Regtool.Data.Gen.DaycareSignup
  , module Regtool.Data.Gen.DaycareTimeslot
  , module Regtool.Data.Gen.Discount
  , module Regtool.Data.Gen.Doctor
  , module Regtool.Data.Gen.EmailAddress
  , module Regtool.Data.Gen.Entity
  , module Regtool.Data.Gen.Nationality
  , module Regtool.Data.Gen.OccasionalDaycarePayment
  , module Regtool.Data.Gen.OccasionalDaycareSignup
  , module Regtool.Data.Gen.OccasionalTransportPayment
  , module Regtool.Data.Gen.OccasionalTransportSignup
  , module Regtool.Data.Gen.Parent
  , module Regtool.Data.Gen.PaymentReceivedEmail
  , module Regtool.Data.Gen.RelationShip
  , module Regtool.Data.Gen.SchoolDay
  , module Regtool.Data.Gen.SchoolLevel
  , module Regtool.Data.Gen.SchoolYear
  , module Regtool.Data.Gen.Semestre
  , module Regtool.Data.Gen.StandardOrRaw
  , module Regtool.Data.Gen.Stripe
  , module Regtool.Data.Gen.StripeCustomer
  , module Regtool.Data.Gen.StripePayment
  , module Regtool.Data.Gen.TransportEnrollment
  , module Regtool.Data.Gen.TransportPayment
  , module Regtool.Data.Gen.TransportPaymentPlan
  , module Regtool.Data.Gen.TransportSignup
  , module Regtool.Data.Gen.Utils
  , module Regtool.Data.Gen.YearlyFeePayment
  ) where

import Import

import Data.Time

import Yesod.Form.Fields

import Regtool.Data

import Regtool.Data.Gen.Account ()
import Regtool.Data.Gen.Amount
import Regtool.Data.Gen.BusDirection ()
import Regtool.Data.Gen.BusLine ()
import Regtool.Data.Gen.BusLineStop ()
import Regtool.Data.Gen.BusStop ()
import Regtool.Data.Gen.Checkout ()
import Regtool.Data.Gen.Child ()
import Regtool.Data.Gen.Company
import Regtool.Data.Gen.Country
import Regtool.Data.Gen.DaycareActivity ()
import Regtool.Data.Gen.DaycareActivityPayment ()
import Regtool.Data.Gen.DaycareActivitySignup ()
import Regtool.Data.Gen.DaycarePayment ()
import Regtool.Data.Gen.DaycareSemestre ()
import Regtool.Data.Gen.DaycareSignup ()
import Regtool.Data.Gen.DaycareTimeslot ()
import Regtool.Data.Gen.Discount ()
import Regtool.Data.Gen.Doctor ()
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Entity
import Regtool.Data.Gen.Nationality ()
import Regtool.Data.Gen.OccasionalDaycarePayment ()
import Regtool.Data.Gen.OccasionalDaycareSignup
import Regtool.Data.Gen.OccasionalTransportPayment ()
import Regtool.Data.Gen.OccasionalTransportSignup
import Regtool.Data.Gen.Parent ()
import Regtool.Data.Gen.PaymentReceivedEmail ()
import Regtool.Data.Gen.RelationShip
import Regtool.Data.Gen.SchoolDay ()
import Regtool.Data.Gen.SchoolLevel ()
import Regtool.Data.Gen.SchoolYear ()
import Regtool.Data.Gen.Semestre ()
import Regtool.Data.Gen.StandardOrRaw
import Regtool.Data.Gen.Stripe ()
import Regtool.Data.Gen.StripeCustomer ()
import Regtool.Data.Gen.StripePayment ()
import Regtool.Data.Gen.TransportEnrollment ()
import Regtool.Data.Gen.TransportPayment ()
import Regtool.Data.Gen.TransportPaymentPlan ()
import Regtool.Data.Gen.TransportSignup
import Regtool.Data.Gen.Utils
import Regtool.Data.Gen.YearlyFeePayment ()

instance GenValid LanguageSection where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid ChildOf where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid Email where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid PasswordResetEmail where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid VerificationEmail where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

genSimpleBirthDate :: Gen Day
genSimpleBirthDate = genValid

genSimpleSchoolLevel :: Gen SchoolLevel
genSimpleSchoolLevel = genValid

genSimpleNationality :: Gen Nationality
genSimpleNationality = genSimpleStandardOrRaw genValid

genSimpleAllergies :: Gen (Maybe Textarea)
genSimpleAllergies = mgen genSimpleTextarea

genSimpleHealthInfo :: Gen (Maybe Textarea)
genSimpleHealthInfo = mgen genSimpleTextarea

genSimpleMedication :: Gen (Maybe Textarea)
genSimpleMedication = mgen genSimpleTextarea

genSimplePickupAllowed :: Gen (Maybe Textarea)
genSimplePickupAllowed = mgen genSimpleTextarea

genSimplePickupDisallowed :: Gen (Maybe Textarea)
genSimplePickupDisallowed = mgen genSimpleTextarea

genSimplePicturePermission :: Gen Bool
genSimplePicturePermission = genValid

genSimpleDelijnBus :: Gen Bool
genSimpleDelijnBus = genValid
