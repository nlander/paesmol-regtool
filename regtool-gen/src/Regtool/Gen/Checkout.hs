{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Gen.Checkout where

import GenImport

import Regtool.Data.Gen ()

import Regtool.Handler.Checkout.Stripe
import Regtool.Utils.Consequences

instance GenValid StripeForm where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid PaymentConsequences where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid TransportPaymentConsequences where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid DaycarePaymentConsequences where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering

instance GenValid DaycareActivityPaymentConsequences where
  genValid = genValidStructurallyWithoutExtraChecking
  shrinkValid = shrinkValidStructurallyWithoutExtraFiltering
