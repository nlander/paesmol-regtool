Account
    username              Text         Maybe
    emailAddress          EmailAddress
    verified              Bool             
    verificationKey       Text         Maybe -- INVARIANT: if verified is False, then this must satisfy isJust
    saltedPassphraseHash  Text
    resetPassphraseKey    Text         Maybe
    creationTime          TimeStamp

    UniqueAccountUsername username     !force
    UniqueAccountEmail    emailAddress

    deriving Show
    deriving Eq
    deriving Ord
    deriving Generic
    deriving Typeable
