Email
    to              EmailAddress
    from            EmailAddress
    fromName        Text
    subject         Text
    textContent     Text
    htmlContent     Text
    status          Text                -- 'Unsent' | 'Sending' | 'Sent' | 'Error'
    error           Text         Maybe
    sesId           Text         Maybe 
    addedTimestamp  TimeStamp
    deriving Show
    deriving Eq
    deriving Ord
    deriving Generic
    deriving Typeable
