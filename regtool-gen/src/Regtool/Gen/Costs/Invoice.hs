{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Gen.Costs.Invoice where

import GenImport

import Regtool.Data.Gen ()

import Regtool.Utils.Costs.Invoice

instance GenValid Invoice where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid CostLine where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid DiscountLine where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
