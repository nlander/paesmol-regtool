{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Utils.Costs.Input where

import Import

import qualified Database.Esqueleto as E
import Database.Esqueleto ((^.))

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Utils.Checkout
import Regtool.Utils.Children
import Regtool.Utils.Parents
import Regtool.Utils.Signup.OccasionalTransport
import Regtool.Utils.Signup.Transport

{-# ANN module ("HLint: ignore Avoid lambda" :: String) #-}

{-# ANN module ("HLint: ignore Redundant if" :: String) #-}

getCostsInput :: Handler CostsInput
getCostsInput = do
  aid <- requireAuthId
  runDB $ getCostsInputForAccount aid

getCostsInputForAccount :: MonadIO m => AccountId -> ReaderT SqlBackend m CostsInput
getCostsInputForAccount costsInputAccount = do
  costsInputParents <- getParentsOf costsInputAccount
  costsInputChildren <- getChildrenOf costsInputAccount
  costsInputTransportSignups <- getOurTransportSignups $ map entityKey costsInputChildren
  costsInputStripeCustomer <- getStripeCustomerOf costsInputAccount
  costsInputStripePayments <- maybe (pure []) (getPaymentsOf . entityKey) costsInputStripeCustomer
  costsInputYearlyFeePayments <- selectList [YearlyFeePaymentAccount ==. costsInputAccount] []
  costsInputTransportEnrollments <-
    getTransportEnrollmentsOf $ map entityKey costsInputTransportSignups
  costsInputTransportPaymentPlans <-
    getTransportPaymentPlansOf $ map entityVal costsInputTransportEnrollments
  costsInputTransportPayments <-
    getTransportPaymentsOf $ map entityKey costsInputTransportPaymentPlans
  costsInputOccasionalTransportSignups <-
    getOurOccasionalTransportSignups $ map entityKey costsInputChildren
  costsInputOccasionalTransportPayments <-
    getOccasionalTransportPaymentsOf $ map entityKey costsInputOccasionalTransportSignups
  costsInputDaycareSemestres <- selectList [] []
  costsInputDaycareTimeslots <- selectList [] []
  costsInputDaycareSignups <-
    selectList [DaycareSignupChild <-. map entityKey costsInputChildren] []
  costsInputDaycarePayments <-
    selectList [DaycarePaymentSignup <-. map entityKey costsInputDaycareSignups] []
  costsInputOccasionalDaycareSignups <-
    selectList [OccasionalDaycareSignupChild <-. map entityKey costsInputChildren] []
  costsInputOccasionalDaycarePayments <-
    selectList
      [OccasionalDaycarePaymentSignup <-. map entityKey costsInputOccasionalDaycareSignups]
      []
  costsInputDaycareActivities <- selectList [] []
  costsInputDaycareActivitySignups <-
    selectList [DaycareActivitySignupChild <-. map entityKey costsInputChildren] []
  costsInputDaycareActivityPayments <-
    selectList [DaycareActivityPaymentSignup <-. map entityKey costsInputDaycareActivitySignups] []
  costsInputDiscounts <- selectList [DiscountAccount ==. costsInputAccount] []
  costsInputCheckouts <- selectList [CheckoutAccount ==. costsInputAccount] []
  pure CostsInput {..}

getChildrensTransportEnrolment :: AccountId -> Handler [(Entity TransportSignup, Entity Child)]
getChildrensTransportEnrolment aid = do
  cs <- runDB $ getChildrenOf aid
  runDB $
    E.select $
    E.from $ \(transportSignup `E.InnerJoin` child) -> do
      E.on $ child ^. ChildId E.==. transportSignup ^. TransportSignupChild
      E.where_ $ child ^. ChildId `E.in_` E.valList (map entityKey cs)
      pure (transportSignup, child)

data CostsInput =
  CostsInput
    { costsInputAccount :: AccountId
    , costsInputParents :: [Entity Parent]
    , costsInputChildren :: [Entity Child]
    , costsInputTransportSignups :: [Entity TransportSignup]
    , costsInputCheckouts :: [Entity Checkout]
    , costsInputStripeCustomer :: Maybe (Entity StripeCustomer)
    , costsInputStripePayments :: [Entity StripePayment]
    , costsInputYearlyFeePayments :: [Entity YearlyFeePayment]
    , costsInputTransportPaymentPlans :: [Entity TransportPaymentPlan]
    , costsInputTransportPayments :: [Entity TransportPayment]
    , costsInputTransportEnrollments :: [Entity TransportEnrollment]
    , costsInputOccasionalTransportSignups :: [Entity OccasionalTransportSignup]
    , costsInputOccasionalTransportPayments :: [Entity OccasionalTransportPayment]
    , costsInputDaycareSemestres :: [Entity DaycareSemestre]
    , costsInputDaycareTimeslots :: [Entity DaycareTimeslot]
    , costsInputDaycareSignups :: [Entity DaycareSignup]
    , costsInputDaycarePayments :: [Entity DaycarePayment]
    , costsInputOccasionalDaycareSignups :: [Entity OccasionalDaycareSignup]
    , costsInputOccasionalDaycarePayments :: [Entity OccasionalDaycarePayment]
    , costsInputDaycareActivities :: [Entity DaycareActivity]
    , costsInputDaycareActivitySignups :: [Entity DaycareActivitySignup]
    , costsInputDaycareActivityPayments :: [Entity DaycareActivityPayment]
    , costsInputDiscounts :: [Entity Discount]
    }
  deriving (Show, Eq, Generic)

instance Validity CostsInput where
  validate ci@CostsInput {..} =
    mconcat
            -- Each sub-part is valid.
      [ genericValidate ci
            -- Each list of entities has entities with seperate id's.
      , check (seperateIds costsInputParents) "Parents have seperate IDs"
      , check (seperateIds costsInputChildren) "Children have seperate IDs"
      , check (seperateIds costsInputTransportSignups) "Transport Signups have seperate IDs"
      , check (seperateIds costsInputCheckouts) "Checkouts have seperate IDs"
      , check (seperateIds costsInputStripePayments) "StripePayments Signups have seperate IDs"
      , check (seperateIds costsInputYearlyFeePayments) "Yearly Fee Payments have seperate IDs"
      , check
          (seperateIds costsInputTransportPaymentPlans)
          "Transport Payment Plans have seperate IDs"
      , check (seperateIds costsInputTransportPayments) "Transport Payments have seperate IDs"
      , check (seperateIds costsInputTransportEnrollments) "Transport Enrollments have seperate IDs"
      , check
          (seperateIds costsInputOccasionalTransportSignups)
          "Occasional Transport Signups have seperate IDs"
      , check
          (seperateIds costsInputOccasionalTransportPayments)
          "Occasional Transport Payments have seperate IDs"
      , check (seperateIds costsInputDaycareSemestres) "DaycareSemestres have seperate IDs"
      , check (seperateIds costsInputDaycareTimeslots) "Daycare Timeslots have seperate IDs"
      , check (seperateIds costsInputDaycareSignups) "Daycare Signups have seperate IDs"
      , check (seperateIds costsInputDaycarePayments) "Daycare Payments have seperate IDs"
      , check
          (seperateIds costsInputOccasionalDaycareSignups)
          "Occasional Daycare Signups have seperate IDs"
      , check
          (seperateIds costsInputOccasionalDaycarePayments)
          "Occasional Daycare Payments have seperate IDs"
      , check (seperateIds costsInputDaycareActivities) "Daycare Activities have seperate IDs"
      , check
          (seperateIds costsInputDaycareActivitySignups)
          "Daycare activity Signups have seperate IDs"
      , check
          (seperateIds costsInputDaycareActivityPayments)
          "Daycare Activity Payments have seperate IDs"
      , check (seperateIds costsInputDiscounts) "Discounts have seperate IDs"
            -- All of the foreign keys are internal.
      , declare "The parents belong to the account" $
        validateForeignKey costsInputParents parentAccount costsInputAccount
      , declare "The signups belong to the children" $
        validateForeignKeys costsInputTransportSignups transportSignupChild costsInputChildren
      , declareUniquenessConstraint
          "There is a maximum of one signup per child per schoolyear"
          costsInputTransportSignups
          (\(Entity _ TransportSignup {..}) -> (transportSignupChild, transportSignupSchoolYear))
      , declare "The stripe customer is the account, if it exists" $
        maybe
          True
          ((== costsInputAccount) . stripeCustomerAccount . entityVal)
          costsInputStripeCustomer
      , declareUniquenessConstraint
          "There is a maximum of one stripe payment per customer per timestamp"
          costsInputStripePayments
          (\(Entity _ StripePayment {..}) -> (stripePaymentCustomer, stripePaymentTimestamp))
      , declareUniquenessConstraint
          "There is a maximum of one stripe payment per charge"
          costsInputStripePayments
          (\(Entity _ StripePayment {..}) -> stripePaymentCharge)
      , declare "The stripe payments belong to the stripe customer" $
        maybe
          True
          (\esc -> validateForeignKey costsInputStripePayments stripePaymentCustomer (entityKey esc))
          costsInputStripeCustomer
      , declareUniquenessConstraint
          "There is a maximum of one yearly fee payment per account per schoolyear"
          costsInputYearlyFeePayments
          (\(Entity _ YearlyFeePayment {..}) -> (costsInputAccount, yearlyFeePaymentSchoolyear))
      , declare "The yearly fee payments belong to the account" $
        validateForeignKey costsInputYearlyFeePayments yearlyFeePaymentAccount costsInputAccount
      , declare "The yearly fee payments use the checkouts" $
        validateForeignKeys costsInputYearlyFeePayments yearlyFeePaymentCheckout costsInputCheckouts
      , declare "The transport payments belong to the payment plans" $
        validateForeignKeys
          costsInputTransportPayments
          transportPaymentPaymentPlan
          costsInputTransportPaymentPlans
      , declare "The transport payments belong to checkouts" $
        validateForeignKeys costsInputTransportPayments transportPaymentCheckout costsInputCheckouts
      , declareUniquenessConstraint
          "There is a maximum of one enrollment per signup"
          costsInputTransportEnrollments
          (\(Entity _ TransportEnrollment {..}) -> transportEnrollmentSignup)
      , declare "The transport enrollments belong to the signups" $
        validateForeignKeys
          costsInputTransportEnrollments
          transportEnrollmentSignup
          costsInputTransportSignups
      , declare "The transport enrollments belong to the payment plans" $
        validateForeignKeys
          costsInputTransportEnrollments
          transportEnrollmentPaymentPlan
          costsInputTransportPaymentPlans
      , declare "The occasional transport signups belong to the children" $
        validateForeignKeys
          costsInputOccasionalTransportSignups
          occasionalTransportSignupChild
          costsInputChildren
      , declareUniquenessConstraint
          "There is a maximum of one occasional transport signup per child per day"
          costsInputOccasionalTransportSignups
          (\(Entity _ OccasionalTransportSignup {..}) ->
             ( occasionalTransportSignupChild
             , occasionalTransportSignupDay
             , occasionalTransportSignupDirection))
      , declare "The occasional transport payments belong to occasional transport signups" $
        validateForeignKeys
          costsInputOccasionalTransportPayments
          occasionalTransportPaymentSignup
          costsInputOccasionalTransportSignups
      , declare "The occasional transport payments belong to the checkouts" $
        validateForeignKeys
          costsInputOccasionalTransportPayments
          occasionalTransportPaymentCheckout
          costsInputCheckouts
      , declareUniquenessConstraint
          "There is a maximum of one occasional transport payment per occasional transport signup"
          costsInputOccasionalTransportPayments
          (\(Entity _ OccasionalTransportPayment {..}) -> occasionalTransportPaymentSignup)
      , declareUniquenessConstraint
          "There is a maximum of one daycare semestre per year per semestre"
          costsInputDaycareSemestres
          (\(Entity _ DaycareSemestre {..}) -> (daycareSemestreYear, daycareSemestreSemestre))
      , declare "The daycare timeslots belong to the semestres" $
        validateForeignKeys
          costsInputDaycareTimeslots
          daycareTimeslotSemestre
          costsInputDaycareSemestres
      , declare "The daycare signups belong to the children" $
        validateForeignKeys costsInputDaycareSignups daycareSignupChild costsInputChildren
      , declare "The daycare signups belong to the timeslots" $
        validateForeignKeys
          costsInputDaycareSignups
          daycareSignupTimeslot
          costsInputDaycareTimeslots
      , declareUniquenessConstraint
          "There is a maximum of one daycare signup per child per time slot"
          costsInputDaycareSignups
          (\(Entity _ DaycareSignup {..}) -> (daycareSignupChild, daycareSignupTimeslot))
      , declare "The daycare payments belong to the signups" $
        validateForeignKeys costsInputDaycarePayments daycarePaymentSignup costsInputDaycareSignups
      , declare "The daycare payments belong to the checkouts" $
        validateForeignKeys costsInputDaycarePayments daycarePaymentCheckout costsInputCheckouts
      , declareUniquenessConstraint
          "There is a maximum of one daycare payment per checkout per daycare signup"
          costsInputDaycarePayments
          (\(Entity _ DaycarePayment {..}) -> (daycarePaymentSignup, daycarePaymentCheckout))
      , declare "The occasional daycare signups belong to the children" $
        validateForeignKeys
          costsInputOccasionalDaycareSignups
          occasionalDaycareSignupChild
          costsInputChildren
      , declareUniquenessConstraint
          "There is a maximum of one occasional daycare signup per child per day"
          costsInputOccasionalDaycareSignups
          (\(Entity _ OccasionalDaycareSignup {..}) ->
             ( occasionalDaycareSignupChild
             , occasionalDaycareSignupTimeslot
             , occasionalDaycareSignupDay))
      , declare "The occasional daycare payments belong to occasional daycare signups" $
        validateForeignKeys
          costsInputOccasionalDaycarePayments
          occasionalDaycarePaymentSignup
          costsInputOccasionalDaycareSignups
      , declare "The occasional daycare payments belong to the checkouts" $
        validateForeignKeys
          costsInputOccasionalDaycarePayments
          occasionalDaycarePaymentCheckout
          costsInputCheckouts
      , declareUniquenessConstraint
          "There is a maximum of one occasional daycare payment per occasional daycare signup"
          costsInputOccasionalDaycarePayments
          (\(Entity _ OccasionalDaycarePayment {..}) -> occasionalDaycarePaymentSignup)
      , declare "The daycareActivities belong to the semestres" $
        validateForeignKeys
          costsInputDaycareActivities
          daycareActivitySemestre
          costsInputDaycareSemestres
      , declare "The daycareActivities belong to the timeslots" $
        validateForeignKeys
          costsInputDaycareActivities
          daycareActivityTimeslot
          costsInputDaycareTimeslots
      , declare "The daycareActivity signups belong to the children" $
        validateForeignKeys
          costsInputDaycareActivitySignups
          daycareActivitySignupChild
          costsInputChildren
      , declare "The daycareActivity signups belong to the timeslots" $
        validateForeignKeys
          costsInputDaycareActivitySignups
          daycareActivitySignupActivity
          costsInputDaycareActivities
      , declareUniquenessConstraint
          "There is a maximum of one daycareActivity signup per child per time slot"
          costsInputDaycareActivitySignups
          (\(Entity _ DaycareActivitySignup {..}) ->
             (daycareActivitySignupChild, daycareActivitySignupActivity))
      , declare "The daycareActivity payments belong to the signups" $
        validateForeignKeys
          costsInputDaycareActivityPayments
          daycareActivityPaymentSignup
          costsInputDaycareActivitySignups
      , declare "The daycareActivity payments belong to the checkouts" $
        validateForeignKeys
          costsInputDaycareActivityPayments
          daycareActivityPaymentCheckout
          costsInputCheckouts
      , declareUniquenessConstraint
          "There is a maximum of one daycareActivity payment per checkout per daycareActivity signup"
          costsInputDaycareActivityPayments
          (\(Entity _ DaycareActivityPayment {..}) ->
             (daycareActivityPaymentSignup, daycareActivityPaymentCheckout))
      , declare "The discounts belong to the account" $
        validateForeignKey costsInputDiscounts discountAccount costsInputAccount
      , declare "There is a maximum of one stripe payment per checkout" $
        distinct $ mapMaybe (checkoutPayment . entityVal) costsInputCheckouts
      , declare "The checkouts belong to the account" $
        validateForeignKey costsInputCheckouts checkoutAccount costsInputAccount
      , declare "The checkouts belong to the stripe payments" $
        validateForeignKeysM costsInputCheckouts checkoutPayment costsInputStripePayments
      , decorate "The checkouts make sense" $
        validateCheckouts
          costsInputCheckouts
          costsInputStripePayments
          costsInputYearlyFeePayments
          costsInputTransportPayments
          costsInputOccasionalTransportPayments
          costsInputDaycarePayments
          costsInputOccasionalDaycarePayments
          costsInputDaycareActivityPayments
          costsInputDiscounts
      ]

validateCheckouts ::
     [Entity Checkout]
  -> [Entity StripePayment]
  -> [Entity YearlyFeePayment]
  -> [Entity TransportPayment]
  -> [Entity OccasionalTransportPayment]
  -> [Entity DaycarePayment]
  -> [Entity OccasionalDaycarePayment]
  -> [Entity DaycareActivityPayment]
  -> [Entity Discount]
  -> Validation
validateCheckouts checkouts stripePayments yearlyFeePayments transportPayments occasionalTransportPayments daycarePayments occasionalDaycarePayments daycareActivityPayments discounts =
  decorateList checkouts $ \(Entity cid Checkout {..}) ->
    mconcat
      [ declare
          "If the checkout amount is strictly positive, then the checkout has as stripe payment" $
        if checkoutAmount > zeroAmount
          then isJust checkoutPayment
          else True
      , declare
          (unwords
             [ "If the checkout amount is strictly negative, (it is"
             , show checkoutAmount
             , ",) then the checkout has a discount"
             ]) $
        if checkoutAmount < zeroAmount
          then any ((== Just cid) . discountCheckout) (map entityVal discounts)
          else True
      , declare "The stripe payment is in the list of stripe payments" $
        maybe True (`elem` map entityKey stripePayments) checkoutPayment
      , declare "The stripe payment has the same amount as the checkout" $
        case checkoutPayment of
          Nothing -> True
          Just pid ->
            case find ((== pid) . entityKey) stripePayments of
              Nothing -> False
              Just (Entity _ StripePayment {..}) -> stripePaymentAmount == checkoutAmount
      , let yfas =
              map yearlyFeePaymentAmount $
              filter ((== cid) . yearlyFeePaymentCheckout) $ map entityVal yearlyFeePayments
            tpas =
              map transportPaymentAmount $
              filter ((== cid) . transportPaymentCheckout) $ map entityVal transportPayments
            otpas =
              map occasionalTransportPaymentAmount $
              filter ((== cid) . occasionalTransportPaymentCheckout) $
              map entityVal occasionalTransportPayments
            dpas =
              map daycarePaymentAmount $
              filter ((== cid) . daycarePaymentCheckout) $ map entityVal daycarePayments
            dapas =
              map daycareActivityPaymentAmount $
              filter ((== cid) . daycareActivityPaymentCheckout) $
              map entityVal daycareActivityPayments
            odpas =
              map occasionalDaycarePaymentAmount $
              filter ((== cid) . occasionalDaycarePaymentCheckout) $
              map entityVal occasionalDaycarePayments
            das =
              map discountAmount $
              filter ((== Just cid) . discountCheckout) $ map entityVal discounts
            gross = sumAmount (concat [yfas, tpas, otpas, dpas, odpas, dapas])
            net = gross `subAmount` sumAmount das
            b = net == checkoutAmount
            showAmount = show . amountCents
            showAmounts = show . map amountCents
            explanation =
              unlines
                [ "The yearly fee payments were"
                , showAmounts yfas
                , "The transport payments were"
                , showAmounts tpas
                , "The occasional transport payments were"
                , showAmounts otpas
                , "The daycare payments were"
                , showAmounts dpas
                , "The daycare activity  payments were"
                , showAmounts dapas
                , "The occasional daycare payments were"
                , showAmounts odpas
                , "The sum of those is"
                , showAmount gross
                , "The discounts were"
                , showAmounts das
                , "The sum minus the discounts"
                , showAmount net
                , "The total amount was"
                , showAmount checkoutAmount
                , "The difference between the actual and the expected is"
                , showAmount $ checkoutAmount `subAmount` net
                ]
         in declare
              (unlines
                 [ "The amounts of everything that went into the checkout sum to the checkout amount"
                 , explanation
                 ])
              b
      ]
