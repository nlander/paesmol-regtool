{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Costs.Cache
  ( setChoicesCache
  , getChoicesCache
  , deleteChoicesCache
  ) where

import Import

import Data.Aeson as JSON
import qualified Data.ByteString.Lazy as LB

import Database.Persist

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Utils.Costs.Choice

setChoicesCache :: AccountId -> Choices -> Handler ()
setChoicesCache aid choices = do
  let bs = LB.toStrict $ JSON.encode choices
  void $
    runDB $
    upsert
      (ChoicesCache {choicesCacheAccount = aid, choicesCacheJson = bs})
      [ChoicesCacheJson =. bs]

getChoicesCache :: AccountId -> Handler (Maybe Choices)
getChoicesCache aid = do
  mc <- runDB $ getBy $ UniqueChoices aid
  case mc of
    Nothing -> pure Nothing
    Just (Entity _ ChoicesCache {..}) -> pure $ JSON.decode $ LB.fromStrict choicesCacheJson

deleteChoicesCache :: AccountId -> Handler ()
deleteChoicesCache aid =
  runDB $ deleteBy $ UniqueChoices aid
