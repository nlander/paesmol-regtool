module Regtool.Utils.Parents
  ( getParentsOf
  , getParentsOfChild
  , canDeleteParent
  ) where

import Import

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Core.Foundation

getParentsOf :: MonadIO m => AccountId -> ReaderT SqlBackend m [Entity Parent]
getParentsOf aid = selectList [ParentAccount ==. aid] []

getParentsOfChild ::
     MonadIO m => ChildId -> ReaderT SqlBackend m [Entity Parent]
getParentsOfChild cid = do
  as <- map (childOfParent . entityVal) <$> selectList [ChildOfChild ==. cid] []
  fmap concat $ forM as $ \aid -> selectList [ParentAccount ==. aid] []

canDeleteParent :: ParentId -> Handler (Deletable ParentId)
canDeleteParent pid = pure $ CanDelete $ DB.delete pid
