{-# LANGUAGE OverloadedStrings #-}

module Regtool.ChildrenSpec
  ( spec
  ) where

import TestImport

import Database.Persist hiding (get)
import Database.Persist.Sqlite (toSqlKey)

import Yesod.Test

import Regtool.Application ()
import Regtool.Children.Utils
import Regtool.Core.Foundation.Regtool
import Regtool.Core.Migrations
import Regtool.Doctors.Utils
import Regtool.Gen ()
import Regtool.LanguageSection.Utils
import Regtool.Parents.Utils
import Regtool.TestUtils

spec :: RegtoolSpec
spec = do
  ydescribe "RegisterChildR" $ do
    needsLoginSpec GET RegisterChildR
    needsLoginSpec POST RegisterChildR
    multiYit "Registers a child succesfully" $ \i -> do
      let rp = generateDeterministicallyWithSeed i genSimpleRegisterParent
      let rd = generateDeterministicallyWithSeed i genSimpleRegisterDoctor
      withAnyVerifiedAccountLogin $ \(Entity aid _) -> do
        registerParent rp
        registerDoctor rd
        runDB setupLanguageSections
        (Entity did _) <- getDoctorByRegister aid rd
        (Entity lsid _) <- getLanguageSection
        rc <- liftIO $ generate $ genSimpleRegisterChild did lsid
        registerChild rc
        verifyChildRegistered aid rc
  ydescribe "ChildrenR" $ do
    needsLoginSpec GET ChildrenR
    yit "returns a 200 for an unverified user" $
      withAnyVerifiedAccountLogin_ $ do
        get ChildrenR
        statusIs 200
    yit "returns a 200 for a verified user" $
      withAnyVerifiedAccountLogin_ $ do
        get ChildrenR
        statusIs 200
  ydescribe "ChildR" $ needsLoginSpec GET $ ChildR $ toSqlKey 0
