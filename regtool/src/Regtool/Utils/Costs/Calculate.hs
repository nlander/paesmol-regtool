{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Costs.Calculate
  ( CalculatedCosts(..)
  , CalculatedYearlyCosts(..)
  , CalculatedSemestrelyCosts(..)
  , YearlyFeePaymentStatus(..)
  , TransportPaymentStatus(..)
  , OccasionalTransportPaymentStatus(..)
  , occasionalTransportPaymentStatusSignup
  , DaycarePaymentStatus(..)
  , DaycareActivityPaymentStatus(..)
  , OccasionalDaycarePaymentStatus(..)
  , occasionalDaycarePaymentStatusSignup
  , calculateCosts
  , calculateYearlyCosts
  , calculateSemestrelyCosts
  , calculateYearlyPayment
  , calculateTransportPayments
  , calculateTransportPaymentStatusForOneChild
  , calculateOccasionalTransportPayment
  ) where

import Import

import Control.Monad.State
import Data.List (head, tail)
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M
import Data.Map (Map)

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Utils.Costs.Abstract
import Regtool.Utils.Costs.Config

{-# ANN module ("HLint: ignore Use record patterns" :: String) #-}

data CalculatedCosts =
  CalculatedCosts
    { calculatedCostsMap :: Map SchoolYear CalculatedYearlyCosts
    , calculatedCostsOccasionalTransport :: [OccasionalTransportPaymentStatus]
    , calculatedCostsOccasionalDaycare :: [OccasionalDaycarePaymentStatus]
    , calculatedCostsDiscounts :: [Entity Discount]
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity CalculatedCosts where
  validate cc@CalculatedCosts {..} =
    mconcat
      [ genericValidate cc
      , declare "The occasional transport stati are about distinct signups" $
        distinct $ map occasionalTransportPaymentStatusSignup calculatedCostsOccasionalTransport
      , declare "The occasional daycare stati are about distinct signups" $
        distinct $ map occasionalDaycarePaymentStatusSignup calculatedCostsOccasionalDaycare
      , decorate "None of the discounts have a checkout" $
        decorateList calculatedCostsDiscounts $ \(Entity _ Discount {..}) ->
          declare "the discount has no checkout" $ isNothing discountCheckout
      ]

data CalculatedYearlyCosts =
  CalculatedYearlyCosts
    { cycYearly :: YearlyFeePaymentStatus
    , cycBus :: Map (Entity Child) TransportPaymentStatus
    , cycSemestrely :: Map (Entity DaycareSemestre) CalculatedSemestrelyCosts
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity CalculatedYearlyCosts

data CalculatedSemestrelyCosts =
  CalculatedSemestrelyCosts
    { cscDaycarePayments :: [DaycarePaymentStatus]
    , cscDaycareActivityPayments :: [DaycareActivityPaymentStatus]
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity CalculatedSemestrelyCosts

data YearlyFeePaymentStatus
  = YearlyFeeNotPaid Amount
  | YearlyFeePaid (Entity YearlyFeePayment)
  | YearlyFeeNotRequired
  deriving (Show, Eq, Ord, Generic)

instance Validity YearlyFeePaymentStatus where
  validate yfps@(YearlyFeeNotPaid a) =
    decorate "YearlyFeeNotPaid" $
    mconcat
      [ genericValidate yfps
      , declare "The amount is not zero" $ a /= zeroAmount
      , declare "The amount is positive" $ a >= zeroAmount
      ]
  validate yfps = genericValidate yfps

data TransportPaymentStatus
  = TransportNotSignedUp
  | TransportPaymentNotStartedButNecessary
      (Entity TransportSignup)
      TransportPaymentPlan
      (NonEmpty Amount)
  | TransportPaymentStartedButNotDone
      (Entity TransportSignup)
      (Entity TransportEnrollment)
      (Entity TransportPaymentPlan)
      [Entity TransportPayment]
      [Entity Checkout]
      (NonEmpty Amount)
  | TransportPaymentEntirelyDone
      (Entity TransportSignup)
      (Entity TransportEnrollment)
      (Entity TransportPaymentPlan)
      [Entity TransportPayment]
      [Entity Checkout]
  | TransportPaymentNotRequiredBecauseOfAmount (Entity TransportSignup)
  | TransportPaymentNotRequiredBecauseOfParents (Entity TransportSignup)
  deriving (Show, Eq, Ord, Generic)

instance Validity TransportPaymentStatus where
  validate tps@(TransportPaymentNotStartedButNecessary _ _ as) =
    decorate "TransportPaymentNotStartedButNecessary" $
    mconcat
      [ genericValidate tps
      , decorate "The amounts" $
        decorateList (NE.toList as) $ \a ->
          mconcat
            [ declare "The amount is not zero" $ a /= zeroAmount
            , declare "The amount is positive" $ a >= zeroAmount
            ]
      ]
  validate tps@(TransportPaymentStartedButNotDone _ _ etpp etps cs as) =
    decorate "TransportPaymentStartedButNotDone" $
    mconcat
      [ genericValidate tps
      , declare "Transport Payments have seperate IDs" $ seperateIds etps
      , declare "Stripe Payments have seperate IDs" $ seperateIds cs
      , declare "The transport payments belong to the payments" $
        validateForeignKeys etps transportPaymentCheckout cs
      , decorate "The amounts" $
        decorateList (NE.toList as) $ \a ->
          mconcat
            [ declare "The amount is not zero" $ a /= zeroAmount
            , declare "The amount is positive" $ a >= zeroAmount
            ]
      , let a1 = sumAmount (map (transportPaymentAmount . entityVal) etps)
            a2 = transportPaymentPlanTotalAmount (entityVal etpp)
         in declare
              (unwords
                 [ "The transport payments' amounts sum to more than the transport payment plan total amount"
                 , fmtAmount a1
                 , "vs"
                 , fmtAmount a2
                 ]) $
            a1 <= a2
      ]
  validate tps@(TransportPaymentEntirelyDone _ _ etpp etps cs) =
    decorate "TransportPaymentEntirelyDone" $
    mconcat
      [ genericValidate tps
      , declare "Transport Payments have seperate IDs" $ seperateIds etps
      , declare "Stripe Payments have seperate IDs" $ seperateIds cs
      , declare "The transport payments belong to the payments" $
        validateForeignKeys etps transportPaymentCheckout cs
      , let a1 = sumAmount (map (transportPaymentAmount . entityVal) etps)
            a2 = transportPaymentPlanTotalAmount (entityVal etpp)
         in declare
              (unwords
                 [ "The transport payments' amounts sum to more than the transport payment plan total amount"
                 , fmtAmount a1
                 , "vs"
                 , fmtAmount a2
                 ]) $
            a1 >= a2
      ]
  validate tps = genericValidate tps

data OccasionalTransportPaymentStatus
  = OccasionalTransportPaymentUnpaid (Entity Child) (Entity OccasionalTransportSignup) Amount
  | OccasionalTransportPaymentPaid
      (Entity Child)
      (Entity OccasionalTransportSignup)
      (Entity OccasionalTransportPayment)
  deriving (Show, Eq, Ord, Generic)

instance Validity OccasionalTransportPaymentStatus where
  validate obfs@(OccasionalTransportPaymentUnpaid ec eots a) =
    decorate "OccasionalTransportPaymentUnpaid" $
    mconcat
      [ genericValidate obfs
      , declare "The transport signup refers to the child" $
        occasionalTransportSignupChild (entityVal eots) == entityKey ec
      , declare "The amount is positive" $ a >= zeroAmount
      ]
  validate obfs@(OccasionalTransportPaymentPaid ec eots eotp) =
    decorate "OccasionalTransportPaymentPaid" $
    mconcat
      [ genericValidate obfs
      , declare "The transport signup refers to the child" $
        occasionalTransportSignupChild (entityVal eots) == entityKey ec
      , declare "The transport payment refers to the transport signup" $
        occasionalTransportPaymentSignup (entityVal eotp) == entityKey eots
      ]

occasionalTransportPaymentStatusSignup ::
     OccasionalTransportPaymentStatus -> Entity OccasionalTransportSignup
occasionalTransportPaymentStatusSignup otfs =
  case otfs of
    OccasionalTransportPaymentUnpaid _ e _ -> e
    OccasionalTransportPaymentPaid _ e _ -> e

data DaycarePaymentStatus
  = DaycarePaymentUnpaid (Entity Child) (Entity DaycareTimeslot) (Entity DaycareSignup) Amount
  | DaycarePaymentPaid
      (Entity Child)
      (Entity DaycareTimeslot)
      (Entity DaycareSignup)
      (Entity DaycarePayment)
  deriving (Show, Eq, Ord, Generic)

instance Validity DaycarePaymentStatus where
  validate dcfs =
    mconcat
      [ genericValidate dcfs
      , case dcfs of
          DaycarePaymentUnpaid ec edcts eds a ->
            decorate "DaycarePaymentUnpaid" $
            mconcat
              [ declare "The signup belongs to the child" $
                daycareSignupChild (entityVal eds) == entityKey ec
              , declare "The signup belongs to the timeslot" $
                daycareSignupTimeslot (entityVal eds) == entityKey edcts
              , validatePositiveAmount a
              ]
          DaycarePaymentPaid ec edcts eds edp ->
            decorate "DaycarePaymentPpaid" $
            mconcat
              [ declare "The signup belongs to the child" $
                daycareSignupChild (entityVal eds) == entityKey ec
              , declare "The signup belongs to the timeslot" $
                daycareSignupTimeslot (entityVal eds) == entityKey edcts
              , declare "The payment belongs to the signup" $
                daycarePaymentSignup (entityVal edp) == entityKey eds
              ]
      ]

data DaycareActivityPaymentStatus
  = DaycareActivityPaymentUnpaid
      (Entity Child)
      (Entity DaycareActivity)
      (Entity DaycareActivitySignup)
      Amount
  | DaycareActivityPaymentPaid
      (Entity Child)
      (Entity DaycareActivity)
      (Entity DaycareActivitySignup)
      (Entity DaycareActivityPayment)
  deriving (Show, Eq, Ord, Generic)

instance Validity DaycareActivityPaymentStatus where
  validate dcfs =
    mconcat
      [ genericValidate dcfs
      , case dcfs of
          DaycareActivityPaymentUnpaid ec edcts eds a ->
            decorate "DaycareActivityPaymentUnpaid" $
            mconcat
              [ declare "The signup belongs to the child" $
                daycareActivitySignupChild (entityVal eds) == entityKey ec
              , declare "The signup belongs to the activity" $
                daycareActivitySignupActivity (entityVal eds) == entityKey edcts
              , validatePositiveAmount a
              ]
          DaycareActivityPaymentPaid ec edcts eds edp ->
            decorate "DaycareActivityPaymentPpaid" $
            mconcat
              [ declare "The signup belongs to the child" $
                daycareActivitySignupChild (entityVal eds) == entityKey ec
              , declare "The signup belongs to the activity" $
                daycareActivitySignupActivity (entityVal eds) == entityKey edcts
              , declare "The payment belongs to the signup" $
                daycareActivityPaymentSignup (entityVal edp) == entityKey eds
              ]
      ]

data OccasionalDaycarePaymentStatus
  = OccasionalDaycarePaymentUnpaid (Entity Child) (Entity OccasionalDaycareSignup) Amount
  | OccasionalDaycarePaymentPaid
      (Entity Child)
      (Entity OccasionalDaycareSignup)
      (Entity OccasionalDaycarePayment)
  deriving (Show, Eq, Ord, Generic)

instance Validity OccasionalDaycarePaymentStatus where
  validate obfs@(OccasionalDaycarePaymentUnpaid ec eots a) =
    decorate "OccasionalDaycarePaymentUnpaid" $
    mconcat
      [ genericValidate obfs
      , declare "The daycare signup refers to the child" $
        occasionalDaycareSignupChild (entityVal eots) == entityKey ec
      , declare "The amount is positive" $ a >= zeroAmount
      ]
  validate obfs@(OccasionalDaycarePaymentPaid ec eots eotp) =
    decorate "OccasionalDaycarePaymentPaid" $
    mconcat
      [ genericValidate obfs
      , declare "The daycare signup refers to the child" $
        occasionalDaycareSignupChild (entityVal eots) == entityKey ec
      , declare "The daycare payment refers to the daycare signup" $
        occasionalDaycarePaymentSignup (entityVal eotp) == entityKey eots
      ]

occasionalDaycarePaymentStatusSignup ::
     OccasionalDaycarePaymentStatus -> Entity OccasionalDaycareSignup
occasionalDaycarePaymentStatusSignup otfs =
  case otfs of
    OccasionalDaycarePaymentUnpaid _ e _ -> e
    OccasionalDaycarePaymentPaid _ e _ -> e

calculateCosts :: CostsConfig -> AbstractCosts -> CalculatedCosts
calculateCosts cc AbstractCosts {..} =
  CalculatedCosts
    { calculatedCostsMap = M.map (calculateYearlyCosts cc) abstractCostsMap
    , calculatedCostsOccasionalTransport =
        map (calculateOccasionalTransportPayment cc) abstractCostsOccasionalTransport
    , calculatedCostsOccasionalDaycare =
        map calculateOccasionalDaycarePayment abstractCostsOccasionalDaycare
    , calculatedCostsDiscounts = abstractCostsDiscounts
    }

calculateYearlyCosts :: CostsConfig -> AbstractYearlyCosts -> CalculatedYearlyCosts
calculateYearlyCosts cc AbstractYearlyCosts {..} =
  CalculatedYearlyCosts
    { cycYearly = calculateYearlyPayment cc aycYearlyFee
    , cycBus = calculateTransportPayments cc aycYearlyBus
    , cycSemestrely = calculateSemestrelyCosts cc aycYearlySemestrely
    }

calculateYearlyPayment :: CostsConfig -> YearlyFeeStatus -> YearlyFeePaymentStatus
calculateYearlyPayment CostsConfig {..} YearlyFeeStatusNotPaid =
  if costsConfigAnnualFee <= zeroAmount
    then YearlyFeeNotRequired
    else YearlyFeeNotPaid costsConfigAnnualFee
calculateYearlyPayment _ (YearlyFeeStatusPaid yfp) = YearlyFeePaid yfp
calculateYearlyPayment _ YearlyFeeStatusNotRequired = YearlyFeeNotRequired

calculateTransportPayments ::
     CostsConfig
  -> Map (Entity Child) TransportFeeStatus
  -> Map (Entity Child) TransportPaymentStatus
calculateTransportPayments CostsConfig {..} =
  M.fromList .
  flip evalState list .
  traverse (\(c, tfs) -> (,) c <$> go tfs) . sortOn (feeStatusNeedsStart . snd) . M.toList
  where
    list = costsConfigBusYearly ++ repeat costsConfigBusYearlyDefault -- Infinite list
    go :: TransportFeeStatus -> State [Amount] TransportPaymentStatus
    go tfs = do
      a <- gets head -- Safe because the list is infinite
      let (b, tps) = calculateTransportPaymentStatusForOneChild tfs a
      when b $ modify tail -- Safe because the list is infinite
      pure tps

-- The bool indicates whether the amount was used.
calculateTransportPaymentStatusForOneChild ::
     TransportFeeStatus -> Amount -> (Bool, TransportPaymentStatus)
calculateTransportPaymentStatusForOneChild tfs a =
  case tfs of
    TransportFeePaymentEntirelyDone ts te tpp tps cs ->
      (True, TransportPaymentEntirelyDone ts te tpp tps cs)
    TransportFeePaymentStartedButNotDone ts te tpp tps cs
                -- 'a' (from the state)
                -- is the amount that is to be paid according to the config
                -- 'transportPaymentPlanTotalAmount' is the amount that is to be paid
                -- according to the payment plan.
                -- If the config has changed, then these could be different.
                -- That would be a mistake, so we should just follow the payment
                -- plan here.
     ->
      let totalToBePaid = transportPaymentPlanTotalAmount
          nbPaymentsAlreadyDone = length tps
                -- The number of payments that are left to be made is then the
                -- the number of instalments minus that.
                -- This number could be negative, which means the number of
                -- instalments has been changed after payments have been made.
          nbPaymentsLeft = transportSignupInstalments (entityVal ts) - nbPaymentsAlreadyDone
          totalAlreadyPaid = sumAmount $ map (transportPaymentAmount . entityVal) tps
          totalLeftToBePaid = totalToBePaid (entityVal tpp) `subAmount` totalAlreadyPaid
          ps
            | totalLeftToBePaid <= zeroAmount = TransportPaymentEntirelyDone ts te tpp tps cs
            | nbPaymentsLeft <= 1
                                                    -- Then we just have one more installment
                                                    -- Note [Word check]
             = TransportPaymentStartedButNotDone ts te tpp tps cs (totalLeftToBePaid :| [])
            | otherwise =
              case divAmount
                     totalLeftToBePaid
                     (fromIntegral nbPaymentsLeft) -- Safe because of previous check [Word check]
                    of
                DivByZero ->
                  TransportPaymentStartedButNotDone ts te tpp tps cs (totalLeftToBePaid :| [])
                                          -- Should not happen, but is fine if it does
                DivOfZero ->
                  TransportPaymentStartedButNotDone ts te tpp tps cs (totalLeftToBePaid :| [])
                                          -- Should not happen, but is fine if it does
                DivSuccess instalments ->
                  TransportPaymentStartedButNotDone ts te tpp tps cs instalments
       in (True, ps)
    TransportFeePaymentNotStartedButNecessary ts ->
      if a <= zeroAmount
        then (True, TransportPaymentNotRequiredBecauseOfAmount ts)
        else let tpp = TransportPaymentPlan {transportPaymentPlanTotalAmount = a}
                 nbInstalments = transportSignupInstalments (entityVal ts)
                 tps =
                   if nbInstalments <= 0
                     then TransportPaymentNotStartedButNecessary ts tpp (a :| [])
                     else case divAmount a (fromIntegral nbInstalments) of
                            DivByZero ->
                              TransportPaymentNotStartedButNecessary
                                ts
                                tpp
                                (a :| []) -- Should not happen, but would be fine.
                            DivOfZero ->
                              TransportPaymentNotStartedButNecessary
                                ts
                                tpp
                                (a :| []) -- Should not happen, but would be fine.
                            DivSuccess instalments ->
                              TransportPaymentNotStartedButNecessary ts tpp instalments
              in (True, tps)
    TransportFeeNotRequired ts -> (False, TransportPaymentNotRequiredBecauseOfParents ts)
    TransportFeeNotSignedUp -> (False, TransportNotSignedUp)

-- The order here matters, don't change it willy-nilly.
data NeedsStart
  = Started
  | NotStarted
  | NotSignedUp
  deriving (Show, Eq, Ord, Enum, Bounded)

feeStatusNeedsStart :: TransportFeeStatus -> NeedsStart
feeStatusNeedsStart TransportFeeNotSignedUp = NotSignedUp
feeStatusNeedsStart (TransportFeePaymentNotStartedButNecessary _) = NotStarted
feeStatusNeedsStart (TransportFeePaymentStartedButNotDone _ _ _ _ _) = Started
feeStatusNeedsStart (TransportFeePaymentEntirelyDone _ _ _ _ _) = Started
feeStatusNeedsStart (TransportFeeNotRequired _) = NotSignedUp

calculateOccasionalTransportPayment ::
     CostsConfig -> OccasionalTransportFeeStatus -> OccasionalTransportPaymentStatus
calculateOccasionalTransportPayment cc otfs =
  case otfs of
    (OccasionalTransportFeeUnpaid ec eots) ->
      OccasionalTransportPaymentUnpaid ec eots $ costsConfigOccasionalBus cc
    (OccasionalTransportFeePaid ec eots eotp) -> OccasionalTransportPaymentPaid ec eots eotp

calculateSemestrelyCosts ::
     CostsConfig
  -> Map (Entity DaycareSemestre) AbstractSemestrelyCosts
  -> Map (Entity DaycareSemestre) CalculatedSemestrelyCosts
calculateSemestrelyCosts _ = M.map calculateSemestrelyCostsFor

calculateSemestrelyCostsFor :: AbstractSemestrelyCosts -> CalculatedSemestrelyCosts
calculateSemestrelyCostsFor AbstractSemestrelyCosts {..} =
  CalculatedSemestrelyCosts
    { cscDaycarePayments = map calculateDaycarePayments ascDaycareFees
    , cscDaycareActivityPayments = map calculateDaycareActivityPayments ascDaycareActivityFees
    }

calculateDaycarePayments :: DaycareFeeStatus -> DaycarePaymentStatus
calculateDaycarePayments (DaycareFeeUnpaid ec edcts es) =
  DaycarePaymentUnpaid ec edcts es (daycareTimeslotFee $ entityVal edcts)
calculateDaycarePayments (DaycareFeePaid ec edcts es ep) = DaycarePaymentPaid ec edcts es ep

calculateDaycareActivityPayments :: DaycareActivityFeeStatus -> DaycareActivityPaymentStatus
calculateDaycareActivityPayments (DaycareActivityFeeUnpaid ec edcts es) =
  DaycareActivityPaymentUnpaid ec edcts es (daycareActivityFee $ entityVal edcts)
calculateDaycareActivityPayments (DaycareActivityFeePaid ec edcts es ep) =
  DaycareActivityPaymentPaid ec edcts es ep

calculateOccasionalDaycarePayment :: OccasionalDaycareFeeStatus -> OccasionalDaycarePaymentStatus
calculateOccasionalDaycarePayment otfs =
  case otfs of
    (OccasionalDaycareFeeUnpaid ec edcts eots) ->
      OccasionalDaycarePaymentUnpaid ec eots (daycareTimeslotOccasionalFee $ entityVal edcts)
    (OccasionalDaycareFeePaid ec _edcts eots eotp) -> OccasionalDaycarePaymentPaid ec eots eotp
