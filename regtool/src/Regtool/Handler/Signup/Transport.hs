{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Signup.Transport
  ( SignupChildTransport(..)
  , getSignupTransportR
  , postSignupTransportR
  , postSignupTransportSignoffR
  ) where

import Import

import qualified Data.Text as T

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Utils.Children
import Regtool.Utils.Signup.Transport

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildTransport =
  SignupChildTransport
    { signupChildTransportChild :: ChildId
    , signupChildTransportBusStop :: BusStopId
    , signupChildTransportSchoolYear :: SchoolYear
    , signupChildTransportInstalments :: Int
    }
  deriving (Show, Eq, Generic)

instance Validity SignupChildTransport

signupChildTransportForm ::
     UTCTime
  -> [Entity Child]
  -> [(Entity BusLine, [Entity BusStop])]
  -> AForm Handler SignupChildTransport
signupChildTransportForm now cs bls =
  SignupChildTransport <$>
  areq
    (selectFieldList $
     map (\(Entity cid Child {..}) -> (T.unwords [childFirstName, childLastName], cid)) cs)
    (bfs ("Child" :: Text))
    Nothing <*>
  areq
    (selectFieldList $ do
       (Entity _ bl, Entity bsid bs) <- unTup bls
       let t =
             T.unwords
               [ "Line:"
               , busLineName bl
               , "; Stop:"
               , unTextarea $ busStopCity bs
               , unTextarea $ busStopLocation bs
               ]
       pure (t, bsid))
    (bfs ("Bus Stop" :: Text))
    Nothing <*>
  areq
    (selectFieldList $ map (\sy -> (schoolYearText sy, sy)) [currentSchoolYear now])
    (bfs ("School Year" :: Text))
    Nothing <*>
  areq
    (checkBool (> 0) ("Number of instalments must be strictly positive" :: Text) $
     selectFieldList
       [("Single payment" :: Text, 1), ("Per semester" :: Text, 2), ("Per trimester" :: Text, 3)])
    (bfs ("Instalments" :: Text))
    Nothing
  where
    unTup :: [(a, [b])] -> [(a, b)]
    unTup = concatMap (\(a, bs) -> (,) a <$> bs)

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  busLines <- runDB $ selectList [] [Asc BusLineId]
  busStops <- runDB $ selectList [BusStopArchived ==. False] [Asc BusStopId]
  busLineTups <- getBusLines
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurTransportSignups $ map entityKey children
  let ets = signupTrips signups children busStops
  now <- liftIO getCurrentTime
  token <- genToken
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $ signupChildTransportForm now children busLineTups
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/transport")

getSignupTransportR :: Handler Html
getSignupTransportR = getSignupPage Nothing

getBusLines :: Handler [(Entity BusLine, [Entity BusStop])]
getBusLines = do
  busLines <- runDB $ selectList [] [Asc BusLineId]
  busStops <- runDB $ selectList [BusStopArchived ==. False] [Asc BusStopId]
  attachments <- runDB $ selectList [] [Asc BusLineStopId]
  let go blid (Entity bsid _) =
        any
          (\(Entity _ bls) -> busLineStopLine bls == blid && busLineStopStop bls == bsid)
          attachments
  pure $ flip map busLines $ \bl@(Entity blid _) -> (bl, filter (go blid) busStops)

postSignupTransportR :: Handler Html
postSignupTransportR = do
  aid <- requireAuthId
  busLineTups <- getBusLines
  children <- runDB $ getChildrenOf aid
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost $
    renderCustomForm BootstrapBasicForm $ signupChildTransportForm now children busLineTups
  case result of
    FormSuccess SignupChildTransport {..} -> do
      let signup =
            TransportSignup
              { transportSignupChild = signupChildTransportChild
              , transportSignupBusStop = signupChildTransportBusStop
              , transportSignupSchoolYear = signupChildTransportSchoolYear
              , transportSignupInstalments = signupChildTransportInstalments
              , transportSignupTime = now
              }
      void $ runDB $ insertValidUnique signup
      setMessage "Succesfully signed up for transport services. Do not forget to pay for them!"
      redirect SignupTransportR
    fr -> getSignupPage (Just fr)

postSignupTransportSignoffR :: TransportSignupId -> Handler Html
postSignupTransportSignoffR sid = do
  aid <- requireAuthId
  TransportSignup {..} <- runDB $ get404 sid
  mcof <- runDB $ selectFirst [ChildOfParent ==. aid, ChildOfChild ==. transportSignupChild] []
  case mcof of
    Nothing -> invalidArgs ["This is not your child to unregister."]
    Just _ -> do
      mte <- runDB $ selectFirst [TransportEnrollmentSignup ==. sid] []
      case mte of
        Just _ -> invalidArgs ["This signup has already been paid for, it cannot be unregistered."]
        Nothing -> do
          runDB $ DB.delete sid
          redirect SignupTransportR
