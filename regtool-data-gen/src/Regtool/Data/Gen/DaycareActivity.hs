{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.DaycareActivity where

import Import

import Regtool.Data

import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.SchoolDay ()
import Regtool.Data.Gen.SchoolLevel ()
import Regtool.Data.Gen.Utils ()

instance GenValid DaycareActivity where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
