module Regtool.Looper.Backuper
  ( backupLooper
  ) where

import Import

import Regtool.Looper.Types

backupLooper :: Looper ()
backupLooper = do
  dbFile <- asks looperDatabaseFilename
  now <- liftIO getZonedTime
  let backupFilename = formatTime defaultTimeLocale "%F_%T%z" now ++ ".db"
  backupDir <- liftIO $ resolveDir' "backups"
  backupFile <- liftIO $ resolveFile backupDir backupFilename
  liftIO $ ensureDir $ parent backupFile
  copyFile dbFile backupFile
