{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.PasswordReset
  ( passwordResetEmailsDirective
  , passwordResetEmailTextContent
  , passwordResetEmailHtmlContent
  ) where

import Import

import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB

import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet
import Text.Shakespeare.Text

import Regtool.Core.Foundation
import Regtool.Data

import Regtool.Looper.Emailer.Types
import Regtool.Looper.Types

passwordResetEmailsDirective :: EmailCreationDirective PasswordResetEmail
passwordResetEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = PasswordResetEmailEmail
    , emailTimestampSelector = PasswordResetEmailTimestamp
    , makeEmail = makePasswordResetEmail
    }

makePasswordResetEmail :: PasswordResetEmail -> Renderer -> Looper Email
makePasswordResetEmail pre@PasswordResetEmail {..} render = do
  now <- liftIO getCurrentTime
  ea <- asks looperEmailAddress
  pure
    Email
      { emailTo = passwordResetEmailTo
      , emailFrom = ea
      , emailFromName = "Registration Tool Password Reset"
      , emailSubject = "Password Reset"
      , emailTextContent = passwordResetEmailTextContent pre render
      , emailHtmlContent = passwordResetEmailHtmlContent pre render
      , emailStatus = unsent
      , emailError = Nothing
      , emailSesId = Nothing
      , emailAddedTimestamp = now
      }

passwordResetEmailTextContent :: PasswordResetEmail -> Renderer -> Text
passwordResetEmailTextContent PasswordResetEmail {..} render =
  LT.toStrict $
  LTB.toLazyText $ $(textFile "templates/auth/password-reset-email.txt") render

passwordResetEmailHtmlContent :: PasswordResetEmail -> Renderer -> Text
passwordResetEmailHtmlContent PasswordResetEmail {..} render =
  LT.toStrict $
  renderHtml $ $(hamletFile "templates/auth/password-reset-email.hamlet") render
