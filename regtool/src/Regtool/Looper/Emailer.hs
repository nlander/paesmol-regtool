{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer
  ( emailLooper
  ) where

import Import

import Control.Lens

import System.IO

import Control.Exception
import Control.Monad.Trans.AWS hiding (seconds)
import Network.AWS.SES

import qualified Data.Text as T

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Core.Foundation
import Regtool.Looper.Emailer.PasswordReset
import Regtool.Looper.Emailer.PaymentReceived
import Regtool.Looper.Emailer.Types
import Regtool.Looper.Emailer.Verification
import Regtool.Looper.Types
import Regtool.Looper.Utils

emailLooper :: Looper ()
emailLooper = do
  makeEmailsFrom verificationEmailsDirective
  makeEmailsFrom passwordResetEmailsDirective
  makeEmailsFrom paymentReceivedEmailsDirective
  sendEmails

makeEmailsFrom :: PersistRecordBackend a SqlBackend => EmailCreationDirective a -> Looper ()
makeEmailsFrom dir@EmailCreationDirective {..} = do
  list <- looperDB $ selectFirst [emailIdSelector ==. Nothing] [Asc emailTimestampSelector]
  case list of
    Nothing -> pure ()
    Just (Entity seid se) -> do
      y <- asks looperRegtool
      ar <- asks looperApproot
      email <- makeEmail se (yesodRender y ar)
      looperDB $ do
        eid <- insertValid email
        DB.update seid [emailIdSelector DB.=. Just eid]
      makeEmailsFrom dir -- Go on until there are no more conversions to be made

sendEmails :: Looper ()
sendEmails = do
  list <- looperDB $ selectFirst [EmailStatus ==. unsent] [Asc EmailAddedTimestamp]
  case list of
    Nothing -> pure ()
    Just (Entity emailId email) -> do
      looperDB $ DB.update emailId [EmailStatus DB.=. emailSending]
      newStatus <- liftIO $ sendSingleEmail email
      looperDB $
        DB.update emailId $
        case newStatus of
          EmailSending -> [EmailStatus DB.=. emailSending]
          EmailSent hid -> [EmailStatus DB.=. emailSent, EmailSesId DB.=. Just hid]
          EmailErrored err -> [EmailStatus DB.=. emailErrored, EmailError DB.=. Just err]
          EmailUnsent -> []
      sendEmails -- Go on until there are no more emails to be sent.

sendSingleEmail :: Email -> IO EmailStatus
sendSingleEmail Email {..} = do
  lgr <- newLogger Debug stdout
  env <- set envLogger lgr <$> newEnv Discover
  runResourceT . runAWST env $ do
    let txt = content emailTextContent
    let html = content emailHtmlContent
    let bod = body & bText .~ Just txt & bHTML .~ Just html
    let sub = content emailSubject
    let mesg = message sub bod
    let dest = destination & dToAddresses .~ [emailAddressText emailTo]
    let req = sendEmail (emailAddressText emailFrom) dest mesg
    errOrResp <- trying _Error $ send req
    pure $ case errOrResp of
      Left err -> EmailErrored $ T.pack $ displayException err
      Right resp ->
        case resp ^. sersResponseStatus of
          200 -> EmailSent $ resp ^. sersMessageId
          _ -> EmailErrored "Error while sending email."
