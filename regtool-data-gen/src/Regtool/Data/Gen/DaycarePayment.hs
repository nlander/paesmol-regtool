{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.DaycarePayment where

import Import

import Regtool.Data

import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.DaycareSignup ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenUnchecked DaycarePayment

instance GenValid DaycarePayment where
  genValid = genValidStructurally
