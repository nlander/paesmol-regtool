{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Utils.Children
  ( getChildrenOf
  , withOwnChild
  , canDeleteChild
  , childName
  ) where

import Import

import qualified Data.Text as T
import qualified Database.Esqueleto as E
import Database.Esqueleto ((^.))
import qualified Database.Persist.Sqlite as DB

import Regtool.Data

import Regtool.Core.Foundation

getChildrenOf :: MonadIO m => AccountId -> ReaderT SqlBackend m [Entity Child]
getChildrenOf accid =
  E.select $
  E.from $ \(child `E.InnerJoin` childOf) -> do
    E.on $ childOf ^. ChildOfChild E.==. child ^. ChildId
    E.where_ $ childOf ^. ChildOfParent E.==. E.val accid
    pure child

withOwnChild :: ChildId -> Handler Html -> Handler Html
withOwnChild cid func = do
  aid <- requireAuthId
  isOwnChild <- isChildOf cid aid
  isAdminAcc <- (== Just True) <$> isAdmin
  if isOwnChild || isAdminAcc
    then func
    else permissionDenied "Not your child."

isChildOf :: ChildId -> AccountId -> Handler Bool
isChildOf cid aid = fmap isJust $ runDB $ getBy $ UniqueChild cid aid

canDeleteChild :: ChildId -> Handler (Deletable ChildId)
canDeleteChild cid = do
  mts <- runDB $ selectFirst [TransportSignupChild ==. cid] []
  pure $
    case mts of
      Nothing ->
        CanDelete $ do
          DB.delete cid
          DB.deleteWhere [ChildOfChild ==. cid]
      Just _ ->
        CannotDelete ["This child is still signed up for a transport service."]

childName :: Child -> Text
childName Child {..} = T.unwords [childFirstName, childLastName]
