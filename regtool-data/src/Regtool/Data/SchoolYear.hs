{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.SchoolYear
  ( SchoolYear(..)
  , schoolYearStartYear
  , schoolYearEndYear
  , schoolYearText
  , schoolYearString
  , currentSchoolYears
  , currentSchoolYear
  , getCurrentSchoolyear
  , getCurrentSchoolyears
  ) where

import Import

import Data.Aeson
import qualified Data.Text as T
import Data.Time

import Database.Persist
import Database.Persist.Sql

import Yesod.Core.Dispatch (PathPiece(..))

newtype SchoolYear =
  SchoolYear
    { unSchoolYear :: Int
    }
  deriving ( Show
           , Read
           , Eq
           , Ord
           , Enum
           , Generic
           , PersistField
           , PersistFieldSql
           , PathPiece
           )

instance Validity SchoolYear

instance FromJSON SchoolYear

instance FromJSONKey SchoolYear

instance ToJSON SchoolYear

instance ToJSONKey SchoolYear

schoolYearStartYear :: SchoolYear -> Int
schoolYearStartYear = unSchoolYear

schoolYearEndYear :: SchoolYear -> Int
schoolYearEndYear = (+ 1) . schoolYearStartYear

schoolYearText :: SchoolYear -> Text
schoolYearText = T.pack . schoolYearString

schoolYearString :: SchoolYear -> String
schoolYearString sy =
  unwords [show $ schoolYearStartYear sy, "-", show $ schoolYearEndYear sy]

currentSchoolYears :: UTCTime -> [SchoolYear]
currentSchoolYears now = [SchoolYear (y - 1), SchoolYear y]
  where
    y = fromInteger yi
    (yi, _, _) = toGregorian d
    d = utctDay now

currentSchoolYear :: UTCTime -> SchoolYear
currentSchoolYear now =
  if m >= 6
    then SchoolYear y
    else SchoolYear (y - 1)
  where
    y = fromInteger yi
    (yi, m, _) = toGregorian d
    d = utctDay now

getCurrentSchoolyear :: IO SchoolYear
getCurrentSchoolyear = currentSchoolYear <$> getCurrentTime

getCurrentSchoolyears :: IO [SchoolYear]
getCurrentSchoolyears = currentSchoolYears <$> getCurrentTime
