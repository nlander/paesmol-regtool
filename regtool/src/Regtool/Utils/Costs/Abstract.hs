{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Costs.Abstract
  ( AbstractCosts(..)
  , AbstractYearlyCosts(..)
  , nullAbstractYearlyCosts
  , AbstractSemestrelyCosts(..)
  , YearlyFeeStatus(..)
  , TransportFeeStatus(..)
  , OccasionalTransportFeeStatus(..)
  , occasionalTransportFeeStatusSignup
  , DaycareFeeStatus(..)
  , daycareFeeStatusSignup
  , DaycareActivityFeeStatus(..)
  , daycareActivityFeeStatusSignup
  , OccasionalDaycareFeeStatus(..)
  , occasionalDaycareFeeStatusSignup
  , makeAbstractCosts
  , makeAbstractYearlyCosts
  , makeAbstractYearlyBusCosts
  , makeAbstractYearlyBusCostsForChild
  , makeAbstractOccasionalTransportCosts
  , makeAbstractOccasionalDaycareCosts
  , makeAbstractYearlyFeeCosts
  , makeAbstractSemestrelyCosts
  , makeAbstractSemestrelyCostsFor
  ) where

import Import

import qualified Data.Map as M
import Data.Map (Map)

import Regtool.Data

import Regtool.Core.Foundation
import Regtool.Utils.Costs.Input

{-# ANN module ("HLint: ignore Redundant if" :: String) #-}

{-# ANN module ("HLint: ignore Use ++" :: String) #-}

data AbstractCosts =
  AbstractCosts
    { abstractCostsMap :: Map SchoolYear AbstractYearlyCosts
    , abstractCostsOccasionalTransport :: [OccasionalTransportFeeStatus]
    , abstractCostsOccasionalDaycare :: [OccasionalDaycareFeeStatus]
    , abstractCostsDiscounts :: [Entity Discount]
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity AbstractCosts where
  validate ac@AbstractCosts {..} =
    mconcat
      [ genericValidate ac
      , declare "The occasional transport stati are about distinct signups" $
        distinct $ map occasionalTransportFeeStatusSignup abstractCostsOccasionalTransport
      , declare "The occasional daycare stati are about distinct signups" $
        distinct $ map occasionalDaycareFeeStatusSignup abstractCostsOccasionalDaycare
      , decorate "None of the discounts have a checkout" $
        decorateList abstractCostsDiscounts $ \(Entity _ Discount {..}) ->
          declare "the discount has no checkout" $ isNothing discountCheckout
      , declare "None of the yearly costs are empty" $
        all (not . nullAbstractYearlyCosts) abstractCostsMap
      ]

data AbstractYearlyCosts =
  AbstractYearlyCosts
    { aycYearlyFee :: YearlyFeeStatus
    , aycYearlyBus :: Map (Entity Child) TransportFeeStatus
    , aycYearlySemestrely :: Map (Entity DaycareSemestre) AbstractSemestrelyCosts
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity AbstractYearlyCosts where
  validate AbstractYearlyCosts {..} =
    mconcat
      [ annotate aycYearlyFee "aycYearlyFee"
      , annotate aycYearlyBus "aycYearlyBus"
      , let isNotRequired (TransportFeeNotRequired _) = True
            isNotRequired _ = False
            notRequiredOrNotSignedUp TransportFeeNotSignedUp = True
            notRequiredOrNotSignedUp (TransportFeeNotRequired _) = True
            notRequiredOrNotSignedUp _ = False
         in check
              (if any isNotRequired aycYearlyBus
                 then all notRequiredOrNotSignedUp aycYearlyBus
                 else True)
              "If any fee is not required, then all of them must be not required or not signed up"
      , declare "None of the semstrely costs are empty" $
        all (not . nullAbstractSemestrelyCosts) aycYearlySemestrely
      ]

nullAbstractYearlyCosts :: AbstractYearlyCosts -> Bool
nullAbstractYearlyCosts AbstractYearlyCosts {..} = null aycYearlyBus && null aycYearlySemestrely

data AbstractSemestrelyCosts =
  AbstractSemestrelyCosts
    { ascDaycareFees :: [DaycareFeeStatus]
    , ascDaycareActivityFees :: [DaycareActivityFeeStatus]
    }
  deriving (Show, Eq, Ord, Generic)

instance Validity AbstractSemestrelyCosts

nullAbstractSemestrelyCosts :: AbstractSemestrelyCosts -> Bool
nullAbstractSemestrelyCosts AbstractSemestrelyCosts {..} =
  null ascDaycareFees && null ascDaycareActivityFees

data YearlyFeeStatus
  = YearlyFeeStatusNotPaid
  | YearlyFeeStatusPaid (Entity YearlyFeePayment)
  | YearlyFeeStatusNotRequired
  deriving (Show, Eq, Ord, Generic)

instance Validity YearlyFeeStatus

data TransportFeeStatus
  = TransportFeeNotSignedUp
  | TransportFeePaymentNotStartedButNecessary (Entity TransportSignup)
  | TransportFeePaymentStartedButNotDone
      (Entity TransportSignup)
      (Entity TransportEnrollment)
      (Entity TransportPaymentPlan)
      [Entity TransportPayment]
      [Entity Checkout]
  | TransportFeePaymentEntirelyDone
      (Entity TransportSignup)
      (Entity TransportEnrollment)
      (Entity TransportPaymentPlan)
      [Entity TransportPayment]
      [Entity Checkout]
  | TransportFeeNotRequired (Entity TransportSignup)
  deriving (Show, Eq, Ord, Generic)

instance Validity TransportFeeStatus where
  validate tfs@(TransportFeePaymentStartedButNotDone _ _ etpp etps cs) =
    decorate "TransportFeePaymentStartedButNotDone" $
    mconcat
      [ genericValidate tfs
      , declare "Transport Payments have seperate IDs" $ seperateIds etps
      , declare "Stripe Payments have seperate IDs" $ seperateIds cs
      , declare "The checkouts belong to the payments" $
        validateForeignKeys etps transportPaymentCheckout cs
      , let a1 = sumAmount (map (transportPaymentAmount . entityVal) etps)
            a2 = transportPaymentPlanTotalAmount (entityVal etpp)
         in declare
              (unwords
                 [ "The transport payments' amounts sum to more than the transport payment plan total amount"
                 , fmtAmount a1
                 , "vs"
                 , fmtAmount a2
                 ]) $
            a1 <= a2
      ]
  validate tfs@(TransportFeePaymentEntirelyDone _ _ etpp etps cs) =
    decorate "TransportFeePaymentEntirelyDone" $
    mconcat
      [ genericValidate tfs
      , declare "Transport Payments have seperate IDs" $ seperateIds etps
      , declare "Stripe Payments have seperate IDs" $ seperateIds cs
      , declare "The transport payments belong to the payments" $
        validateForeignKeys etps transportPaymentCheckout cs
      , let a1 = sumAmount (map (transportPaymentAmount . entityVal) etps)
            a2 = transportPaymentPlanTotalAmount (entityVal etpp)
         in declare
              (unwords
                 [ "The transport payments' amounts sum to more than the transport payment plan total amount"
                 , fmtAmount a1
                 , "vs"
                 , fmtAmount a2
                 ]) $
            a1 >= a2
      ]
  validate tfs = genericValidate tfs

data OccasionalTransportFeeStatus
  = OccasionalTransportFeeUnpaid (Entity Child) (Entity OccasionalTransportSignup)
  | OccasionalTransportFeePaid
      (Entity Child)
      (Entity OccasionalTransportSignup)
      (Entity OccasionalTransportPayment)
  deriving (Show, Eq, Ord, Generic)

instance Validity OccasionalTransportFeeStatus where
  validate obfs@(OccasionalTransportFeeUnpaid ec eots) =
    decorate "OccasionalTransportFeeUnpaid" $
    mconcat
      [ genericValidate obfs
      , declare "The transport signup refers to the child" $
        occasionalTransportSignupChild (entityVal eots) == entityKey ec
      ]
  validate obfs@(OccasionalTransportFeePaid ec eots eotp) =
    decorate "OccasionalTransportFeePaid" $
    mconcat
      [ genericValidate obfs
      , declare "The transport signup refers to the child" $
        occasionalTransportSignupChild (entityVal eots) == entityKey ec
      , declare "The transport payment refers to the transport signup" $
        occasionalTransportPaymentSignup (entityVal eotp) == entityKey eots
      ]

occasionalTransportFeeStatusSignup ::
     OccasionalTransportFeeStatus -> Entity OccasionalTransportSignup
occasionalTransportFeeStatusSignup otfs =
  case otfs of
    OccasionalTransportFeeUnpaid _ e -> e
    OccasionalTransportFeePaid _ e _ -> e

data DaycareFeeStatus
  = DaycareFeeUnpaid (Entity Child) (Entity DaycareTimeslot) (Entity DaycareSignup)
  | DaycareFeePaid
      (Entity Child)
      (Entity DaycareTimeslot)
      (Entity DaycareSignup)
      (Entity DaycarePayment)
  deriving (Show, Eq, Ord, Generic)

instance Validity DaycareFeeStatus where
  validate dcfs =
    mconcat
      [ genericValidate dcfs
      , case dcfs of
          DaycareFeeUnpaid ec edcts eds ->
            decorate "DaycareFeeUnpaid" $
            mconcat
              [ declare "The signup belongs to the child" $
                daycareSignupChild (entityVal eds) == entityKey ec
              , declare "The signup belongs to the timeslot" $
                daycareSignupTimeslot (entityVal eds) == entityKey edcts
              ]
          DaycareFeePaid ec edcts eds edp ->
            decorate "DaycareFeePpaid" $
            mconcat
              [ declare "The signup belongs to the child" $
                daycareSignupChild (entityVal eds) == entityKey ec
              , declare "The signup belongs to the timeslot" $
                daycareSignupTimeslot (entityVal eds) == entityKey edcts
              , declare "The payment belongs to the signup" $
                daycarePaymentSignup (entityVal edp) == entityKey eds
              ]
      ]

daycareFeeStatusSignup :: DaycareFeeStatus -> Entity DaycareSignup
daycareFeeStatusSignup dfs =
  case dfs of
    (DaycareFeeUnpaid _ _ s) -> s
    (DaycareFeePaid _ _ s _) -> s

data DaycareActivityFeeStatus
  = DaycareActivityFeeUnpaid (Entity Child) (Entity DaycareActivity) (Entity DaycareActivitySignup)
  | DaycareActivityFeePaid
      (Entity Child)
      (Entity DaycareActivity)
      (Entity DaycareActivitySignup)
      (Entity DaycareActivityPayment)
  deriving (Show, Eq, Ord, Generic)

instance Validity DaycareActivityFeeStatus where
  validate dcfs =
    mconcat
      [ genericValidate dcfs
      , case dcfs of
          DaycareActivityFeeUnpaid ec edcts eds ->
            decorate "DaycareActivityFeeUnpaid" $
            mconcat
              [ declare "The signup belongs to the child" $
                daycareActivitySignupChild (entityVal eds) == entityKey ec
              , declare "The signup belongs to the timeslot" $
                daycareActivitySignupActivity (entityVal eds) == entityKey edcts
              ]
          DaycareActivityFeePaid ec edcts eds edp ->
            decorate "DaycareActivityFeePpaid" $
            mconcat
              [ declare "The signup belongs to the child" $
                daycareActivitySignupChild (entityVal eds) == entityKey ec
              , declare "The signup belongs to the timeslot" $
                daycareActivitySignupActivity (entityVal eds) == entityKey edcts
              , declare "The payment belongs to the signup" $
                daycareActivityPaymentSignup (entityVal edp) == entityKey eds
              ]
      ]

daycareActivityFeeStatusSignup :: DaycareActivityFeeStatus -> Entity DaycareActivitySignup
daycareActivityFeeStatusSignup dfs =
  case dfs of
    (DaycareActivityFeeUnpaid _ _ s) -> s
    (DaycareActivityFeePaid _ _ s _) -> s
data OccasionalDaycareFeeStatus
  = OccasionalDaycareFeeUnpaid
      (Entity Child)
      (Entity DaycareTimeslot)
      (Entity OccasionalDaycareSignup)
  | OccasionalDaycareFeePaid
      (Entity Child)
      (Entity DaycareTimeslot)
      (Entity OccasionalDaycareSignup)
      (Entity OccasionalDaycarePayment)
  deriving (Show, Eq, Ord, Generic)

instance Validity OccasionalDaycareFeeStatus where
  validate obfs@(OccasionalDaycareFeeUnpaid ec edts eots) =
    decorate "OccasionalDaycareFeeUnpaid" $
    mconcat
      [ genericValidate obfs
      , declare "The daycare signup refers to the child" $
        occasionalDaycareSignupChild (entityVal eots) == entityKey ec
      , declare "The daycare signup refers to the daycare timeslot" $
        occasionalDaycareSignupTimeslot (entityVal eots) == entityKey edts
      ]
  validate obfs@(OccasionalDaycareFeePaid ec edts eots eotp) =
    decorate "OccasionalDaycareFeePaid" $
    mconcat
      [ genericValidate obfs
      , declare "The daycare signup refers to the child" $
        occasionalDaycareSignupChild (entityVal eots) == entityKey ec
      , declare "The daycare signup refers to the daycare timestamp" $
        occasionalDaycareSignupTimeslot (entityVal eots) == entityKey edts
      , declare "The daycare payment refers to the daycare signup" $
        occasionalDaycarePaymentSignup (entityVal eotp) == entityKey eots
      ]

occasionalDaycareFeeStatusSignup :: OccasionalDaycareFeeStatus -> Entity OccasionalDaycareSignup
occasionalDaycareFeeStatusSignup otfs =
  case otfs of
    OccasionalDaycareFeeUnpaid _ _ e -> e
    OccasionalDaycareFeePaid _ _ e _ -> e

makeAbstractCosts :: CostsInput -> AbstractCosts
makeAbstractCosts ci =
  let years =
        concat
          [ map (transportSignupSchoolYear . entityVal) (costsInputTransportSignups ci)
          , map (daycareSemestreYear . entityVal) (costsInputDaycareSemestres ci)
          ]
   in AbstractCosts
        { abstractCostsMap =
            M.fromList $
            flip mapMaybe years $ \sy ->
              (,) sy <$>
              makeAbstractYearlyCosts
                sy
                (ci
                   { costsInputTransportSignups =
                       filter
                         ((== sy) . transportSignupSchoolYear . entityVal)
                         (costsInputTransportSignups ci)
                   , costsInputDaycareSemestres =
                       filter
                         ((== sy) . daycareSemestreYear . entityVal)
                         (costsInputDaycareSemestres ci)
                   })
        , abstractCostsOccasionalTransport =
            makeAbstractOccasionalTransportCosts
              (costsInputChildren ci)
              (costsInputOccasionalTransportSignups ci)
              (costsInputOccasionalTransportPayments ci)
        , abstractCostsOccasionalDaycare =
            makeAbstractOccasionalDaycareCosts
              (costsInputChildren ci)
              (costsInputDaycareTimeslots ci)
              (costsInputOccasionalDaycareSignups ci)
              (costsInputOccasionalDaycarePayments ci)
        , abstractCostsDiscounts =
            filter (isNothing . discountCheckout . entityVal) $ costsInputDiscounts ci
        }

makeAbstractYearlyCosts :: SchoolYear -> CostsInput -> Maybe AbstractYearlyCosts
makeAbstractYearlyCosts sy ci =
  let aycYearlyBus = makeAbstractYearlyBusCosts ci
      aycYearlySemestrely = makeAbstractSemestrelyCosts sy ci
      aycYearlyFee =
        makeAbstractYearlyFeeCosts
          sy
          (costsInputYearlyFeePayments ci)
          (M.elems aycYearlyBus)
          aycYearlySemestrely
      ayc = AbstractYearlyCosts {..}
   in if nullAbstractYearlyCosts ayc
        then Nothing
        else Just ayc

makeAbstractYearlyBusCosts :: CostsInput -> Map (Entity Child) TransportFeeStatus
makeAbstractYearlyBusCosts ci@CostsInput {..} =
  M.fromList $
            -- For each child
  flip map costsInputChildren $ \c -> (,) c $ makeAbstractYearlyBusCostsForChild ci $ entityKey c

makeAbstractYearlyBusCostsForChild :: CostsInput -> ChildId -> TransportFeeStatus
makeAbstractYearlyBusCostsForChild CostsInput {..} cid
                -- Find its transport signup
 =
  case find ((== cid) . transportSignupChild . entityVal) costsInputTransportSignups
                    -- If it's not signed up, then it's not signed up. Duh
        of
    Nothing -> TransportFeeNotSignedUp
                    -- If it's signed up, then we have to figure out payment
    Just ts@(Entity tsid TransportSignup {..})
                     -- First we try to find the enrollment and the
                     -- payment plan for this signup.
     ->
      if any ((/= OtherCompany) . parentCompany . entityVal) costsInputParents
            -- Then it's not needed
        then TransportFeeNotRequired ts
        else case (do te@(Entity _ TransportEnrollment {..}) <-
                        find
                          ((== tsid) . transportEnrollmentSignup . entityVal)
                          costsInputTransportEnrollments
                      tpp <-
                        find
                          ((== transportEnrollmentPaymentPlan) . entityKey)
                          costsInputTransportPaymentPlans
                      pure (te, tpp)) of
               Just (te@(Entity _ TransportEnrollment {..}), tpp@(Entity tppid TransportPaymentPlan {..})) ->
                 let tps =
                       filter
                         ((== tppid) . transportPaymentPaymentPlan . entityVal)
                         costsInputTransportPayments
                     cs =
                       filter
                         ((`elem` map (transportPaymentCheckout . entityVal) tps) . entityKey)
                         costsInputCheckouts
                  in if sumAmount (map (transportPaymentAmount . entityVal) tps) >=
                        transportPaymentPlanTotalAmount
                       then TransportFeePaymentEntirelyDone ts te tpp tps cs
                       else TransportFeePaymentStartedButNotDone ts te tpp tps cs
                            -- If there is no payment
               Nothing -> TransportFeePaymentNotStartedButNecessary ts

makeAbstractOccasionalTransportCosts ::
     [Entity Child]
  -> [Entity OccasionalTransportSignup]
  -> [Entity OccasionalTransportPayment]
  -> [OccasionalTransportFeeStatus]
makeAbstractOccasionalTransportCosts ecs eotss eotps =
  flip mapMaybe eotss $ \eots@(Entity otsi ots) -> do
    c <- find ((== occasionalTransportSignupChild ots) . entityKey) ecs
    pure $
      case find (\(Entity _ otp) -> occasionalTransportPaymentSignup otp == otsi) eotps of
        Nothing -> OccasionalTransportFeeUnpaid c eots
        Just eotp -> OccasionalTransportFeePaid c eots eotp

makeAbstractOccasionalDaycareCosts ::
     [Entity Child]
  -> [Entity DaycareTimeslot]
  -> [Entity OccasionalDaycareSignup]
  -> [Entity OccasionalDaycarePayment]
  -> [OccasionalDaycareFeeStatus]
makeAbstractOccasionalDaycareCosts ecs dtss eotss eotps =
  flip mapMaybe eotss $ \eots@(Entity otsi ots) -> do
    c <- find ((== occasionalDaycareSignupChild ots) . entityKey) ecs
    edts <- find ((== occasionalDaycareSignupTimeslot ots) . entityKey) dtss
    pure $
      case find (\(Entity _ otp) -> occasionalDaycarePaymentSignup otp == otsi) eotps of
        Nothing -> OccasionalDaycareFeeUnpaid c edts eots
        Just eotp -> OccasionalDaycareFeePaid c edts eots eotp

makeAbstractYearlyFeeCosts ::
     SchoolYear
  -> [Entity YearlyFeePayment]
  -> [TransportFeeStatus]
  -> Map (Entity DaycareSemestre) AbstractSemestrelyCosts
  -> YearlyFeeStatus
makeAbstractYearlyFeeCosts sy costsInputYearlyFeePayments tfss semMap
            -- Find a yearly fee payment
 =
  case find ((== sy) . yearlyFeePaymentSchoolyear . entityVal) costsInputYearlyFeePayments of
    Just yfp
                 -- If it's been paid then it's been paid, simple
     -> YearlyFeeStatusPaid yfp
                -- If it's not been paid yet, then it needs to be paid if any of the
                -- children have been signed up for a bus or if any daycare services are used
    Nothing ->
      if all (== TransportFeeNotSignedUp) tfss
        then if null semMap
               then YearlyFeeStatusNotRequired
               else YearlyFeeStatusNotPaid
        else YearlyFeeStatusNotPaid

makeAbstractSemestrelyCosts ::
     SchoolYear -> CostsInput -> Map (Entity DaycareSemestre) AbstractSemestrelyCosts
makeAbstractSemestrelyCosts sy ci@CostsInput {..} =
  let sems = filter ((== sy) . daycareSemestreYear . entityVal) costsInputDaycareSemestres
   in M.fromList $ flip mapMaybe sems $ \e -> (,) e <$> makeAbstractSemestrelyCostsFor e ci

makeAbstractSemestrelyCostsFor ::
     Entity DaycareSemestre -> CostsInput -> Maybe AbstractSemestrelyCosts
makeAbstractSemestrelyCostsFor (Entity dsemid DaycareSemestre {..}) CostsInput {..} =
  let timeslots =
        filter ((== dsemid) . daycareTimeslotSemestre . entityVal) costsInputDaycareTimeslots
      signups =
        filter
          ((`elem` map entityKey timeslots) . daycareSignupTimeslot . entityVal)
          costsInputDaycareSignups
      ascDaycareFees =
        flip mapMaybe signups $ \eds@(Entity dsid DaycareSignup {..}) -> do
          child <- find ((== daycareSignupChild) . entityKey) costsInputChildren
          timeslot <-
            find
              ((== daycareSignupTimeslot) . entityKey)
              (filter ((== daycareSignupTimeslot) . entityKey) costsInputDaycareTimeslots)
          pure $
            case find ((== dsid) . daycarePaymentSignup . entityVal) costsInputDaycarePayments of
              Nothing -> DaycareFeeUnpaid child timeslot eds
              Just edp -> DaycareFeePaid child timeslot eds edp
      activities =
        filter ((== dsemid) . daycareActivitySemestre . entityVal) costsInputDaycareActivities
      activitySignups =
        filter
          ((`elem` map entityKey activities) . daycareActivitySignupActivity . entityVal)
          costsInputDaycareActivitySignups
      ascDaycareActivityFees =
        flip mapMaybe activitySignups $ \eds@(Entity dsid DaycareActivitySignup {..}) -> do
          child <- find ((== daycareActivitySignupChild) . entityKey) costsInputChildren
          timeslot <-
            find
              ((== daycareActivitySignupActivity) . entityKey)
              (filter ((== daycareActivitySignupActivity) . entityKey) costsInputDaycareActivities)
          pure $
            case find
                   ((== dsid) . daycareActivityPaymentSignup . entityVal)
                   costsInputDaycareActivityPayments of
              Nothing -> DaycareActivityFeeUnpaid child timeslot eds
              Just edp -> DaycareActivityFeePaid child timeslot eds edp
      asc = AbstractSemestrelyCosts {..}
   in if nullAbstractSemestrelyCosts asc
        then Nothing
        else Just asc
