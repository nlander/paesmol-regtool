{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Config where

import Import

import qualified Data.ByteString as SB
import qualified Data.Text as T
import Data.Yaml as Yaml

import Yesod.Core
import Yesod.Form.Bootstrap3

import Regtool.Data

import Regtool.Core.Form

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation
import Regtool.Utils.Costs.Config

getAdminConfigurationR :: Handler Html
getAdminConfigurationR = do
  cform <- makeConfigurationForm
  (formWidget, formEncType) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm cform
  withNavBar $(widgetFile "admin/configuration")

postAdminConfigurationR :: Handler Html
postAdminConfigurationR = do
  cform <- makeConfigurationForm
  ((result, formWidget), formEncType) <-
    runFormPost $ renderCustomForm BootstrapBasicForm cform
  case result of
    FormSuccess newConfig -> do
      putConfiguration newConfig
      redirect $ AdminR AdminConfigurationR
    fr -> withFormResultNavBar fr $(widgetFile "admin/configuration")

newtype Configuration =
  Configuration
    { confCostsConfig :: CostsConfig
    }
  deriving (Show, Eq, Generic)

instance FromJSON Configuration where
  parseJSON = withObject "Configuration" $ \o -> Configuration <$> o .: "costs"

instance ToJSON Configuration where
  toJSON Configuration {..} = object ["costs" .= confCostsConfig]

defaultConfiguration :: Configuration
defaultConfiguration = Configuration {confCostsConfig = defaultCostsConfig}

makeConfigurationForm :: Handler (AForm Handler Configuration)
makeConfigurationForm = do
  conf <- getConfiguration
  pure $ configurationForm $ Just conf

configurationForm :: Maybe Configuration -> AForm Handler Configuration
configurationForm mc = Configuration <$> costsConfigForm (dv confCostsConfig)
  where
    dv :: (Configuration -> a) -> Maybe a
    dv func = func <$> mc

costsConfigForm :: Maybe CostsConfig -> AForm Handler CostsConfig
costsConfigForm mcc =
  CostsConfig <$>
  areq
    amountField
    (bfs ("Yearly Membership Fee" :: Text))
    (dv costsConfigAnnualFee) <*>
  yearlyBusFeeForm (dv costsConfigBusYearly) <*>
  areq
    amountField
    (bfs ("Yearly Bus fee for every child after that" :: Text))
    (dv costsConfigBusYearlyDefault) <*>
  areq
    amountField
    (bfs ("Occasional Bus Fee" :: Text))
    (dv costsConfigOccasionalBus)
  where
    dv :: (CostsConfig -> a) -> Maybe a
    dv func = func <$> mcc

yearlyBusFeeForm :: Maybe [Amount] -> AForm Handler [Amount]
yearlyBusFeeForm mas =
  for [1 .. 5] $ \i ->
    areq
      amountField
      (bfs (("Yearly bus fee for child " <> T.pack (show i)) :: Text))
      ((mas >>= (`atMay` (i - 1))) `mplus` Just zeroAmount)

getConfiguration :: Handler Configuration
getConfiguration = do
  let configFile = $(mkRelFile "config.yaml")
  mcontents <- liftIO $ forgivingAbsence $ SB.readFile $ toFilePath configFile
  case mcontents of
    Nothing -> pure defaultConfiguration
    Just contents ->
      case Yaml.decodeEither contents of
        Left err -> do
          setMessage $
            fromString $
            "Something went wrong while loading config file, using default configuraion: " <>
            err
          pure defaultConfiguration
        Right c -> pure c

putConfiguration :: Configuration -> Handler ()
putConfiguration = liftIO . encodeFile "config.yaml"
