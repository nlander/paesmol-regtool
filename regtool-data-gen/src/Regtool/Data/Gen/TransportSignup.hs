{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.TransportSignup where

import Import

import Database.Persist (Entity(..))

import Regtool.Data

import Regtool.Data.Gen.Entity
import Regtool.Data.Gen.SchoolYear ()
import Regtool.Data.Gen.Utils

instance GenUnchecked TransportSignup

instance GenValid TransportSignup where
  genValid = genValidStructurally

transportSignupsForChildren :: [ChildId] -> Gen [Entity TransportSignup]
transportSignupsForChildren cids =
  fmap
    catMaybes
    (forM cids $ \cid ->
       mgen $ modEnt (\su -> su {transportSignupChild = cid}) <$> genValid) `suchThat`
  seperateIds
