{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.DaycareActivityPayment where

import Import

import Regtool.Data

import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenUnchecked DaycareActivityPayment

instance GenValid DaycareActivityPayment where
  genValid = genValidStructurally
