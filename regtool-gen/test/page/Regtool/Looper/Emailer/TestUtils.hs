{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.TestUtils
  ( emailGoldenTests
  ) where

import TestImport

import qualified Data.Text as T
import qualified Data.Text.IO as T

import Yesod.Core
import Yesod.Test

import Regtool.Core.Foundation.Regtool
import Regtool.Looper.Emailer.Types

emailGoldenTests ::
     String
  -> (value -> Renderer -> Text)
  -> (value -> Renderer -> Text)
  -> value
  -> YesodSpec Regtool
emailGoldenTests path textMaker htmlMaker value = do
  let resolvedRoot = "https://registration.paesmol.eu"
  yit "is compliant with the golden test for text" $ do
    y <- getTestYesod
    let render = yesodRender y resolvedRoot
    liftIO $ do
      let ePath = path ++ ".txt"
      expected <- T.readFile ePath
      let actual = textMaker value render
      shouldBeText ePath actual expected
  yit "is compliant with the golden test for text" $ do
    y <- getTestYesod
    let render = yesodRender y resolvedRoot
    liftIO $ do
      let ePath = path ++ ".html"
      expected <- T.readFile ePath
      let actual = htmlMaker value render
      shouldBeText ePath actual expected
  where
    shouldBeText p actual expected =
      unless (actual == expected) $ do
        putStrLn $
          unlines
            [ "These two should have been equal:"
            , show actual
            , show expected
            , "If this was intentional, please put the following text in " ++ p
            , "---[BEGIN EXPECTED]---"
            , T.unpack actual
            , "---[  END EXPECTED]---"
            ]
        actual `shouldBe` expected
