{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Daycare.Semestre
  ( getAdminRegisterDaycareSemestreR
  , postAdminRegisterDaycareSemestreR
  , getAdminDaycareSemestresR
  , postAdminDaycareSemestreDeleteR
  , getAdminDaycareSemestreR
  , getAdminDaycareSemestreOverviewR
  ) where

import Import

import qualified Database.Persist as DB
import Yesod.Core
import Yesod.Form.Bootstrap3

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Form
import Regtool.Core.Foundation

getAdminDaycareSemestresR :: Handler Html
getAdminDaycareSemestresR = do
  daycareSemestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  token <- genToken
  withNavBar $(widgetFile "admin/daycare/semestres")

data RegisterDaycareSemestre =
  RegisterDaycareSemestre
    { registerDaycareSemestreSchoolYear :: SchoolYear
    , registerDaycareSemestreSemestre :: Semestre
    , registerDaycareSemestreId :: Maybe DaycareSemestreId
    }
  deriving (Show, Eq)

registerDaycareSemestreForm ::
     [SchoolYear] -> Maybe (Entity DaycareSemestre) -> AForm Handler RegisterDaycareSemestre
registerDaycareSemestreForm sys mce =
  RegisterDaycareSemestre <$>
  areq
    (selectFieldList $ map (\sy -> (schoolYearText sy, sy)) sys)
    (bfs ("School Year" :: Text))
    (dv daycareSemestreYear) <*>
  areq (selectField optionsEnum) (bfs ("Semestre" :: Text)) (dv daycareSemestreSemestre) <*>
  aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (DaycareSemestre -> a) -> Maybe a
    dv func = (func . entityVal) <$> mce

getCurrentAndNextSchoolYears :: IO [SchoolYear]
getCurrentAndNextSchoolYears = do
  sy <- getCurrentSchoolyear
  pure [sy, succ sy, succ $ succ sy]

getAdminRegisterDaycareSemestreR :: Handler Html
getAdminRegisterDaycareSemestreR = do
  sys <- liftIO getCurrentAndNextSchoolYears
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm $ registerDaycareSemestreForm sys Nothing
  withNavBar $(widgetFile "admin/daycare/register-semestre")

postAdminRegisterDaycareSemestreR :: Handler Html
postAdminRegisterDaycareSemestreR = do
  sys <- liftIO getCurrentAndNextSchoolYears
  ((result, formWidget), formEnctype) <-
    runFormPost $ renderCustomForm BootstrapBasicForm $ registerDaycareSemestreForm sys Nothing
  case result of
    FormSuccess RegisterDaycareSemestre {..} -> do
      now <- liftIO getCurrentTime
      let daycareSemestre_ =
            DaycareSemestre
              { daycareSemestreYear = registerDaycareSemestreSchoolYear
              , daycareSemestreSemestre = registerDaycareSemestreSemestre
              , daycareSemestreCreationTime = now
              }
      case registerDaycareSemestreId of
        Just i -> do
          runDB $ replaceValid i daycareSemestre_
          redirect $ AdminR AdminDaycareSemestresR
        Nothing -> do
          did <- runDB $ insertValid daycareSemestre_
          redirect $ AdminR $ AdminDaycareSemestreR did
    _ -> withNavBar $(widgetFile "admin/daycare/register-semestre")

getAdminDaycareSemestreR :: DaycareSemestreId -> Handler Html
getAdminDaycareSemestreR bid = do
  daycareSemestre <- runDB $ get404 bid
  sys <- liftIO getCurrentAndNextSchoolYears
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $
    registerDaycareSemestreForm sys $ Just $ Entity bid daycareSemestre
  withNavBar $(widgetFile "admin/daycare/semestre")

getAdminDaycareSemestreOverviewR :: DaycareSemestreId -> Handler Html
getAdminDaycareSemestreOverviewR sid = do
  daycareSemestre <- runDB $ get404 sid
  withNavBar $(widgetFile "admin/daycare/semestre-overview")

postAdminDaycareSemestreDeleteR :: DaycareSemestreId -> Handler Html
postAdminDaycareSemestreDeleteR lsid = do
  canDeleteDaycareSemestre lsid >>= deleteOrError
  redirect $ AdminR AdminDaycareSemestresR

canDeleteDaycareSemestre :: DaycareSemestreId -> Handler (Deletable DaycareSemestreId)
canDeleteDaycareSemestre dcsid = do
  mc <- runDB $ selectFirst [DaycareTimeslotSemestre ==. dcsid] []
  ma <- runDB $ selectFirst [DaycareActivitySemestre ==. dcsid] []
  pure $
    case (,) <$> mc <*> ma of
      Nothing -> CanDelete $ DB.delete dcsid
      Just _ -> CannotDelete ["There is still a timeslot or activity in this daycare semestre."]
