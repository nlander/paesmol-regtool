{-# LANGUAGE OverloadedStrings #-}

module Regtool.Helper.SetupTestDatabase where

import Import

import Control.Monad.Logger (runStderrLoggingT)
import Control.Monad.Trans.Resource (runResourceT)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE

import Test.QuickCheck

import Yesod.Auth.Email (saltPass)

import Database.Persist.Sqlite

import Regtool.Data

import Regtool.OptParse

setupTestDatabase :: SetupTestDatabaseSettings -> IO ()
setupTestDatabase sets = do
  let dbf = setupTestDababaseSetsDbFile sets
  ignoringAbsence $ removeFile dbf
  runStderrLoggingT $
    withSqlitePool (T.pack $ toFilePath dbf) 1 $ \pool ->
      liftIO $
      runResourceT $
      flip runSqlPool pool $ do
        runMigration migrateAll
        setupTestDababaseMigration

setupTestDababaseMigration :: MonadIO m => ReaderT SqlBackend m ()
setupTestDababaseMigration = do
  now <- liftIO getCurrentTime
  let gen = liftIO . generate
  accs <-
    forM accounts $ \(username, domain) -> do
      let email =
            unsafeEmailAddress (TE.encodeUtf8 username) (TE.encodeUtf8 domain)
      pass <- liftIO $ saltPass username
      key <- gen $ oneof [pure Nothing, Just . T.pack <$> arbitrary]
      un <- gen $ elements [Just username, Nothing]
      pure
        Account
          { accountUsername = un
          , accountEmailAddress = email
          , accountVerified = isNothing key
          , accountVerificationKey = key
          , accountSaltedPassphraseHash = pass
          , accountResetPassphraseKey = Nothing
          , accountCreationTime = now
          }
  mapM_ insert accs

accounts :: [(Text, Text)]
accounts =
  [ ("user", "example.com")
  , ("example", "example.com")
  , ("jan", "example.be")
  , ("jaap", "example.be")
  , ("joris", "example.be")
  ]
