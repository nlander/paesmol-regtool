{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE TypeApplications #-}

module Regtool.Costs.InputSpec
  ( spec
  ) where

import TestImport

import qualified Data.List.NonEmpty as NE

import Regtool.Data

import Regtool.Utils.Costs.Input

import Regtool.Gen ()

import Regtool.Gen.Costs.Input

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

spec :: Spec
spec = do
  describe "parentsWithAccount" $
    it "generates valids on valids" $
    forAllValid $ \aid -> forAll (parentsWithAccount aid) shouldBeValid
  describe "stripeCustomerWithAccount" $
    it "generates valids on valids" $
    forAllValid $ \aid -> forAll (stripeCustomerWithAccount aid) shouldBeValid
  describe "stripeCustomerWithAccount" $
    it "generates valids on valids" $
    forAllValid $ \mscid -> forAll (stripePaymentsWithCustomer mscid) shouldBeValid
  describe "enrollmentsAndPlansFor" $
    it "generates valids on valids" $
    forAllValid $ \tsids -> forAll (enrollmentsAndPlansFor tsids) shouldBeValid
  describe "checkoutsFor" $
    it "generates valids on valids" $
    forAllValid $ \aid -> forAllValid $ \esps -> forAll (checkoutsFor aid esps) shouldBeValid
  let sameCheckouts = shouldBe `on` (map fst . sort . NE.toList)
  describe "transportPaymentsFor" $ do
    it "generates valids on valids" $
      forAllValid $ \tppids ->
        forAllValid $ \ces -> forAll (transportPaymentsFor ces tppids) shouldBeValid
    it "doesn't get rid of checkouts" $
      forAllValid $ \tppids ->
        forAllValid $ \ces ->
          forAll (transportPaymentsFor ces tppids) $ \(ces', _) -> sameCheckouts ces ces'
  describe "yearlyFeePayments" $ do
    it "generates valids on valids" $
      forAllValid $ \aid ->
        forAll (NE.nubBy ((==) `on` fst) <$> genValid) $ \checkouts ->
          forAll (yearlyFeePayments aid checkouts) shouldBeValid
    it "has seperate ids" $
      forAllValid $ \(aid, ces) ->
        forAll (yearlyFeePayments aid ces) $ \(_, yfps) -> seperateIds yfps
    it "doesn't get rid of checkouts" $
      forAllValid $ \(aid, ces) ->
        forAll (yearlyFeePayments aid ces) $ \(ces', _) -> sameCheckouts ces ces'
  describe "occasionalTransportPayments" $ do
    it "generates valids on valids" $
      forAllValid $ \otsis ->
        forAll (NE.nubBy ((==) `on` fst) <$> genValid) $ \checkouts ->
          forAll (occasionalTransportPayments checkouts otsis) shouldBeValid
    it "has seperate ids" $
      forAllValid $ \(ces, otsis) ->
        forAll (occasionalTransportPayments ces otsis) $ \(_, yfps) -> seperateIds yfps
    it "doesn't get rid of checkouts" $
      forAllValid $ \(ces, otsis) ->
        forAll (occasionalTransportPayments ces otsis) $ \(ces', _) -> sameCheckouts ces ces'
  describe "daycarePayments" $ do
    it "generates valids on valids" $
      forAllValid $ \dsis ->
        forAll (NE.nubBy ((==) `on` fst) <$> genValid) $ \checkouts ->
          forAll (daycarePayments checkouts dsis) shouldBeValid
    it "has seperate ids" $
      forAllValid $ \(ces, dsis) ->
        forAll (daycarePayments ces dsis) $ \(_, yfps) -> seperateIds yfps
    it "doesn't get rid of checkouts" $
      forAllValid $ \(ces, dsis) ->
        forAll (daycarePayments ces dsis) $ \(ces', _) -> sameCheckouts ces ces'
  describe "chooseCheckoutAndPartialAmount" $
    it "picks out strictly positive amounts" $
    forAllValid $ \tups ->
      forAll (chooseCheckoutAndPartialAmount tups) $ \(_, (_, a)) ->
        unless (a > zeroAmount) $
        expectationFailure $ unwords ["Should have been strictly positive:", show a]
  describe "choosePartialAmountFromCheckout" $ do
    it "picks out strictly positive amounts" $
      forAllValid $ \a ->
        forAll (choosePartialAmountFromCheckout a) $ \(a', _) ->
          unless (a' > zeroAmount) $
          expectationFailure $ unwords ["Should have been strictly positive:", show a']
    it "picks out an affine sum" $
      forAllValid $ \a ->
        forAll (choosePartialAmountFromCheckout a) $ \(a', a'') -> (a' `addAmount` a'') `shouldBe` a
  describe "chooseTotalAmountFromCheckout" $ do
    it "picks out strictly positive amounts" $
      forAllValid $ \a ->
        forAll (chooseTotalAmountFromCheckout a) $ \(a', _) ->
          unless (a' > zeroAmount) $
          expectationFailure $ unwords ["Should have been strictly positive:", show a']
    it "picks out an affine sum" $
      forAllValid $ \a ->
        forAll (chooseTotalAmountFromCheckout a) $ \(a', a'') -> (a' `addAmount` a'') `shouldBe` a
  describe "GenValid CostsInput" $
    describe "genValid   :: Gen CostsInput" $
    it "only generates valid 'CostsInput's" $
    forAll (genValid @CostsInput) $ \ci ->
      case prettyValidate ci of
        Right _ -> pure ()
        Left err -> expectationFailure $ unlines [err, ppShow ci]
  -- modifyMaxSuccess (`div` 10) $ shrinkValidSpecWithLimit @CostsInput 100
  -- modifyMaxSuccess (`div` 10) $ doesNotShrinkToItselfSpec @CostsInput
