{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Application where

import Yesod.Core

import Yesod.Auth

import Regtool.Core.Foundation

import Regtool.Handler.Admin
import Regtool.Handler.Checkout
import Regtool.Handler.Children
import Regtool.Handler.Chosen
import Regtool.Handler.Doctors
import Regtool.Handler.Home
import Regtool.Handler.Overview
import Regtool.Handler.Parents
import Regtool.Handler.Payments
import Regtool.Handler.Profile
import Regtool.Handler.Signup

mkYesodDispatch "Regtool" resourcesRegtool
