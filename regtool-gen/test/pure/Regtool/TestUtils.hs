{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Regtool.TestUtils
  ( failure
  , doesNotShrinkToItselfSpec
  ) where

import TestImport

import Regtool.Application ()

failure :: MonadIO m => String -> m a
failure msg = do
  liftIO $ expectationFailure msg
  undefined

-- TODO consider putting this in validity
doesNotShrinkToItselfSpec ::
     forall a. (Show a, Eq a, GenValid a)
  => Spec
doesNotShrinkToItselfSpec =
  describe "shrinkValid" $
  it "does not shrink a value to itself for the first 100 elements" $
  forAll genValid $ \v -> take 100 (shrinkValid (v :: a)) `shouldNotContain` [v]
