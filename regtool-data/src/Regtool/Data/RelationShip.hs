{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.RelationShip where

import Import

import Data.Aeson

import Regtool.Data.StandardOrRaw

data StandardRelationShip
  = Mother
  | Father
  | StepMother
  | StepFather
  | Tutor
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity StandardRelationShip

instance FromJSON StandardRelationShip

instance ToJSON StandardRelationShip

standardRelationShipText :: StandardRelationShip -> Text
standardRelationShipText r =
  case r of
    Mother -> "Mother"
    Father -> "Father"
    StepMother -> "Step Mother"
    StepFather -> "Step Father"
    Tutor -> "Tutor"

parseStandardRelationShip :: Text -> Maybe StandardRelationShip
parseStandardRelationShip t =
  case t of
    "Mother" -> Just Mother
    "Father" -> Just Father
    "Step Mother" -> Just StepMother
    "Step Father" -> Just StepFather
    "Tutor" -> Just Tutor
    _ -> Nothing

instance StandardField StandardRelationShip where
  standardToText = standardRelationShipText
  standardFromText = parseStandardRelationShip

type RelationShip = StandardOrRaw StandardRelationShip

relationShipText :: RelationShip -> Text
relationShipText = standardOrRawText standardRelationShipText

parseRelationShip :: Text -> RelationShip
parseRelationShip = parseStandardOrRaw parseStandardRelationShip
