{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.DaycareTimeslot where

import Import

import Regtool.Data

import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.SchoolDay ()
import Regtool.Data.Gen.SchoolLevel ()
import Regtool.Data.Gen.Utils ()

instance GenUnchecked DaycareTimeslot

instance GenValid DaycareTimeslot where
  genValid = genValidStructurally
