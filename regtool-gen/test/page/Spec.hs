module Main
  ( main
  ) where

import TestImport

import Regtool.TestUtils

import qualified Regtool.AdminSpec
import qualified Regtool.AuthSpec
import qualified Regtool.CheckoutSpec
import qualified Regtool.ChildrenSpec
import qualified Regtool.DoctorsSpec
import qualified Regtool.HomeSpec
import qualified Regtool.LooperSpec
import qualified Regtool.OverviewSpec
import qualified Regtool.ParentsSpec
import qualified Regtool.ProfileSpec
import qualified Regtool.Signup.TransportSpec

main :: IO ()
main = hspec $ regtoolSpec completeSpec

completeSpec :: RegtoolSpec
completeSpec = do
  Regtool.AuthSpec.spec
  Regtool.HomeSpec.spec
  Regtool.AdminSpec.spec
  Regtool.OverviewSpec.spec
  Regtool.ProfileSpec.spec
  Regtool.ParentsSpec.spec
  Regtool.DoctorsSpec.spec
  Regtool.ChildrenSpec.spec
  Regtool.Signup.TransportSpec.spec
  Regtool.CheckoutSpec.spec
  Regtool.LooperSpec.spec
