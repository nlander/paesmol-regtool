{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Children.Utils
  ( RegisterChild(..)
  , registerChild
  , genSimpleRegisterChild
  , verifyChildRegistered
  , getChildByRegister
  ) where

import TestImport

import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types

import Yesod.Form
import Yesod.Test

import Regtool.Data

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Handler.Children
import Regtool.TestUtils
import Regtool.Utils.Children

{-# ANN module ("HLint: ignore Use &&" :: String) #-}

registerChild :: RegisterChild -> RegtoolExample ()
registerChild RegisterChild {..} = do
  get RegisterChildR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl RegisterChildR
    addToken
    addPostParam "f1" registerChildFirstName
    addPostParam "f2" registerChildLastName
    addPostParam "f3" $ dayText registerChildBirthDate
    addPostParam "f4" $ enumText registerChildSchoolLevel
    addPostParam "f5" $ sqlKeyText registerChildLanguageSection
    addPostParamStandardOrOther "f6" "f7" registerChildNationality
    addPostParamMaybe "f8" $ unTextarea <$> registerChildAllergies
    addPostParamMaybe "f9" $ unTextarea <$> registerChildHealthInfo
    addPostParamMaybe "f10" $ unTextarea <$> registerChildMedication
    addPostParamMaybe "f11" $ unTextarea <$> registerChildComments
    addPostParamMaybe "f12" $ unTextarea <$> registerChildPickupAllowed
    addPostParamMaybe "f13" $ unTextarea <$> registerChildPickupDisallowed
    addPostParam "f14" $ boolText registerChildPicturePermission
    addPostParam "f15" $ boolText registerChildDelijnBus
    addPostParam "f16" $ sqlKeyText registerChildDoctor
  shouldRedirectTo ChildrenR

verifyChildRegistered :: AccountId -> RegisterChild -> RegtoolExample ()
verifyChildRegistered aid RegisterChild {..} = do
  children <- runDB $ getChildrenOf aid
  (Entity _ Child {..}) <-
    case children of
      [] -> failure "Should have found one child, got empty list instead."
      [c] -> pure c
      cs ->
        failure $
        unlines $
        "Should have found one child, got these instead: " : map show cs
  liftIO $ do
    childFirstName `shouldBe` registerChildFirstName
    childLastName `shouldBe` registerChildLastName
    childBirthdate `shouldBe` registerChildBirthDate
    childLevel `shouldBe` registerChildSchoolLevel
    childNationality `shouldBe` registerChildNationality
    childAllergies `shouldBe` registerChildAllergies
    childHealthInfo `shouldBe` registerChildHealthInfo
    childMedication `shouldBe` registerChildMedication
    childComments `shouldBe` registerChildComments
    childPickupAllowed `shouldBe` registerChildPickupAllowed
    childPickupDisallowed `shouldBe` registerChildPickupDisallowed
    childPicturePermission `shouldBe` registerChildPicturePermission
    childDelijnBus `shouldBe` registerChildDelijnBus
    childDoctor `shouldBe` registerChildDoctor

getChildByRegister ::
     AccountId -> RegisterChild -> RegtoolExample (Entity Child)
getChildByRegister aid RegisterChild {..} = do
  children <- runDB $ getChildrenOf aid
  let mc =
        listToMaybe $
        filter
          (\(Entity _ Child {..}) ->
             and
               [ childFirstName == registerChildFirstName
               , childLastName == registerChildLastName
               ])
          children
  case mc of
    Nothing -> failure "Should have had a child"
    Just ch -> pure ch
