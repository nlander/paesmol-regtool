{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Signup.Daycare
  ( SignupChildDaycare(..)
  , getSignupDaycareR
  , postSignupDaycareR
  , postSignupDaycareSignoffR
  ) where

import Import

import qualified Data.Set as S
import qualified Data.Text as T

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Utils.Children
import Regtool.Utils.Signup.Daycare
import Regtool.Utils.Time

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildDaycare =
  SignupChildDaycare
    { signupChildDaycareChild :: Entity Child
    , signupChildDaycareTimeslot :: Entity DaycareTimeslot
    }
  deriving (Show, Eq, Generic)

instance Validity SignupChildDaycare

signupChildDaycareForm ::
     [Entity Child]
  -> [(Entity DaycareSemestre, Entity DaycareTimeslot)]
  -> AForm Handler SignupChildDaycare
signupChildDaycareForm cs tups =
  SignupChildDaycare <$>
  areq
    (selectFieldList $
     map (\e@(Entity _ Child {..}) -> (T.unwords [childFirstName, childLastName], e)) cs)
    (bfs ("Child" :: Text))
    Nothing <*>
  areq
    (selectFieldList $ map (\(Entity _ s, e@(Entity _ ts)) -> (timeslotDescription s ts, e)) tups)
    (bfs ("Timeslot" :: Text))
    Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurDaycareSignups $ map entityKey children
  let tups = timeslotsTups semestres timeslots
  let ets = signupTrips signups children semestres timeslots
  token <- genToken
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm $ signupChildDaycareForm children tups
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/daycare")

getSignupDaycareR :: Handler Html
getSignupDaycareR = getSignupPage Nothing

postSignupDaycareR :: Handler Html
postSignupDaycareR = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  children <- runDB $ getChildrenOf aid
  let tups = timeslotsTups semestres timeslots
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost $ renderCustomForm BootstrapBasicForm $ signupChildDaycareForm children tups
  case result of
    FormSuccess SignupChildDaycare {..} -> do
      let Entity cid c = signupChildDaycareChild
      let Entity tsid ts = signupChildDaycareTimeslot
      unless (childLevel c `S.member` daycareTimeslotAccessible ts) $
        invalidArgs ["This time slot is not accessible to a child in that level"]
      let signup =
            DaycareSignup
              { daycareSignupChild = cid
              , daycareSignupTimeslot = tsid
              , daycareSignupCreationTime = now
              }
      void $ runDB $ insertValidUnique signup
      setMessage "Succesfully signed up for daycare services. Do not forget to pay for them!"
      redirect SignupDaycareR
    fr -> getSignupPage (Just fr)

postSignupDaycareSignoffR :: DaycareSignupId -> Handler Html
postSignupDaycareSignoffR sid = do
  DaycareSignup {..} <- runDB $ get404 sid
    -- TODO make sure that deregistration only works if it's not been paid for
  runDB $ DB.delete sid
  redirect SignupDaycareR
