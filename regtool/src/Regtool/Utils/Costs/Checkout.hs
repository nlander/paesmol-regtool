{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Costs.Checkout where

import Import

import qualified Data.Map as M

import Regtool.Core.Foundation

import Regtool.Data

import Regtool.Utils.Costs.Input

newtype CheckoutsOverview =
  CheckoutsOverview
    { checkoutsOverviewCheckouts :: Map CheckoutId CheckoutOverview
    }
  deriving (Show, Eq, Generic)

instance Validity CheckoutsOverview

data CheckoutOverview =
  CheckoutOverview
    { checkoutOverviewCheckout :: !Checkout
    , checkoutOverviewPayment :: !(Maybe (Entity StripePayment))
    , checkoutOverviewYearlyFeePayments :: ![Entity YearlyFeePayment]
    , checkoutOverviewTransportPayments :: ![Entity TransportPayment]
    , checkoutOverviewOccasionalTransportPayments :: ![Entity OccasionalTransportPayment]
    , checkoutOverviewDaycarePayments :: ![Entity DaycarePayment]
    , checkoutOverviewDaycareActivityPayments :: ![Entity DaycareActivityPayment]
    , checkoutOverviewOccasionalDaycarePayments :: ![Entity OccasionalDaycarePayment]
    , checkoutOverviewDiscounts :: ![Entity Discount]
    }
  deriving (Show, Eq, Generic)

instance Validity CheckoutOverview

deriveCheckoutsOverview :: CostsInput -> CheckoutsOverview
deriveCheckoutsOverview CostsInput {..} =
  CheckoutsOverview
    { checkoutsOverviewCheckouts =
        M.fromList $
        flip map costsInputCheckouts $ \(Entity cid checkoutOverviewCheckout@Checkout {..}) ->
          let checkoutOverviewPayment = do
                pid <- checkoutPayment
                find ((== pid) . entityKey) costsInputStripePayments
              checkoutOverviewYearlyFeePayments =
                filter ((== cid) . yearlyFeePaymentCheckout . entityVal) costsInputYearlyFeePayments
              checkoutOverviewTransportPayments =
                filter ((== cid) . transportPaymentCheckout . entityVal) costsInputTransportPayments
              checkoutOverviewOccasionalTransportPayments =
                filter
                  ((== cid) . occasionalTransportPaymentCheckout . entityVal)
                  costsInputOccasionalTransportPayments
              checkoutOverviewDaycarePayments =
                filter ((== cid) . daycarePaymentCheckout . entityVal) costsInputDaycarePayments
              checkoutOverviewDaycareActivityPayments =
                filter
                  ((== cid) . daycareActivityPaymentCheckout . entityVal)
                  costsInputDaycareActivityPayments
              checkoutOverviewOccasionalDaycarePayments =
                filter
                  ((== cid) . occasionalDaycarePaymentCheckout . entityVal)
                  costsInputOccasionalDaycarePayments
              checkoutOverviewDiscounts =
                filter ((== Just cid) . discountCheckout . entityVal) costsInputDiscounts
              coo = CheckoutOverview {..}
           in (cid, coo)
    }
