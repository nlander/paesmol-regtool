{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper
  ( runAllLoopers
  ) where

import Import

import Looper

import Regtool.Looper.Backuper
import Regtool.Looper.Emailer
import Regtool.Looper.Types
import Regtool.OptParse.Types

runAllLoopers :: RunSettings -> LooperEnv -> IO ()
runAllLoopers sets looperEnv =
  flip runReaderT looperEnv $ runLoopers $ regtoolLoopers $ runSetsLoopers sets

regtoolLoopers :: LoopersSettings -> [LooperDef Looper]
regtoolLoopers LoopersSettings {..} =
  [ mkLooperDef "emailer" loopersSetsEmailer emailLooper
  , mkLooperDef "backuper" loopersSetsBackuper backupLooper
  ]
