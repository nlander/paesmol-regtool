{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Handler.Doctors
  ( RegisterDoctor(..)
  , getDoctorsR
  , getDoctorR
  , getRegisterDoctorR
  , postRegisterDoctorR
  , postDeregisterDoctorR
  ) where

import Import

import Yesod.Auth
import Yesod.Core
import Yesod.Form.Bootstrap3

import Regtool.Core.Form
import Regtool.Core.Foundation

import Regtool.Data

import Regtool.Utils.Doctors

import Regtool.Component.NavigationBar

getDoctorsR :: Handler Html
getDoctorsR = do
  accid <- requireAuthId
  doctors <- runDB $ selectList [DoctorAccount ==. accid] []
  token <- genToken
  withNavBar $(widgetFile "doctors")

data RegisterDoctor =
  RegisterDoctor
    { registerDoctorFirstName :: Text
    , registerDoctorLastName :: Text
    , registerDoctorAddressLine1 :: Text
    , registerDoctorAddressLine2 :: Maybe Text
    , registerDoctorPostalCode :: Text
    , registerDoctorCity :: Text
    , registerDoctorCountry :: Country
    , registerDoctorPhoneNumber1 :: Text
    , registerDoctorPhoneNumber2 :: Maybe Text
    , registerDoctorEmail1 :: EmailAddress
    , registerDoctorEmail2 :: Maybe EmailAddress
    , registerDoctorComments :: Maybe Textarea
    , registerDoctorId :: Maybe DoctorId
    }
  deriving (Show, Eq)

registerDoctorForm :: Maybe (Entity Doctor) -> AForm Handler RegisterDoctor
registerDoctorForm mce =
  RegisterDoctor <$> areq textField (bfs ("First Name" :: Text)) (dv doctorFirstName) <*>
  areq textField (bfs ("Last Name" :: Text)) (dv doctorLastName) <*>
  areq textField (bfs ("Address line 1 (Street and number)" :: Text)) (dv doctorAddressLine1) <*>
  aopt textField (bfs ("Address line 2" :: Text)) (dv doctorAddressLine2) <*>
  areq textField (bfs ("Zip code" :: Text)) (dv doctorPostalCode) <*>
  areq textField (bfs ("City" :: Text)) (dv doctorCity) <*>
  standardOrRawForm (bfs ("Country" :: Text)) (dv doctorCountry) <*>
  areq textField (bfs ("Phone number (primary)" :: Text)) (dv doctorPhoneNumber1) <*>
  aopt textField (bfs ("Phone number (secondary)" :: Text)) (dv doctorPhoneNumber2) <*>
  areq emailAddressField (bfs ("Email address (primary)" :: Text)) (dv doctorEmail1) <*>
  aopt emailAddressField (bfs ("Email address (secondary)" :: Text)) (dv doctorEmail2) <*>
  aopt textareaField (bfs ("Extra Comments" :: Text)) (dv doctorComments) <*>
  aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (Doctor -> a) -> Maybe a
    dv func = (func . entityVal) <$> mce

getRegisterDoctorR :: Handler Html
getRegisterDoctorR = do
  void requireAuthId
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm $ registerDoctorForm Nothing
  withNavBar $(widgetFile "register-doctor")

postRegisterDoctorR :: Handler Html
postRegisterDoctorR = do
  aid <- requireAuthId
  ((result, formWidget), formEnctype) <-
    runFormPost $ renderCustomForm BootstrapBasicForm $ registerDoctorForm Nothing
  case result of
    FormSuccess RegisterDoctor {..} -> do
      now <- liftIO getCurrentTime
      let doctor_ =
            Doctor
              { doctorAccount = aid
              , doctorFirstName = registerDoctorFirstName
              , doctorLastName = registerDoctorLastName
              , doctorAddressLine1 = registerDoctorAddressLine1
              , doctorAddressLine2 = registerDoctorAddressLine2
              , doctorPostalCode = registerDoctorPostalCode
              , doctorCity = registerDoctorCity
              , doctorCountry = registerDoctorCountry
              , doctorPhoneNumber1 = registerDoctorPhoneNumber1
              , doctorPhoneNumber2 = registerDoctorPhoneNumber2
              , doctorEmail1 = registerDoctorEmail1
              , doctorEmail2 = registerDoctorEmail2
              , doctorComments = registerDoctorComments
              , doctorRegistrationTime = now
              }
      case registerDoctorId of
        Just i ->
          withOwnDoctor i $ do
            runDB $ replaceValid i doctor_
            redirect DoctorsR
        Nothing -> do
          runDB $ insertValid_ doctor_
          redirect DoctorsR
    fr -> withFormResultNavBar fr $(widgetFile "register-doctor")

getDoctorR :: DoctorId -> Handler Html
getDoctorR cid =
  withOwnDoctor cid $ do
    doctor_ <- runDB $ get404 cid
    (formWidget, formEnctype) <-
      generateFormPost $
      renderCustomForm BootstrapBasicForm $ registerDoctorForm $ Just (Entity cid doctor_)
    withNavBar $(widgetFile "doctor")

isDoctorOf :: DoctorId -> AccountId -> Handler Bool
isDoctorOf pid aid = ((== Just True) . fmap ((== aid) . doctorAccount)) <$> runDB (get pid)

withOwnDoctor :: DoctorId -> Handler Html -> Handler Html
withOwnDoctor cid func = do
  aid <- requireAuthId
  isOwnDoctor <- isDoctorOf cid aid
  isAdminAcc <- (== Just True) <$> isAdmin
  if isOwnDoctor || isAdminAcc
    then func
    else permissionDenied "Not your doctor."

postDeregisterDoctorR :: DoctorId -> Handler Html
postDeregisterDoctorR did =
  withOwnDoctor did $ do
    canDeleteDoctor did >>= deleteOrError
    redirect DoctorsR
