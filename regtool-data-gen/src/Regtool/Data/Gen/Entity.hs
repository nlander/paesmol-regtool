{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -O0 #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}

module Regtool.Data.Gen.Entity where

import Import

import qualified Data.List.NonEmpty as NE
import Data.List.NonEmpty (NonEmpty(..))

import Database.Persist.Sql

import Regtool.Data (distinct)

modEnt :: (a -> a) -> Entity a -> Entity a
modEnt f (Entity i e) = Entity i (f e)

modEntL :: (a -> a) -> [Entity a] -> [Entity a]
modEntL f = map (modEnt f)

instance ToBackendKey SqlBackend record => GenUnchecked (Key record) where
  genUnchecked = toSqlKey <$> arbitrary -- TODO replace with genUnchecked
  shrinkUnchecked = shrinkNothing

instance ToBackendKey SqlBackend record => GenValid (Key record) where
  genValid = toSqlKey <$> arbitrary -- TODO replace with genValid

instance (GenUnchecked a, PersistEntity a, ToBackendKey SqlBackend a) =>
         GenUnchecked (Entity a) where
  genUnchecked = Entity <$> genUnchecked <*> genUnchecked
  shrinkUnchecked (Entity k v) = [Entity k' v' | (k', v') <- shrinkUnchecked (k, v)]

instance (GenValid a, PersistEntity a, ToBackendKey SqlBackend a) => GenValid (Entity a) where
  genValid = Entity <$> genValid <*> genValid
  shrinkValid (Entity k v) = [Entity k' v' | (k', v') <- shrinkValid (k, v)]

genSeperate :: Eq a => Gen a -> Gen [a]
genSeperate g = nub <$> genListOf g

genSeperateFor :: Eq b => Gen b -> [a] -> Gen [(b, a)]
genSeperateFor _ [] = pure []
genSeperateFor g (a:as) = NE.toList <$> genSeperateForNE g (a :| as)

genSeperateForNE :: Eq b => Gen b -> NonEmpty a -> Gen (NonEmpty (b, a))
genSeperateForNE g (a :| as) = do
  restTups <- genSeperateFor g as
  b <- g `suchThat` (`notElem` map fst restTups)
  pure ((b, a) :| restTups)

genValidSeperateFor :: (GenValid b, Eq b) => [a] -> Gen [(b, a)]
genValidSeperateFor = genSeperateFor genValid

genValidSeperateForNE :: (GenValid b, Eq b) => NonEmpty a -> Gen (NonEmpty (b, a))
genValidSeperateForNE = genSeperateForNE genValid

genSeperateIds ::
     forall a. (PersistEntity a, ToBackendKey SqlBackend a, GenValid a, Eq (Key a))
  => Gen [Key a]
genSeperateIds = genSeperate genValid

genSeperateIdsFor ::
     forall a. (PersistEntity a, ToBackendKey SqlBackend a, GenValid a, Eq (Key a))
  => [a]
  -> Gen [Entity a]
genSeperateIdsFor [] = pure []
genSeperateIdsFor (a:as) = NE.toList <$> genSeperateIdsForNE (a :| as)

genSeperateIdsForNE ::
     forall a. (PersistEntity a, ToBackendKey SqlBackend a, GenValid a, Eq (Key a))
  => NonEmpty a
  -> Gen (NonEmpty (Entity a))
genSeperateIdsForNE (a :| as) = do
  es <- genSeperateIdsFor as
  i <- genValid `suchThat` (`notElem` map entityKey es)
  pure (Entity i a :| es)

genValidsWithSeperateIDs ::
     forall a. (PersistEntity a, ToBackendKey SqlBackend a, GenValid a, Eq (Key a))
  => Gen a
  -> Gen [Entity a]
genValidsWithSeperateIDs gen = do
  len <- genValid `suchThat` (>= 0)
  go len
  where
    go :: Int -> Gen [Entity a]
    go 0 = pure []
    go n = do
      es <- go $ n - 1
      ei <- genValid `suchThat` (`notElem` map entityKey es)
      e <- gen
      pure $ Entity ei e : es

validsWithSeperateIDs ::
     forall a. (PersistEntity a, ToBackendKey SqlBackend a, GenValid a, Eq (Key a))
  => Gen [Entity a]
validsWithSeperateIDs = genValidsWithSeperateIDs genValid

shrinkValidWithSeperateIds ::
     (PersistEntity a, ToBackendKey SqlBackend a, GenValid a, Eq (Key a))
  => [Entity a]
  -> [[Entity a]]
shrinkValidWithSeperateIds = filter (distinct . map entityKey) . shrinkValid
