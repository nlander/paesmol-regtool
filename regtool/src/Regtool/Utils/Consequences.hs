{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Consequences where

import Import

import qualified Database.Persist as DB

import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Utils.Costs.Choice
import Regtool.Utils.Costs.Invoice

makeCheckout ::
     MonadIO m
  => AccountId
  -> Choices
  -> Maybe StripePaymentId
  -> ReaderT SqlBackend m (Entity Checkout)
makeCheckout aid cs mspid = do
  now <- liftIO getCurrentTime
  insertValidEntity $ choicesCheckout aid cs mspid now

choicesCheckout :: AccountId -> Choices -> Maybe StripePaymentId -> UTCTime -> Checkout
choicesCheckout aid choices mspid ts =
  Checkout
    { checkoutAccount = aid
    , checkoutAmount = invoiceTotal $ deriveInvoice choices
    , checkoutPayment = mspid
    , checkoutTimestamp = ts
    }

-- The new entries to be added to the database.
data PaymentConsequences =
  PaymentConsequences
    { paymentConsequencesYearlyFeePayments :: ![YearlyFeePayment]
    , paymentConsequencesTransportPaymentConsequences :: ![TransportPaymentConsequences]
    , paymentConsequencesOccasionalTransportPaymentConsequences :: ![OccasionalTransportPayment]
    , paymentConsequencesDaycarePaymentConsequences :: ![DaycarePaymentConsequences]
    , paymentConsequencesDaycareActivityPaymentConsequences :: ![DaycareActivityPaymentConsequences]
    , paymentConsequencesOccasionalDaycarePaymentConsequences :: ![OccasionalDaycarePayment]
    , paymentConsequencesCompleteDiscounts :: ![(CheckoutId, DiscountId)]
    , paymentConsequencesNewDiscount :: !(Maybe Discount)
    }
  deriving (Show, Eq, Generic)

instance Validity PaymentConsequences

instance Monoid PaymentConsequences where
  mempty = PaymentConsequences [] [] [] [] [] [] [] Nothing
  mappend (PaymentConsequences l1 l2 l3 l4 l5 l6 l7 l8) (PaymentConsequences l1' l2' l3' l4' l5' l6' l7' l8') =
    PaymentConsequences
      (l1 <> l1')
      (l2 <> l2')
      (l3 <> l3')
      (l4 <> l4')
      (l5 <> l5')
      (l6 <> l6')
      (l7 <> l7')
      (addMDiscount l8 l8')

addMDiscount :: Maybe Discount -> Maybe Discount -> Maybe Discount
addMDiscount Nothing Nothing = Nothing
addMDiscount (Just d1) Nothing = Just d1
addMDiscount Nothing (Just d2) = Just d2
addMDiscount (Just d1) (Just d2) =
  Just d1 {discountAmount = addAmount (discountAmount d1) (discountAmount d2)}

data TransportPaymentConsequences
  = NewEnrollmentWithPlanAndPayments
      !CheckoutId
      !TransportSignupId
      !TransportPaymentPlan
      !(NonEmpty Amount)
  | NewPaymentsWith (NonEmpty TransportPayment)
  deriving (Show, Eq, Generic)

instance Validity TransportPaymentConsequences where
  validate tpc@(NewEnrollmentWithPlanAndPayments _ _ _ as) =
    decorate "NewEnrollmentWithPlanAndPayments" $
    mconcat
      [ genericValidate tpc
      , decorate "NonEmpty Amount" $
        decorateList (NE.toList as) $ \a ->
          mconcat
            [ declare "The amount is not zero" $ a /= zeroAmount
            , declare "The amount is positive" $ a >= zeroAmount
            ]
      ]
  validate tpc = genericValidate tpc

data SecondStageTransportPaymentConsequence =
  SecondStageTransportPaymentConsequence !(NonEmpty TransportPayment) !TransportEnrollment
  deriving (Show, Eq, Generic)

instance Validity SecondStageTransportPaymentConsequence

data DaycarePaymentConsequences =
  DaycarePaymentConsequences DaycareSignupId CheckoutId Amount
  deriving (Show, Eq, Generic)

instance Validity DaycarePaymentConsequences

data DaycareActivityPaymentConsequences =
  DaycareActivityPaymentConsequences DaycareActivitySignupId CheckoutId Amount
  deriving (Show, Eq, Generic)

instance Validity DaycareActivityPaymentConsequences

realisePaymentConsequences :: MonadIO m => PaymentConsequences -> ReaderT SqlBackend m ()
realisePaymentConsequences PaymentConsequences {..} = do
  now <- liftIO getCurrentTime
  void $ insertManyValid paymentConsequencesYearlyFeePayments
  forM_ paymentConsequencesTransportPaymentConsequences $ \tpc ->
    case tpc of
      NewEnrollmentWithPlanAndPayments cid tsid tpp as -> do
        tppid <- insertValid tpp
        let SecondStageTransportPaymentConsequence tps te =
              secondStageTransportPaymentConsequence tppid cid tsid as
        void $ insertManyValid $ NE.toList tps
        insertValid_ te
      NewPaymentsWith tps -> void $ insertManyValid $ NE.toList tps
  forM_ paymentConsequencesOccasionalTransportPaymentConsequences insertValid_
  forM_ paymentConsequencesDaycarePaymentConsequences $ \(DaycarePaymentConsequences dsid cid a) ->
    insertValid
      DaycarePayment
        { daycarePaymentSignup = dsid
        , daycarePaymentCheckout = cid
        , daycarePaymentAmount = a
        , daycarePaymentTime = now
        }
  forM_ paymentConsequencesDaycareActivityPaymentConsequences $ \(DaycareActivityPaymentConsequences dsid cid a) ->
    insertValid
      DaycareActivityPayment
        { daycareActivityPaymentSignup = dsid
        , daycareActivityPaymentCheckout = cid
        , daycareActivityPaymentAmount = a
        , daycareActivityPaymentTime = now
        }
  forM_ paymentConsequencesOccasionalDaycarePaymentConsequences insertValid_
  forM_ paymentConsequencesCompleteDiscounts $ \(cid, did) ->
    DB.update did [DiscountCheckout DB.=. Just cid]
  forM_ paymentConsequencesNewDiscount insertValid

paymentConsequences :: AccountId -> CheckoutId -> UTCTime -> Choices -> PaymentConsequences
paymentConsequences aid cid now cs@Choices {..} =
  let yearlyConsequences =
        flip map (M.toList choicesYearlyMap) $ \(sy, YearlyChoices {..}) ->
          let ycs =
                mempty
                  { paymentConsequencesYearlyFeePayments =
                      yearlyFeePayments aid sy cid yearlyChoicesYearlyFee
                  , paymentConsequencesTransportPaymentConsequences =
                      transportPaymentConsequences cid yearlyChoicesTransportInstalments
                  }
              spcs = semestrelyPaymentConsequences cid yearlyChoicesSemestrelyMap
           in mconcat $ ycs : spcs
      globalConsequences =
        mempty
          { paymentConsequencesOccasionalTransportPaymentConsequences =
              occasionalTransportPaymentConsequences cid choicesOccasionalTransport
          , paymentConsequencesOccasionalDaycarePaymentConsequences =
              occasionalDaycarePaymentConsequences cid now choicesOccasionalDaycare
          , paymentConsequencesCompleteDiscounts = map ((,) cid . entityKey) choicesDiscounts
          , paymentConsequencesNewDiscount = newDiscount aid now cs
          }
   in mconcat $ globalConsequences : yearlyConsequences

yearlyFeePayments :: AccountId -> SchoolYear -> CheckoutId -> Maybe Amount -> [YearlyFeePayment]
yearlyFeePayments aid sy cid yearlyChoicesYearlyFee =
  case yearlyChoicesYearlyFee of
    Just a ->
      [ YearlyFeePayment
          { yearlyFeePaymentAccount = aid
          , yearlyFeePaymentSchoolyear = sy
          , yearlyFeePaymentCheckout = cid
          , yearlyFeePaymentAmount = a
          }
      ]
    Nothing -> []

transportPaymentConsequences ::
     CheckoutId -> Map (Entity Child) TransportInstallmentChoices -> [TransportPaymentConsequences]
transportPaymentConsequences cid yearlyChoicesTransportInstalments =
  M.elems $
  flip M.map yearlyChoicesTransportInstalments $ \TransportInstallmentChoices {..} ->
    case transportInstallmentChoicesTransportPaymentPlan of
      Left tppe ->
        NewPaymentsWith $
        flip NE.map transportInstallmentChoicesInstalments $ \a ->
          TransportPayment
            { transportPaymentCheckout = cid
            , transportPaymentPaymentPlan = entityKey tppe
            , transportPaymentAmount = a
            }
      Right tpp ->
        NewEnrollmentWithPlanAndPayments
          cid
          (entityKey transportInstallmentChoicesTransportSignup)
          tpp
          transportInstallmentChoicesInstalments

occasionalTransportPaymentConsequences ::
     CheckoutId -> [OccasionalTransportChoice] -> [OccasionalTransportPayment]
occasionalTransportPaymentConsequences cid choicesOccasionalTransport =
  flip map choicesOccasionalTransport $ \OccasionalTransportChoice {..} ->
    OccasionalTransportPayment
      { occasionalTransportPaymentSignup = entityKey occasionalTransportChoiceSignup
      , occasionalTransportPaymentCheckout = cid
      , occasionalTransportPaymentAmount = occasionalTransportChoiceAmount
      }

occasionalDaycarePaymentConsequences ::
     CheckoutId -> UTCTime -> [OccasionalDaycareChoice] -> [OccasionalDaycarePayment]
occasionalDaycarePaymentConsequences cid now choicesOccasionalDaycare =
  flip map choicesOccasionalDaycare $ \OccasionalDaycareChoice {..} ->
    OccasionalDaycarePayment
      { occasionalDaycarePaymentSignup = entityKey occasionalDaycareChoiceSignup
      , occasionalDaycarePaymentCheckout = cid
      , occasionalDaycarePaymentAmount = occasionalDaycareChoiceAmount
      , occasionalDaycarePaymentTime = now
      }

semestrelyPaymentConsequences ::
     CheckoutId -> Map (Entity DaycareSemestre) SemestrelyChoices -> [PaymentConsequences]
semestrelyPaymentConsequences cid yearlyChoicesSemestrelyMap =
  flip map (M.toList yearlyChoicesSemestrelyMap) $ \(_, SemestrelyChoices {..}) ->
    mempty
      { paymentConsequencesDaycarePaymentConsequences =
          flip map semestrelyChoicesDaycareChoices $ \DaycareChoice {..} ->
            DaycarePaymentConsequences (entityKey daycareChoiceSignup) cid daycareChoiceAmount
      , paymentConsequencesDaycareActivityPaymentConsequences =
          flip map semestrelyChoicesDaycareActivityChoices $ \DaycareActivityChoice {..} ->
            DaycareActivityPaymentConsequences (entityKey daycareActivityChoiceSignup) cid daycareActivityChoiceAmount
      }

newDiscount :: AccountId -> UTCTime -> Choices -> Maybe Discount
newDiscount aid now cs =
  let total = invoiceTotal (deriveInvoice cs)
   in if total < zeroAmount
        then Just
               Discount
                 { discountAccount = aid
                 , discountAmount = negateAmount total
                 , discountReason = Just "Leftover change discount from previous discount"
                 , discountCheckout = Nothing
                 , discountTimestamp = now
                 }
        else Nothing

secondStageTransportPaymentConsequence ::
     TransportPaymentPlanId
  -> CheckoutId
  -> TransportSignupId
  -> NonEmpty Amount
  -> SecondStageTransportPaymentConsequence
secondStageTransportPaymentConsequence tppid cid tsid as =
  SecondStageTransportPaymentConsequence
    (flip NE.map as $ \a ->
       TransportPayment
         { transportPaymentPaymentPlan = tppid
         , transportPaymentCheckout = cid
         , transportPaymentAmount = a
         })
    TransportEnrollment {transportEnrollmentSignup = tsid, transportEnrollmentPaymentPlan = tppid}
