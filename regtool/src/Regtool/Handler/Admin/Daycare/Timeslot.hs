{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Daycare.Timeslot
  ( getAdminRegisterDaycareTimeslotR
  , postAdminRegisterDaycareTimeslotR
  , getAdminDaycareTimeslotsR
  , postAdminDaycareTimeslotDeleteR
  , getAdminDaycareTimeslotR
  , getAdminDaycareTimeslotOverviewR
  ) where

import Import

import qualified Data.Set as S
import Data.Set (Set)
import qualified Database.Persist as DB
import Yesod.Core
import Yesod.Form.Bootstrap3

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Form
import Regtool.Core.Foundation
import Regtool.Utils.Time

getAdminDaycareTimeslotsR :: DaycareSemestreId -> Handler Html
getAdminDaycareTimeslotsR sid = do
  daycareTimeslots <- runDB $ selectList [DaycareTimeslotSemestre ==. sid] [Asc DaycareTimeslotId]
  token <- genToken
  withNavBar $(widgetFile "admin/daycare/timeslots")

data RegisterDaycareTimeslot =
  RegisterDaycareTimeslot
    { registerDaycareTimeslotSchoolDay :: SchoolDay
    , registerDaycareTimeslotStart :: TimeOfDay
    , registerDaycareTimeslotEnd :: TimeOfDay
    , registerDaycareTimeslotFee :: Amount
    , registerDaycareTimeslotOccasionalFee :: Amount
    , registerDaycareTimeslotAccessible :: Set SchoolLevel
    , registerDaycareTimeslotId :: Maybe DaycareTimeslotId
    }
  deriving (Show, Eq)

registerDaycareTimeslotForm ::
     Maybe (Entity DaycareTimeslot) -> AForm Handler RegisterDaycareTimeslot
registerDaycareTimeslotForm mce =
  RegisterDaycareTimeslot <$>
  areq (selectField optionsEnum) (bfs ("School Day" :: Text)) (dv daycareTimeslotSchoolDay) <*>
  areq timeFieldTypeTime (bfs ("Start time" :: Text)) (dv daycareTimeslotStart) <*>
  areq timeFieldTypeTime (bfs ("End time" :: Text)) (dv daycareTimeslotEnd) <*>
  areq amountField (bfs ("Fee" :: Text)) (dv daycareTimeslotFee) <*>
  areq amountField (bfs ("Fee for occasional use" :: Text)) (dv daycareTimeslotOccasionalFee) <*>
  accessibilityForm mce <*>
  aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (DaycareTimeslot -> a) -> Maybe a
    dv func = (func . entityVal) <$> mce

accessibilityForm :: Maybe (Entity DaycareTimeslot) -> AForm Handler (Set SchoolLevel)
accessibilityForm mce =
  fmap (S.fromList . catMaybes) $
  sequenceA $
  flip map [minBound .. maxBound] $ \sl ->
    (\b ->
       if b
         then Just sl
         else Nothing) <$>
    areq
      checkBoxField
      (bfs $ schoolLevelText sl)
      (((sl `elem`) . daycareTimeslotAccessible . entityVal) <$> mce)

getAdminRegisterDaycareTimeslotR :: DaycareSemestreId -> Handler Html
getAdminRegisterDaycareTimeslotR _ = do
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm $ registerDaycareTimeslotForm Nothing
  withNavBar $(widgetFile "admin/daycare/register-timeslot")

postAdminRegisterDaycareTimeslotR :: DaycareSemestreId -> Handler Html
postAdminRegisterDaycareTimeslotR sid = do
  ((result, formWidget), formEnctype) <-
    runFormPost $ renderCustomForm BootstrapBasicForm $ registerDaycareTimeslotForm Nothing
  case result of
    FormSuccess RegisterDaycareTimeslot {..} -> do
      now <- liftIO getCurrentTime
      let daycareTimeslot_ =
            DaycareTimeslot
              { daycareTimeslotSemestre = sid
              , daycareTimeslotSchoolDay = registerDaycareTimeslotSchoolDay
              , daycareTimeslotStart = registerDaycareTimeslotStart
              , daycareTimeslotEnd = registerDaycareTimeslotEnd
              , daycareTimeslotFee = registerDaycareTimeslotFee
              , daycareTimeslotOccasionalFee = registerDaycareTimeslotOccasionalFee
              , daycareTimeslotAccessible = registerDaycareTimeslotAccessible
              , daycareTimeslotCreationTime = now
              }
      case registerDaycareTimeslotId of
        Just i -> do
          runDB $ replaceValid i daycareTimeslot_
          redirect $ AdminR $ AdminDaycareTimeslotsR sid
        Nothing -> do
          did <- runDB $ insertValid daycareTimeslot_
          redirect $ AdminR $ AdminDaycareTimeslotR sid did
    _ -> withNavBar $(widgetFile "admin/daycare/register-timeslot")

getAdminDaycareTimeslotR :: DaycareSemestreId -> DaycareTimeslotId -> Handler Html
getAdminDaycareTimeslotR sid tid = do
  daycareTimeslot <- runDB $ get404 tid
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $
    registerDaycareTimeslotForm $ Just $ Entity tid daycareTimeslot
  withNavBar $(widgetFile "admin/daycare/timeslot")

getAdminDaycareTimeslotOverviewR :: DaycareSemestreId -> DaycareTimeslotId -> Handler Html
getAdminDaycareTimeslotOverviewR _ tid = do
  daycareTimeslot <- runDB $ get404 tid
  withNavBar $(widgetFile "admin/daycare/timeslot-overview")

postAdminDaycareTimeslotDeleteR :: DaycareSemestreId -> DaycareTimeslotId -> Handler Html
postAdminDaycareTimeslotDeleteR sid tid = do
  canDeleteDaycareTimeslot tid >>= deleteOrError
  redirect $ AdminR $ AdminDaycareTimeslotsR sid

canDeleteDaycareTimeslot :: DaycareTimeslotId -> Handler (Deletable DaycareTimeslotId)
canDeleteDaycareTimeslot dcsid = do
  mc <- runDB $ selectFirst [DaycareSignupTimeslot ==. dcsid] []
  pure $
    case mc of
      Nothing -> CanDelete $ DB.delete dcsid
      Just _ -> CannotDelete ["There are still registrations for this time slot."]
