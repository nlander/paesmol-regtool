{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Utils.Signup.DaycareActivity
  ( activitiesTups
  , signupTrips
  , getOurDaycareActivitySignups
  , activityDescription
  ) where

import Import

import qualified Data.Text as T

import Regtool.Data

import Regtool.Core.Foundation
import Regtool.Utils.Time

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

activitiesTups ::
     [Entity DaycareSemestre]
  -> [Entity DaycareActivity]
  -> [(Entity DaycareSemestre, Entity DaycareActivity)]
activitiesTups semestres activities =
  flip mapMaybe activities $ \ts -> do
    s <- find ((== daycareActivitySemestre (entityVal ts)) . entityKey) semestres
    pure (s, ts)

signupTrips ::
     [Entity DaycareActivitySignup]
  -> [Entity Child]
  -> [Entity DaycareSemestre]
  -> [Entity DaycareActivity]
  -> [(DaycareActivitySignupId, Entity Child, Entity DaycareSemestre, Entity DaycareActivity)]
signupTrips signups children semestres activities =
  flip mapMaybe signups $ \(Entity eid DaycareActivitySignup {..}) -> do
    c <- find ((== daycareActivitySignupChild) . entityKey) children
    a <- find ((== daycareActivitySignupActivity) . entityKey) activities
    s <- find ((== daycareActivitySemestre (entityVal a)) . entityKey) semestres
    pure (eid, c, s, a)

getOurDaycareActivitySignups ::
     MonadIO m => [ChildId] -> ReaderT SqlBackend m [Entity DaycareActivitySignup]
getOurDaycareActivitySignups cids = selectList [DaycareActivitySignupChild <-. cids] []

activityDescription :: DaycareSemestre -> DaycareActivity -> Text
activityDescription DaycareSemestre {..} DaycareActivity {..} =
  T.unwords
    [ schoolYearText daycareSemestreYear
    , semestreText daycareSemestreSemestre
    , schoolDayText daycareActivitySchoolDay
    , T.pack $ formatTimestamp daycareActivityStart
    , "-"
    , T.pack $ formatTimestamp daycareActivityEnd
    , ": "
    , daycareActivityName
    ]
