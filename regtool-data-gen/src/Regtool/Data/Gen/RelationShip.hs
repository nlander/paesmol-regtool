{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

module Regtool.Data.Gen.RelationShip where

import Import

import Regtool.Data

import Regtool.Data.Gen.StandardOrRaw

instance GenUnchecked StandardRelationShip

instance GenValid StandardRelationShip

genSimpleRelationShip :: Gen RelationShip
genSimpleRelationShip = genSimpleStandardOrRaw genValid
