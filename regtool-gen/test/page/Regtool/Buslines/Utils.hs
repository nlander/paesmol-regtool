module Regtool.Buslines.Utils
  ( getBusStop
  ) where

import TestImport

import Database.Persist

import Regtool.Data

import Regtool.Application ()
import Regtool.Gen ()
import Regtool.TestUtils

getBusStop :: RegtoolExample (Entity BusStop)
getBusStop = do
  mbs <- runDB $ selectFirst ([] :: [Filter BusStop]) []
  case mbs of
    Nothing -> failure "Should have had a bus stop"
    Just bs -> pure bs
