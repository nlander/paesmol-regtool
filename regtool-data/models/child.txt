Child
  firstName                 Text
  lastName                  Text
  birthdate                 Day
  level                     SchoolLevel
  languageSection           LanguageSectionId
  nationality               Nationality
  allergies                 Textarea       Maybe
  healthInfo                Textarea       Maybe
  medication                Textarea       Maybe
  comments                  Textarea       Maybe
  pickupAllowed             Textarea       Maybe
  pickupDisallowed          Textarea       Maybe
  picturePermission         Bool
  delijnBus                 Bool
  doctor                    DoctorId
  registrationTime          UTCTime

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable


ChildOf
  child                     ChildId
  parent                    AccountId
  UniqueChild child parent

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable

