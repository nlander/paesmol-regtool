{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Technical
  ( getAdminTechnicalR
  , postAdminTechnicalRefreshStripeCustomersR
  ) where

import Import

import Database.Persist
import Database.Persist.Sql

import Yesod.Core

import qualified Web.Stripe.Customer as Stripe

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation
import Regtool.Utils.Stripe

getAdminTechnicalR :: Handler Html
getAdminTechnicalR = do
  token <- genToken
  withNavBar $(widgetFile "admin/technical")

postAdminTechnicalRefreshStripeCustomersR :: Handler Html
postAdminTechnicalRefreshStripeCustomersR = do
  scs <- runDB $ selectList [] [Asc StripeCustomerId]
  as <- runDB $ selectList [] [Asc AccountId]
  ress <-
    forM scs $ \(Entity scid StripeCustomer {..}) -> do
      Account {..} <-
        case find ((== stripeCustomerAccount) . entityKey) as of
          Nothing -> error "Should not happen."
          Just (Entity _ a) -> pure a
      gscr <- getStripeCustomer accountEmailAddress
      pure (scid, accountEmailAddress, stripeCustomerIdentifier, gscr)
  runDB $
    forM_ ress $ \(scid, _, _, res) ->
      case res of
        GotStripeCustomer cid -> update scid [StripeCustomerIdentifier =. Just cid]
        NoSuchStripeCustomer -> update scid [StripeCustomerIdentifier =. Nothing]
        _ -> pure ()
  withNavBar $(widgetFile "admin/technical/refresh-stripe-customers")

statusClass :: Maybe Stripe.CustomerId -> GetStripeCustomerResult -> Text
statusClass mOldId res =
  case (mOldId, res) of
    (Nothing, NoSuchStripeCustomer) -> "info"
    (Nothing, GotStripeCustomer _) -> "success"
    (Just oldId, GotStripeCustomer newId) ->
      if oldId == newId
        then "info"
        else "success"
    (Just _, NoSuchStripeCustomer) -> "success"
    _ -> "danger"
