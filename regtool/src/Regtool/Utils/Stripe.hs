{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Utils.Stripe
  ( stripeName
  , currency
  , showCurrency
  , runStripe
  , stripeCreateCustomerIfNecessary
  , getStripeCustomer
  , GetStripeCustomerResult(..)
  , stripeCreateCharge
  ) where

import Import

import Data.Char as Char
import qualified Data.Text as T

import Database.Persist

import Web.Stripe (StripeError(..), StripeRequest(..), StripeReturn, (-&-), stripe)
import Web.Stripe.Charge (Currency(..))
import qualified Web.Stripe.Charge as Stripe
import qualified Web.Stripe.Customer as Stripe
import qualified Web.Stripe.StripeRequest as Stripe

import Regtool.Data

import Regtool.Core.Foundation

stripeName :: Text
stripeName = "PAESMOL"

currency :: Stripe.Currency
currency = EUR

showCurrency :: Stripe.Currency -> String
showCurrency = map Char.toLower . show

deriving instance Show (StripeRequest a)

runStripe ::
     FromJSON (StripeReturn a) => StripeRequest a -> Handler (Either StripeError (StripeReturn a))
runStripe sr = do
  liftIO $ print sr
  mss <- regtoolStripeSettings <$> getYesod
  case mss of
    Nothing -> liftIO $ die "Cannot run stripe without stripe settings."
    Just StripeSettings {..} -> liftIO $ stripe stripeSetsConfig sr

stripeCreateCharge :: Stripe.TokenId -> Amount -> Handler StripePaymentId
stripeCreateCharge token a = do
  (identifier, scid) <- stripeCreateCustomerIfNecessary token
  result <- runStripe $ Stripe.createCharge (Stripe.Amount $ amountCents a) EUR -&- identifier
  liftIO $ print result
  case result of
    Left stripeError -> invalidArgs [T.pack $ show stripeError]
    Right c -> do
      now <- liftIO getCurrentTime
      runDB $
        insertValid
          StripePayment
            { stripePaymentAmount = a
            , stripePaymentCustomer = scid
            , stripePaymentCharge = Stripe.chargeId c
            , stripePaymentTimestamp = now
            }

stripeCreateCustomerIfNecessary :: Stripe.TokenId -> Handler (Stripe.CustomerId, StripeCustomerId)
stripeCreateCustomerIfNecessary token = do
  (Entity aid Account {..}) <- requireAuth
  msc <- runDB $ selectFirst [StripeCustomerAccount ==. aid] []
  case msc of
    Just (Entity scid StripeCustomer {..}) ->
      case stripeCustomerIdentifier of
        Nothing -> do
          cid <- getOrCreate accountEmailAddress
          runDB $ update scid [StripeCustomerIdentifier =. Just cid]
          pure (cid, scid)
        Just cid -> pure (cid, scid)
    Nothing -> do
      cid <- getOrCreate accountEmailAddress
      scid <-
        runDB $
        insertValid
          StripeCustomer {stripeCustomerAccount = aid, stripeCustomerIdentifier = Just cid}
      pure (cid, scid)
  where
    getOrCreate email = do
      res1 <- getStripeCustomer email
      case res1 of
        GotStripeError e -> invalidArgs [e]
        MoreThanOneStripeCustomer ->
          invalidArgs ["Got more than one stripe customer, that doesn't make sense."]
        GotStripeCustomer sc -> pure sc
        NoSuchStripeCustomer -> do
          res2 <- createStripeCustomer token email
          case res2 of
            FailedToCreate e -> invalidArgs [e]
            CreatedStripeCustomer sc -> pure sc

getStripeCustomer :: EmailAddress -> Handler GetStripeCustomerResult
getStripeCustomer email = do
  res <-
    runStripe
      (Stripe.getCustomers
         { Stripe.queryParams =
             ("email", emailAddressByteString email) : Stripe.queryParams Stripe.getCustomers
         } :: Stripe.StripeRequest Stripe.GetCustomers)
  liftIO $ print res
  pure $
    case res of
      Left stripeError -> GotStripeError $ T.pack $ show stripeError
      Right cs ->
        case Stripe.list cs of
          [c] -> GotStripeCustomer $ Stripe.customerId c
          [] -> NoSuchStripeCustomer
          _ -> MoreThanOneStripeCustomer

data GetStripeCustomerResult
  = GotStripeCustomer Stripe.CustomerId
  | GotStripeError Text
  | MoreThanOneStripeCustomer
  | NoSuchStripeCustomer
  deriving (Show, Eq)

createStripeCustomer :: Stripe.TokenId -> EmailAddress -> Handler CreateStripeCustomerResult
createStripeCustomer token email = do
  res <- runStripe $ Stripe.createCustomer -&- (Stripe.Email $ emailAddressText email) -&- token
  liftIO $ print res
  pure $
    case res of
      Left stripeError -> FailedToCreate $ T.pack $ show stripeError
      Right c -> CreatedStripeCustomer $ Stripe.customerId c

data CreateStripeCustomerResult
  = FailedToCreate Text
  | CreatedStripeCustomer Stripe.CustomerId
  deriving (Show, Eq)
