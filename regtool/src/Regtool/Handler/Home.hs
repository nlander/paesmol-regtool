{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Home where

import Import

import Yesod.Auth
import Yesod.Core

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

getHomeR :: Handler Html
getHomeR = do
  aid <- maybeAuthId
  case aid of
    Nothing ->
      withNavBar $ do
        setTitle "PAESMOL Registration"
        $(widgetFile "home")
    Just id_ -> do
      b <- accIsAdmin id_
      if b
        then redirect $ AdminR PanelR
        else redirect OverviewR
