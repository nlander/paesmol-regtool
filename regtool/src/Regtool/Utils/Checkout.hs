module Regtool.Utils.Checkout
  ( getStripeCustomerOf
  , getPaymentsOf
  , getTransportEnrollmentsOf
  , getTransportPaymentPlansOf
  , getTransportPaymentsOf
  , getOccasionalTransportPaymentsOf
  ) where

import Import

import Database.Persist.Sqlite

import Regtool.Data

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

getStripeCustomerOf ::
     MonadIO m
  => AccountId
  -> ReaderT SqlBackend m (Maybe (Entity StripeCustomer))
getStripeCustomerOf aid = getBy $ UniqueAccountCustomer aid

getPaymentsOf ::
     MonadIO m
  => StripeCustomerId
  -> ReaderT SqlBackend m [Entity StripePayment]
getPaymentsOf scid =
  selectList [StripePaymentCustomer ==. scid] [Asc StripePaymentTimestamp]

getTransportEnrollmentsOf ::
     MonadIO m
  => [TransportSignupId]
  -> ReaderT SqlBackend m [Entity TransportEnrollment]
getTransportEnrollmentsOf sids =
  selectList [TransportEnrollmentSignup <-. sids] [Asc TransportEnrollmentId]

getTransportPaymentPlansOf ::
     MonadIO m
  => [TransportEnrollment]
  -> ReaderT SqlBackend m [Entity TransportPaymentPlan]
getTransportPaymentPlansOf teids =
  selectList
    [TransportPaymentPlanId <-. map transportEnrollmentPaymentPlan teids]
    [Asc TransportPaymentPlanId]

getTransportPaymentsOf ::
     MonadIO m
  => [TransportPaymentPlanId]
  -> ReaderT SqlBackend m [Entity TransportPayment]
getTransportPaymentsOf pptid =
  selectList [TransportPaymentPaymentPlan <-. pptid] [Asc TransportPaymentId]

getOccasionalTransportPaymentsOf ::
     MonadIO m
  => [OccasionalTransportSignupId]
  -> ReaderT SqlBackend m [Entity OccasionalTransportPayment]
getOccasionalTransportPaymentsOf otsid =
  selectList
    [OccasionalTransportPaymentSignup <-. otsid]
    [Asc OccasionalTransportPaymentId]
