{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.Types where

import Import

import Text.Hamlet

import Regtool.Core.Foundation
import Regtool.Data

import Regtool.Looper.Types

type Renderer = Render (Route Regtool)

data EmailCreationDirective a =
  EmailCreationDirective
    { emailIdSelector :: EntityField a (Maybe EmailId)
    , emailTimestampSelector :: EntityField a TimeStamp
    , makeEmail :: a -> Renderer -> Looper Email
    }

data EmailStatus
  = EmailUnsent
  | EmailSending
  | EmailSent Text -- ^ Id of the mail in the AWS API
  | EmailErrored Text -- ^ Error message describing what went wrong
  deriving (Show, Eq)

unsent :: Text
unsent = "Unsent"

emailSending :: Text
emailSending = "Sending"

emailSent :: Text
emailSent = "Sent"

emailErrored :: Text
emailErrored = "Error"
