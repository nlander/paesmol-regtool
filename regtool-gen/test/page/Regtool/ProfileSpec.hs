module Regtool.ProfileSpec
  ( spec
  ) where

import TestImport

import Yesod.Test

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.TestUtils

spec :: RegtoolSpec
spec = do
  ydescribe "ProfileR" $ do
    needsLoginSpec GET ProfileR
    yit "returns a 200 for a verified account" $
      withAnyVerifiedAccountLogin_ $ do
        get ChildrenR
        statusIs 200
  ydescribe "ProfileSetUsernameR" $ needsLoginSpec POST ProfileSetUserNameR
