{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

module Regtool.Data.Gen.Nationality where

import Import

import Regtool.Data

instance GenUnchecked StandardNationality

instance GenValid StandardNationality
