{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Costs.AbstractSpec
  ( spec
  ) where

import TestImport

import qualified Data.Map as M

import Database.Persist
import Database.Persist.Sql

import Regtool.Data
import Regtool.Gen ()

import Regtool.Checkout.Utils

import Regtool.Utils.Costs.Abstract
import Regtool.Utils.Costs.Input

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

{-# ANN module ("HLint: ignore Use lambda-case" :: String) #-}

spec :: Spec
spec = do
  genValidSpec @YearlyFeeStatus
  -- shrinkValidSpecWithLimit @YearlyFeeStatus 100
  -- doesNotShrinkToItselfSpec @YearlyFeeStatus
  genValidSpec @OccasionalTransportFeeStatus
  -- shrinkValidSpecWithLimit @OccasionalTransportFeeStatus 100
  -- doesNotShrinkToItselfSpec @OccasionalTransportFeeStatus
  genValidSpec @TransportFeeStatus
  -- shrinkValidSpecWithLimit @TransportFeeStatus 100
  -- doesNotShrinkToItselfSpec @TransportFeeStatus
  genValidSpec @DaycareFeeStatus
  -- shrinkValidSpecWithLimit @DaycareFeeStatus 100
  -- doesNotShrinkToItselfSpec @DaycareFeeStatus
  genValidSpec @DaycareActivityFeeStatus
  -- shrinkValidSpecWithLimit @DaycareActivityFeeStatus 100
  -- doesNotShrinkToItselfSpec @DaycareActivityFeeStatus
  genValidSpec @OccasionalDaycareFeeStatus
  -- shrinkValidSpecWithLimit @OccasionalDaycareFeeStatus 100
  -- doesNotShrinkToItselfSpec @OccasionalDaycareFeeStatus
  genValidSpec @AbstractSemestrelyCosts
  -- shrinkValidSpecWithLimit @AbstractSemestrelyCosts 100
  -- doesNotShrinkToItselfSpec @AbstractSemestrelyCosts
  genValidSpec @AbstractYearlyCosts
  -- shrinkValidSpecWithLimit @AbstractYearlyCosts 100
  -- doesNotShrinkToItselfSpec @AbstractYearlyCosts
  genValidSpec @AbstractCosts
  -- shrinkValidSpecWithLimit @AbstractCosts 100
  -- doesNotShrinkToItselfSpec @AbstractCosts
  describe "makeAbstractSemestrelyCostsFor" $
    it "produces valid abstract semestrely costs" $
    producesValidsOnValids2 makeAbstractSemestrelyCostsFor
  describe "makeAbstractSemestrelyCosts" $
    it "produces valid abstract semestrely costs" $
    producesValidsOnValids2 makeAbstractSemestrelyCosts
  describe "makeAbstractYearlyBusCostsForChild" $ do
    it "produces valid bus costs for one child" $
      producesValidsOnValids2 makeAbstractYearlyBusCostsForChild
    it
      "says that the fee is not required or the child is not signed up if any parent works for the EU" $
      forAll
        (genValid `suchThat`
         (any ((/= OtherCompany) . parentCompany . entityVal) . costsInputParents)) $ \ci ->
        forAllValid $ \c ->
          let r = makeAbstractYearlyBusCostsForChild ci c
           in case r of
                TransportFeeNotSignedUp -> pure ()
                TransportFeeNotRequired _ -> pure ()
                _ -> expectationFailure $ unlines ["Was this instead:", ppShow r]
  describe "makeAbstractYearlyBusCosts" $ do
    it
      "ensures that if any fee is not required, then all kids either aren't signed up, or don't require a fee either" $
      forAllValid $ \ci -> do
        let m = makeAbstractYearlyBusCosts ci
            isNotRequired (TransportFeeNotRequired _) = True
            isNotRequired _ = False
            notRequiredOrNotSignedUp TransportFeeNotSignedUp = True
            notRequiredOrNotSignedUp (TransportFeeNotRequired _) = True
            notRequiredOrNotSignedUp _ = False
            notRequireds = M.filter isNotRequired m
            requireds = M.filter (not . notRequiredOrNotSignedUp) m
        when (not (M.null notRequireds) && not (M.null requireds)) $
          expectationFailure $
          unlines
            [ ppShow $ costsInputParents ci
            , "notRequireds:"
            , ppShow $ M.mapKeys entityKey notRequireds
            , "requireds:"
            , ppShow $ M.mapKeys entityKey requireds
            ]
    it "produces valid bus costs" $ producesValidsOnValids makeAbstractYearlyBusCosts
  describe "makeAbstractOccasionalTransportCosts" $
    it "produces valid fee stati" $ producesValidsOnValids3 makeAbstractOccasionalTransportCosts
  describe "makeAbstractOccasionalDaycareCosts" $
    it "produces valid fee stati" $
    forAllValid $ producesValidsOnValids3 . makeAbstractOccasionalDaycareCosts
  describe "makeAbstractYearlyFeeCosts" $ do
    it "produces valid yearly fee costs" $
      forAllValid $ producesValidsOnValids3 . makeAbstractYearlyFeeCosts
    it "says that the yearly fee is not required if there are no transport signups or payments" $
      forAllValid $ \sy ->
        makeAbstractYearlyFeeCosts sy [] [] M.empty `shouldBe` YearlyFeeStatusNotRequired
    it "says that the yearly fee is not required if there are no payments and no child is signed up" $
      forAllValid $ \sy ->
        forAll (genListOf (pure TransportFeeNotSignedUp)) $ \tfss ->
          makeAbstractYearlyFeeCosts sy [] tfss M.empty `shouldBe` YearlyFeeStatusNotRequired
  describe "makeAbstractYearlyCosts" $
    it "produces valid abstract yearly costs" $ producesValidsOnValids2 makeAbstractYearlyCosts
  describe "makeAbstractCosts" $
    it "produces valid abstract costs" $ producesValidsOnValids makeAbstractCosts
  describe "unit" $ do
    it "works for this example where no payments have started yet" $
      forAllValid $ \(schoolYear, customerId) ->
        forAllValid $ \(c1, c2, c3, c4, c5) ->
          forAllValid $ \(ts1, ts2, ts3, ts4, ts5) -> do
            let aid = toSqlKey 0
                ec1 = Entity (toSqlKey 1) c1
                ec2 = Entity (toSqlKey 2) c2
                ec3 = Entity (toSqlKey 3) c3
                ec4 = Entity (toSqlKey 4) c4
                ec5 = Entity (toSqlKey 5) c5
                ets1 =
                  Entity (toSqlKey 1) $
                  ts1
                    { transportSignupChild = toSqlKey 1
                    , transportSignupInstalments = 1
                    , transportSignupSchoolYear = schoolYear
                    }
                ets2 =
                  Entity (toSqlKey 2) $
                  ts2
                    { transportSignupChild = toSqlKey 2
                    , transportSignupInstalments = 2
                    , transportSignupSchoolYear = schoolYear
                    }
                ets3 =
                  Entity (toSqlKey 3) $
                  ts3
                    { transportSignupChild = toSqlKey 3
                    , transportSignupInstalments = 3
                    , transportSignupSchoolYear = schoolYear
                    }
                ets4 =
                  Entity (toSqlKey 4) $
                  ts4
                    { transportSignupChild = toSqlKey 4
                    , transportSignupInstalments = 6
                    , transportSignupSchoolYear = schoolYear
                    }
                ets5 =
                  Entity (toSqlKey 5) $
                  ts5
                    { transportSignupChild = toSqlKey 5
                    , transportSignupInstalments = 12
                    , transportSignupSchoolYear = schoolYear
                    }
                ci =
                  CostsInput
                    { costsInputAccount = aid
                    , costsInputParents = []
                    , costsInputChildren = [ec1, ec2, ec3, ec4, ec5]
                    , costsInputTransportSignups = [ets1, ets2, ets3, ets4, ets5]
                    , costsInputStripeCustomer =
                        Just $
                        Entity
                          (toSqlKey 1)
                          StripeCustomer
                            {stripeCustomerAccount = aid, stripeCustomerIdentifier = customerId}
                    , costsInputStripePayments = []
                    , costsInputCheckouts = []
                    , costsInputYearlyFeePayments = []
                    , costsInputTransportPaymentPlans = []
                    , costsInputTransportPayments = []
                    , costsInputTransportEnrollments = []
                    , costsInputOccasionalTransportSignups = []
                    , costsInputOccasionalTransportPayments = []
                    , costsInputDaycareSemestres = []
                    , costsInputDaycareTimeslots = []
                    , costsInputDaycareSignups = []
                    , costsInputDaycarePayments = []
                    , costsInputOccasionalDaycareSignups = []
                    , costsInputOccasionalDaycarePayments = []
                    , costsInputDaycareActivities = []
                    , costsInputDaycareActivitySignups = []
                    , costsInputDaycareActivityPayments = []
                    , costsInputDiscounts = []
                    }
            shouldBeValid ci -- Doesn't make sense otherwise.
            let ac = makeAbstractCosts ci
            shouldBeValid ac
            (sy, AbstractYearlyCosts {..}) <- expectSingleAbstractCost ac
            sy `shouldBe` schoolYear
            aycYearlyFee `shouldBe` YearlyFeeStatusNotPaid
            let expected =
                  M.fromList
                    [ (ec1, TransportFeePaymentNotStartedButNecessary ets1)
                    , (ec2, TransportFeePaymentNotStartedButNecessary ets2)
                    , (ec3, TransportFeePaymentNotStartedButNecessary ets3)
                    , (ec4, TransportFeePaymentNotStartedButNecessary ets4)
                    , (ec5, TransportFeePaymentNotStartedButNecessary ets5)
                    ]
            yearlyAbstractBusFeesShouldBe aycYearlyBus expected
    it "works for this example where one payment has been made with only one child." $
      forAllValid $ \(schoolYear, customerId, chargeId, timestamp) ->
        forAllValid $ \c1 ->
          forAllValid $ \ts1 -> do
            let aid = toSqlKey 0
                ec1 = Entity (toSqlKey 1) c1
                ets1 =
                  Entity (toSqlKey 1) $
                  ts1
                    { transportSignupChild = toSqlKey 1
                    , transportSignupInstalments = 1
                    , transportSignupSchoolYear = schoolYear
                    }
                yfa = amountInCents 50
                tppa = amountInCents 432
                ta = amountInCents 432
                a = sumAmount [yfa, ta]
                esp =
                  Entity
                    (toSqlKey 1)
                    StripePayment
                      { stripePaymentCustomer = toSqlKey 1
                      , stripePaymentAmount = a
                      , stripePaymentCharge = chargeId
                      , stripePaymentTimestamp = timestamp
                      }
                ec =
                  Entity
                    (toSqlKey 1)
                    Checkout
                      { checkoutAccount = aid
                      , checkoutAmount = a
                      , checkoutPayment = Just $ toSqlKey 1
                      , checkoutTimestamp = timestamp
                      }
                eyfp =
                  Entity
                    (toSqlKey 1)
                    YearlyFeePayment
                      { yearlyFeePaymentAccount = aid
                      , yearlyFeePaymentSchoolyear = schoolYear
                      , yearlyFeePaymentCheckout = toSqlKey 1
                      , yearlyFeePaymentAmount = yfa
                      }
                etpp1 =
                  Entity (toSqlKey 1) TransportPaymentPlan {transportPaymentPlanTotalAmount = tppa}
                etp1 =
                  Entity
                    (toSqlKey 1)
                    TransportPayment
                      { transportPaymentPaymentPlan = toSqlKey 1
                      , transportPaymentCheckout = toSqlKey 1
                      , transportPaymentAmount = ta
                      }
                ete1 =
                  Entity
                    (toSqlKey 1)
                    TransportEnrollment
                      { transportEnrollmentSignup = toSqlKey 1
                      , transportEnrollmentPaymentPlan = toSqlKey 1
                      }
                ci =
                  CostsInput
                    { costsInputAccount = aid
                    , costsInputParents = []
                    , costsInputChildren = [ec1]
                    , costsInputTransportSignups = [ets1]
                    , costsInputStripeCustomer =
                        Just $
                        Entity
                          (toSqlKey 1)
                          StripeCustomer
                            {stripeCustomerAccount = aid, stripeCustomerIdentifier = customerId}
                    , costsInputStripePayments = [esp]
                    , costsInputCheckouts = [ec]
                    , costsInputYearlyFeePayments = [eyfp]
                    , costsInputTransportPaymentPlans = [etpp1]
                    , costsInputTransportPayments = [etp1]
                    , costsInputTransportEnrollments = [ete1]
                    , costsInputOccasionalTransportSignups = []
                    , costsInputOccasionalTransportPayments = []
                    , costsInputDaycareSemestres = []
                    , costsInputDaycareTimeslots = []
                    , costsInputDaycareSignups = []
                    , costsInputDaycarePayments = []
                    , costsInputOccasionalDaycareSignups = []
                    , costsInputOccasionalDaycarePayments = []
                    , costsInputDaycareActivities = []
                    , costsInputDaycareActivitySignups = []
                    , costsInputDaycareActivityPayments = []
                    , costsInputDiscounts = []
                    }
            shouldBeValid ci -- Doesn't make sense otherwise.
            let ac = makeAbstractCosts ci
            shouldBeValid ac
            (sy, AbstractYearlyCosts {..}) <- expectSingleAbstractCost ac
            sy `shouldBe` schoolYear
            aycYearlyFee `shouldBe` YearlyFeeStatusPaid eyfp
            let expected =
                  M.fromList [(ec1, TransportFeePaymentEntirelyDone ets1 ete1 etpp1 [etp1] [ec])]
            yearlyAbstractBusFeesShouldBe aycYearlyBus expected
    it "works for this example where one payment has been made." $
      forAllValid $ \(schoolYear, customerId, chargeId, timestamp) ->
        forAllValid $ \(c1, c2, c3, c4, c5) ->
          forAllValid $ \(ts1, ts2, ts3, ts4, ts5) -> do
            let aid = toSqlKey 0
                ec1 = Entity (toSqlKey 1) c1
                ec2 = Entity (toSqlKey 2) c2
                ec3 = Entity (toSqlKey 3) c3
                ec4 = Entity (toSqlKey 4) c4
                ec5 = Entity (toSqlKey 5) c5
                ets1 =
                  Entity (toSqlKey 1) $
                  ts1
                    { transportSignupChild = toSqlKey 1
                    , transportSignupInstalments = 1
                    , transportSignupSchoolYear = schoolYear
                    }
                ets2 =
                  Entity (toSqlKey 2) $
                  ts2
                    { transportSignupChild = toSqlKey 2
                    , transportSignupInstalments = 2
                    , transportSignupSchoolYear = schoolYear
                    }
                ets3 =
                  Entity (toSqlKey 3) $
                  ts3
                    { transportSignupChild = toSqlKey 3
                    , transportSignupInstalments = 3
                    , transportSignupSchoolYear = schoolYear
                    }
                ets4 =
                  Entity (toSqlKey 4) $
                  ts4
                    { transportSignupChild = toSqlKey 4
                    , transportSignupInstalments = 6
                    , transportSignupSchoolYear = schoolYear
                    }
                ets5 =
                  Entity (toSqlKey 5) $
                  ts5
                    { transportSignupChild = toSqlKey 5
                    , transportSignupInstalments = 12
                    , transportSignupSchoolYear = schoolYear
                    }
                yfa = amountInCents 50
                tppas = map amountInCents [432, 216, 108, 54, 54]
                tas = map amountInCents [432, 108, 36, 9, 5]
                a = sumAmount $ yfa : tas
                esp =
                  Entity
                    (toSqlKey 1)
                    StripePayment
                      { stripePaymentCustomer = toSqlKey 1
                      , stripePaymentAmount = a
                      , stripePaymentCharge = chargeId
                      , stripePaymentTimestamp = timestamp
                      }
                ec =
                  Entity
                    (toSqlKey 1)
                    Checkout
                      { checkoutAccount = aid
                      , checkoutAmount = a
                      , checkoutPayment = Just $ toSqlKey 1
                      , checkoutTimestamp = timestamp
                      }
                eyfp =
                  Entity
                    (toSqlKey 1)
                    YearlyFeePayment
                      { yearlyFeePaymentAccount = aid
                      , yearlyFeePaymentSchoolyear = schoolYear
                      , yearlyFeePaymentCheckout = toSqlKey 1
                      , yearlyFeePaymentAmount = yfa
                      }
                [etpp1, etpp2, etpp3, etpp4, etpp5] =
                  map
                    (\(i, a_) ->
                       Entity
                         (toSqlKey i)
                         TransportPaymentPlan {transportPaymentPlanTotalAmount = a_}) $
                  zip [1 ..] tppas
                [etp1, etp2, etp3, etp4, etp5] =
                  map
                    (\(i, a_) ->
                       Entity
                         (toSqlKey i)
                         TransportPayment
                           { transportPaymentPaymentPlan = toSqlKey i
                           , transportPaymentCheckout = toSqlKey 1
                           , transportPaymentAmount = a_
                           }) $
                  zip [1 ..] tas
                [ete1, ete2, ete3, ete4, ete5] =
                  map
                    (\i ->
                       Entity
                         (toSqlKey i)
                         TransportEnrollment
                           { transportEnrollmentSignup = toSqlKey i
                           , transportEnrollmentPaymentPlan = toSqlKey i
                           })
                    [1 .. 5]
                ci =
                  CostsInput
                    { costsInputAccount = aid
                    , costsInputParents = []
                    , costsInputChildren = [ec1, ec2, ec3, ec4, ec5]
                    , costsInputTransportSignups = [ets1, ets2, ets3, ets4, ets5]
                    , costsInputStripeCustomer =
                        Just $
                        Entity
                          (toSqlKey 1)
                          StripeCustomer
                            {stripeCustomerAccount = aid, stripeCustomerIdentifier = customerId}
                    , costsInputStripePayments = [esp]
                    , costsInputCheckouts = [ec]
                    , costsInputYearlyFeePayments = [eyfp]
                    , costsInputTransportPaymentPlans = [etpp1, etpp2, etpp3, etpp4, etpp5]
                    , costsInputTransportPayments = [etp1, etp2, etp3, etp4, etp5]
                    , costsInputTransportEnrollments = [ete1, ete2, ete3, ete4, ete5]
                    , costsInputOccasionalTransportSignups = []
                    , costsInputOccasionalTransportPayments = []
                    , costsInputDaycareSemestres = []
                    , costsInputDaycareTimeslots = []
                    , costsInputDaycareSignups = []
                    , costsInputDaycarePayments = []
                    , costsInputOccasionalDaycareSignups = []
                    , costsInputOccasionalDaycarePayments = []
                    , costsInputDaycareActivities = []
                    , costsInputDaycareActivitySignups = []
                    , costsInputDaycareActivityPayments = []
                    , costsInputDiscounts = []
                    }
            shouldBeValid ci -- Doesn't make sense otherwise.
            let ac = makeAbstractCosts ci
            shouldBeValid ac
            (sy, AbstractYearlyCosts {..}) <- expectSingleAbstractCost ac
            sy `shouldBe` schoolYear
            aycYearlyFee `shouldBe` YearlyFeeStatusPaid eyfp
            let expected =
                  M.fromList
                    [ (ec1, TransportFeePaymentEntirelyDone ets1 ete1 etpp1 [etp1] [ec])
                    , (ec2, TransportFeePaymentStartedButNotDone ets2 ete2 etpp2 [etp2] [ec])
                    , (ec3, TransportFeePaymentStartedButNotDone ets3 ete3 etpp3 [etp3] [ec])
                    , (ec4, TransportFeePaymentStartedButNotDone ets4 ete4 etpp4 [etp4] [ec])
                    , (ec5, TransportFeePaymentStartedButNotDone ets5 ete5 etpp5 [etp5] [ec])
                    ]
            yearlyAbstractBusFeesShouldBe aycYearlyBus expected
