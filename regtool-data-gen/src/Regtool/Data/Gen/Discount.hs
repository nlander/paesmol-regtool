{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Discount where

import Import

import Regtool.Data

import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()

instance GenValid Discount where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
