{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Checkout.Page
  ( getCheckoutChoosePage
  ) where

import Import

import Database.Persist.Sql

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

import Regtool.Handler.Admin.Config

import Regtool.Utils.Consequences
import Regtool.Utils.Costs.Abstract
import Regtool.Utils.Costs.Calculate
import Regtool.Utils.Costs.Choice
import Regtool.Utils.Costs.Input

import Regtool.Handler.Checkout.Form

getCheckoutChoosePage :: Maybe (FormResult a) -> Handler Html
getCheckoutChoosePage mfr = do
  ci <- getCostsInput
  env <- regtoolEnvironment <$> getYesod
  let ac = makeAbstractCosts ci
  conf <- confCostsConfig <$> getConfiguration
  let cc = calculateCosts conf ac
  let choices = deriveChoices cc
  let emptyChoices = choices == mempty
  (formWidget, formEncType) <- generateFormPost $ chooseForm choices
  now <- liftIO getCurrentTime
  let proposedConsequences -- Only for debugging
       = paymentConsequences (costsInputAccount ci) (toSqlKey (-1)) now choices
  let errors =
        let e :: (Show a, Validity a) => String -> a -> Maybe (String, String, String)
            e n v =
              case prettyValidate v of
                Left err -> Just (n, ppShow v, err)
                Right _ -> Nothing
         in catMaybes
              [ e "Costs Input" ci
              , e "Abstract Costs" ac
              , e "Environment" env
              , e "Costs Config" conf
              , e "Calculated Costs" cc
              , e "Choices" choices
              , e "Proposed payment consequences" proposedConsequences
              ]
  maybe withNavBar withFormResultNavBar mfr $
    case errors of
      [] -> mconcat [$(widgetFile "checkout-choose"), $(widgetFile "checkout-choose-debug")]
      _ -> $(widgetFile "checkout-choose-error")
