{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Daycare.ActivityEnrollments
  ( getAdminDaycareActivityEnrollmentsSemestreR
  , getAdminDaycareActivityEnrollmentsActivityR
  ) where

import Import

import Yesod.Core

import qualified Database.Esqueleto as E
import Database.Esqueleto ((^.))

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation
import Regtool.Utils.Time

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

getAdminDaycareActivityEnrollmentsSemestreR :: DaycareSemestreId -> Handler Html
getAdminDaycareActivityEnrollmentsSemestreR dcsid = do
  dcs <- runDB $ get404 dcsid
  tss <- runDB $ selectList [DaycareActivitySemestre ==. dcsid] []
  let thd (_, _, z) = z
  trips <- sortOn (Down . thd) <$> runDB (getSignupsForSemestre dcsid)
  let signups tsid = filter (\(Entity tsid' _, _, _) -> tsid == tsid') trips
      paid = filter (\(_, _, m) -> isJust m) . signups
  withNavBar $(widgetFile "admin/daycare/activity-enrollments/semestre")

getSignupsForSemestre ::
     MonadIO m
  => DaycareSemestreId
  -> ReaderT SqlBackend m [( Entity DaycareActivity
                           , Entity Child
                           , Maybe (Entity DaycareActivityPayment))]
getSignupsForSemestre dcsid =
  E.select $
  E.from $ \((daycareActivity `E.InnerJoin` daycareActivitySignup `E.InnerJoin` child) `E.LeftOuterJoin` daycareActivityPayment) -> do
    E.on
      (E.just (daycareActivitySignup ^. DaycareActivitySignupId) E.==. daycareActivityPayment E.?.
       DaycareActivityPaymentSignup)
    E.on (daycareActivitySignup ^. DaycareActivitySignupChild E.==. child ^. ChildId)
    E.on
      (daycareActivity ^. DaycareActivityId E.==. daycareActivitySignup ^.
       DaycareActivitySignupActivity)
    E.where_ (daycareActivity ^. DaycareActivitySemestre E.==. E.val dcsid)
    return (daycareActivity, child, daycareActivityPayment)

getAdminDaycareActivityEnrollmentsActivityR ::
     DaycareSemestreId -> DaycareActivityId -> Handler Html
getAdminDaycareActivityEnrollmentsActivityR dcsid dctsid = do
  dcs <- runDB $ get404 dcsid
  dcts <- runDB $ get404 dctsid
  tups <- sortOn (Down . snd) <$> runDB (getSignupsForActivity dctsid)
  let paid =
        filter (\(_, m) -> isJust m) tups
  withNavBar $(widgetFile "admin/daycare/activity-enrollments/activity")

getSignupsForActivity ::
     MonadIO m
  => DaycareActivityId
  -> ReaderT SqlBackend m [(Entity Child, Maybe (Entity DaycareActivityPayment))]
getSignupsForActivity tsid =
  E.select $
  E.from $ \((daycareActivitySignup `E.InnerJoin` child) `E.LeftOuterJoin` daycareActivityPayment) -> do
    E.on
      (E.just (daycareActivitySignup ^. DaycareActivitySignupId) E.==. daycareActivityPayment E.?.
       DaycareActivityPaymentSignup)
    E.on (daycareActivitySignup ^. DaycareActivitySignupChild E.==. child ^. ChildId)
    E.where_ (daycareActivitySignup ^. DaycareActivitySignupActivity E.==. E.val tsid)
    return (child, daycareActivityPayment)

mClass :: Maybe a -> Text
mClass Nothing = "danger"
mClass (Just _) = "success"
