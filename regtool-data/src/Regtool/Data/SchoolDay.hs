{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.SchoolDay where

import Import

import Data.Aeson
import Data.Time
import Data.Time.Calendar.WeekDate

import Database.Persist
import Database.Persist.Sql

data SchoolDay
  = Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity SchoolDay

instance FromJSON SchoolDay

instance ToJSON SchoolDay

instance ToField SchoolDay where
  toField = toField . schoolDayText

schoolDayText :: SchoolDay -> Text
schoolDayText r =
  case r of
    Monday -> "Monday"
    Tuesday -> "Tuesday"
    Wednesday -> "Wednesday"
    Thursday -> "Thursday"
    Friday -> "Friday"

parseSchoolDay :: Text -> Maybe SchoolDay
parseSchoolDay t =
  case t of
    "Monday" -> Just Monday
    "Tuesday" -> Just Tuesday
    "Wednesday" -> Just Wednesday
    "Thursday" -> Just Thursday
    "Friday" -> Just Friday
    _ -> Nothing

instance PersistField SchoolDay where
  toPersistValue = toPersistValue . schoolDayText
  fromPersistValue pv = do
    t <- fromPersistValue pv
    case parseSchoolDay t of
      Nothing -> Left "Could not decode schoolDay."
      Just c -> pure c

instance PersistFieldSql SchoolDay where
  sqlType Proxy = sqlType (Proxy :: Proxy String)

dayMatchesSchoolDay :: Day -> SchoolDay -> Bool
dayMatchesSchoolDay d sd =
  let (_, _, i) = toWeekDate d
   in case (i, sd) of
        (1, Monday) -> True
        (2, Tuesday) -> True
        (3, Wednesday) -> True
        (4, Thursday) -> True
        (5, Friday) -> True
        _ -> False
