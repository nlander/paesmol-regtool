PAESMOL Registration: Payment Received

Payment on #{defaultFormatUTCTime $ checkoutTimestamp $ checkoutOverviewCheckout coo}

Total: #{fmtAmount $ checkoutAmount $ checkoutOverviewCheckout coo}

Please use a browser to view the full version of this message.
