{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Checkout.Pay
  ( postCheckoutPayR
  ) where

import Import

import Database.Persist
import Database.Persist.Sqlite

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Utils.Consequences
import Regtool.Utils.Stripe

import Regtool.Utils.Costs.Cache
import Regtool.Utils.Costs.Invoice

import Regtool.Handler.Checkout.Page
import Regtool.Handler.Checkout.Stripe

postCheckoutPayR :: Handler Html
postCheckoutPayR = do
  ea@(Entity aid _) <- requireAuth
  mChoices <- getChoicesCache aid
  case mChoices of
    Nothing -> do
      setMessage "No choices found"
      redirect CheckoutChooseR
    Just chosenChoices -> do
      paymentNotNecessaryResult <- runInputPostResult paymentNotNecessaryForm
      let total = invoiceTotal $ deriveInvoice chosenChoices
      stripeResult <- runInputPostResult stripeInputForm
      now <- liftIO getCurrentTime
      case stripeResult of
        FormSuccess StripeForm {..} -> do
          deleteChoicesCache aid
          unless (total > zeroAmount) $ invalidArgs ["Internal error: should not have had to pay"]
          spid <-
            stripeCreateCharge stripeFormToken $ invoiceTotal $ deriveInvoice chosenChoices
          runDB $ do
            Entity cid _ <- makeCheckout aid chosenChoices $ Just spid
            let pcs = paymentConsequences aid cid now chosenChoices
            realisePaymentConsequences pcs
            sendPaymentReceivedEmail ea cid
          setMessage "The payment went through successfully!"
          redirect PaymentsR
        _ ->
          case paymentNotNecessaryResult of
            FormSuccess PaymentNotNecessary -> do
              unless (total <= zeroAmount) $ invalidArgs ["Internal error: should have had to pay"]
              runDB $ do
                Entity cid _ <- makeCheckout aid chosenChoices Nothing
                let pcs = paymentConsequences aid cid now chosenChoices
                realisePaymentConsequences pcs
              setMessage "Success!"
              redirect PaymentsR
            fr -> getCheckoutChoosePage $ Just fr

data PaymentNotNecessary =
  PaymentNotNecessary
  deriving (Show, Eq, Generic)

instance Validity PaymentNotNecessary

paymentNotNecessaryForm :: FormInput Handler PaymentNotNecessary
paymentNotNecessaryForm =
  PaymentNotNecessary <$ iopt (hiddenField :: Field Handler ()) "payment-not-necessary"

sendPaymentReceivedEmail :: Entity Account -> CheckoutId -> SqlPersistT Handler ()
sendPaymentReceivedEmail (Entity aid Account {..}) cid = do
  now <- liftIO getCurrentTime
  insert_
    PaymentReceivedEmail
      { paymentReceivedEmailTo = accountEmailAddress
      , paymentReceivedEmailAccount = aid
      , paymentReceivedEmailCheckout = cid
      , paymentReceivedEmailTimestamp = now
      , paymentReceivedEmailEmail = Nothing
      }
