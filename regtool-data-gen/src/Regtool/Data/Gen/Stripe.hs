{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Stripe where

import Import

import Regtool.Data ()

import Regtool.Data.Gen.Utils ()

import Web.Stripe.Charge as Stripe

instance GenValid Stripe.TokenId where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
