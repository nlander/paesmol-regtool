{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Signup.Transport.Utils
  ( SignupChildTransport(..)
  , signupChildTransport
  , verifyChildSignedUpForTransport
  ) where

import TestImport

import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types

import Yesod.Test

import Regtool.Data

import Regtool.Application ()
import Regtool.Core.Foundation hiding (get, runDB)
import Regtool.Handler.Signup.Transport
import Regtool.TestUtils

signupChildTransport :: SignupChildTransport -> RegtoolExample ()
signupChildTransport SignupChildTransport {..} = do
  get SignupTransportR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl SignupTransportR
    addToken
    addPostParam "f1" $ sqlKeyText signupChildTransportChild
    addPostParam "f2" $ sqlKeyText signupChildTransportBusStop
    addPostParam "f3" "1" -- The first option in the drop-down
    addPostParam "f4" "2" -- The second option in the drop-down
  shouldRedirectTo SignupTransportR

verifyChildSignedUpForTransport :: SignupChildTransport -> RegtoolExample ()
verifyChildSignedUpForTransport SignupChildTransport {..} = do
  signups <-
    runDB $
    selectList
      [ TransportSignupChild ==. signupChildTransportChild
      , TransportSignupBusStop ==. signupChildTransportBusStop
      , TransportSignupSchoolYear ==. signupChildTransportSchoolYear
      ]
      []
  (Entity _ TransportSignup {..}) <-
    case signups of
      [] -> failure "Should have found one signup, got an empty list instead."
      [tsu] -> pure tsu
      tsus ->
        failure $
        unlines $
        "Should have found one transport signup, got these instead: " :
        map show tsus
  liftIO $ do
    transportSignupChild `shouldBe` signupChildTransportChild
    transportSignupBusStop `shouldBe` signupChildTransportBusStop
    transportSignupSchoolYear `shouldBe` signupChildTransportSchoolYear
