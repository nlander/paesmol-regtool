{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Payments
  ( getPaymentsR
  ) where

import Import

import qualified Data.Map as M

import Yesod.Core

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation

import Regtool.Utils.Costs.Checkout
import Regtool.Utils.Costs.Input

getPaymentsR :: Handler Html
getPaymentsR = do
  ci <- getCostsInput
  let co = deriveCheckoutsOverview ci
  withNavBar $(widgetFile "payments")
