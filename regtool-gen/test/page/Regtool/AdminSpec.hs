{-# LANGUAGE FlexibleContexts #-}

module Regtool.AdminSpec
  ( spec
  ) where

import TestImport

import Network.HTTP.Types

import Yesod.Auth
import Yesod.Core (RedirectUrl)
import Yesod.Test

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils

spec :: RegtoolSpec
spec = do
  ydescribe "AdminR" $ adminSpec GET $ AdminR PanelR
  ydescribe "AdminRawDataR" $ adminSpec GET $ AdminR AdminRawDataR

adminSpec :: RedirectUrl Regtool url => StdMethod -> url -> RegtoolSpec
adminSpec method route = do
  yit "returns a 303 if a user is not logged-in" $ t 303
  yit "returns a 403 for a non-admin" $ withAnyVerifiedAccount_ $ t 403
  where
    t s = do
      let isPost = renderStdMethod method == methodPost
      when isPost $ get $ AuthR LoginR -- To make sure there's a CSRF cookie available
      request $ do
        setMethod $ renderStdMethod method
        when isPost addTokenFromCookie
        setUrl route
      statusIs s
