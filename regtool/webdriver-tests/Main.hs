module Main
    ( main
    ) where

import TestImport

import Control.Exception

import Test.Hspec

import Regtool.Webdriver.CompleteSpec
import Regtool.Webdriver.Constants
import Regtool.Webdriver.Utils

main :: IO ()
main = do
    testDBFile <- resolveFile' testDbPath
    ignoringAbsence $ removeFile testDBFile
    mv <- newEmptyMVar
    startRegtool mv
    hspec (completeSpec mv) `finally` stopRegtoolIfRunning mv
