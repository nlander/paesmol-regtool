{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -O0 #-}

-- At least until the infinite compile-time is fixed.
module Regtool.Handler.Parents
  ( RegisterParent(..)
  , getParentsR
  , getParentR
  , getRegisterParentR
  , postRegisterParentR
  , postDeregisterParentR
  ) where

import Import hiding (isParentOf)

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Utils.Parents

import Regtool.Component.NavigationBar

getParentsR :: Handler Html
getParentsR = do
  accid <- requireAuthId
  parents <- runDB $ selectList [ParentAccount ==. accid] []
  token <- genToken
  withNavBar $(widgetFile "parents")

data RegisterParent =
  RegisterParent
    { registerParentFirstName :: Text
    , registerParentLastName :: Text
    , registerParentAddressLine1 :: Text
    , registerParentAddressLine2 :: Maybe Text
    , registerParentPostalCode :: Text
    , registerParentCity :: Text
    , registerParentCountry :: Country
    , registerParentPhoneNumber1 :: Text
    , registerParentPhoneNumber2 :: Maybe Text
    , registerParentEmail1 :: EmailAddress
    , registerParentEmail2 :: Maybe EmailAddress
    , registerParentCompany :: Company
    , registerParentRelationShip :: RelationShip
    , registerParentComments :: Maybe Textarea
    , registerParentId :: Maybe ParentId
    }
  deriving (Show, Eq, Generic)

instance Validity RegisterParent

registerParentForm :: Maybe (Entity Parent) -> AForm Handler RegisterParent
registerParentForm mce =
  RegisterParent <$> areq textField (bfs ("First Name" :: Text)) (dv parentFirstName) <*>
  areq textField (bfs ("Last Name" :: Text)) (dv parentLastName) <*>
  areq textField (bfs ("Address line 1 (Street and number)" :: Text)) (dv parentAddressLine1) <*>
  aopt textField (bfs ("Address line 2" :: Text)) (dv parentAddressLine2) <*>
  areq textField (bfs ("Zip code" :: Text)) (dv parentPostalCode) <*>
  areq textField (bfs ("City" :: Text)) (dv parentCity) <*>
  standardOrRawForm (bfs ("Country" :: Text)) (dv parentCountry) <*>
  areq textField (bfs ("Phone number (primary)" :: Text)) (dv parentPhoneNumber1) <*>
  aopt textField (bfs ("Phone number (secondary)" :: Text)) (dv parentPhoneNumber2) <*>
  areq emailAddressField (bfs ("Email address (primary)" :: Text)) (dv parentEmail1) <*>
  aopt emailAddressField (bfs ("Email address (secondary)" :: Text)) (dv parentEmail2) <*>
  areq
    (selectField $ optionsPairs $ map (\c -> (companyText c, c)) [minBound .. maxBound])
    (bfs ("Company" :: Text))
    (dv parentCompany `mplus` Just OtherCompany) <*>
  standardOrRawForm (bfs ("Relation to the children" :: Text)) (dv parentRelationShip) <*>
  aopt textareaField (bfs ("Extra Comments" :: Text)) (dv parentComments) <*>
  aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (Parent -> a) -> Maybe a
    dv func = (func . entityVal) <$> mce

getRegisterParentR :: Handler Html
getRegisterParentR = do
  void requireAuthId
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm $ registerParentForm Nothing
  withNavBar $(widgetFile "register-parent")

postRegisterParentR :: Handler Html
postRegisterParentR = do
  aid <- requireAuthId
  ((result, formWidget), formEnctype) <-
    runFormPost $ renderCustomForm BootstrapBasicForm $ registerParentForm Nothing
  case result of
    FormSuccess RegisterParent {..} -> do
      now <- liftIO getCurrentTime
      let parent_ =
            Parent
              { parentAccount = aid
              , parentFirstName = registerParentFirstName
              , parentLastName = registerParentLastName
              , parentAddressLine1 = registerParentAddressLine1
              , parentAddressLine2 = registerParentAddressLine2
              , parentPostalCode = registerParentPostalCode
              , parentCity = registerParentCity
              , parentCountry = registerParentCountry
              , parentPhoneNumber1 = registerParentPhoneNumber1
              , parentPhoneNumber2 = registerParentPhoneNumber2
              , parentEmail1 = registerParentEmail1
              , parentEmail2 = registerParentEmail2
              , parentCompany = registerParentCompany
              , parentRelationShip = registerParentRelationShip
              , parentComments = registerParentComments
              , parentRegistrationTime = now
              }
      case registerParentId of
        Just i ->
          withOwnParent i $ do
            runDB $ replaceValid i parent_
            redirect ParentsR
        Nothing -> do
          runDB $ insertValid_ parent_
          redirect ParentsR
    fr -> withFormResultNavBar fr $(widgetFile "register-parent")

getParentR :: ParentId -> Handler Html
getParentR cid =
  withOwnParent cid $ do
    parent_ <- runDB $ get404 cid
    (formWidget, formEnctype) <-
      generateFormPost $
      renderCustomForm BootstrapBasicForm $ registerParentForm $ Just (Entity cid parent_)
    withNavBar $(widgetFile "parent")

isParentOf :: ParentId -> AccountId -> Handler Bool
isParentOf pid aid = ((== Just True) . fmap ((== aid) . parentAccount)) <$> runDB (get pid)

withOwnParent :: ParentId -> Handler Html -> Handler Html
withOwnParent cid func = do
  aid <- requireAuthId
  isOwnParent <- isParentOf cid aid
  isAdminAcc <- (== Just True) <$> isAdmin
  if isOwnParent || isAdminAcc
    then func
    else permissionDenied "Not your parent."

postDeregisterParentR :: ParentId -> Handler Html
postDeregisterParentR pid =
  withOwnParent pid $ do
    canDeleteParent pid >>= deleteOrError
    redirect ParentsR
