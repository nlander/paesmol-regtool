{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Regtool.Data.StandardOrRaw where

import Import

import Data.Aeson

import Database.Persist
import Database.Persist.Sql

data StandardOrRaw a
  = Standard a
  | Raw Text
  deriving (Show, Eq, Ord, Generic)

instance Validity a => Validity (StandardOrRaw a)

instance FromJSON a => FromJSON (StandardOrRaw a)

instance ToJSON a => ToJSON (StandardOrRaw a)

standard :: StandardOrRaw a -> Maybe a
standard (Standard a) = Just a
standard (Raw _) = Nothing

raw :: StandardOrRaw a -> Maybe Text
raw (Standard _) = Nothing
raw (Raw t) = Just t

standardOrRawText :: (a -> Text) -> StandardOrRaw a -> Text
standardOrRawText func (Standard a) = func a
standardOrRawText _ (Raw t) = t

parseStandardOrRaw :: (Text -> Maybe a) -> Text -> StandardOrRaw a
parseStandardOrRaw pFunc t =
  case pFunc t of
    Just a -> Standard a
    Nothing -> Raw t

splitStandardOrRaw :: StandardOrRaw a -> (Maybe a, Maybe Text)
splitStandardOrRaw (Standard a) = (Just a, Nothing)
splitStandardOrRaw (Raw t) = (Nothing, Just t)

completeStandardOrRaw ::
     (Text -> Maybe a) -> Maybe a -> Maybe Text -> Maybe (StandardOrRaw a)
completeStandardOrRaw parseFunc mv mt =
  case (mv, mt) of
    (Just r, _) -> Just (Standard r)
    (Nothing, Nothing) -> Nothing
    (Nothing, Just "") -> Nothing
    (Nothing, Just t) -> (Standard <$> parseFunc t) `mplus` Just (Raw t)

class StandardField a where
  standardToText :: a -> Text
  standardFromText :: Text -> Maybe a

standardOrOtherIndex ::
     forall a. (Enum a, Bounded a)
  => StandardOrRaw a
  -> Int
standardOrOtherIndex (Standard a) = fromEnum a + 1
standardOrOtherIndex (Raw _) =
  standardOrOtherIndex (Standard (maxBound :: a)) + 1

instance StandardField a => PersistField (StandardOrRaw a) where
  toPersistValue (Standard v) = toPersistValue $ standardToText v
  toPersistValue (Raw t) = toPersistValue t
  fromPersistValue pv =
    case pv of
      PersistText t -> Right $ parseStandardOrRaw standardFromText t
      _ -> Left "Invalid persist value type for parsing RelationShip"

instance StandardField a => PersistFieldSql (StandardOrRaw a) where
  sqlType Proxy = SqlString
