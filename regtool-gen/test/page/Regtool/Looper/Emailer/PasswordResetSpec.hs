{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.PasswordResetSpec
  ( spec
  ) where

import TestImport

import Data.Time

import Yesod.Test

import Regtool.Data

import Regtool.Core.Foundation.Regtool
import Regtool.Looper.Emailer.PasswordReset

import Regtool.Looper.Emailer.TestUtils
import Regtool.TestUtils

spec :: RegtoolSpec
spec =
  ydescribe "password reset email" $
  passwordResetGoldenTest
    "test_resources/email/password-reset/1"
    PasswordResetEmail
      { passwordResetEmailTo = unsafeEmailAddress "example" "domain.com"
      , passwordResetEmailKey = "key"
      , passwordResetEmailLink = "link"
      , passwordResetEmailTimestamp = UTCTime (fromGregorian 1970 1 1) 0
      , passwordResetEmailEmail = Nothing
      }

passwordResetGoldenTest :: String -> PasswordResetEmail -> YesodSpec Regtool
passwordResetGoldenTest path =
  emailGoldenTests
    path
    passwordResetEmailTextContent
    passwordResetEmailHtmlContent
