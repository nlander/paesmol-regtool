{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Core.Form
  ( amountField
  , emailAddressField
  , standardOrRawForm
  , renderCustomForm
  , module Yesod.Form
  , module Yesod.Form.Bootstrap3
  ) where

import Import

import qualified Data.Text as T

import Yesod.Core
import Yesod.Form
import Yesod.Form.Bootstrap3

import Regtool.Data

amountField ::
     forall m. Monad m
  => RenderMessage (HandlerSite m) FormMessage =>
       Field m Amount
amountField =
  modFieldFunc $
  checkMMap
    (pure . (Right :: b -> Either FormMessage b) . unsafeAmount)
    unsafeUnAmount
    doubleField
  where
    modFieldFunc :: Field m Amount -> Field m Amount
    modFieldFunc f = f {fieldView = modFunc (fieldView f)}
      where
        modFunc :: FieldViewFunc m Amount -> FieldViewFunc m Amount
        modFunc func id_ name attributes errOrRes req =
          [whamlet|
                    <div .input-group>
                      <span .input-group-addon>€
                      ^{func id_ name attributes errOrRes req}
                |]

emailAddressField ::
     Monad m
  => RenderMessage (HandlerSite m) FormMessage =>
       Field m EmailAddress
emailAddressField =
  checkMMap
    (pure . left T.pack . emailValidateFromText)
    emailAddressText
    emailField

standardOrRawForm ::
     (Eq a, Enum a, Bounded a, StandardField a, RenderMessage site FormMessage)
  => FieldSettings site
  -> Maybe (StandardOrRaw a)
  -> AForm (HandlerT site IO) (StandardOrRaw a)
standardOrRawForm fs mv =
  AForm $ \env1 menv2 ints -> do
    let AForm af1 = f1
    let AForm af2 = f2
    (fr1, dlist1, ints1, enctype1) <- af1 env1 menv2 ints
    (fr2, dlist2, ints2, enctype2) <- af2 env1 menv2 ints1
    let fr3 =
          case (fr1, fr2) of
            (FormFailure ts1, FormFailure ts2) -> FormFailure $ ts1 ++ ts2
            (FormMissing, _) -> FormMissing
            (_, FormMissing) -> FormMissing
            (FormFailure ts, _) -> FormFailure ts
            (_, FormFailure ts) -> FormFailure ts
            (FormSuccess a, FormSuccess b) ->
              case completeStandardOrRaw standardFromText a b of
                Nothing ->
                  FormFailure
                    [ "You must fill in the 'Other' field when selecting the 'Other' option."
                    ]
                Just r -> FormSuccess r
    pure (fr3, dlist1 . dlist2, ints2, enctype1 <> enctype2)
  where
    dvs = splitStandardOrRaw <$> mv
    fs1 = fs {fsId = (<> "a") <$> fsId fs, fsName = (<> "a") <$> fsName fs}
    f1 =
      areq
        (selectFieldList $
         map
           (\mv_ ->
              ( case mv_ of
                  Just v -> standardToText v
                  Nothing -> "Other"
              , mv_))
           (map Just [minBound .. maxBound] ++ [Nothing]))
        fs1
        (fst <$> dvs)
    fs2 =
      fs
        { fsLabel = "Other"
        , fsTooltip = Just "Fill this in when selecting 'Other'"
        , fsId = (<> "b") <$> fsId fs
        , fsName = (<> "b") <$> fsName fs
        }
    f2 = aopt textField fs2 (snd <$> dvs)

renderCustomForm :: Monad m => BootstrapFormLayout -> FormRender m a
renderCustomForm formLayout aform fragment = do
  (res, views') <- aFormToForm aform
  let views = views' []
      has (Just _) = True
      has Nothing = False
      widget =
        [whamlet|
            $newline never
            #{fragment}
            $forall view <- views
              <div .form-group :fvRequired view:.required :not $ fvRequired view:.optional :has $ fvErrors view:.has-error>
                $case formLayout
                  $of BootstrapBasicForm
                    $if fvId view /= bootstrapSubmitId
                      <label for=#{fvId view}>
                        #{fvLabel view}
                        $if fvRequired view
                           &emsp; (*)
                    ^{fvInput view}
                    ^{helpWidget view}
                  $of BootstrapInlineForm
                    $if fvId view /= bootstrapSubmitId
                      <label .sr-only for=#{fvId view}>#{fvLabel view}
                    ^{fvInput view}
                    ^{helpWidget view}
                  $of BootstrapHorizontalForm labelOffset labelSize inputOffset inputSize
                    $if fvId view /= bootstrapSubmitId
                      <label .control-label .#{toOffset labelOffset} .#{toColumn labelSize} for=#{fvId view}>#{fvLabel view}
                      <div .#{toOffset inputOffset} .#{toColumn inputSize}>
                        ^{fvInput view}
                        ^{helpWidget view}
                    $else
                      <div .#{toOffset (addGO inputOffset (addGO labelOffset labelSize))} .#{toColumn inputSize}>
                        ^{fvInput view}
                        ^{helpWidget view}
                |]
  return (res, widget)

helpWidget :: FieldView site -> WidgetT site IO ()
helpWidget view =
  [whamlet|
    $maybe tt <- fvTooltip view
      <span .help-block>#{tt}
    $maybe err <- fvErrors view
      <span .help-block .error-block>#{err}
|]

toColumn :: BootstrapGridOptions -> String
toColumn (ColXs 0) = ""
toColumn (ColSm 0) = ""
toColumn (ColMd 0) = ""
toColumn (ColLg 0) = ""
toColumn (ColXs columns) = "col-xs-" ++ show columns
toColumn (ColSm columns) = "col-sm-" ++ show columns
toColumn (ColMd columns) = "col-md-" ++ show columns
toColumn (ColLg columns) = "col-lg-" ++ show columns

toOffset :: BootstrapGridOptions -> String
toOffset (ColXs 0) = ""
toOffset (ColSm 0) = ""
toOffset (ColMd 0) = ""
toOffset (ColLg 0) = ""
toOffset (ColXs columns) = "col-xs-offset-" ++ show columns
toOffset (ColSm columns) = "col-sm-offset-" ++ show columns
toOffset (ColMd columns) = "col-md-offset-" ++ show columns
toOffset (ColLg columns) = "col-lg-offset-" ++ show columns

addGO :: BootstrapGridOptions -> BootstrapGridOptions -> BootstrapGridOptions
addGO (ColXs a) (ColXs b) = ColXs (a + b)
addGO (ColSm a) (ColSm b) = ColSm (a + b)
addGO (ColMd a) (ColMd b) = ColMd (a + b)
addGO (ColLg a) (ColLg b) = ColLg (a + b)
addGO a b
  | a > b = addGO b a
addGO (ColXs a) other = addGO (ColSm a) other
addGO (ColSm a) other = addGO (ColMd a) other
addGO (ColMd a) other = addGO (ColLg a) other
addGO (ColLg _) _ = error "Yesod.Form.Bootstrap.addGO: never here"

-- | A royal hack.  Magic id used to identify whether a field
-- should have no label.  A valid HTML4 id which is probably not
-- going to clash with any other id should someone use
-- 'bootstrapSubmit' outside 'renderCustomForm'.
bootstrapSubmitId :: Text
bootstrapSubmitId = "b:ootstrap___unique__:::::::::::::::::submit-id"
