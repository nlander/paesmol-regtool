{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Utils.Signup.Daycare
  ( timeslotsTups
  , signupTrips
  , getOurDaycareSignups
  , timeslotDescription
  , occasionalTimeslotDescription
  ) where

import Import

import qualified Data.Text as T

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Utils.Time

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

timeslotsTups ::
     [Entity DaycareSemestre]
  -> [Entity DaycareTimeslot]
  -> [(Entity DaycareSemestre, Entity DaycareTimeslot)]
timeslotsTups semestres timeslots =
  flip mapMaybe timeslots $ \ts -> do
    s <- find ((== daycareTimeslotSemestre (entityVal ts)) . entityKey) semestres
    pure (s, ts)

signupTrips ::
     [Entity DaycareSignup]
  -> [Entity Child]
  -> [Entity DaycareSemestre]
  -> [Entity DaycareTimeslot]
  -> [(DaycareSignupId, Entity Child, Entity DaycareSemestre, Entity DaycareTimeslot)]
signupTrips signups children semestres timeslots =
  flip mapMaybe signups $ \(Entity eid DaycareSignup {..}) -> do
    c <- find ((== daycareSignupChild) . entityKey) children
    ts <- find ((== daycareSignupTimeslot) . entityKey) timeslots
    s <- find ((== daycareTimeslotSemestre (entityVal ts)) . entityKey) semestres
    pure (eid, c, s, ts)

getOurDaycareSignups :: MonadIO m => [ChildId] -> ReaderT SqlBackend m [Entity DaycareSignup]
getOurDaycareSignups cids = selectList [DaycareSignupChild <-. cids] []

timeslotDescription :: DaycareSemestre -> DaycareTimeslot -> Text
timeslotDescription DaycareSemestre {..} DaycareTimeslot {..} =
  T.unwords
    [ schoolYearText daycareSemestreYear
    , semestreText daycareSemestreSemestre
    , schoolDayText daycareTimeslotSchoolDay
    , T.pack $ formatTimestamp daycareTimeslotStart
    , "-"
    , T.pack $ formatTimestamp daycareTimeslotEnd
    , T.pack $ "(" <> fmtAmount daycareTimeslotFee <> " €)"
    ]

occasionalTimeslotDescription :: DaycareSemestre -> DaycareTimeslot -> Text
occasionalTimeslotDescription DaycareSemestre {..} DaycareTimeslot {..} =
  T.unwords
    [ schoolYearText daycareSemestreYear
    , semestreText daycareSemestreSemestre
    , schoolDayText daycareTimeslotSchoolDay
    , T.pack $ formatTimestamp daycareTimeslotStart
    , "-"
    , T.pack $ formatTimestamp daycareTimeslotEnd
    , T.pack $ "(" <> fmtAmount daycareTimeslotOccasionalFee <> " €)"
    ]
