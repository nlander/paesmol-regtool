{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Doctors.Utils
  ( RegisterDoctor(..)
  , registerDoctor
  , genSimpleRegisterDoctor
  , verifyDoctorRegistered
  , getDoctorByRegister
  ) where

import TestImport

import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types

import Yesod.Form
import Yesod.Test

import Regtool.Data

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Handler.Doctors
import Regtool.TestUtils

registerDoctor :: RegisterDoctor -> RegtoolExample ()
registerDoctor RegisterDoctor {..} = do
  get RegisterDoctorR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl RegisterDoctorR
    addToken
    addPostParam "f1" registerDoctorFirstName
    addPostParam "f2" registerDoctorLastName
    addPostParam "f3" registerDoctorAddressLine1
    addPostParamMaybe "f4" registerDoctorAddressLine2
    addPostParam "f5" registerDoctorPostalCode
    addPostParam "f6" registerDoctorCity
    addPostParamStandardOrOther "f7" "f8" registerDoctorCountry
    addPostParam "f9" registerDoctorPhoneNumber1
    addPostParamMaybe "f10" registerDoctorPhoneNumber2
    addPostParam "f11" $ emailAddressText registerDoctorEmail1
    addPostParamMaybe "f12" $ emailAddressText <$> registerDoctorEmail2
    addPostParamMaybe "f13" $ unTextarea <$> registerDoctorComments
  shouldRedirectTo DoctorsR

verifyDoctorRegistered :: AccountId -> RegisterDoctor -> RegtoolExample ()
verifyDoctorRegistered aid RegisterDoctor {..} = do
  doctors <- runDB $ selectList [DoctorAccount ==. aid] []
  (Entity _ p) <-
    case doctors of
      [] -> failure "Should have found one doctor, got empty list instead."
      [p] -> pure p
      ps -> failure $ unlines $ "Should have found one doctor, got these instead: " : map show ps
  liftIO $ do
    doctorAccount p `shouldBe` aid
    doctorFirstName p `shouldBe` registerDoctorFirstName
    doctorLastName p `shouldBe` registerDoctorLastName
    doctorAddressLine1 p `shouldBe` registerDoctorAddressLine1
    doctorAddressLine2 p `shouldBe` registerDoctorAddressLine2
    doctorPostalCode p `shouldBe` registerDoctorPostalCode
    doctorCity p `shouldBe` registerDoctorCity
    doctorCountry p `shouldBe` registerDoctorCountry
    doctorPhoneNumber1 p `shouldBe` registerDoctorPhoneNumber1
    doctorPhoneNumber2 p `shouldBe` registerDoctorPhoneNumber2
    doctorEmail1 p `shouldBe` registerDoctorEmail1
    doctorEmail2 p `shouldBe` registerDoctorEmail2
    doctorComments p `shouldBe` registerDoctorComments

getDoctorByRegister :: AccountId -> RegisterDoctor -> RegtoolExample (Entity Doctor)
getDoctorByRegister aid RegisterDoctor {..} = do
  md <-
    runDB $
    selectFirst
      [ DoctorAccount ==. aid
      , DoctorFirstName ==. registerDoctorFirstName
      , DoctorLastName ==. registerDoctorLastName
      ]
      []
  case md of
    Nothing -> failure "Should have had a doctor"
    Just ed -> pure ed
