{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Daycare.Enrollments
  ( getAdminDaycareEnrollmentsR
  , getAdminDaycareEnrollmentsSemestreR
  , getAdminDaycareEnrollmentsTimeslotR
  , getAdminDaycareEnrollmentsDayR
  ) where

import Import

import Yesod.Core

import qualified Database.Esqueleto as E
import Database.Esqueleto ((^.))

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation
import Regtool.Utils.Signup.Daycare
import Regtool.Utils.Time

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

getAdminDaycareEnrollmentsR :: Handler Html
getAdminDaycareEnrollmentsR = do
  semestres <- runDB $ selectList [] [Asc DaycareSemestreYear, Asc DaycareSemestreSemestre]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  let tups = timeslotsTups semestres timeslots
  (formWidget, formEnctype) <-
    generateFormGet' $ renderCustomForm BootstrapBasicForm $ searchDaycareEnrollmentsForm tups
  withNavBar $(widgetFile "admin/daycare/enrollments")

getAdminDaycareEnrollmentsSemestreR :: DaycareSemestreId -> Handler Html
getAdminDaycareEnrollmentsSemestreR dcsid = do
  dcs <- runDB $ get404 dcsid
  tss <- runDB $ selectList [DaycareTimeslotSemestre ==. dcsid] []
  let thd (_, _, z) = z
  trips <- sortOn (Down . thd) <$> runDB (getSignupsForSemestre dcsid)
  let signups tsid = filter (\(Entity tsid' _, _, _) -> tsid == tsid') trips
      paid = filter (\(_, _, m) -> isJust m) . signups
  withNavBar $(widgetFile "admin/daycare/enrollments/semestre")

getSignupsForSemestre ::
     MonadIO m
  => DaycareSemestreId
  -> ReaderT SqlBackend m [(Entity DaycareTimeslot, Entity Child, Maybe (Entity DaycarePayment))]
getSignupsForSemestre dcsid =
  E.select $
  E.from $ \((daycareTimeslot `E.InnerJoin` daycareSignup `E.InnerJoin` child) `E.LeftOuterJoin` daycarePayment) -> do
    E.on (E.just (daycareSignup ^. DaycareSignupId) E.==. daycarePayment E.?. DaycarePaymentSignup)
    E.on (daycareSignup ^. DaycareSignupChild E.==. child ^. ChildId)
    E.on (daycareTimeslot ^. DaycareTimeslotId E.==. daycareSignup ^. DaycareSignupTimeslot)
    E.where_ (daycareTimeslot ^. DaycareTimeslotSemestre E.==. E.val dcsid)
    return (daycareTimeslot, child, daycarePayment)

getAdminDaycareEnrollmentsTimeslotR :: DaycareSemestreId -> DaycareTimeslotId -> Handler Html
getAdminDaycareEnrollmentsTimeslotR dcsid dctsid = do
  dcs <- runDB $ get404 dcsid
  dcts <- runDB $ get404 dctsid
  trips <- sortOn (Down . snd) <$> runDB (getSignupsForTimeslot dctsid)
  withNavBar $(widgetFile "admin/daycare/enrollments/timeslot")

getSignupsForTimeslot ::
     MonadIO m
  => DaycareTimeslotId
  -> ReaderT SqlBackend m [(Entity Child, Maybe (Entity DaycarePayment))]
getSignupsForTimeslot tsid =
  E.select $
  E.from $ \((daycareSignup `E.InnerJoin` child) `E.LeftOuterJoin` daycarePayment) -> do
    E.on (E.just (daycareSignup ^. DaycareSignupId) E.==. daycarePayment E.?. DaycarePaymentSignup)
    E.on (daycareSignup ^. DaycareSignupChild E.==. child ^. ChildId)
    E.where_ (daycareSignup ^. DaycareSignupTimeslot E.==. E.val tsid)
    return (child, daycarePayment)

mClass :: Maybe a -> Text
mClass Nothing = "danger"
mClass (Just _) = "success"

data SearchDaycareEnrollments =
  SearchDaycareEnrollments
    { searchDaycareEnrollmentsTimeslot :: (DaycareSemestreId, DaycareTimeslotId)
    , searchDaycareEnrollmentsDay :: Day
    }
  deriving (Show, Eq, Generic)

searchDaycareEnrollmentsForm ::
     [(Entity DaycareSemestre, Entity DaycareTimeslot)] -> AForm Handler SearchDaycareEnrollments
searchDaycareEnrollmentsForm tups =
  SearchDaycareEnrollments <$>
  areq
    (selectFieldList $
     map (\(Entity sid s, (Entity tsid ts)) -> (timeslotDescription s ts, (sid, tsid))) tups)
    (bfs ("Timeslot" :: Text))
    Nothing <*>
  areq dayField (bfs ("Day" :: Text)) Nothing

getAdminDaycareEnrollmentsDayR :: Handler Html
getAdminDaycareEnrollmentsDayR = do
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  let tups = timeslotsTups semestres timeslots
  ((result, _), _) <-
    runFormGet $ renderCustomForm BootstrapBasicForm $ searchDaycareEnrollmentsForm tups
  case result of
    FormSuccess SearchDaycareEnrollments {..} -> do
      let (semestreId, timeslotId) = searchDaycareEnrollmentsTimeslot
      daycareTimeslot <- runDB $ get404 timeslotId
      daycareSemestre <- runDB $ get404 semestreId
      trips <- runDB $ getSignupsForTimeslot timeslotId
      occasionalOccupancy <-
        runDB $ getOccasionalSignupsForTimeslot searchDaycareEnrollmentsDay timeslotId
      withNavBar $(widgetFile "admin/daycare/enrollments/day")
    _ -> redirect $ AdminR AdminDaycareEnrollmentsR

getOccasionalSignupsForTimeslot ::
     MonadIO m
  => Day
  -> DaycareTimeslotId
  -> ReaderT SqlBackend m [(Entity Child, Maybe (Entity OccasionalDaycarePayment))]
getOccasionalSignupsForTimeslot day tsid =
  E.select $
  E.from $ \((occasionalDaycareSignup `E.InnerJoin` child) `E.LeftOuterJoin` occasionalDaycarePayment) -> do
    E.on
      (E.just (occasionalDaycareSignup ^. OccasionalDaycareSignupId) E.==. occasionalDaycarePayment E.?.
       OccasionalDaycarePaymentSignup)
    E.on (occasionalDaycareSignup ^. OccasionalDaycareSignupChild E.==. child ^. ChildId)
    E.where_ (occasionalDaycareSignup ^. OccasionalDaycareSignupTimeslot E.==. E.val tsid)
    E.where_ (occasionalDaycareSignup ^. OccasionalDaycareSignupDay E.==. E.val day)
    return (child, occasionalDaycarePayment)
