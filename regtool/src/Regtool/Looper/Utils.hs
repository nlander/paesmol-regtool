module Regtool.Looper.Utils
  ( looperDB
  ) where

import Import

import Database.Persist.Sqlite (SqlPersistT, runSqlPool)

import Regtool.Looper.Types

looperDB :: SqlPersistT IO b -> Looper b
looperDB query = do
  pool <- asks looperConnectionPool
  liftIO $ runSqlPool query pool
