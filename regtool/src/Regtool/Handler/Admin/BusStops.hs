{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.BusStops where

import Import

import qualified Database.Persist as DB

import Yesod.Core
import Yesod.Form.Bootstrap3

import qualified Database.Esqueleto as E
import Database.Esqueleto ((^.))

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Form
import Regtool.Core.Foundation

getAdminBusStopsR :: Handler Html
getAdminBusStopsR = do
  busStops <- runDB $ selectList [] [Asc BusStopArchived, Asc BusStopId]
  token <- genToken
  withNavBar $(widgetFile "admin/busstops")

data RegisterBusStop =
  RegisterBusStop
    { registerBusStopCity :: Textarea
    , registerBusStopLocation :: Textarea
    , registerBusStopId :: Maybe BusStopId
    }
  deriving (Show, Eq)

registerBusStopForm :: Maybe (Entity BusStop) -> AForm Handler RegisterBusStop
registerBusStopForm mce =
  RegisterBusStop <$> areq textareaField (bfs ("City" :: Text)) (dv busStopCity) <*>
  areq textareaField (bfs ("Location" :: Text)) (dv busStopLocation) <*>
  aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (BusStop -> a) -> Maybe a
    dv func = (func . entityVal) <$> mce

getAdminRegisterBusStopR :: Handler Html
getAdminRegisterBusStopR = do
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $ registerBusStopForm Nothing
  withNavBar $(widgetFile "admin/register-busstop")

postAdminRegisterBusStopR :: Handler Html
postAdminRegisterBusStopR = do
  ((result, formWidget), formEnctype) <-
    runFormPost $
    renderCustomForm BootstrapBasicForm $ registerBusStopForm Nothing
  case result of
    FormSuccess RegisterBusStop {..} -> do
      now <- liftIO getCurrentTime
      let busStop_ =
            BusStop
              { busStopCity = registerBusStopCity
              , busStopLocation = registerBusStopLocation
              , busStopArchived = False
              , busStopCreationTime = now
              }
      case registerBusStopId of
        Just i -> runDB $ replaceValid i busStop_
        Nothing -> runDB $ insertValid_ busStop_
      redirect $ AdminR AdminBusStopsR
    _ -> withNavBar $(widgetFile "admin/register-busstop")

getAdminBusStopR :: BusStopId -> Handler Html
getAdminBusStopR bsid = do
  busStop <- runDB $ get404 bsid
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $
    registerBusStopForm $ Just $ Entity bsid busStop
  withNavBar $(widgetFile "admin/busstop")

postAdminBusStopArchiveR :: BusStopId -> Handler Html
postAdminBusStopArchiveR bsid = do
  runDB $ DB.update bsid [BusStopArchived DB.=. True]
  redirect $ AdminR AdminBusStopsR

postAdminBusStopUnarchiveR :: BusStopId -> Handler Html
postAdminBusStopUnarchiveR bsid = do
  runDB $ DB.update bsid [BusStopArchived DB.=. False]
  redirect $ AdminR AdminBusStopsR

getAdminBusStopOverviewR :: BusStopId -> Handler Html
getAdminBusStopOverviewR bsid = do
  busStop <- runDB $ get404 bsid
  enrolledChildren <- getChildrenEnrolledAtBusStop bsid
  withNavBar $(widgetFile "admin/busstop-overview")

getChildrenEnrolledAtBusStop :: BusStopId -> Handler [Entity Child]
getChildrenEnrolledAtBusStop bid =
  runDB $
  E.select $
  E.from $ \(child `E.InnerJoin` transportSignup) -> do
    E.on (child ^. ChildId E.==. transportSignup ^. TransportSignupChild)
    E.where_ $ transportSignup ^. TransportSignupBusStop E.==. E.val bid
    return child
