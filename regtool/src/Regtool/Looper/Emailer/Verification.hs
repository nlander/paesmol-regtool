{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.Verification
  ( verificationEmailsDirective
  , verificationEmailTextContent
  , verificationEmailHtmlContent
  ) where

import Import

import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Builder as LTB

import Text.Blaze.Html.Renderer.Text (renderHtml)
import Text.Hamlet
import Text.Shakespeare.Text

import Regtool.Core.Foundation
import Regtool.Data

import Regtool.Looper.Emailer.Types
import Regtool.Looper.Types

verificationEmailsDirective :: EmailCreationDirective VerificationEmail
verificationEmailsDirective =
  EmailCreationDirective
    { emailIdSelector = VerificationEmailEmail
    , emailTimestampSelector = VerificationEmailTimestamp
    , makeEmail = makeVerificationEmail
    }

makeVerificationEmail :: VerificationEmail -> Renderer -> Looper Email
makeVerificationEmail vere@VerificationEmail {..} render = do
  now <- liftIO getCurrentTime
  ea <- asks looperEmailAddress
  pure
    Email
      { emailTo = verificationEmailTo
      , emailFrom = ea
      , emailFromName = "Registration Tool Verification"
      , emailSubject = "Please verify your email address"
      , emailTextContent = verificationEmailTextContent vere render
      , emailHtmlContent = verificationEmailHtmlContent vere render
      , emailStatus = unsent
      , emailError = Nothing
      , emailSesId = Nothing
      , emailAddedTimestamp = now
      }

verificationEmailTextContent :: VerificationEmail -> Renderer -> Text
verificationEmailTextContent VerificationEmail {..} render =
  LT.toStrict $ LTB.toLazyText $ $(textFile "templates/auth/verification-email.txt") render

verificationEmailHtmlContent :: VerificationEmail -> Renderer -> Text
verificationEmailHtmlContent VerificationEmail {..} render =
  LT.toStrict $ renderHtml $ $(hamletFile "templates/auth/verification-email.hamlet") render
