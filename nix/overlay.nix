final:
  previous:
    with final.haskell.lib;
    {
      release-target = final.stdenv.mkDerivation {
        name = "regtool-release";
        buildInputs = [
          (final.regtool-static)
        ];
        nativeBuildInputs = [
          (final.haskellPackages.regtool)
          (final.haskellPackages.regtool-gen)
          (final.haskellPackages.regtool-data)
          (final.haskellPackages.regtool-data-gen)
        ];
        buildCommand = ''
          cp -r ${final.regtool-static} $out
        '';
      };
      regtool-static = justStaticExecutables (final.haskellPackages.regtool);
      haskellPackages = previous.haskellPackages.extend (self:
        super:
          let
            pathFor = name:
                  builtins.path {
                      inherit name;
                      path = ../. + "/${name}";
                      filter = path: type:
                        !(final.lib.hasPrefix "." (baseNameOf path));
                    };

            regtoolPkg = name:
              addBuildDepend (dontHaddock (failOnAllWarnings (disableLibraryProfiling (super.callCabal2nix name (pathFor name) {})))) final.haskellPackages.autoexporter;
          in with final.haskellPackages;
          final.lib.genAttrs [
            "regtool-data"
            "regtool-data-gen"
            "regtool-gen"
          ] regtoolPkg // {
            "regtool" = 
              let   
                bootstrap-css = builtins.fetchurl {
                  url = https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css;
                  sha256 = "sha256:0p33lbv1pkdc35xjydqxs61m5j0vyd2imqmiyhr19l9vr1n88ppp";
                };
                jquery-js = builtins.fetchurl {
                  url = https://code.jquery.com/jquery-3.2.1.min.js;
                  sha256 = "sha256:1pl2imaca804jkq29flhbkfga5qqk39rj6j1n179h5b0rj13h247";
                };
                bootstrap-js = builtins.fetchurl {
                  url = https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js;
                  sha256 = "sha256:1vw66ik7zxmc3gg0cfcrwkf01384v0yk1k2fsgdfhd66lxw495jk";
                };
                font-awesome-css = builtins.fetchurl {
                  url = https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css;
                  sha256 = "sha256:1gch64hq7xc9jqvs7npsil2hwsigdjnvf78v1vpgswq3rhjyp6kr";
                };
                font-awesome-webfont-ttf = builtins.fetchurl {
                  url = https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf;
                  sha256 = "sha256:1a6nq3nx9mpcjik80d9k16darafp8g04av3sbhpv03ws4czz6n5a";
                };
                font-awesome-webfont-woff = builtins.fetchurl {
                  url = https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff;
                  sha256 = "sha256:01sg8wrpb2la7qigmpbpb10rbnfhwag614rz3fs5q3s5npg5j35s";
                };
                font-awesome-webfont-woff2 = builtins.fetchurl {
                  url = https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2;
                  sha256 = "sha256:1zpk2gd37kmnqjivfxfncjm9g6d0qnfqf5ylyby1hz8y0jygrpia";
                };


              in overrideCabal (addBuildDepend (dontHaddock (failOnAllWarnings (disableLibraryProfiling (super.callCabal2nix "regtool" (pathFor "regtool") {})))) final.haskellPackages.autoexporter) (old: {
                  preConfigure = ''
                    ${old.preConfigure or ""}

                    mkdir -p static/
                    mkdir -p fonts/
                    cp "${bootstrap-css}" "static/bootstrap.css"
                    cp "${jquery-js}" "static/jquery.js"
                    cp "${bootstrap-js}" "static/bootstrap.js"
                    cp "${font-awesome-css}" "static/font-awesome.css"
                    cp "${font-awesome-webfont-ttf}" "fonts/fontawesome-webfont.ttf"
                    cp "${font-awesome-webfont-woff}" "fonts/fontawesome-webfont.woff"
                    cp "${font-awesome-webfont-woff2}" "fonts/fontawesome-webfont.woff2"
                  '';
              });

            "looper" = let
              repo = final.fetchFromGitHub {
                owner = "NorfairKing";
                repo = "looper";
                rev = "929a8ad6a99a84624767bd9d619cc5318c6bda56";
                sha256 = "07wc2as7p2pz08a9qfx2jm3kz1cvfg73d872il3zhyplbd6yhzbx";
              };
            in super.callCabal2nix "looper" repo {};
            "yesod-static-remote" = let
              repo = final.fetchFromGitHub {
                owner = "NorfairKing";
                repo = "yesod-static-remote";
                rev = "22c0a92c1d62f1b8d432003844ef0636a9131b08";
                sha256 = "sha256:1mz1fb421wccx7mbpn9qaj214w4sl4qali5rclx9fqp685jkfj05";
              };
            in super.callCabal2nix "yesod-static-remote" repo {};
          });
    }
