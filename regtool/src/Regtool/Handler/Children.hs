{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Handler.Children
  ( RegisterChild(..)
  , getChildrenR
  , getChildR
  , getRegisterChildR
  , postRegisterChildR
  , postDeregisterChildR
  ) where

import Import

import qualified Data.Text as T

import Yesod.Auth
import Yesod.Core
import Yesod.Form
import Yesod.Form.Bootstrap3

import Regtool.Core.Form
import Regtool.Core.Foundation

import Regtool.Data

import Regtool.Utils.Children

import Regtool.Component.NavigationBar

getChildrenR :: Handler Html
getChildrenR = do
  accid <- requireAuthId
  children <- runDB $ getChildrenOf accid
  token <- genToken
  withNavBar $(widgetFile "children")

data RegisterChild =
  RegisterChild
    { registerChildFirstName :: Text
    , registerChildLastName :: Text
    , registerChildBirthDate :: Day
    , registerChildSchoolLevel :: SchoolLevel
    , registerChildLanguageSection :: LanguageSectionId
    , registerChildNationality :: Nationality
    , registerChildAllergies :: Maybe Textarea
    , registerChildHealthInfo :: Maybe Textarea
    , registerChildMedication :: Maybe Textarea
    , registerChildComments :: Maybe Textarea
    , registerChildPickupAllowed :: Maybe Textarea
    , registerChildPickupDisallowed :: Maybe Textarea
    , registerChildPicturePermission :: Bool
    , registerChildDelijnBus :: Bool
    , registerChildDoctor :: DoctorId
    , registerChildId :: Maybe ChildId
    }
  deriving (Show, Eq)

registerChildForm ::
     Maybe (Entity Child)
  -> [Entity Doctor]
  -> [Entity LanguageSection]
  -> AForm Handler RegisterChild
registerChildForm mce docOpts lsOpts =
  RegisterChild <$>
  areq textField (bfs ("First Name" :: Text)) (dv childFirstName) <*>
  areq textField (bfs ("Last Name" :: Text)) (dv childLastName) <*>
  areq dayField (bfs ("Date of Birth" :: Text)) (dv childBirthdate) <*>
  areq
    (selectFieldList $ map (\l -> (schoolLevelText l, l)) [minBound .. maxBound])
    (bfs ("Level and Grade" :: Text))
    (dv childLevel) <*>
  areq
    (selectFieldList $
     map (\(Entity lsid l) -> (languageSectionName l, lsid)) lsOpts)
    (bfs ("Language Section" :: Text))
    (dv childLanguageSection) <*>
  standardOrRawForm (bfs ("Nationality" :: Text)) (dv childNationality) <*>
  aopt textareaField (bfs ("Allergy Information" :: Text)) (dv childAllergies) <*>
  aopt
    textareaField
    (bfs ("Other Health Information" :: Text))
    (dv childHealthInfo) <*>
  aopt textareaField (bfs ("Medication" :: Text)) (dv childMedication) <*>
  aopt textareaField (bfs ("Extra Comments" :: Text)) (dv childComments) <*>
  aopt
    textareaField
    (bfs ("People who are allowed to pick up this child" :: Text))
    (dv childPickupAllowed) <*>
  aopt
    textareaField
    (bfs ("People who are not allowed to pick up this child" :: Text))
    (dv childPickupDisallowed) <*>
  areq
    checkBoxField
    (bfs ("Permission to use pictures (without names)" :: Text))
    (dv childPicturePermission) <*>
  areq
    checkBoxField
    (bfs ("Needs to be brought to the bus" :: Text))
    (dv childDelijnBus) <*>
  areq
    (selectFieldList $
     map
       (\(Entity did d) ->
          (T.unwords [doctorFirstName d, doctorLastName d], did))
       docOpts)
    (bfs ("Doctor" :: Text))
    (dv childDoctor) <*>
  aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (Child -> a) -> Maybe a
    dv func = (func . entityVal) <$> mce

makeRegisterChildForm ::
     Maybe (Entity Child) -> Handler (AForm Handler RegisterChild)
makeRegisterChildForm mcid = do
  aid <- requireAuthId
  ds <- runDB $ selectList [DoctorAccount ==. aid] []
  lss <- runDB $ selectList [] [Asc LanguageSectionId]
  pure $ registerChildForm mcid ds lss

getRegisterChildR :: Handler Html
getRegisterChildR = do
  void requireAuthId
  childForm <- makeRegisterChildForm Nothing
  (formWidget, formEnctype) <-
    generateFormPost $ renderCustomForm BootstrapBasicForm childForm
  withNavBar $(widgetFile "register-child")

postRegisterChildR :: Handler Html
postRegisterChildR = do
  aid <- requireAuthId
  childForm <- makeRegisterChildForm Nothing
  ((result, formWidget), formEnctype) <-
    runFormPost $ renderCustomForm BootstrapBasicForm childForm
  case result of
    FormSuccess RegisterChild {..} -> do
      now <- liftIO getCurrentTime
      let child =
            Child
              { childFirstName = registerChildFirstName
              , childLastName = registerChildLastName
              , childBirthdate = registerChildBirthDate
              , childLevel = registerChildSchoolLevel
              , childLanguageSection = registerChildLanguageSection
              , childNationality = registerChildNationality
              , childAllergies = registerChildAllergies
              , childHealthInfo = registerChildHealthInfo
              , childMedication = registerChildMedication
              , childComments = registerChildComments
              , childPickupAllowed = registerChildPickupAllowed
              , childPickupDisallowed = registerChildPickupDisallowed
              , childPicturePermission = registerChildPicturePermission
              , childDelijnBus = registerChildDelijnBus
              , childDoctor = registerChildDoctor
              , childRegistrationTime = now
              }
      case registerChildId of
        Just i ->
          withOwnChild i $ do
            runDB $ replaceValid i child
            redirect ChildrenR
        Nothing -> do
          runDB $ do
            cid <- insertValid child
            insertValid_ $ ChildOf cid aid
          redirect ChildrenR
    fr -> withFormResultNavBar fr $(widgetFile "register-child")

getChildR :: ChildId -> Handler Html
getChildR cid =
  withOwnChild cid $ do
    child <- runDB $ get404 cid
    childForm <- makeRegisterChildForm (Just (Entity cid child))
    (formWidget, formEnctype) <-
      generateFormPost $ renderCustomForm BootstrapBasicForm childForm
    withNavBar $(widgetFile "child")

postDeregisterChildR :: ChildId -> Handler Html
postDeregisterChildR cid =
  withOwnChild cid $ do
    canDeleteChild cid >>= deleteOrError
    redirect ChildrenR
