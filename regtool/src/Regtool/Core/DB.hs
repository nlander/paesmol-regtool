{-# LANGUAGE GADTs #-}

module Regtool.Core.DB
  ( Entity(..)
  , Key
  , EntityField
  , PersistEntity
  , PersistEntityBackend
  , PersistRecordBackend
  , ToBackendKey
  , PersistValue(..)
  , insertValid
  , insertValid_
  , insertValidEntity
  , insertValidUnique
  , insertManyValid
  , insertManyValid_
  , replaceValid
  , SqlBackend
  , (==.)
  , (!=.)
  , (<=.)
  , (>=.)
  , (>.)
  , (<.)
  , (<-.)
  , (/<-.)
  , selectList
  , selectFirst
  , get
  , getBy
  , SelectOpt(..)
  ) where

import Import

import Database.Persist
  ( Entity
  , EntityField
  , Key
  , PersistEntity
  , PersistEntityBackend
  , PersistRecordBackend
  , PersistValue(..)
  , SelectOpt(..)
  , ToBackendKey
  , (!=.)
  , (/<-.)
  , (<-.)
  , (<.)
  , (<=.)
  , (==.)
  , (>.)
  , (>=.)
  , get
  , getBy
  , selectFirst
  , selectList
  )
import Database.Persist.Sql (SqlBackend)

import qualified Database.Persist as DB

insertValid ::
     (Validity record, PersistRecordBackend record SqlBackend, MonadIO m)
  => record
  -> ReaderT SqlBackend m (Key record)
insertValid v =
  case prettyValidate v of
    Left err -> liftIO $ ioError $ userError err
    Right v_ -> DB.insert v_

insertValid_ ::
     (Validity record, PersistRecordBackend record SqlBackend, MonadIO m)
  => record
  -> ReaderT SqlBackend m ()
insertValid_ v =
  case prettyValidate v of
    Left err -> liftIO $ ioError $ userError err
    Right v_ -> DB.insert_ v_

insertValidEntity ::
     (Validity record, PersistRecordBackend record SqlBackend, MonadIO m)
  => record
  -> ReaderT SqlBackend m (Entity record)
insertValidEntity v =
  case prettyValidate v of
    Left err -> liftIO $ ioError $ userError err
    Right v_ -> DB.insertEntity v_

insertValidUnique ::
     (Validity record, PersistRecordBackend record SqlBackend, MonadIO m)
  => record
  -> ReaderT SqlBackend m (Maybe (Key record))
insertValidUnique v =
  case prettyValidate v of
    Left err -> liftIO $ ioError $ userError err
    Right v_ -> DB.insertUnique v_

insertManyValid ::
     (Validity record, PersistRecordBackend record SqlBackend, MonadIO m)
  => [record]
  -> ReaderT SqlBackend m [Key record]
insertManyValid vs =
  case prettyValidate vs of
    Left err -> liftIO $ ioError $ userError err
    Right vs_ -> DB.insertMany vs_

insertManyValid_ ::
     (Validity record, PersistRecordBackend record SqlBackend, MonadIO m)
  => [record]
  -> ReaderT SqlBackend m ()
insertManyValid_ vs =
  case prettyValidate vs of
    Left err -> liftIO $ ioError $ userError err
    Right vs_ -> DB.insertMany_ vs_

replaceValid ::
     (Validity record, PersistRecordBackend record SqlBackend, MonadIO m)
  => Key record
  -> record
  -> ReaderT SqlBackend m ()
replaceValid k v =
  case prettyValidate v of
    Left err -> liftIO $ ioError $ userError err
    Right v_ -> DB.replace k v_
