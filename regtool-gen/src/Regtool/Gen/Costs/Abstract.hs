{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Gen.Costs.Abstract where

import GenImport

import qualified Data.Map as M

import Database.Persist (Entity(..))

import Regtool.Data
import Regtool.Data.Gen ()
import Regtool.Data.Gen.Entity

import Regtool.Utils.Costs.Abstract

instance GenValid YearlyFeeStatus where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid TransportFeeStatus where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalTransportFeeStatus where
  genValid = do
    ec <- genValid
    eots <- modEnt (\ots -> ots {occasionalTransportSignupChild = entityKey ec}) <$> genValid
    oneof
      [ pure $ OccasionalTransportFeeUnpaid ec eots
      , do eotp <-
             modEnt (\otp -> otp {occasionalTransportPaymentSignup = entityKey eots}) <$> genValid
           pure $ OccasionalTransportFeePaid ec eots eotp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareFeeStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eds <-
      modEnt
        (\ds -> ds {daycareSignupChild = entityKey ec, daycareSignupTimeslot = entityKey edcts}) <$>
      genValid
    oneof
      [ pure $ DaycareFeeUnpaid ec edcts eds
      , do edp <- modEnt (\dp -> dp {daycarePaymentSignup = entityKey eds}) <$> genValid
           pure $ DaycareFeePaid ec edcts eds edp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareActivityFeeStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eds <-
      modEnt
        (\ds ->
           ds
             { daycareActivitySignupChild = entityKey ec
             , daycareActivitySignupActivity = entityKey edcts
             }) <$>
      genValid
    oneof
      [ pure $ DaycareActivityFeeUnpaid ec edcts eds
      , do edp <- modEnt (\dp -> dp {daycareActivityPaymentSignup = entityKey eds}) <$> genValid
           pure $ DaycareActivityFeePaid ec edcts eds edp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalDaycareFeeStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eots <-
      modEnt
        (\ots ->
           ots
             { occasionalDaycareSignupChild = entityKey ec
             , occasionalDaycareSignupTimeslot = entityKey edcts
             }) <$>
      genValid
    oneof
      [ pure $ OccasionalDaycareFeeUnpaid ec edcts eots
      , do eotp <-
             modEnt (\otp -> otp {occasionalDaycarePaymentSignup = entityKey eots}) <$> genValid
           pure $ OccasionalDaycareFeePaid ec edcts eots eotp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid AbstractSemestrelyCosts where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid AbstractYearlyCosts where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid AbstractCosts where
  genValid = do
    abstractCostsMap <-
      M.fromList <$> genListOf (genValid `suchThat` (not . nullAbstractYearlyCosts . snd))
    abstractCostsOccasionalTransport <-
      genValid `suchThat` (distinct . map occasionalTransportFeeStatusSignup)
    abstractCostsOccasionalDaycare <-
      genValid `suchThat` (distinct . map occasionalDaycareFeeStatusSignup)
    abstractCostsDiscounts <- modEntL (\d -> d {discountCheckout = Nothing}) <$> genValid
    pure AbstractCosts {..}
  shrinkValid = shrinkValidStructurally
