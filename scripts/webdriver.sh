#!/bin/bash

set -e
set -x

cd regtool

stack build :regtool-webdriver-tests \
  --file-watch \
  --exec='regtool-webdriver-tests' \
  --ghc-options="-freverse-errors -DDEVELOPMENT -O0" \
  --fast
