{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Handler.Admin.Raw
  ( getAdminRawDataR
  , postProfileVerifyManuallyR
  , postAdminExportRawDataR
  ) where

import Import

import Database.Persist
import Database.Persist.Sql

import Codec.Archive.Zip
import qualified Data.ByteString.Lazy as LB
import Data.Csv
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE

import Yesod.Core

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation
import Regtool.Utils.Time

getAdminRawDataR :: Handler Html
getAdminRawDataR = do
  navigationBar <- makeNavigationBarWidget
  ts <-
    sequence
      [ getTableFor @Account
      , getTableFor @BusLine
      , getTableFor @BusStop
      , getTableFor @BusLineStop
      , getTableFor @TransportSignup
      , getTableFor @TransportPayment
      , getTableFor @TransportPaymentPlan
      , getTableFor @TransportEnrollment
      , getTableFor @OccasionalTransportSignup
      , getTableFor @OccasionalTransportPayment
      , getTableFor @Child
      , getTableFor @ChildOf
      , getTableFor @Doctor
      , getTableFor @Email
      , getTableFor @LanguageSection
      , getTableFor @Parent
      , getTableFor @PasswordResetEmail
      , getTableFor @PaymentReceivedEmail
      , getTableFor @StripeCustomer
      , getTableFor @StripePayment
      , getTableFor @YearlyFeePayment
      , getTableFor @Discount
      , getTableFor @VerificationEmail
      , getTableFor @DaycareSemestre
      , getTableFor @DaycareTimeslot
      , getTableFor @DaycareSignup
      , getTableFor @DaycarePayment
      , getTableFor @OccasionalDaycareSignup
      , getTableFor @OccasionalDaycarePayment
      , getTableFor @DaycareActivity
      , getTableFor @DaycareActivitySignup
      , getTableFor @DaycareActivityPayment
      ]
  let tables = zip [(1 :: Int) ..] ts
  token <- genToken
  defaultLayout $(widgetFile "admin/raw")

postProfileVerifyManuallyR :: AccountId -> Handler Html
postProfileVerifyManuallyR aid = do
  maid <- runDB $ get aid
  case maid of
    Nothing -> invalidArgs ["Unknown account"]
    Just acc -> do
      verifyAccount $ Entity aid acc
      redirect $ AdminR AdminRawDataR

getTableFor ::
     forall record.
     ( PersistEntity record
     , PersistEntityBackend record ~ SqlBackend
     , ToBackendKey SqlBackend record
     , Validity record
     , Show record
     )
  => Handler (Text, Widget)
getTableFor = do
  values <- runDB $ selectList [] [Asc (persistIdField :: EntityField record (Key record))]
  let def = entityDef (pure undefined :: IO record)
  let pvalues = map (\(Entity i r) -> (i, isValid r, map toPersistValue $ toPersistFields r)) values
  pure (unDBName $ entityDB def, $(widgetFile "admin/raw/table"))

fieldText :: PersistValue -> Text
fieldText (PersistText t) = t
fieldText (PersistByteString bs) = tshow bs
fieldText (PersistInt64 i) = tshow i
fieldText (PersistDouble d) = tshow d
fieldText (PersistRational r) = tshow r
fieldText (PersistBool b) = tshow b
fieldText (PersistDay d) = tshow d
fieldText (PersistTimeOfDay tod) = tshow tod
fieldText (PersistUTCTime utct) = T.pack $ adminTime utct
fieldText PersistNull = ""
fieldText (PersistList ls) = tshow ls
fieldText (PersistMap m) = tshow m
fieldText (PersistObjectId bs) = tshow bs
fieldText (PersistDbSpecific bs) = tshow bs

tshow :: Show v => v -> Text
tshow = T.pack . show

postAdminExportRawDataR :: Handler TypedContent
postAdminExportRawDataR = do
  csvs <-
    sequence
      [ getCsvFor @Account
      , getCsvFor @BusLine
      , getCsvFor @BusStop
      , getCsvFor @BusLineStop
      , getCsvFor @TransportSignup
      , getCsvFor @TransportPayment
      , getCsvFor @TransportPaymentPlan
      , getCsvFor @TransportEnrollment
      , getCsvFor @OccasionalTransportSignup
      , getCsvFor @OccasionalTransportPayment
      , getCsvFor @OccasionalTransportSignup
      , getCsvFor @OccasionalTransportPayment
      , getCsvFor @Child
      , getCsvFor @ChildOf
      , getCsvFor @Doctor
      , getCsvFor @Email
      , getCsvFor @LanguageSection
      , getCsvFor @Parent
      , getCsvFor @PasswordResetEmail
      , getCsvFor @PaymentReceivedEmail
      , getCsvFor @StripeCustomer
      , getCsvFor @StripePayment
      , getCsvFor @YearlyFeePayment
      , getCsvFor @Discount
      , getCsvFor @VerificationEmail
      , getCsvFor @DaycareSemestre
      , getCsvFor @DaycareTimeslot
      , getCsvFor @DaycareSignup
      , getCsvFor @DaycarePayment
      , getCsvFor @OccasionalDaycareSignup
      , getCsvFor @OccasionalDaycarePayment
      , getCsvFor @DaycareActivity
      , getCsvFor @DaycareActivitySignup
      , getCsvFor @DaycareActivityPayment
      ]
  let archive =
        foldl' (flip addEntryToArchive) emptyArchive (map (\(fp, bs) -> toEntry fp 0 bs) csvs)
  addHeader "Content-Disposition" $ T.concat ["attachment; filename=\"", "regtool-data.zip", "\""]
  sendResponse (TE.encodeUtf8 "application/zip", toContent $ fromArchive archive)

getCsvFor ::
     forall record.
     ( PersistEntity record
     , PersistEntityBackend record ~ SqlBackend
     , ToBackendKey SqlBackend record
     , Validity record
     , Show record
     )
  => Handler (FilePath, LB.ByteString)
getCsvFor = do
  values <- runDB $ selectList [] [Asc (persistIdField :: EntityField record (Key record))]
  let def = entityDef (pure undefined :: IO record)
  let pvalues =
        map
          (\(Entity i r) ->
             (tshow (fromSqlKey i) : map (fieldText . toPersistValue) (toPersistFields r)))
          values
  pure (T.unpack (unDBName $ entityDB def) ++ ".csv", encode pvalues)
