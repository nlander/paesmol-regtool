{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Doctor where

import Import

import Regtool.Data

import Regtool.Data.Gen.Country ()
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenValid Doctor where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
