{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.SchoolLevel
  ( SchoolLevel(..)
  , SchoolLevelType(..)
  , schoolLevelDecompose
  , schoolLevelTypeText
  , schoolLevelText
  ) where

import Import

import Data.Aeson

import qualified Data.Text as T

import Database.Persist
import Database.Persist.Sql

data SchoolLevel
  = Nursery1
  | Nursery2
  | Nursery3
  | Primary1
  | Primary2
  | Primary3
  | Primary4
  | Primary5
  | Secondary1
  | Secondary2
  | Secondary3
  | Secondary4
  | Secondary5
  | Secondary6
  | Secondary7
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity SchoolLevel

instance FromJSON SchoolLevel

instance ToJSON SchoolLevel

instance ToField SchoolLevel where
  toField = toField . schoolLevelText

data SchoolLevelType
  = Nursery
  | Primary
  | Secondary
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity SchoolLevelType

instance FromJSON SchoolLevelType

instance ToJSON SchoolLevelType

instance ToField SchoolLevelType where
  toField = toField . schoolLevelTypeText

schoolLevelDecompose :: SchoolLevel -> (SchoolLevelType, Int)
schoolLevelDecompose s =
  case s of
    Nursery1 -> (Nursery, 1)
    Nursery2 -> (Nursery, 2)
    Nursery3 -> (Nursery, 3)
    Primary1 -> (Primary, 1)
    Primary2 -> (Primary, 2)
    Primary3 -> (Primary, 3)
    Primary4 -> (Primary, 4)
    Primary5 -> (Primary, 5)
    Secondary1 -> (Secondary, 1)
    Secondary2 -> (Secondary, 2)
    Secondary3 -> (Secondary, 3)
    Secondary4 -> (Secondary, 4)
    Secondary5 -> (Secondary, 5)
    Secondary6 -> (Secondary, 6)
    Secondary7 -> (Secondary, 7)

schoolLevelTypeText :: SchoolLevelType -> Text
schoolLevelTypeText Nursery = "Nursery"
schoolLevelTypeText Primary = "Primary"
schoolLevelTypeText Secondary = "Secondary"

schoolLevelText :: SchoolLevel -> Text
schoolLevelText s =
  case schoolLevelDecompose s of
    (t, i) -> T.unwords [schoolLevelTypeText t, T.pack $ show i]

schoolLevelInt :: SchoolLevel -> Int
schoolLevelInt = fromEnum

parseSchoolLevelInt :: Int -> Either Text SchoolLevel
parseSchoolLevelInt i
  | i >= schoolLevelInt minBound && i <= schoolLevelInt maxBound =
    Right $ toEnum i
  | otherwise = Left $ "Unknown school level: " <> T.pack (show i)

instance PersistField SchoolLevel where
  toPersistValue = toPersistValue . schoolLevelInt
  fromPersistValue pv =
    case pv of
      PersistInt64 i -> parseSchoolLevelInt $ fromIntegral i
      _ -> Left "Invalid persist value type for parsing SchoolLint"

instance PersistFieldSql SchoolLevel where
  sqlType Proxy = SqlInt64
