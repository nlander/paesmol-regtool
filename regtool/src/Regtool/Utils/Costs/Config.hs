{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Costs.Config where

import Import

import Data.Aeson

import Regtool.Data

data CostsConfig =
  CostsConfig
    { costsConfigAnnualFee :: Amount
    , costsConfigBusYearly :: [Amount]
    , costsConfigBusYearlyDefault :: Amount
    , costsConfigOccasionalBus :: Amount
    }
  deriving (Show, Eq, Generic)

instance Validity CostsConfig where
  validate CostsConfig {..} =
    mconcat
      [ delve "costsConfigAnnualFee" costsConfigAnnualFee
      , delve "costsConfigBusYearly" costsConfigBusYearly
      , delve "costsConfigBusYearlyDefault" costsConfigBusYearlyDefault
      , decorate "costsConfigAnnualFee" $ validatePositiveAmount costsConfigAnnualFee
      , decorate "costsConfigBusYearly" $ decorateList costsConfigBusYearly validatePositiveAmount
      , decorate "costsConfigBusYearlyDefault" $ validatePositiveAmount costsConfigBusYearlyDefault
      , decorate "costsConfigOccasionalBus" $ validatePositiveAmount costsConfigOccasionalBus
      ]

instance FromJSON CostsConfig where
  parseJSON =
    withObject "CostsConfig" $ \o ->
      CostsConfig <$> o .: "annual membership fee" <*> o .: "bus yearly" <*>
      o .: "bus yearly default" <*>
      o .: "occasional bus fee"

instance ToJSON CostsConfig where
  toJSON CostsConfig {..} =
    object
      [ "annual membership fee" .= costsConfigAnnualFee
      , "bus yearly" .= costsConfigBusYearly
      , "bus yearly default" .= costsConfigBusYearlyDefault
      , "occasional bus fee" .= costsConfigOccasionalBus
      ]

defaultCostsConfig :: CostsConfig
defaultCostsConfig =
  CostsConfig
    { costsConfigAnnualFee = unsafeAmount 30
    , costsConfigBusYearly = map unsafeAmount [1600, 1200, 800]
    , costsConfigBusYearlyDefault = zeroAmount
    , costsConfigOccasionalBus = unsafeAmount 15
    }
