module Regtool.LooperSpec
  ( spec
  ) where

import Regtool.TestUtils

import qualified Regtool.Looper.Emailer.PasswordResetSpec
import qualified Regtool.Looper.Emailer.PaymentReceivedSpec
import qualified Regtool.Looper.Emailer.VerificationEmailSpec

spec :: RegtoolSpec
spec = do
  Regtool.Looper.Emailer.PaymentReceivedSpec.spec
  Regtool.Looper.Emailer.PasswordResetSpec.spec
  Regtool.Looper.Emailer.VerificationEmailSpec.spec
