{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.BusLines where

import Import

import qualified Data.Text as T

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Form
import Regtool.Core.Foundation

import Regtool.Handler.Admin.BusLines.Utils

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

getAdminBusLinesR :: Handler Html
getAdminBusLinesR = do
  busLines <- runDB $ selectList [] [Asc BusLineId]
  token <- genToken
  withNavBar $(widgetFile "admin/buslines")

type Moment = Text

momentField :: Field Handler Moment
momentField = textField

data RegisterBusLine =
  RegisterBusLine
    { registerBusLineName :: Text
    , registerBusLineDescription :: Textarea
    , registerBusLineSeats :: Int
    , registerBusLineMorning :: Text
    , registerBusLineEvening :: Text
    , registerBusLineWednesday :: Text
    , registerBusLineId :: Maybe BusLineId
    }
  deriving (Show, Eq)

registerBusLineForm :: Maybe (Entity BusLine) -> AForm Handler RegisterBusLine
registerBusLineForm mce =
  RegisterBusLine <$> areq textField (bfs ("Name" :: Text)) (dv busLineName) <*>
  areq textareaField (bfs ("Description" :: Text)) (dv busLineDescription) <*>
  areq
    (checkBool
       (> 0)
       ("The number of seats available must be positive." :: Text)
       intField)
    (bfs ("Total seats available" :: Text))
    (dv busLineSeats) <*>
  areq
    textField
    (bfs ("Arrives at ES Mol in the Morning at" :: Text))
    (dv busLineEsMolMorningTime) <*>
  areq
    textField
    (bfs ("Leaves from ES Mol in the Evening at" :: Text))
    (dv busLineEsMolEveningTime) <*>
  areq
    textField
    (bfs ("Leaves from ES Mol in the afternoon on Wednesday" :: Text))
    (dv busLineEsMolWednesdayTime) <*>
  aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (BusLine -> a) -> Maybe a
    dv func = (func . entityVal) <$> mce

getAdminRegisterBusLineR :: Handler Html
getAdminRegisterBusLineR = do
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $ registerBusLineForm Nothing
  withNavBar $(widgetFile "admin/register-busline")

postAdminRegisterBusLineR :: Handler Html
postAdminRegisterBusLineR = do
  ((result, formWidget), formEnctype) <-
    runFormPost $
    renderCustomForm BootstrapBasicForm $ registerBusLineForm Nothing
  case result of
    FormSuccess RegisterBusLine {..} -> do
      now <- liftIO getCurrentTime
      let busLine_ =
            BusLine
              { busLineName = registerBusLineName
              , busLineDescription = registerBusLineDescription
              , busLineSeats = registerBusLineSeats
              , busLineEsMolMorningTime = registerBusLineMorning
              , busLineEsMolEveningTime = registerBusLineEvening
              , busLineEsMolWednesdayTime = registerBusLineWednesday
              , busLineCreationTime = now
              }
      case registerBusLineId of
        Just i -> do
          runDB $ replaceValid i busLine_
          redirect $ AdminR AdminBusLinesR
        Nothing -> do
          did <- runDB $ insertValid busLine_
          redirect $ AdminR $ AdminBusLineR did
    _ -> withNavBar $(widgetFile "admin/register-busline")

getAdminBusLineR :: BusLineId -> Handler Html
getAdminBusLineR bid = do
  busLine <- runDB $ get404 bid
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $
    registerBusLineForm $ Just $ Entity bid busLine
  withNavBar $(widgetFile "admin/busline")

postAdminBusLineDeleteR :: BusLineId -> Handler Html
postAdminBusLineDeleteR blid = do
  runDB $ DB.delete blid
  redirect $ AdminR AdminBusLinesR

data AttachBusStop =
  AttachBusStop
    { attachBusStop :: BusStopId
    , attachBusStopMorningTime :: Moment
    , attachBusStopEveningTime :: Moment
    , attachBusStopWednesdayTime :: Moment
    }
  deriving (Show, Eq, Generic)

attachBusStopForm :: [Entity BusStop] -> AForm Handler AttachBusStop
attachBusStopForm bss =
  AttachBusStop <$>
  areq
    (selectFieldList $ do
       Entity bsid bs <- bss
       let t =
             T.unwords
               [unTextarea $ busStopCity bs, unTextarea $ busStopLocation bs]
       pure (t, bsid))
    (bfs ("Bus Stop" :: Text))
    Nothing <*>
  areq textField (bfs ("Morning" :: Text)) Nothing <*>
  areq textField (bfs ("Evening" :: Text)) Nothing <*>
  areq textField (bfs ("Wednesday" :: Text)) Nothing

getAdminBusLineOverviewR :: BusLineId -> Handler Html
getAdminBusLineOverviewR blid = do
  busLine <- runDB $ get404 blid
  stops <- getBusStopsOf blid
  busStops <-
    runDB $
    selectList [BusStopId /<-. map (entityKey . snd) stops] [Asc BusStopId]
  (attachStopForm, _) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $ attachBusStopForm busStops
  token <- genToken
  withNavBar $(widgetFile "admin/busline-overview")

postAdminBusLineAttachR :: BusLineId -> Handler Html
postAdminBusLineAttachR blid = do
  busLine <- runDB $ get404 blid
  stops <- getBusStopsOf blid
  busStops <-
    runDB $
    selectList [BusStopId /<-. map (entityKey . snd) stops] [Asc BusStopId]
  ((result, attachStopForm), _) <-
    runFormPost $
    renderCustomForm BootstrapBasicForm $ attachBusStopForm busStops
  case result of
    FormSuccess AttachBusStop {..} -> do
      now <- liftIO getCurrentTime
      let busLineStop =
            BusLineStop
              { busLineStopLine = blid
              , busLineStopStop = attachBusStop
              , busLineStopMorningTime = attachBusStopMorningTime
              , busLineStopEveningTime = attachBusStopEveningTime
              , busLineStopWednesdayTime = attachBusStopWednesdayTime
              , busLineStopCreationTime = now
              }
      void $ runDB $ insertValidUnique busLineStop
      redirect $ AdminR $ AdminBusLineOverviewR blid
    fr -> do
      token <- genToken
      withFormResultNavBar fr $(widgetFile "admin/busline-overview")

postAdminBusLineDetachR :: BusLineId -> BusStopId -> Handler Html
postAdminBusLineDetachR blid bsid = do
  runDB $ DB.deleteBy $ UniqueBusLineStop blid bsid
  redirect $ AdminR $ AdminBusLineOverviewR blid
