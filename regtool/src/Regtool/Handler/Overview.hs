{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Overview
  ( getOverviewR
  ) where

import Yesod.Auth
import Yesod.Core

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar
import Regtool.Utils.Readiness

getOverviewR :: Handler Html
getOverviewR = do
  aid <- requireAuthId
  Readiness {..} <- getReadiness aid
  withNavBar $(widgetFile "overview")
