{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.TransportPayment where

import Import

import Regtool.Data

import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenUnchecked TransportPayment

instance GenValid TransportPayment where
  genValid = genValidStructurally
