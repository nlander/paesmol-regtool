{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Gen.Costs.Calculate where

import GenImport

import Database.Persist (Entity(..))

import Regtool.Data
import Regtool.Data.Gen

import Regtool.Utils.Costs.Calculate

instance GenValid YearlyFeePaymentStatus where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid TransportPaymentStatus where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalTransportPaymentStatus where
  genValid = do
    ec <- genValid
    eots <- modEnt (\ots -> ots {occasionalTransportSignupChild = entityKey ec}) <$> genValid
    oneof
      [ OccasionalTransportPaymentUnpaid ec eots <$> strictlyPositiveValidAmount
      , do eotp <-
             modEnt (\otp -> otp {occasionalTransportPaymentSignup = entityKey eots}) <$> genValid
           pure $ OccasionalTransportPaymentPaid ec eots eotp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycarePaymentStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eds <-
      modEnt
        (\ds -> ds {daycareSignupChild = entityKey ec, daycareSignupTimeslot = entityKey edcts}) <$>
      genValid
    oneof
      [ DaycarePaymentUnpaid ec edcts eds <$> strictlyPositiveValidAmount
      , do edp <- modEnt (\dp -> dp {daycarePaymentSignup = entityKey eds}) <$> genValid
           pure $ DaycarePaymentPaid ec edcts eds edp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid DaycareActivityPaymentStatus where
  genValid = do
    ec <- genValid
    edcts <- genValid
    eds <-
      modEnt
        (\ds ->
           ds
             { daycareActivitySignupChild = entityKey ec
             , daycareActivitySignupActivity = entityKey edcts
             }) <$>
      genValid
    oneof
      [ DaycareActivityPaymentUnpaid ec edcts eds <$> strictlyPositiveValidAmount
      , do edp <- modEnt (\dp -> dp {daycareActivityPaymentSignup = entityKey eds}) <$> genValid
           pure $ DaycareActivityPaymentPaid ec edcts eds edp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid OccasionalDaycarePaymentStatus where
  genValid = do
    ec <- genValid
    eots <- modEnt (\ots -> ots {occasionalDaycareSignupChild = entityKey ec}) <$> genValid
    oneof
      [ OccasionalDaycarePaymentUnpaid ec eots <$> strictlyPositiveValidAmount
      , do eotp <-
             modEnt (\otp -> otp {occasionalDaycarePaymentSignup = entityKey eots}) <$> genValid
           pure $ OccasionalDaycarePaymentPaid ec eots eotp
      ]
  shrinkValid = shrinkValidStructurally

instance GenValid CalculatedSemestrelyCosts where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid CalculatedYearlyCosts where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid CalculatedCosts where
  genValid = do
    calculatedCostsMap <- genValid
    calculatedCostsOccasionalTransport <-
      genValid `suchThat` (distinct . map occasionalTransportPaymentStatusSignup)
    calculatedCostsOccasionalDaycare <-
      genValid `suchThat` (distinct . map occasionalDaycarePaymentStatusSignup)
    calculatedCostsDiscounts <- modEntL (\d -> d {discountCheckout = Nothing}) <$> genValid
    pure CalculatedCosts {..}
  shrinkValid = shrinkValidStructurally
