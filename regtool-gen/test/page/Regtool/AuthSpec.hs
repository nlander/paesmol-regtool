{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.AuthSpec
  ( spec
  ) where

import TestImport

import Yesod.Test

import Network.HTTP.Types

import Regtool.Application ()
import Regtool.Core.Foundation hiding (get, runDB)
import Regtool.Data
import Regtool.Gen ()
import Regtool.TestUtils

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

spec :: RegtoolSpec
spec = do
  ydescribe "LoginR" $ do
    yit "returns status 200" $ do
      get $ AuthR LoginR
      statusIs 200
    yit "returns status 200 for a verified account" $
      withAnyVerifiedAccountLogin_ $ do
        get $ AuthR LoginR
        statusIs 200
  ydescribe "LogoutR" $ do
    yit "returns status 200" $ do
      get $ AuthR LogoutR
      statusIs 200
    yit "returns status 200 for a verified account" $
      withAnyVerifiedAccountLogin_ $ do
        get $ AuthR LogoutR
        statusIs 200
  ydescribe "Register" $ do
    yit "returns status 200" $ do
      get $ AuthR registerR
      statusIs 200
    yit "returns status 200 for a verified account" $
      withAnyVerifiedAccountLogin_ $ do
        get $ AuthR registerR
        statusIs 200
  ydescribe "Registration Workflow" $
    ydescribe "Email" $
    yit "correctly registers a new email address" $ do
      (e, p) <- genEmailAndPass
                -- The user goes to the register page
      get $ AuthR registerR
      statusIs 200
                -- The user fills out the register form
      request $ do
        setMethod methodPost
        setUrl $ AuthR registerR
        addTokenFromCookie
        addPostParam "email" $ emailAddressText e
        addPostParam "passphrase" p
        addPostParam "passphrase-confirm" p
                -- The user gets redirected to the login form TODO log in automatically
      statusIs 303
      loc <- followRedirect
      liftIO $ loc `shouldBe` Right "/auth/login"
                -- The database then contains the newly created account
      Entity _ account1 <- lookupExistingByEmail e
                -- ... with the correct email
      liftIO $ accountEmailAddress account1 `shouldBe` e
                -- ... and it's unverified, but it does have a verification key
      liftIO $ accountVerified account1 `shouldBe` False
      liftIO $ accountVerificationKey account1 `shouldSatisfy` isJust
      verkey <-
        case accountVerificationKey account1 of
          Just key -> pure key
          _ -> failure "Account should have been unverified and have a verkey"
      let verificationUrl = verifyR (accountEmailAddress account1) verkey
                -- ... The database also contains a verification email for this email
      mver <- runDB $ selectFirst [VerificationEmailTo ==. e] [Desc VerificationEmailTimestamp]
      case mver of
        Nothing -> failure "There should have been such a verification email."
        Just (Entity _ VerificationEmail {..}) ->
          liftIO $ do
            verificationEmailTo `shouldBe` e
            verificationEmailKey `shouldBe` verkey
            verificationEmailEmail `shouldBe` Nothing -- Not converted to an email yet.
                -- The user folows the verification url
      get $ AuthR verificationUrl
      shouldRedirectTo HomeR
                -- The account is now verified.
      Entity _ account2 <- lookupExistingByEmail e
      liftIO $ accountVerified account2 `shouldBe` True
      liftIO $ accountVerificationKey account2 `shouldBe` Nothing
  ydescribe "Password reset workflow" $
    yit "correctly resets a password for a verified account" $ do
      (e, p) <- genEmailAndPass
      (_, p2) <- genEmailAndPass
      withVerifiedAccount e p $ \(Entity _ acc) -> do
        get $ AuthR resetPasswordR
        statusIs 200
        request $ do
          setMethod methodPost
          setUrl $ AuthR resetPasswordR
          addTokenFromCookie
          addPostParam "userkey" (emailAddressText $ accountEmailAddress acc)
        statusIs 303
        loc <- followRedirect
        liftIO $ loc `shouldBe` Right "/auth/login"
                    -- The database now contains a password reset email
        mver <-
          runDB $
          selectFirst
            [PasswordResetEmailTo ==. accountEmailAddress acc]
            [Desc PasswordResetEmailTimestamp]
                    -- That email contains a link
        resetKey <-
          case mver of
            Nothing -> failure "There should have been a password reset email."
            Just (Entity _ pre) -> pure $ passwordResetEmailKey pre
                    -- If the user follows that link, they get a form to reset their password
        get $ AuthR $ setPasswordWithKeyR (emailAddressText $ accountEmailAddress acc) resetKey
        statusIs 200
        request $ do
          setMethod methodPost
          setUrl $ AuthR setPasswordWithKeyTargetR
          addTokenFromCookie
          addPostParam "userkey" (emailAddressText $ accountEmailAddress acc)
          addPostParam "resetkey" resetKey
          addPostParam "passphrase" p2
          addPostParam "passphrase-confirm" p2
        statusIs 303
        loc' <- followRedirect
        liftIO $ loc' `shouldBe` Right "/"
        Entity _ acc' <- lookupExistingByEmail (accountEmailAddress acc)
        liftIO $ verifyPassword p2 (accountSaltedPassphraseHash acc') `shouldBe` True
  ydescribe "Change password workflow" $
    yit "correctly changes a password for a logged-in verified account" $ do
      (e, p) <- genEmailAndPass
      (_, p2) <- genEmailAndPass
      withVerifiedAccountLogin e p $ \(Entity _ acc) -> do
        get $ AuthR setPasswordWithOldR
        statusIs 200
        request $ do
          setMethod methodPost
          setUrl $ AuthR setPasswordWithOldR
          addTokenFromCookie
          addPostParam "userkey" (emailAddressText $ accountEmailAddress acc)
          addPostParam "old-passphrase" p
          addPostParam "new-passphrase" p2
          addPostParam "new-passphrase-confirm" p2
        statusIs 303
        loc <- followRedirect
        liftIO $ loc `shouldBe` Right "/"
        Entity _ acc' <- lookupExistingByEmail (accountEmailAddress acc)
        liftIO $ verifyPassword p2 (accountSaltedPassphraseHash acc') `shouldBe` True
