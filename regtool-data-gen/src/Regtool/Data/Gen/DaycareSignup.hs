{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.DaycareSignup where

import Import

import Regtool.Data

import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenUnchecked DaycareSignup

instance GenValid DaycareSignup where
  genValid = genValidStructurally
