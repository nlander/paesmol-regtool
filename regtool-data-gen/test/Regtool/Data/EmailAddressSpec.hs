{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.EmailAddressSpec
  ( spec
  ) where

import TestImport

import Regtool.Data

import Regtool.Data.Gen ()

spec :: Spec
spec = do
  genValidSpec @EmailAddress
  shrinkValidSpec @EmailAddress
  eqSpecOnValid @EmailAddress
  ordSpecOnValid @EmailAddress
  describe "normalizeEmail" $
    it "produces valids" $ producesValidsOnValids normalizeEmail
  describe "emailValidateFromText" $
    it "produces valids" $ producesValidsOnValids emailValidateFromText
  describe "emailValidateFromString" $
    it "produces valids" $ producesValidsOnValids emailValidateFromString
  describe "emailAddressFromText" $
    it "produces valids" $ producesValidsOnValids emailAddressFromText
  describe "emailAddressFromString" $
    it "produces valids" $ producesValidsOnValids emailAddressFromString
  describe "emailAddressText" $
    it "produces valids" $ producesValidsOnValids emailAddressText
  describe "emailAddressByteString" $
    it "produces valids" $ producesValidsOnValids emailAddressByteString
  describe "domainPart" $
    it "produces valids" $ producesValidsOnValids domainPart
  describe "localPart" $ it "produces valids" $ producesValidsOnValids localPart
