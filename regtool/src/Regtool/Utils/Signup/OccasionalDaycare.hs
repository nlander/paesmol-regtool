{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Signup.OccasionalDaycare
  ( getOurOccasionalDaycareSignups
  , occasionalSignupTrips
  ) where

import Import

import Regtool.Data

import Regtool.Core.Foundation

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

getOurOccasionalDaycareSignups ::
     MonadIO m
  => [ChildId]
  -> ReaderT SqlBackend m [Entity OccasionalDaycareSignup]
getOurOccasionalDaycareSignups cids =
  selectList [OccasionalDaycareSignupChild <-. cids] []

occasionalSignupTrips ::
     [Entity OccasionalDaycareSignup]
  -> [Entity Child]
  -> [Entity DaycareSemestre]
  -> [Entity DaycareTimeslot]
  -> [( Entity OccasionalDaycareSignup
      , Entity Child
      , Entity DaycareSemestre
      , Entity DaycareTimeslot)]
occasionalSignupTrips signups children semestres timeslots =
  flip mapMaybe signups $ \odse@(Entity _ OccasionalDaycareSignup {..}) -> do
    c <- find ((== occasionalDaycareSignupChild) . entityKey) children
    ts <- find ((== occasionalDaycareSignupTimeslot) . entityKey) timeslots
    s <-
      find ((== daycareTimeslotSemestre (entityVal ts)) . entityKey) semestres
    pure (odse, c, s, ts)
