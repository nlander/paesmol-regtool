{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.OptParse
  ( getInstructions
  , Dispatch(..)
  , Settings(..)
  , RunSettings(..)
  , RunVerificationSettings(..)
  , SetupTestDatabaseSettings(..)
  , MigrateVersionSettings(..)
  , VerifyDatabaseSettings(..)
  ) where

import Import

import Control.Monad.Logger
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import Looper
import Text.Read

import System.Environment (getArgs, getEnvironment)

import Web.Stripe (StripeConfig(..), StripeKey(..))

import Options.Applicative

import Regtool.Core.Foundation.Regtool
import Regtool.Data
import Regtool.OptParse.Types

getInstructions :: IO Instructions
getInstructions = do
  args <- getArguments
  env <- getFullEnvironment
  buildInstructions args env

defaultDatabaseFile :: FilePath
defaultDatabaseFile = "regtool.db"

buildInstructions :: Arguments -> Environment -> IO Instructions
buildInstructions (cmd, Flags) Environment {..} = (,) <$> disp <*> sets
  where
    disp =
      case cmd of
        CommandRun RunFlags {..} ->
          fmap DispatchRun $ do
            let requireVal name mv =
                  case mv of
                    Nothing -> die $ unwords ["No", name, "supplied."]
                    Just v -> pure v
            e <-
              do e_ <- requireVal "Environment" $ runFlagsEnv <|> envEnv
                 case readMaybe e_ of
                   Nothing ->
                     die $
                     unwords
                       [ e_
                       , "is not a valid option for the environment."
                       , "The valid options are " ++ envOptionsStr
                       ]
                   Just e -> pure e
            dbfile <- resolveFile' $ fromMaybe defaultDatabaseFile $ runFlagsDbFile <|> envDBFile
            let mssk = runFlagsStripeSecretKey <|> envStripeSecretKey
            let mspk = runFlagsStripePublishableKey <|> envStripePublishableKey
            mss <-
              case e of
                EnvAutomatedTesting -> do
                  unless (isNothing mssk && isNothing mspk) $
                    die "Must not configure stripe settings during automated testing."
                  pure Nothing
                _ -> do
                  ssk <- requireVal "Stripe Secret Key" mssk
                  spk <- requireVal "Stripe Publishable Key" mspk
                  pure $
                    Just
                      StripeSettings
                        { stripeSetsConfig =
                            StripeConfig {secretKey = StripeKey $ TE.encodeUtf8 $ T.pack ssk}
                        , stripeSetsPublishableKey = T.pack spk
                        }
            let ls =
                  let emailerSpecifics =
                        deriveLooperSettings
                          (seconds 0)
                          (seconds 5)
                          runFlagsLooperEmail
                          envLooperEmail
                          Nothing
                      backuperSpecifics =
                        deriveLooperSettings
                          (seconds 0)
                          (hours 12)
                          runFlagsLooperEmail
                          envLooperEmail
                          Nothing
                   in LoopersSettings
                        { loopersSetsEmailer = emailerSpecifics
                        , loopersSetsBackuper = backuperSpecifics
                        }
                ll =
                  fromMaybe
                    (case e of
                       EnvManualTesting -> LevelDebug
                       EnvAutomatedTesting -> LevelError
                       EnvStaging -> LevelInfo
                       EnvProduction -> LevelWarn) $
                  runFlagsLogLevel <|> envLogLevel
            let rvs = decideOnRunVerificationSettings runFlagsRunVerificationFlags
            pure
              RunSettings
                { runSetsEnv = e
                , runSetsDbFile = dbfile
                , runSetsHost = T.pack $ fromMaybe "localhost" $ runFlagsHost <|> envHost
                , runSetsPort = fromMaybe 3001 $ runFlagsPort <|> envPort
                , runSetsLogLevel = ll
                , runSetsEmailAddress =
                    fromMaybe (unsafeEmailAddress "registration" "paesmol.eu") $
                    runFlagsEmailAddress <|> envEmailAddress
                , runSetsLoopers = ls
                , runSetsStripeSettings = mss
                , runSetsVerification = rvs
                , runSetsMaintenance = fromMaybe False $ runFlagsMaintenance <|> envMaintenance
                }
        CommandSetupTestDatabase SetupTestDatabaseFlags {..} ->
          fmap DispatchSetupTestDatabase $ do
            dbfile <-
              resolveFile' $ fromMaybe "test.db" $ setupTestDatabaseFlagsDbFile <|> envDBFile
            pure SetupTestDatabaseSettings {setupTestDababaseSetsDbFile = dbfile}
        CommandMigrateVersion MigrateVersionFlags {..} ->
          fmap DispatchMigrateVersion $ do
            fromfile <- resolveFile' $ fromMaybe "from.db" migrateVersionFlagsFromDb
            tofile <- resolveFile' $ fromMaybe "to.db" migrateVersionFlagsToDb
            pure
              MigrateVersionSettings
                {migrateVersionSetsFromDb = fromfile, migrateVersionSetsToDb = tofile}
        CommandVerifyDatabase VerifyDatabaseFlags {..} ->
          fmap DispatchVerifyDatabase $ do
            dbfile <-
              resolveFile' $ fromMaybe defaultDatabaseFile $ verifyDatabaseFlagsDbFile <|> envDBFile
            pure VerifyDatabaseSettings {verifyDababaseSetsDbFile = dbfile}
    sets = pure Settings

decideOnRunVerificationSettings :: RunVerificationFlags -> RunVerificationSettings
decideOnRunVerificationSettings RunVerificationFlags {..}
  | runVerificationAndFailFlag = RunVerificationAndFail
  | runVerificationButDoNotFailFlag = RunVerificationButDoNotFail
  | doNotRunVerificationFlag = DoNotRunVerification
  | otherwise = RunVerificationAndFail

getArguments :: IO Arguments
getArguments = do
  args <- getArgs
  let result = runArgumentsParser args
  handleParseResult result

runArgumentsParser :: [String] -> ParserResult Arguments
runArgumentsParser =
  execParserPure
    ParserPrefs
      { prefMultiSuffix = ""
      , prefDisambiguate = True
      , prefShowHelpOnError = True
      , prefShowHelpOnEmpty = True
      , prefBacktrack = True
      , prefColumns = 80
      }
    argParser

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) (fullDesc <> progDesc "Regtool")

parseArgs :: Parser Arguments
parseArgs = (,) <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand =
  hsubparser $
  mconcat
    [ command "run" parseCommandRun
    , command "setup-test-db" parseCommandSetupTestDatabase
    , command "migrate-version" parseCommandMigrateVersion
    , command "verify-db" parseCommandVerifyDatabase
    ]

parseCommandRun :: ParserInfo Command
parseCommandRun = info parser modifier
  where
    parser =
      CommandRun <$>
      (RunFlags <$>
       option
         (Just <$> str)
         (mconcat
            [ long "environment"
            , metavar "ENV"
            , value Nothing
            , help $ unlines ["The environment to run regtool in.", "Options: " ++ envOptionsStr]
            ]) <*>
       option
         (Just <$> str)
         (mconcat [long "db-file", metavar "FILE", value Nothing, help "The database file to use"]) <*>
       option
         (Just <$> str)
         (mconcat [long "host", metavar "HOST", value Nothing, help "the host to serve on"]) <*>
       option
         (Just <$> auto)
         (mconcat [long "port", metavar "PORT", value Nothing, help "the port to serve on"]) <*>
       option
         (Just <$> auto)
         (mconcat
            [ long "log-level"
            , metavar "LOG_LEVEL"
            , value Nothing
            , help $
              "the log level, possible values: " <>
              show [LevelDebug, LevelInfo, LevelWarn, LevelError]
            ]) <*>
       option
         (Just <$> eitherReader emailValidateFromString)
         (mconcat
            [ long "email-address"
            , metavar "EMAIL_ADDRESS"
            , value Nothing
            , help "the email address to send emails from"
            ]) <*>
       getLooperFlags "emailer" <*>
       getLooperFlags "backuper" <*>
       option
         (Just <$> str)
         (mconcat
            [ long "stripe-secret-key"
            , metavar "KEY"
            , value Nothing
            , help "The secret key to use for stripe"
            ]) <*>
       option
         (Just <$> str)
         (mconcat
            [ long "stripe-publishable-key"
            , metavar "KEY"
            , value Nothing
            , help "The publishable key to use for stripe"
            ]) <*>
       parseRunVerificationFlags <*>
       flag
         Nothing
         (Just True)
         (mconcat [long "maintenance", help "The publishable key to use for stripe"]))
    modifier = fullDesc <> progDesc "Run regtool"

parseRunVerificationFlags :: Parser RunVerificationFlags
parseRunVerificationFlags =
  RunVerificationFlags <$> switch (mconcat [long "no-verification", help "Do not run verification"]) <*>
  switch (mconcat [long "verify-and-fail", help "Verify and fail if necessary"]) <*>
  switch
    (mconcat [long "verify-but-do-not-fail", help "Verifiy but do not fail if verification fails"])

parseCommandSetupTestDatabase :: ParserInfo Command
parseCommandSetupTestDatabase = info parser modifier
  where
    parser =
      CommandSetupTestDatabase <$>
      (SetupTestDatabaseFlags <$>
       option
         (Just <$> str)
         (mconcat
            [long "db-file", metavar "FILE", value Nothing, help "The database file to set up."]))
    modifier = fullDesc <> progDesc "Set up a test database to mess around"

parseCommandMigrateVersion :: ParserInfo Command
parseCommandMigrateVersion = info parser modifier
  where
    parser =
      CommandMigrateVersion <$>
      (MigrateVersionFlags <$>
       option
         (Just <$> str)
         (mconcat [long "from", metavar "FILE", value Nothing, help "The database file to migrate."]) <*>
       option
         (Just <$> str)
         (mconcat
            [long "to", metavar "FILE", value Nothing, help "The database file to migrate to."]))
    modifier =
      fullDesc <>
      progDesc
        "Migrate the database from the format of the latest master commit to the current schema."

parseCommandVerifyDatabase :: ParserInfo Command
parseCommandVerifyDatabase = info parser modifier
  where
    parser =
      CommandVerifyDatabase <$>
      (VerifyDatabaseFlags <$>
       option
         (Just <$> str)
         (mconcat
            [long "db-file", metavar "FILE", value Nothing, help "The database file to set up."]))
    modifier = fullDesc <> progDesc "Verify all the invariants of a given database"

parseFlags :: Parser Flags
parseFlags = pure Flags

getFullEnvironment :: IO Environment
getFullEnvironment = do
  env <- getEnvironment
  let v k = lookup ("REGTOOL_" <> k) env
  let r :: Read a => String -> Maybe a
      r k = v k >>= readMay
      le = readLooperEnvironment "REGTOOL_LOOPER_"
  ea <-
    forM (v "EMAIL_ADDRESS") $ \s ->
      case emailValidateFromString s of
        Left err -> die $ "Error while parsing email address: " <> err
        Right ea -> pure ea
  pure
    Environment
      { envEnv = v "ENVIRONMENT"
      , envDBFile = v "DB_FILE" <|> v "DATABASE_FILE"
      , envHost = v "HOST"
      , envPort = r "PORT"
      , envLogLevel = r "LOG_LEVEL"
      , envEmailAddress = ea
      , envLooperEmail = le "EMAILER" env
      , envLooperBackup = le "BACKUPER" env
      , envStripeSecretKey = v "STRIPE_SECRET_KEY"
      , envStripePublishableKey = v "STRIPE_PUBLISHABLE_KEY"
      , envMaintenance = r "MAINTENANCE"
      }

envOptionsStr :: String
envOptionsStr = show (map show [minBound .. maxBound :: RegtoolEnvironment])
