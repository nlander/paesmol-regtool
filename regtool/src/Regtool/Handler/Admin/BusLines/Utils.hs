module Regtool.Handler.Admin.BusLines.Utils
  ( getBusStopsOf
  ) where

import Import

import qualified Database.Esqueleto as E
import Database.Esqueleto ((^.))

import Regtool.Data

import Regtool.Core.Foundation

getBusStopsOf :: BusLineId -> Handler [(Entity BusLineStop, Entity BusStop)]
getBusStopsOf bid =
  runDB $
  E.select $
  E.from $ \(busLine `E.InnerJoin` busLineStop `E.InnerJoin` busStop) -> do
    E.on (busLine ^. BusLineId E.==. busLineStop ^. BusLineStopLine)
    E.on $ busLineStop ^. BusLineStopStop E.==. busStop ^. BusStopId
    E.where_ $ busLine ^. BusLineId E.==. E.val bid
    return (busLineStop, busStop)
