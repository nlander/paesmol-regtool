{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.PaymentReceivedSpec
  ( spec
  ) where

import TestImport

import Data.Time

import Yesod.Test

import Web.Stripe.Charge as Stripe

import Database.Persist.Sqlite (Entity(..), toSqlKey)

import Regtool.Data

import Regtool.Utils.Costs.Checkout

import Regtool.Core.Foundation.Regtool
import Regtool.Looper.Emailer.PaymentReceived

import Regtool.Looper.Emailer.TestUtils
import Regtool.TestUtils

spec :: RegtoolSpec
spec =
  ydescribe "payment received" $ do
    let cid = toSqlKey 0
        aid = toSqlKey 1
        yfpa = amountInCents 20
        tpa1 = amountInCents 60
        tpa2 = amountInCents 60
        otpa1 = amountInCents 11
        otpa2 = amountInCents 11
        dca1 = amountInCents 19
        dca2 = amountInCents 17
        da = amountInCents 40
        totalAmount = amountInCents 122
        spid = toSqlKey 2
        scid = toSqlKey 3
        yfpid = toSqlKey 4
        tppid1 = toSqlKey 5
        tppid2 = toSqlKey 6
        tpid1 = toSqlKey 7
        tpid2 = toSqlKey 8
        otsid1 = toSqlKey 9
        otsid2 = toSqlKey 10
        otpid1 = toSqlKey 11
        otpid2 = toSqlKey 12
        dpid1 = toSqlKey 13
        dpid2 = toSqlKey 14
        dpid3 = toSqlKey 15
        dapid1 = toSqlKey 15
        dapid2 = toSqlKey 16
        dapid3 = toSqlKey 17
        dapid4 = toSqlKey 18
        odcsid1 = toSqlKey 19
        odcsid2 = toSqlKey 20
        odcpid1 = toSqlKey 21
        odcpid2 = toSqlKey 22
        did = toSqlKey 23
        coo =
          CheckoutOverview
            { checkoutOverviewCheckout =
                Checkout
                  { checkoutAccount = aid
                  , checkoutAmount = totalAmount
                  , checkoutPayment = Just spid
                  , checkoutTimestamp = UTCTime (fromGregorian 1970 1 1) 9
                  }
            , checkoutOverviewPayment =
                Just
                  (Entity
                     spid
                     StripePayment
                       { stripePaymentCustomer = scid
                       , stripePaymentAmount = totalAmount
                       , stripePaymentCharge = Stripe.ChargeId "charge"
                       , stripePaymentTimestamp = UTCTime (fromGregorian 1970 1 1) 8
                       })
            , checkoutOverviewYearlyFeePayments =
                [ Entity
                    yfpid
                    YearlyFeePayment
                      { yearlyFeePaymentAccount = aid
                      , yearlyFeePaymentSchoolyear = SchoolYear 2017
                      , yearlyFeePaymentCheckout = cid
                      , yearlyFeePaymentAmount = yfpa
                      }
                ]
            , checkoutOverviewTransportPayments =
                [ Entity
                    tpid1
                    TransportPayment
                      { transportPaymentPaymentPlan = tppid1
                      , transportPaymentCheckout = cid
                      , transportPaymentAmount = tpa1
                      }
                , Entity
                    tpid2
                    TransportPayment
                      { transportPaymentPaymentPlan = tppid2
                      , transportPaymentCheckout = cid
                      , transportPaymentAmount = tpa2
                      }
                ]
            , checkoutOverviewOccasionalTransportPayments =
                [ Entity
                    otpid1
                    OccasionalTransportPayment
                      { occasionalTransportPaymentSignup = otsid1
                      , occasionalTransportPaymentCheckout = cid
                      , occasionalTransportPaymentAmount = otpa1
                      }
                , Entity
                    otpid2
                    OccasionalTransportPayment
                      { occasionalTransportPaymentSignup = otsid2
                      , occasionalTransportPaymentCheckout = cid
                      , occasionalTransportPaymentAmount = otpa2
                      }
                ]
            , checkoutOverviewDaycarePayments =
                [ Entity
                    dpid1
                    DaycarePayment
                      { daycarePaymentSignup = toSqlKey 1
                      , daycarePaymentCheckout = cid
                      , daycarePaymentAmount = dca1
                      , daycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 7
                      }
                , Entity
                    dpid2
                    DaycarePayment
                      { daycarePaymentSignup = toSqlKey 2
                      , daycarePaymentCheckout = cid
                      , daycarePaymentAmount = dca2
                      , daycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 6
                      }
                , Entity
                    dpid3
                    DaycarePayment
                      { daycarePaymentSignup = toSqlKey 3
                      , daycarePaymentCheckout = cid
                      , daycarePaymentAmount = dca2
                      , daycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 5
                      }
                ]
            , checkoutOverviewDaycareActivityPayments =
                [ Entity
                    dapid1
                    DaycareActivityPayment
                      { daycareActivityPaymentSignup = toSqlKey 1
                      , daycareActivityPaymentCheckout = cid
                      , daycareActivityPaymentAmount = dca1
                      , daycareActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 10
                      }
                , Entity
                    dapid2
                    DaycareActivityPayment
                      { daycareActivityPaymentSignup = toSqlKey 2
                      , daycareActivityPaymentCheckout = cid
                      , daycareActivityPaymentAmount = dca2
                      , daycareActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 9
                      }
                , Entity
                    dapid3
                    DaycareActivityPayment
                      { daycareActivityPaymentSignup = toSqlKey 3
                      , daycareActivityPaymentCheckout = cid
                      , daycareActivityPaymentAmount = dca2
                      , daycareActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 8
                      }
                , Entity
                    dapid4
                    DaycareActivityPayment
                      { daycareActivityPaymentSignup = toSqlKey 3
                      , daycareActivityPaymentCheckout = cid
                      , daycareActivityPaymentAmount = dca2
                      , daycareActivityPaymentTime = UTCTime (fromGregorian 1970 1 1) 7
                      }
                ]
            , checkoutOverviewOccasionalDaycarePayments =
                [ Entity
                    odcpid1
                    OccasionalDaycarePayment
                      { occasionalDaycarePaymentSignup = odcsid1
                      , occasionalDaycarePaymentCheckout = cid
                      , occasionalDaycarePaymentAmount = otpa1
                      , occasionalDaycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 22
                      }
                , Entity
                    odcpid2
                    OccasionalDaycarePayment
                      { occasionalDaycarePaymentSignup = odcsid2
                      , occasionalDaycarePaymentCheckout = cid
                      , occasionalDaycarePaymentAmount = otpa2
                      , occasionalDaycarePaymentTime = UTCTime (fromGregorian 1970 1 1) 21
                      }
                ]
            , checkoutOverviewDiscounts =
                [ Entity
                    did
                    Discount
                      { discountAccount = aid
                      , discountAmount = da
                      , discountReason = Just "Some reason"
                      , discountCheckout = Just cid
                      , discountTimestamp = UTCTime (fromGregorian 1970 1 1) 4
                      }
                ]
            }
        -- shouldBeValid coo TODO This doesn't seem to work?
    paymentReceivedGoldenTest
      coo
      "test_resources/email/payment-received/1"
      PaymentReceivedEmail
        { paymentReceivedEmailTo = unsafeEmailAddress "example" "domain.com"
        , paymentReceivedEmailAccount = aid
        , paymentReceivedEmailCheckout = toSqlKey 1
        , paymentReceivedEmailTimestamp = UTCTime (fromGregorian 1970 1 1) 11
        , paymentReceivedEmailEmail = Nothing
        }

paymentReceivedGoldenTest :: CheckoutOverview -> String -> PaymentReceivedEmail -> YesodSpec Regtool
paymentReceivedGoldenTest coo path =
  emailGoldenTests path (paymentReceivedEmailTextContent coo) (paymentReceivedEmailHtmlContent coo)
