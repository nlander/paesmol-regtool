{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.CheckoutSpec
  ( spec
  ) where

import TestImport

import Yesod.Test

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils

spec :: RegtoolSpec
spec =
  ydescribe "CheckoutR" $ do
    ydescribe "GET" $ do
      needsLoginSpec GET CheckoutChooseR
      yit "returns a 200 for a verified user" $
        withAnyVerifiedAccountLogin_ $ do
          get CheckoutChooseR
          statusIs 200
    ydescribe "POST" $ needsLoginSpec POST CheckoutChooseR
