{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Signup.DaycareActivity
  ( SignupChildDaycareActivity(..)
  , getSignupDaycareActivityR
  , postSignupDaycareActivityR
  , postSignupDaycareActivitySignoffR
  ) where

import Import

import qualified Data.Set as S
import qualified Data.Text as T

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Utils.Children
import Regtool.Utils.Signup.DaycareActivity
import Regtool.Utils.Time

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildDaycareActivity =
  SignupChildDaycareActivity
    { signupChildDaycareActivityChild :: Entity Child
    , signupChildDaycareActivity :: Entity DaycareActivity
    }
  deriving (Show, Eq, Generic)

instance Validity SignupChildDaycareActivity

signupChildDaycareActivityForm ::
     [Entity Child]
  -> [(Entity DaycareSemestre, Entity DaycareActivity)]
  -> AForm Handler SignupChildDaycareActivity
signupChildDaycareActivityForm cs tups =
  SignupChildDaycareActivity <$>
  areq
    (selectFieldList $
     map (\e@(Entity _ Child {..}) -> (T.unwords [childFirstName, childLastName], e)) cs)
    (bfs ("Child" :: Text))
    Nothing <*>
  areq
    (selectFieldList $ map (\(Entity _ s, e@(Entity _ ts)) -> (activityDescription s ts, e)) tups)
    (bfs ("Activity" :: Text))
    Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  activities <- runDB $ selectList [] [Asc DaycareActivityId]
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurDaycareActivitySignups $ map entityKey children
  let tups = activitiesTups semestres activities
  let ets = signupTrips signups children semestres activities
  token <- genToken
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $ signupChildDaycareActivityForm children tups
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/daycare-activity")

getSignupDaycareActivityR :: Handler Html
getSignupDaycareActivityR = getSignupPage Nothing

postSignupDaycareActivityR :: Handler Html
postSignupDaycareActivityR = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  activities <- runDB $ selectList [] [Asc DaycareActivityId]
  children <- runDB $ getChildrenOf aid
  let tups = activitiesTups semestres activities
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost $ renderCustomForm BootstrapBasicForm $ signupChildDaycareActivityForm children tups
  case result of
    FormSuccess SignupChildDaycareActivity {..} -> do
      let Entity cid c = signupChildDaycareActivityChild
      let Entity dcaid da = signupChildDaycareActivity
      unless (childLevel c `S.member` daycareActivityAccessible da) $
        invalidArgs
          [ "This time slot is not accessible to a child in that level, this activity is only available to the following school levels: " <>
            T.unwords (map schoolLevelText (S.toList (daycareActivityAccessible da)))
          ]
      mdse <- runDB $ getBy (UniqueDaycareSignup cid $ daycareActivityTimeslot da)
      case mdse of
        Nothing -> invalidArgs ["Please sign up for the corresponding daycare timeslot first"]
        Just _ -> pure ()
      let signup =
            DaycareActivitySignup
              { daycareActivitySignupChild = cid
              , daycareActivitySignupActivity = dcaid
              , daycareActivitySignupCreationTime = now
              }
      void $ runDB $ insertValidUnique signup
      setMessage
        "Succesfully signed up for daycareActivity services. Do not forget to pay for them!"
      redirect SignupDaycareActivityR
    fr -> getSignupPage (Just fr)

postSignupDaycareActivitySignoffR :: DaycareActivitySignupId -> Handler Html
postSignupDaycareActivitySignoffR sid = do
  DaycareActivitySignup {..} <- runDB $ get404 sid
    -- TODO make sure that deregistration only works if it's not been paid for
  runDB $ DB.delete sid
  redirect SignupDaycareActivityR
