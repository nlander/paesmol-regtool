StripeCustomer
  account AccountId
  identifier Stripe.CustomerId Maybe
  -- Nothing if it was part of the initial mistake.
  -- A new id will be created if this is 'Nothing'
  -- Eventually there should be no 'Nothing's left.

  UniqueAccountCustomer account

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable



StripePayment
  customer              StripeCustomerId
  amount                Amount
  charge                Stripe.ChargeId
  timestamp             TimeStamp

  UniqueStripePaymentTimestamp customer timestamp
  UniqueStripePaymentCharge charge

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable



Checkout
  account               AccountId
  amount                Amount
  payment               StripePaymentId         Maybe
  timestamp             TimeStamp

  UniqueCheckoutPayment payment !force

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable
  


-- Whenever a yearly fee is paid, a YearlyFeePayment
-- is made to signify this, and it will contain the portion
-- of the stripe payment that is allocated to the yearly fee payment.
YearlyFeePayment
  account               AccountId
  schoolyear            SchoolYear
  checkout              CheckoutId
  amount                Amount
  UniqueYearlyFeePayment account schoolyear

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable


Discount
  account               AccountId
  amount                Amount
  reason                Text        Maybe
  checkout              CheckoutId  Maybe -- Nothing means unused, Just means used.
  timestamp             TimeStamp

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable
