{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Signup.OccasionalDaycare
  ( SignupChildOccasionalDaycare(..)
  , getSignupOccasionalDaycareR
  , postSignupOccasionalDaycareR
  , postSignupOccasionalDaycareSignoffR
  ) where

import Import

import qualified Data.Set as S
import qualified Data.Text as T

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Utils.Time
import Regtool.Utils.Children
import Regtool.Utils.Signup.Daycare
import Regtool.Utils.Signup.OccasionalDaycare

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildOccasionalDaycare =
  SignupChildOccasionalDaycare
    { signupChildOccasionalDaycareChild :: Entity Child
    , signupChildOccasionalDaycareTimeslot :: Entity DaycareTimeslot
    , signupChildOccasionalDaycareDay :: Day
    }
  deriving (Show, Eq, Generic)

instance Validity SignupChildOccasionalDaycare

signupChildOccasionalDaycareForm ::
     [Entity Child]
  -> [(Entity DaycareSemestre, Entity DaycareTimeslot)]
  -> AForm Handler SignupChildOccasionalDaycare
signupChildOccasionalDaycareForm cs tups =
  SignupChildOccasionalDaycare <$>
  areq
    (selectFieldList $
     map (\ce@(Entity _ Child {..}) -> (T.unwords [childFirstName, childLastName], ce)) cs)
    (bfs ("Child" :: Text))
    Nothing <*>
  areq
    (selectFieldList $ map (\(Entity _ s, e@(Entity _ ts)) -> (occasionalTimeslotDescription s ts, e)) tups)
    (bfs ("Timeslot" :: Text))
    Nothing <*>
  areq dayField (bfs ("Day" :: Text)) Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurOccasionalDaycareSignups $ map entityKey children
  let tups = timeslotsTups semestres timeslots
  let ets = occasionalSignupTrips signups children semestres timeslots
  token <- genToken
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $ signupChildOccasionalDaycareForm children tups
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/occasional-daycare")

getSignupOccasionalDaycareR :: Handler Html
getSignupOccasionalDaycareR = getSignupPage Nothing

postSignupOccasionalDaycareR :: Handler Html
postSignupOccasionalDaycareR = do
  aid <- requireAuthId
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  children <- runDB $ getChildrenOf aid
  let tups = timeslotsTups semestres timeslots
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost $
    renderCustomForm BootstrapBasicForm $ signupChildOccasionalDaycareForm children tups
  case result of
    FormSuccess SignupChildOccasionalDaycare {..} -> do
      let Entity cid c = signupChildOccasionalDaycareChild
      let Entity otsid ts = signupChildOccasionalDaycareTimeslot
      unless (childLevel c `S.member` daycareTimeslotAccessible ts) $
        invalidArgs ["This time slot is not accessible to a child in that level"]
      unless (dayMatchesSchoolDay signupChildOccasionalDaycareDay (daycareTimeslotSchoolDay ts)) $
        invalidArgs ["This timeslot does not occur on the selected day."]
      let signup =
            OccasionalDaycareSignup
              { occasionalDaycareSignupChild = cid
              , occasionalDaycareSignupTimeslot = otsid
              , occasionalDaycareSignupDay = signupChildOccasionalDaycareDay
              , occasionalDaycareSignupCreationTime = now
              }
      void $ runDB $ insertValidUnique signup
      setMessage
        "Succesfully signed up for occasional daycare services. Do not forget to pay for them!"
      redirect SignupOccasionalDaycareR
    fr -> getSignupPage (Just fr)

postSignupOccasionalDaycareSignoffR :: OccasionalDaycareSignupId -> Handler Html
postSignupOccasionalDaycareSignoffR sid = do
  aid <- requireAuthId
  OccasionalDaycareSignup {..} <- runDB $ get404 sid
  mcof <-
    runDB $ selectFirst [ChildOfParent ==. aid, ChildOfChild ==. occasionalDaycareSignupChild] []
  case mcof of
    Nothing -> invalidArgs ["This is not your child to unregister."]
    Just _ -> do
      mte <- runDB $ selectFirst [OccasionalDaycarePaymentSignup ==. sid] []
      case mte of
        Just _ -> invalidArgs ["This signup has already been paid for, it cannot be unregistered."]
        Nothing -> do
          runDB $ DB.delete sid
          redirect SignupOccasionalDaycareR
