module Regtool.Core.Widget where

import Import

import Data.Default
import Language.Haskell.TH.Syntax (Exp, Q)

import Yesod.Default.Util
  ( WidgetFileSettings
  , widgetFileNoReload
  , widgetFileReload
  )

import Regtool.Core.Constants

widgetFile :: String -> Q Exp
widgetFile =
  if development
    then widgetFileReload widgetFileSettings
    else widgetFileNoReload widgetFileSettings

widgetFileSettings :: WidgetFileSettings
widgetFileSettings = def
