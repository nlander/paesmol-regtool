{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Gen.Costs.Config where

import GenImport

import Regtool.Data.Gen

import Regtool.Utils.Costs.Config

instance GenUnchecked CostsConfig

instance GenValid CostsConfig where
  genValid =
    let pa = positiveValidAmount
     in CostsConfig <$> pa <*> genListOf pa <*> pa <*> pa
  shrinkValid = shrinkValidStructurally
