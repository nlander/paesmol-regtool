module Regtool.Utils.Time where

import Import

import Regtool.Data

adminTime :: TimeStamp -> String
adminTime = formatTime defaultTimeLocale "%F %R"

formatTimestamp :: TimeOfDay -> String
formatTimestamp = formatTime defaultTimeLocale "%R"
