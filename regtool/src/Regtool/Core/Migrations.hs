{-# LANGUAGE OverloadedStrings #-}

module Regtool.Core.Migrations where

import Import

import qualified Data.Set as S
import Regtool.Data

import Regtool.Core.Foundation
import Regtool.Helper.MigrateVersion

regtoolMigrations :: MonadIO m => ReaderT SqlBackend m ()
regtoolMigrations = do
  setupAdmins
  setupLanguageSections
  setupBusLines
  setup2019Semestres
  runVersionMigration

setupAdmins :: MonadIO m => ReaderT SqlBackend m ()
setupAdmins =
  forM_ admins $ \(username, email, saltedPass) -> do
    macc <- getBy $ UniqueAccountEmail email
    case macc of
      Just _ -> pure ()
      Nothing -> do
        now <- liftIO getCurrentTime
        insertValid_
          Account
            { accountUsername = Just username
            , accountEmailAddress = email
            , accountVerified = True
            , accountVerificationKey = Nothing
            , accountSaltedPassphraseHash = saltedPass
            , accountResetPassphraseKey = Nothing
            , accountCreationTime = now
            }

admins :: [(Text, EmailAddress, Text)]
admins =
  [ ( "admin"
    , unsafeEmailAddress "admin" "paesmol.eu"
    , "sha256|16|W7/73pGKLtSKibiGhjXASA==|pOXOOrrI7seNgXIu9gS0LQrx4JA8Hs+haONfxMJRI14=")
  ]

setupLanguageSections :: MonadIO m => ReaderT SqlBackend m ()
setupLanguageSections = do
  ls <- selectList [] [Asc LanguageSectionId]
  when (null ls) $
    forM_ languageSections $ \name -> do
      mls <- getBy $ UniqueLanguageSection name
      case mls of
        Just _ -> pure ()
        Nothing -> do
          now <- liftIO getCurrentTime
          insertValid_
            LanguageSection {languageSectionName = name, languageSectionCreationTime = now}

languageSections :: [Text]
languageSections = ["Dutch", "English", "German", "French"]

setupBusLines :: MonadIO m => ReaderT SqlBackend m ()
setupBusLines = do
  ls <- selectList [] [Asc BusLineId]
  when (null ls) $
    forM_ buslines $ \(n, d, m, e, w, ss) -> do
      mbl <- getBy $ UniqueBusLineName n
      case mbl of
        Just _ -> pure ()
        Nothing -> do
          now <- liftIO getCurrentTime
          lid <-
            insertValid
              BusLine
                { busLineName = n
                , busLineDescription = Textarea d
                , busLineSeats = 52
                , busLineCreationTime = now
                , busLineEsMolMorningTime = m
                , busLineEsMolEveningTime = e
                , busLineEsMolWednesdayTime = w
                }
          forM_ ss $ \(c, l, m_, e_, w_) -> do
            sid <-
              insertValid
                BusStop
                  { busStopCity = Textarea c
                  , busStopLocation = Textarea l
                  , busStopArchived = False
                  , busStopCreationTime = now
                  }
            insertValid_
              BusLineStop
                { busLineStopLine = lid
                , busLineStopStop = sid
                , busLineStopMorningTime = m_
                , busLineStopEveningTime = e_
                , busLineStopWednesdayTime = w_
                , busLineStopCreationTime = now
                }

buslines :: [(Text, Text, Text, Text, Text, [(Text, Text, Text, Text, Text)])]
buslines =
  [ ( "ESW"
    , "Driver: Kris (0477/432064)"
    , "8u10"
    , "16u25"
    , "12u45"
    , [ ("Eindhoven", "Carpark Zwemcentrum Tongelreep Antoon Coolenlaan", "7u10", "17u25", "13u45")
      , ("Veldhoven", "Parking Hospital Maxima Medisch Centrum", "7u25", "17u10", "13u30")
      , ("Steensel", "Eindhovenseweg (Motel Steensel)", "7u35", "17u05", "13u25")
      , ("Eersel", "Busstaion", "7u40", "17u00", "13u20")
      ])
  , ( "ESD"
    , "Driver: Eren (0477/432061)"
    , "8u15"
    , "16u25"
    , "12u45"
    , [ ("Antwerp", "Loosplaats", "7u05", "17u40", "13u55")
      , ( "Borgerhout"
        , "Plantin-Moretuslei\nMorning: Busstop “Ooststation”\nAfternoon: Busstop Mercedes Garage"
        , "7u10"
        , "17u35"
        , "13u50")
      , ("Grobbendonk", "P+R  E313 Exit 20  Herentalsesteenweg", "7u30", "17u10", "13u30")
      , ("Geel", "P+R  E313 Exit 23  Bell-Telephonelaan 2", "7u50", "16u50 ", "13u10")
      ])
  , ( "ESB"
    , "Driver: Michel (0475/894682)"
    , "8u15"
    , "16u25"
    , "12u45"
    , [ ( "Edegem"
        , "Parking place in front of Pizzahut\n(Prins Boudewijnlaan)"
        , "7u00"
        , "17u40"
        , "13u55")
      , ("Wilrijk / Edegem", "Prins Boudewijnlaan\nBusstop “De Burletlaan”", "", "17u35", "13u50")
      , ( "Antwerp"
        , "Le Grellelaan: bridge over A12\n(Next to Crowne Plaza Hotel)"
        , "7u10"
        , "17u30"
        , "13u45")
      , ("Zoersel", "E34, Exit 20, on the bridge", "7u30 ", "17u05", "13u25 ")
      , ("Turnhout", "E34, Exit 23, Busstop Philips", "7u45", "16u50", "13u10")
      ])
  , ( "ESAC"
    , "Driver: Patrick 0477/432063"
    , "8u15"
    , "16u25"
    , "12u45"
    , [ ( "Kalmthout"
        , "Morning : Car Park Shopping Centre\nWilly Vandersteenplein (Heidestatiestraat)\nEvening: Busstop Kapellensteenweg (nearby crossroads with Beauvoislaan)"
        , "6u45"
        , "17u50"
        , "14u10")
      , ( "Kapellen"
        , "Bellefleur = busstop\nAntwerpsesteenweg/Hoogboomsteenweg"
        , "7u00"
        , "17u35"
        , "13u55")
      , ( "Ekeren-Donk\n(Brasschaat)"
        , "Behind Church =\nSt. Huibrechtlei / Donksesteenweg"
        , "7u05"
        , "17u30"
        , "13u50")
      , ("Brasschaat", "Busstop Kapelsesteenweg 1\n(at Showroom PORTAS)", "", "17u25", "13u45")
      , ("‘s Gravenwezel", "Church", "7u20", "BISTECCA", "BISTECCA")
      , ("Schilde", "Church", "7u27", "BISTECCA", "BISTECCA")
      , ( "Ranst"
        , "Morning: E34, Exit 19, Ranst\nEvening (Ranst + Schilde + ‘s Gravenwezel): Oelegembaan (nearby restaurant ‘Bistecca’)"
        , "7u35"
        , "17u05"
        , "13u25")
      ])
  ]

setup2019Semestres :: MonadIO m => ReaderT SqlBackend m ()
setup2019Semestres = do
  ls <- selectList [] [Asc DaycareSemestreId]
  when (null ls) $
    forM_ semestres $ \(sy, semestre) -> do
      mls <- getBy $ UniqueDaycareSemestre sy semestre
      case mls of
        Just _ -> pure ()
        Nothing -> do
          now <- liftIO getCurrentTime
          i <-
            insertValid
              DaycareSemestre
                { daycareSemestreYear = sy
                , daycareSemestreSemestre = semestre
                , daycareSemestreCreationTime = now
                }
          forM_ timeslots $ \(schoolDay, start, end, fee, occasionalFee, accessible, activities) -> do
            dcts <-
              insertValid
                DaycareTimeslot
                  { daycareTimeslotSemestre = i
                  , daycareTimeslotSchoolDay = schoolDay
                  , daycareTimeslotStart = start
                  , daycareTimeslotEnd = end
                  , daycareTimeslotFee = fee
                  , daycareTimeslotOccasionalFee = occasionalFee
                  , daycareTimeslotAccessible = S.fromList accessible
                  , daycareTimeslotCreationTime = now
                  }
            insertManyValid_ $
              flip map activities $ \(schoolDay_, start_, end_, name_, fee_, accessible_) ->
                DaycareActivity
                  { daycareActivitySemestre = i
                  , daycareActivityTimeslot = dcts
                  , daycareActivitySchoolDay = schoolDay_
                  , daycareActivityStart = start_
                  , daycareActivityEnd = end_
                  , daycareActivityName = name_
                  , daycareActivityFee = fee_
                  , daycareActivityAccessible = S.fromList accessible_
                  , daycareActivityCreationTime = now
                  }

semestres :: [(SchoolYear, Semestre)]
semestres = [(SchoolYear 2019, Fall)]

timeslots ::
     [( SchoolDay
      , TimeOfDay
      , TimeOfDay
      , Amount
      , Amount
      , [SchoolLevel]
      , [(SchoolDay, TimeOfDay, TimeOfDay, Text, Amount, [SchoolLevel])])]
timeslots =
  [ ( Monday
    , TimeOfDay 13 30 0
    , TimeOfDay 16 15 0
    , unsafeAmount 120
    , unsafeAmount 15
    , [Nursery1 .. Primary2]
    , [ ( Monday
        , TimeOfDay 13 30 0
        , TimeOfDay 16 15 0
        , "Dutch"
        , unsafeAmount 35
        , [Nursery3 .. Primary2])
      , ( Monday
        , TimeOfDay 13 30 0
        , TimeOfDay 16 15 0
        , "Piano"
        , unsafeAmount 35
        , [Nursery3 .. Primary2])
      , ( Monday
        , TimeOfDay 13 30 0
        , TimeOfDay 16 15 0
        , "Arts and craft"
        , unsafeAmount 35
        , [Nursery2 .. Primary3])
      ])
  , ( Monday
    , TimeOfDay 16 15 0
    , TimeOfDay 18 00 0
    , unsafeAmount 76
    , unsafeAmount 7.5
    , [Nursery1 .. Secondary2]
    , [])
  , ( Tuesday
    , TimeOfDay 16 15 0
    , TimeOfDay 18 00 0
    , unsafeAmount 76
    , unsafeAmount 7.5
    , [Nursery1 .. Secondary2]
    , [])
  , ( Wednesday
    , TimeOfDay 12 30 0
    , TimeOfDay 18 00 0
    , unsafeAmount 235
    , unsafeAmount 22.5
    , [Nursery1 .. Secondary2]
    , [])
  , ( Thursday
    , TimeOfDay 16 15 0
    , TimeOfDay 18 00 0
    , unsafeAmount 76
    , unsafeAmount 7.5
    , [Nursery1 .. Secondary2]
    , [])
  , ( Friday
    , TimeOfDay 12 30 0
    , TimeOfDay 16 15 0
    , unsafeAmount 160
    , unsafeAmount 15
    , [Nursery1 .. Primary5]
    , [ ( Friday
        , TimeOfDay 12 30 0
        , TimeOfDay 16 15 0
        , "Multi sport"
        , unsafeAmount 35
        , [Primary2 .. Primary5])
      , ( Friday
        , TimeOfDay 12 30 0
        , TimeOfDay 16 15 0
        , "Dutch"
        , unsafeAmount 35
        , [Primary3 .. Primary5])
      , ( Friday
        , TimeOfDay 12 30 0
        , TimeOfDay 16 15 0
        , "Piano"
        , unsafeAmount 35
        , [Nursery3 .. Primary2])
      ])
  , ( Friday
    , TimeOfDay 16 15 0
    , TimeOfDay 18 00 0
    , unsafeAmount 76
    , unsafeAmount 7.5
    , [Nursery1 .. Secondary2]
    , [])
  ]
