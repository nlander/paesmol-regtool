{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Data.Stripe
  ( Stripe.CustomerId
  , Stripe.ChargeId
  , Stripe.TokenId
  ) where

import Import

import Data.Aeson

import Database.Persist
import Database.Persist.Sql

import qualified Web.Stripe.Charge as Stripe (ChargeId(..), TokenId(..))
import qualified Web.Stripe.Customer as Stripe (CustomerId(..))

deriving instance Generic Stripe.TokenId

instance Validity Stripe.TokenId

instance FromJSON Stripe.TokenId

instance ToJSON Stripe.TokenId

deriving instance Generic Stripe.CustomerId

deriving instance PersistField Stripe.CustomerId

deriving instance PersistFieldSql Stripe.CustomerId

instance Validity Stripe.CustomerId

instance ToJSON Stripe.CustomerId

deriving instance Generic Stripe.ChargeId

deriving instance PersistField Stripe.ChargeId

deriving instance PersistFieldSql Stripe.ChargeId

instance Validity Stripe.ChargeId

instance ToJSON Stripe.ChargeId
