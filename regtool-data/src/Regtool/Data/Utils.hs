{-# LANGUAGE FlexibleContexts #-}

module Regtool.Data.Utils where

import Import

import Database.Persist

validateForeignKey :: Eq (Key b) => [Entity a] -> (a -> Key b) -> Key b -> Bool
validateForeignKey vals func foreignVal =
  all (== foreignVal) (map (func . entityVal) vals)

validateForeignKeys ::
     Eq (Key b) => [Entity a] -> (a -> Key b) -> [Entity b] -> Bool
validateForeignKeys vals func = validateForeignKeysM vals (Just . func)

validateForeignKeysM ::
     Eq (Key b) => [Entity a] -> (a -> Maybe (Key b)) -> [Entity b] -> Bool
validateForeignKeysM vals func foreignVals =
  all (`elem` map entityKey foreignVals) (mapMaybe (func . entityVal) vals)

declareUniquenessConstraint ::
     (Show b, Ord b) => String -> [a] -> (a -> b) -> Validation
declareUniquenessConstraint n ls func =
  declare (unlines (n : explanation)) $ validateUniquenessConstraint ls func
  where
    explanation = map show . group . sort . map func $ ls

validateUniquenessConstraint :: Eq b => [a] -> (a -> b) -> Bool
validateUniquenessConstraint ls func = distinct $ map func ls

seperateIds :: Eq (Key a) => [Entity a] -> Bool
seperateIds = distinct . map entityKey

distinct :: Eq a => [a] -> Bool
distinct ls = length (nub ls) == length ls
