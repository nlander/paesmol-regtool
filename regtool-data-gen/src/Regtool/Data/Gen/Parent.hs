{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.Parent where

import Import

import Regtool.Data

import Regtool.Data.Gen.Company ()
import Regtool.Data.Gen.EmailAddress ()
import Regtool.Data.Gen.Country ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.RelationShip ()
import Regtool.Data.Gen.Utils ()

instance GenValid Parent where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
