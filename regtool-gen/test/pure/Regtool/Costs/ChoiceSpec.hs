{-# LANGUAGE TypeApplications #-}

module Regtool.Costs.ChoiceSpec
  ( spec
  ) where

import TestImport

import qualified Data.Map as M

import Regtool.Gen

import Regtool.Utils.Costs.Choice

spec :: Spec
spec = do
  genValidSpec @DaycareChoice
  -- shrinkValidSpecWithLimit @DaycareChoice 100
  -- doesNotShrinkToItselfSpec @DaycareChoice
  jsonSpecOnValid @DaycareChoice
  genValidSpec @OccasionalTransportChoice
  -- shrinkValidSpecWithLimit @OccasionalTransportChoice 100
  -- doesNotShrinkToItselfSpec @OccasionalTransportChoice
  jsonSpecOnValid @OccasionalTransportChoice
  genValidSpec @OccasionalDaycareChoice
  -- shrinkValidSpecWithLimit @OccasionalDaycareChoice 100
  -- doesNotShrinkToItselfSpec @OccasionalDaycareChoice
  jsonSpecOnValid @OccasionalDaycareChoice
  genValidSpec @TransportInstallmentChoices
  -- shrinkValidSpecWithLimit @TransportInstallmentChoices 100
  -- doesNotShrinkToItselfSpec @TransportInstallmentChoices
  jsonSpecOnValid @TransportInstallmentChoices
  genValidSpec @SemestrelyChoices
  -- shrinkValidSpecWithLimit @SemestrelyChoices 100
  -- doesNotShrinkToItselfSpec @SemestrelyChoices
  jsonSpecOnValid @SemestrelyChoices
  genValidSpec @YearlyChoices
  -- shrinkValidSpecWithLimit @YearlyChoices 100
  -- doesNotShrinkToItselfSpec @YearlyChoices
  describe "genNonNullMap" $
    it "generates maps where the values arae not empty" $
    forAll genNonNullMap $ \m ->
      forM_ (M.toList m) $ \(k, l) -> do
        shouldBeValid (k :: Int)
        (l :: [Int]) `shouldNotSatisfy` (== mempty)
  jsonSpecOnValid @YearlyChoices
  genValidSpec @Choices
  -- shrinkValidSpecWithLimit @Choices 100
  -- doesNotShrinkToItselfSpec @Choices
  jsonSpecOnValid @Choices
  describe "deriveChoices" $ it "produces valid cost choices" $ producesValidsOnValids deriveChoices
  describe "deriveYearlyChoices" $
    it "produces valid cost choices" $ producesValidsOnValids deriveYearlyChoices
  describe "deriveSemestrelyChoices" $
    it "produces valid cost choices" $ producesValidsOnValids deriveSemestrelyChoices
  describe "deriveOccasionalTransportChoices" $
    it "produces valid occasional transport choices" $
    producesValidsOnValids deriveOccasionalTransportChoices
  describe "deriveOccasionalDaycareChoices" $
    it "produces valid occasional transport choices" $
    producesValidsOnValids deriveOccasionalDaycareChoices
  describe "deriveDaycareChoices" $
    it "produces valid daycare choices" $ producesValidsOnValids deriveDaycareChoice
