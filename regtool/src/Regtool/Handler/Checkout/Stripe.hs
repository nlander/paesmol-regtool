{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Handler.Checkout.Stripe
  ( StripeForm(..)
  , stripeInputForm
  ) where

import Import

import qualified Web.Stripe.Charge as Stripe

import Regtool.Data

import Regtool.Core.Foundation

data StripeForm =
  StripeForm
    { stripeFormToken :: Stripe.TokenId
    , stripeFormTokenType :: Text
    , stripeFormEmail :: EmailAddress
    }
  deriving (Show, Eq, Generic)

instance Validity StripeForm

stripeTokenField :: Field Handler Stripe.TokenId
stripeTokenField =
  checkMMap
    (pure . (Right :: b -> Either FormMessage b) . Stripe.TokenId)
    (\(Stripe.TokenId t) -> t)
    textField

stripeInputForm :: FormInput Handler StripeForm
stripeInputForm =
  StripeForm <$> ireq stripeTokenField "stripeToken" <*> ireq textField "stripeTokenType" <*>
  ireq emailAddressField "stripeEmail"
