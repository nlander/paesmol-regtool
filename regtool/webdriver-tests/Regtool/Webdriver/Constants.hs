{-# LANGUAGE CPP #-}

module Regtool.Webdriver.Constants
    ( webdriverDevelopment
    , allSupportedBrowsers
    , testPort
    , testDbPath
    ) where

import Test.Hspec.WebDriver

webdriverDevelopment :: Bool
#ifdef DEVELOPMENT
webdriverDevelopment = True
#else
webdriverDevelopment = False
#endif
allSupportedBrowsers :: [Capabilities]
allSupportedBrowsers = [chromeCaps]

testPort :: Int
testPort = 8001

testDbPath :: FilePath
testDbPath = "/tmp/regtool-webdriver-test-database.db"
