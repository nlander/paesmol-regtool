{-# OPTIONS_GHC -Wno-orphans #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Gen.Costs.Input where

import GenImport

import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NE

import Database.Persist

import Regtool.Data
import Regtool.Data.Gen

import Regtool.Utils.Costs.Input

instance GenValid CostsInput where
  genValid = do
    costsInputAccount <- genValid
    costsInputParents <- parentsWithAccount costsInputAccount
    costsInputChildren <- validsWithSeperateIDs
    costsInputTransportSignups <- transportSignupsForChildren $ map entityKey costsInputChildren
    costsInputOccasionalTransportSignups <-
      occasionalTransportSignupsForChildren $ map entityKey costsInputChildren
    costsInputDaycareSemestres <- daycareSemestres
    costsInputDaycareTimeslots <- daycareTimeslotsFor $ map entityKey costsInputDaycareSemestres
    costsInputDaycareActivities <-
      daycareActivitiesFor
        (map entityKey costsInputDaycareSemestres)
        (map entityKey costsInputDaycareTimeslots)
    costsInputDaycareSignups <-
      daycareSignupsFor
        (map entityKey costsInputChildren)
        (map entityKey costsInputDaycareTimeslots)
    costsInputOccasionalDaycareSignups <-
      occasionalDaycareSignupsForChildren $ map entityKey costsInputChildren
    costsInputDaycareActivitySignups <-
      daycareActivitySignupsFor
        (map entityKey costsInputChildren)
        (map entityKey costsInputDaycareActivities)
    (costsInputTransportPaymentPlans, costsInputTransportEnrollments) <-
      enrollmentsAndPlansFor $ map entityKey costsInputTransportSignups
        -- We'll generate the following seemingly backward.
        -- First we make a stripe customer.
    costsInputStripeCustomer <- stripeCustomerWithAccount costsInputAccount
        -- If there is no stripe customer, then there cannot have been any payments
        -- If there is a stripe customer, then there can have been payments.
    costsInputStripePayments <- stripePaymentsWithCustomer $ entityKey <$> costsInputStripeCustomer
        -- Each payment belongs to a checkout, but there can also checkouts without a payment
    costsInputCheckouts <- checkoutsFor costsInputAccount costsInputStripePayments
    let cs = map (\(Entity cid c) -> (cid, checkoutAmount c)) costsInputCheckouts
    case NE.nonEmpty cs of
      Nothing -> do
        let costsInputTransportPayments = []
            costsInputOccasionalTransportPayments = []
            costsInputYearlyFeePayments = []
            costsInputDaycarePayments = []
            costsInputOccasionalDaycarePayments = []
            costsInputDaycareActivityPayments = []
        costsInputDiscounts <- discountsWithAccount costsInputAccount []
        pure CostsInput {..}
      Just ne
                -- Each checkout can contain a number of transport payments
       -> do
        (cs', costsInputTransportPayments) <-
          transportPaymentsFor ne (map entityKey costsInputTransportPaymentPlans)
                -- ... and occasional transport payments ...
        (cs'', costsInputOccasionalTransportPayments) <-
          occasionalTransportPayments cs' (map entityKey costsInputOccasionalTransportSignups)
                -- ... and daycare payments
        (cs''', costsInputDaycarePayments) <-
          daycarePayments cs'' (map entityKey costsInputDaycareSignups)
                -- ... and yearly fee payments
        (cs'''', costsInputOccasionalDaycarePayments) <-
          occasionalDaycarePayments cs''' (map entityKey costsInputOccasionalDaycareSignups)
        (cs''''', costsInputDaycareActivityPayments) <-
          daycareActivityPayments cs'''' (map entityKey costsInputDaycareActivitySignups)
                -- ... and occasional daycare payments ...
        (cs'''''', costsInputYearlyFeePayments) <- yearlyFeePayments costsInputAccount cs'''''
                -- Whatever amount was left, must have been discounts.
        costsInputDiscounts <- discountsWithAccount costsInputAccount $ NE.toList cs''''''
        pure CostsInput {..}
  shrinkValid ci =
    filter isValid $ do
      costsInputAccount <- shrinkValid $ costsInputAccount ci
      costsInputParents <- shrinkValidWithSeperateIds $ costsInputParents ci
      costsInputChildren <- shrinkValidWithSeperateIds $ costsInputChildren ci
      costsInputTransportSignups <- shrinkValidWithSeperateIds $ costsInputTransportSignups ci
      costsInputCheckouts <- shrinkValidWithSeperateIds $ costsInputCheckouts ci
      costsInputStripeCustomer <- shrinkValid $ costsInputStripeCustomer ci
      costsInputStripePayments <- shrinkValidWithSeperateIds $ costsInputStripePayments ci
      costsInputYearlyFeePayments <- shrinkValidWithSeperateIds $ costsInputYearlyFeePayments ci
      costsInputTransportPaymentPlans <-
        shrinkValidWithSeperateIds $ costsInputTransportPaymentPlans ci
      costsInputTransportPayments <- shrinkValidWithSeperateIds $ costsInputTransportPayments ci
      costsInputTransportEnrollments <-
        shrinkValidWithSeperateIds $ costsInputTransportEnrollments ci
      costsInputOccasionalTransportSignups <-
        shrinkValidWithSeperateIds $ costsInputOccasionalTransportSignups ci
      costsInputOccasionalTransportPayments <-
        shrinkValidWithSeperateIds $ costsInputOccasionalTransportPayments ci
      costsInputDaycareSemestres <- shrinkValidWithSeperateIds $ costsInputDaycareSemestres ci
      costsInputDaycareTimeslots <- shrinkValidWithSeperateIds $ costsInputDaycareTimeslots ci
      costsInputDaycareSignups <- shrinkValidWithSeperateIds $ costsInputDaycareSignups ci
      costsInputDaycarePayments <- shrinkValidWithSeperateIds $ costsInputDaycarePayments ci
      costsInputOccasionalDaycareSignups <-
        shrinkValidWithSeperateIds $ costsInputOccasionalDaycareSignups ci
      costsInputOccasionalDaycarePayments <-
        shrinkValidWithSeperateIds $ costsInputOccasionalDaycarePayments ci
      costsInputDaycareActivities <- shrinkValidWithSeperateIds $ costsInputDaycareActivities ci
      costsInputDaycareActivitySignups <-
        shrinkValidWithSeperateIds $ costsInputDaycareActivitySignups ci
      costsInputDaycareActivityPayments <-
        shrinkValidWithSeperateIds $ costsInputDaycareActivityPayments ci
      costsInputDiscounts <- shrinkValidWithSeperateIds $ costsInputDiscounts ci
      pure CostsInput {..}

parentsWithAccount :: AccountId -> Gen [Entity Parent]
parentsWithAccount aid = modEntL (\p -> p {parentAccount = aid}) <$> validsWithSeperateIDs

stripeCustomerWithAccount :: AccountId -> Gen (Maybe (Entity StripeCustomer))
stripeCustomerWithAccount aid = fmap (modEnt (\sc -> sc {stripeCustomerAccount = aid})) <$> genValid

stripePaymentsWithCustomer :: Maybe StripeCustomerId -> Gen [Entity StripePayment]
stripePaymentsWithCustomer Nothing = pure []
stripePaymentsWithCustomer (Just i) = do
  charges <- genValid
  if null charges
    then pure []
    else fmap makeStripePaymentsDistinct $
         genValidsWithSeperateIDs $ do
           let stripePaymentCustomer = i
                -- Must have a positive amount
           stripePaymentAmount <- scale (* 10) strictlyPositiveValidAmount
           stripePaymentCharge <- elements charges
           stripePaymentTimestamp <- genValid
           pure StripePayment {..}
  where
    makeStripePaymentsDistinct =
      nubBy ((==) `on` (\(Entity _ StripePayment {..}) -> stripePaymentCharge)) .
      nubBy
        ((==) `on`
         (\(Entity _ StripePayment {..}) -> (stripePaymentCustomer, stripePaymentTimestamp)))

enrollmentsAndPlansFor ::
     [TransportSignupId] -> Gen ([Entity TransportPaymentPlan], [Entity TransportEnrollment])
enrollmentsAndPlansFor signupIds = do
  tppids <- genSeperateIds
  tpeids <- genSeperateIds
  fmap (unzip . catMaybes) $
    forM (zip (zip tppids tpeids) signupIds) $ \((tppid, tpeid), sid) -> do
      b <- genValid
      if b
        then pure Nothing
        else do
          a <- absAmount <$> genValid `suchThat` (> zeroAmount)
          let tpp = TransportPaymentPlan {transportPaymentPlanTotalAmount = a}
          let tpe =
                TransportEnrollment
                  {transportEnrollmentSignup = sid, transportEnrollmentPaymentPlan = tppid}
          pure $ Just (Entity tppid tpp, Entity tpeid tpe)

checkoutsFor :: AccountId -> [Entity StripePayment] -> Gen [Entity Checkout]
checkoutsFor aid spes
    -- A list of checkouts that have a stripe payment attached to it
 = do
  cs1 <-
    forM spes $ \(Entity spid StripePayment {..}) -> do
      let checkoutAccount = aid
      let checkoutAmount = stripePaymentAmount
      let checkoutPayment = Just spid
      checkoutTimestamp <- genValid
      pure Checkout {..}
    -- A list of checkouts that doesn't have a stripe payment attached to it
  cs2 <-
    genListOf $ do
      let checkoutAccount = aid
            -- Must have a negative amount
      checkoutAmount <- negateAmount . absAmount <$> strictlyPositiveValidAmount
      let checkoutPayment = Nothing
      checkoutTimestamp <- genValid
      pure Checkout {..}
  genSeperateIdsFor $ cs1 ++ cs2

transportPaymentsFor ::
     NonEmpty (CheckoutId, Amount) -- Checkouts, and their total amount
  -> [TransportPaymentPlanId]
  -> Gen (NonEmpty (CheckoutId, Amount), [Entity TransportPayment])
    -- Checkouts and the amount left for them. This can be negative. In that case we will need discounts.
transportPaymentsFor checkouts [] = pure (checkouts, [])
transportPaymentsFor checkouts paymentPlanIds = do
  tups <-
    forM checkouts $ \(cid, totalAmount) -> do
      (transportPaymentsAmount, leftover) <- choosePartialAmountFromCheckout totalAmount
      as <- splitAmounts transportPaymentsAmount
      tpps <- go cid as
      pure ((cid, leftover), tpps)
  let (cids', tpps) = NE.unzip tups
  tppes <- genSeperateIdsFor $ mergePayments $ concat tpps
  pure (cids', tppes)
  where
    go cid as =
      forM as $ \a -> do
        ppid <- elements paymentPlanIds
        pure
          TransportPayment
            { transportPaymentPaymentPlan = ppid
            , transportPaymentCheckout = cid
            , transportPaymentAmount = a
            }
    mergePayments :: [TransportPayment] -> [TransportPayment]
    mergePayments = map squash . groupBy ((==) `on` func) . sortOn func
      where
        func TransportPayment {..} = (transportPaymentCheckout, transportPaymentPaymentPlan)
        squash :: [TransportPayment] -> TransportPayment
        squash tps =
          TransportPayment
            { transportPaymentPaymentPlan = transportPaymentPaymentPlan $ head tps
            , transportPaymentCheckout = transportPaymentCheckout $ head tps
            , transportPaymentAmount = sumAmount $ map transportPaymentAmount tps
            }

occasionalTransportPayments ::
     NonEmpty (CheckoutId, Amount) -- Checkouts, and their leftover amount
  -> [OccasionalTransportSignupId]
  -> Gen (NonEmpty (CheckoutId, Amount), [Entity OccasionalTransportPayment])
occasionalTransportPayments checkouts signupIds = do
  let go (cs, otps) signupId = do
        b <- genValid
        if b
          then pure (cs, otps)
          else do
            (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
            pure
              ( cs'
              , OccasionalTransportPayment
                  { occasionalTransportPaymentSignup = signupId
                  , occasionalTransportPaymentCheckout = cid
                  , occasionalTransportPaymentAmount = a
                  } :
                otps)
  (checkouts', otps) <- foldM go (checkouts, []) signupIds
  otpes <- genSeperateIdsFor otps
  pure (checkouts', otpes)

chooseCheckoutAndPartialAmount ::
     NonEmpty (CheckoutId, Amount) -> Gen (NonEmpty (CheckoutId, Amount), (CheckoutId, Amount))
chooseCheckoutAndPartialAmount checkouts = do
  checkouts' <- shuffleNE checkouts
  case checkouts' of
    ((cid, a) :| rest) -> do
      let chooseAmount = do
            (chosenAmount, theRest) <- choosePartialAmountFromCheckout a
            pure ((cid, theRest) :| rest, (cid, chosenAmount))
      if a == zeroAmount -- If the amount is zero, maybe just leave it and try again
        then oneof [chooseCheckoutAndPartialAmount checkouts', chooseAmount]
        else chooseAmount

choosePartialAmountFromCheckout :: Amount -> Gen (Amount, Amount) -- First: chosen, second: leftover
choosePartialAmountFromCheckout a =
  if a > zeroAmount -- If the amount is positive, then we can use either a part, or the whole thing
    then oneof
           [ pure (a, zeroAmount) -- Use the whole thing
           , do (a1, a2) <- splitAmount a -- Use only a part
                pure (a2, a1)
           ]
        -- If the amount is negative, we can use anything and compensate with discounts
    else do
      a' <- strictlyPositiveValidAmount
      pure (a', a `subAmount` a')

chooseCheckoutAndTotalAmount ::
     NonEmpty (CheckoutId, Amount) -> Gen (NonEmpty (CheckoutId, Amount), (CheckoutId, Amount))
chooseCheckoutAndTotalAmount checkouts = do
  checkouts' <- shuffleNE checkouts
  case checkouts' of
    ((cid, a) :| rest) -> do
      let chooseAmount = do
            (chosenAmount, theRest) <- chooseTotalAmountFromCheckout a
            pure ((cid, theRest) :| rest, (cid, chosenAmount))
      if a == zeroAmount -- If the amount is zero, maybe just leave it and try again
        then oneof [chooseCheckoutAndPartialAmount checkouts', chooseAmount]
        else chooseAmount

chooseTotalAmountFromCheckout :: Amount -> Gen (Amount, Amount) -- First: chosen, second: leftover
chooseTotalAmountFromCheckout a =
  if a > zeroAmount -- If the amount is positive then we must use the whole thing
    then pure (a, zeroAmount) -- Use the whole thing
        -- If the amount is negative, we can use anything and compensate with discounts
    else do
      a' <- strictlyPositiveValidAmount
      pure (a', a `subAmount` a')

shuffleNE :: NonEmpty a -> Gen (NonEmpty a)
shuffleNE = fmap (fromJust . NE.nonEmpty) . shuffle . NE.toList

daycareSemestres :: Gen [Entity DaycareSemestre]
daycareSemestres = do
  tss <- genValid
  tups <- genValidSeperateFor tss
  genSeperateIdsFor $
    flip map tups $ \((sy, sem), ts) ->
      DaycareSemestre
        {daycareSemestreYear = sy, daycareSemestreSemestre = sem, daycareSemestreCreationTime = ts}

daycareTimeslotsFor :: [DaycareSemestreId] -> Gen [Entity DaycareTimeslot]
daycareTimeslotsFor ids =
  (>>= genSeperateIdsFor) $
  fmap concat $ forM ids $ \i -> map (\dcts -> dcts {daycareTimeslotSemestre = i}) <$> genValid

daycareActivitiesFor :: [DaycareSemestreId] -> [DaycareTimeslotId] -> Gen [Entity DaycareActivity]
daycareActivitiesFor ids dtsids =
  (>>= genSeperateIdsFor) $
  fmap (catMaybes. concat) $
  forM ids $ \i -> do
    tries <- map (\dcts -> dcts {daycareActivitySemestre = i}) <$> genValid
    forM tries $ \dcts -> do
      case dtsids of
        [] -> pure Nothing
        _ -> do
          tsid <- elements dtsids
          pure $ Just $ dcts {daycareActivityTimeslot = tsid}

daycareSignupsFor :: [ChildId] -> [DaycareTimeslotId] -> Gen [Entity DaycareSignup]
daycareSignupsFor cids dctsids =
  (>>= genSeperateIdsFor) $
  fmap catMaybes $
  forM ((,) <$> cids <*> dctsids) $ \(cid, dctsid) ->
    frequency
      [ (3, pure Nothing)
      , ( 1
        , do ts <- genValid
             pure $
               Just
                 DaycareSignup
                   { daycareSignupChild = cid
                   , daycareSignupTimeslot = dctsid
                   , daycareSignupCreationTime = ts
                   })
      ]

daycarePayments ::
     NonEmpty (CheckoutId, Amount)
  -> [DaycareSignupId]
  -> Gen (NonEmpty (CheckoutId, Amount), [Entity DaycarePayment])
daycarePayments checkouts signupIds = do
  let go (cs, dcps) signupId =
        frequency
          [ (3, pure (cs, dcps))
          , ( 1
            , do (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
                 now <- genValid
                 pure
                   ( cs'
                   , DaycarePayment
                       { daycarePaymentSignup = signupId
                       , daycarePaymentCheckout = cid
                       , daycarePaymentAmount = a
                       , daycarePaymentTime = now
                       } :
                     dcps))
          ]
  (checkouts', dcps) <- foldM go (checkouts, []) signupIds
  dcpes <- genSeperateIdsFor dcps
  pure (checkouts', dcpes)

occasionalDaycarePayments ::
     NonEmpty (CheckoutId, Amount) -- Checkouts, and their leftover amount
  -> [OccasionalDaycareSignupId]
  -> Gen (NonEmpty (CheckoutId, Amount), [Entity OccasionalDaycarePayment])
occasionalDaycarePayments checkouts signupIds = do
  let go (cs, otps) signupId = do
        b <- genValid
        if b
          then pure (cs, otps)
          else do
            (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
            now <- genValid
            pure
              ( cs'
              , OccasionalDaycarePayment
                  { occasionalDaycarePaymentSignup = signupId
                  , occasionalDaycarePaymentCheckout = cid
                  , occasionalDaycarePaymentAmount = a
                  , occasionalDaycarePaymentTime = now
                  } :
                otps)
  (checkouts', otps) <- foldM go (checkouts, []) signupIds
  otpes <- genSeperateIdsFor otps
  pure (checkouts', otpes)

daycareActivitySignupsFor :: [ChildId] -> [DaycareActivityId] -> Gen [Entity DaycareActivitySignup]
daycareActivitySignupsFor cids dctsids =
  (>>= genSeperateIdsFor) $
  fmap catMaybes $
  forM ((,) <$> cids <*> dctsids) $ \(cid, dcaid) ->
    frequency
      [ (3, pure Nothing)
      , ( 1
        , do ts <- genValid
             pure $
               Just
                 DaycareActivitySignup
                   { daycareActivitySignupChild = cid
                   , daycareActivitySignupActivity = dcaid
                   , daycareActivitySignupCreationTime = ts
                   })
      ]

daycareActivityPayments ::
     NonEmpty (CheckoutId, Amount)
  -> [DaycareActivitySignupId]
  -> Gen (NonEmpty (CheckoutId, Amount), [Entity DaycareActivityPayment])
daycareActivityPayments checkouts signupIds = do
  let go (cs, dcps) signupId =
        frequency
          [ (3, pure (cs, dcps))
          , ( 1
            , do (cs', (cid, a)) <- chooseCheckoutAndPartialAmount cs
                 now <- genValid
                 pure
                   ( cs'
                   , DaycareActivityPayment
                       { daycareActivityPaymentSignup = signupId
                       , daycareActivityPaymentCheckout = cid
                       , daycareActivityPaymentAmount = a
                       , daycareActivityPaymentTime = now
                       } :
                     dcps))
          ]
  (checkouts', dcps) <- foldM go (checkouts, []) signupIds
  dcpes <- genSeperateIdsFor dcps
  pure (checkouts', dcpes)

yearlyFeePayments ::
     AccountId
  -> NonEmpty (CheckoutId, Amount)
  -> Gen (NonEmpty (CheckoutId, Amount), [Entity YearlyFeePayment])
yearlyFeePayments aid checkouts = do
  sytups <- genValidSeperateForNE checkouts
  tups <-
    forM sytups $ \(sy, (cid, a)) -> do
      (yearlyFeePaymentAmount, leftover) <- chooseTotalAmountFromCheckout a
      pure
        ( (cid, leftover)
        , YearlyFeePayment
            { yearlyFeePaymentSchoolyear = sy
            , yearlyFeePaymentAccount = aid
            , yearlyFeePaymentCheckout = cid
            , yearlyFeePaymentAmount = yearlyFeePaymentAmount
            })
  let (cids', yfps) = NE.unzip tups
  yfpes <- genSeperateIdsForNE yfps
  pure (cids', NE.toList yfpes)

-- Note, the checkouts with their amounts must be negative
discountsWithAccount :: AccountId -> [(CheckoutId, Amount)] -> Gen [Entity Discount]
discountsWithAccount aid checkouts
    -- Generate discounts for the checkouts that aren't zero now
 = do
  ds1 <-
    fmap catMaybes $
    forM checkouts $ \(cid, amount) ->
      if amount >= zeroAmount
        then if amount == zeroAmount
               then pure Nothing
               else error "cannot possibly get down to zero this way"
        else Just <$> do
               let discountAmount = negateAmount amount
               let discountAccount = aid
               discountReason <- genValid
               let discountCheckout = Just cid
               discountTimestamp <- genValid
               pure Discount {..}
  ds2 <- map (\d -> d {discountAccount = aid, discountCheckout = Nothing}) <$> genValid
    -- Generate discounts that haven't been used
  genSeperateIdsFor $ ds1 ++ ds2
