{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Helper.VerifyDatabase
  ( verifyDatabase
  , failIfVerificationFailed
  , logVerificationError
  , VerifyResult(..)
  , verifyDatabaseWithResult
  ) where

import Import

import Control.Monad.Logger
import Control.Monad.Trans.Resource (runResourceT)
import qualified Data.List.NonEmpty as NE
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.Text as T

import Database.Persist.Sqlite

import Regtool.Data
import Regtool.OptParse

verifyDatabase :: VerifyDatabaseSettings -> IO ()
verifyDatabase sets = do
  let dbf = verifyDababaseSetsDbFile sets
  runStderrLoggingT $ do
    res <-
      withSqlitePool (T.pack $ toFilePath dbf) 1 $ \pool ->
        liftIO $
        runResourceT $
        flip runSqlPool pool $ do
          runMigration migrateAll
          verifyDatabaseWithResult
    failIfVerificationFailed res

failIfVerificationFailed :: MonadLoggerIO m => VerifyResult -> m ()
failIfVerificationFailed res =
  case res of
    VerifiedFine -> pure ()
    VerificationErrors ws -> do
      mapM_ logVerificationError ws
      liftIO $ die "Invalid database detected."

logVerificationError :: MonadLogger m => ValidityError -> m ()
logVerificationError (ValidityError typ i s) =
  logErrorN $
  T.unlines
    [ "WARNING, invalid value detected in database:"
    , T.unwords ["type:", typ]
    , T.unwords ["id:", T.pack $ show i]
    , T.unwords ["value:", s]
    ]

data VerifyResult
  = VerifiedFine
  | VerificationErrors (NonEmpty ValidityError)
  deriving (Show, Eq)

verifyDatabaseWithResult :: MonadIO m => ReaderT SqlBackend m VerifyResult
verifyDatabaseWithResult = do
  ws <- verifyDatabaseAction
  pure $
    case NE.nonEmpty ws of
      Nothing -> VerifiedFine
      Just ne -> VerificationErrors ne

data ValidityError =
  ValidityError Text Int64 Text
  deriving (Show, Eq)

verifyDatabaseAction :: MonadIO m => ReaderT SqlBackend m [ValidityError]
verifyDatabaseAction =
  concat <$>
  sequence
    [ warnInvalidForDBTable @Account
    , warnInvalidForDBTable @BusLine
    , warnInvalidForDBTable @BusStop
    , warnInvalidForDBTable @BusLineStop
    , warnInvalidForDBTable @Checkout
    , warnInvalidForDBTable @Discount
    , warnInvalidForDBTable @TransportSignup
    , warnInvalidForDBTable @TransportPayment
    , warnInvalidForDBTable @TransportPaymentPlan
    , warnInvalidForDBTable @TransportEnrollment
    , warnInvalidForDBTable @OccasionalTransportSignup
    , warnInvalidForDBTable @OccasionalTransportPayment
    , warnInvalidForDBTable @Child
    , warnInvalidForDBTable @ChildOf
    , warnInvalidForDBTable @Doctor
    , warnInvalidForDBTable @Email
    , warnInvalidForDBTable @LanguageSection
    , warnInvalidForDBTable @Parent
    , warnInvalidForDBTable @PasswordResetEmail
    , warnInvalidForDBTable @PaymentReceivedEmail
    , warnInvalidForDBTable @StripeCustomer
    , warnInvalidForDBTable @StripePayment
    , warnInvalidForDBTable @YearlyFeePayment
    , warnInvalidForDBTable @Discount
    , warnInvalidForDBTable @VerificationEmail
    , warnInvalidForDBTable @DaycareSemestre
    , warnInvalidForDBTable @DaycareTimeslot
    , warnInvalidForDBTable @DaycareSignup
    , warnInvalidForDBTable @DaycarePayment
    , warnInvalidForDBTable @OccasionalDaycareSignup
    , warnInvalidForDBTable @OccasionalDaycarePayment
    , warnInvalidForDBTable @DaycareActivity
    , warnInvalidForDBTable @DaycareActivitySignup
    , warnInvalidForDBTable @DaycareActivityPayment
    ]

warnInvalidForDBTable ::
     forall record m.
     ( PersistEntity record
     , PersistEntityBackend record ~ SqlBackend
     , ToBackendKey SqlBackend record
     , Validity record
     , Show record
     , MonadIO m
     )
  => ReaderT SqlBackend m [ValidityError]
warnInvalidForDBTable = do
  let def = entityDef (pure undefined :: IO record)
  concatMap (warnInvalid $ unDBName $ entityDB def) <$>
    selectList [] [Asc (persistIdField :: EntityField record (Key record))]

warnInvalid ::
     (Validity a, Show a, ToBackendKey SqlBackend a) => Text -> Entity a -> [ValidityError]
warnInvalid typ (Entity aid a) =
  [ValidityError typ (fromSqlKey aid) (T.pack $ ppShow a) | not $ isValid a]
