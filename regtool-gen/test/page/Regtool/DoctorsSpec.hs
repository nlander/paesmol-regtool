{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.DoctorsSpec
  ( spec
  ) where

import TestImport

import Database.Persist.Sqlite (toSqlKey)
import Database.Persist.Sqlite hiding (get)

import Yesod.Test

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Doctors.Utils
import Regtool.Gen ()
import Regtool.TestUtils

spec :: RegtoolSpec
spec = do
  ydescribe "RegisterDoctorR" $ do
    needsLoginSpec GET RegisterDoctorR
    needsLoginSpec POST RegisterDoctorR
    multiYit "Registers a doctor succesfully" $ \i -> do
      let rd@RegisterDoctor {..} =
            generateDeterministicallyWithSeed i genSimpleRegisterDoctor
      withAnyVerifiedAccountLogin $ \(Entity aid _) -> do
        registerDoctor rd
        verifyDoctorRegistered aid rd
  ydescribe "DoctorsR" $ do
    needsLoginSpec GET RegisterDoctorR
    yit "returns a 200 for an unverified user" $
      withAnyVerifiedAccountLogin_ $ do
        get DoctorsR
        statusIs 200
    yit "returns a 200 for a verified user" $
      withAnyVerifiedAccountLogin_ $ do
        get DoctorsR
        statusIs 200
  ydescribe "DoctorR" $ needsLoginSpec GET $ DoctorR $ toSqlKey 0
