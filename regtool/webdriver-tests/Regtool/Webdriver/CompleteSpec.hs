{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Webdriver.CompleteSpec
    ( completeSpec
    ) where

import TestImport

import qualified Data.Text as T

import Control.Lens

import Database.Persist.Sqlite

import Test.Hspec
import Test.Hspec.WebDriver
import qualified Test.Hspec.WebDriver as WD
import Test.WebDriver.Commands.Wait

import Control.Monad.Logger (runNoLoggingT)

import Regtool.Application ()
import Regtool.Core.Foundation hiding (runDB)
import Regtool.Data

import Regtool.Webdriver.Constants
import Regtool.Webdriver.Utils

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

-- Don't care that this is slow for now
runDB :: SqlPersistM a -> IO a
runDB func = do
    testDBFile <- resolveFile' testDbPath
    let opts =
            mkSqliteConnectionInfo (T.pack $ toFilePath testDBFile) &
            walEnabled .~ False &
            fkEnabled .~ False
    pool <- runNoLoggingT $ createSqlitePoolFromInfo opts 1
    runSqlPersistMPool func pool

completeSpec :: MVar (Async ()) -> Spec
completeSpec mv =
    describe "Checkout Tests" $ do
        exampleEmail <-
            case emailAddressFromString
                     "this-should-be-a-random-string@example.com" of
                Nothing -> error "invalid email address."
                Just ea -> pure ea
        let pass = "examplepass"
        session "for checkout" $
            using allSupportedBrowsers $ do
                wit "opens the home page" $ do
                    openPage $ renderTestRoute HomeR
                    waitABit
                wit "registers an account" $
                    checkpoint "registration" mv $ do
                        clickButton $ ByLinkText "Login"
                        clickButton $ ByLinkText "Register"
                        fill "email" $ emailAddressText exampleEmail
                        fill "passphrase" pass
                        fill "passphrase-confirm" pass
                        clickButton $ ById "register-submit"
                wit "has now registered an account" $ do
                    ment <-
                        liftIO $ runDB $ getBy $ UniqueAccountEmail exampleEmail
                    ((accountVerified . entityVal) <$> ment) `WD.shouldBe`
                        Just False
                wit "verifies the account and logs out" $
                    checkpoint "verification" mv $ do
                        ment <-
                            liftIO $
                            runDB $ getBy $ UniqueAccountEmail exampleEmail
                        (Entity _ Account {..}) <-
                            case ment of
                                Nothing -> error "should have found it"
                                Just e -> pure e
                        vk <-
                            case accountVerificationKey of
                                Nothing ->
                                    error "should have had a verificationKey"
                                Just vk -> pure vk
                        openPage $
                            renderTestRoute $ AuthR $ verifyR exampleEmail vk
                        waitABit
                        waitABit
                        waitABit
                        waitABit
                        waitABit
                        clickButton $ ByLinkText "Logout"
                        waitABit
                wit "has now verified the account" $ do
                    ment <-
                        liftIO $ runDB $ getBy $ UniqueAccountEmail exampleEmail
                    ((accountVerified . entityVal) <$> ment) `WD.shouldBe`
                        Just True
                wit "opens the home page and logs in" $ do
                    openPage $ renderTestRoute HomeR
                    clickButton $ ByLinkText "Login"
                    fill "userkey" $ emailAddressText exampleEmail
                    fill "passphrase" pass
                    clickButton $ ById "login-submit"
                wit "registers a parent" $
                    checkpoint "parent-registration" mv $ do
                        clickButton $ ByLinkText "Parents"
                        clickButton $ ByLinkText "Register a parent"
                        fill "f1" "first name"
                        fill "f2" "last name"
                        fill "f3" "Address line 1"
                        fill "f4" ""
                        fill "f5" "2400"
                        fill "f6" "Mol"
                        fill "f7" "Belgium"
                        fill "f8" "+32474277870"
                        fill "f9" ""
                        fill "f10" "example@email.com"
                        fill "f11" ""
                        fill "f12" ""
                        fill "f13" "Other"
                        fill "f14" ""
                        fill "f15" ""
                        waitABit
                        clickButton $ ByTag "button"
                        clickButton $ ByLinkText "Overview"
                wit "registers a doctor" $
                    checkpoint "doctor-registration" mv $ do
                        clickButton $ ByLinkText "Doctors"
                        clickButton $ ByLinkText "Register a doctor"
                        fill "f1" "first name"
                        fill "f2" "last name"
                        fill "f3" "Address line 1"
                        fill "f4" ""
                        fill "f5" "2400"
                        fill "f6" "Mol"
                        fill "f7" "Belgium"
                        fill "f8" "+32474277870"
                        fill "f9" ""
                        fill "f10" "example@email.com"
                        fill "f11" ""
                        fill "f12" ""
                        waitABit
                        clickButton $ ByTag "button"
                        clickButton $ ByLinkText "Overview"
                wit "registers a child" $
                    checkpoint "child-registration" mv $ do
                        clickButton $ ByLinkText "Children"
                        clickButton $ ByLinkText "Register a child"
                        fill "f1" "first name"
                        fill "f2" "last name"
                        fill "f3" "25/07/1994"
                        fill "f4" ""
                        fill "f5" ""
                        fill "f6" ""
                        fill "f7" ""
                        fill "f8" ""
                        fill "f9" ""
                        fill "f10" "example@email.com"
                        fill "f11" ""
                        fill "f12" ""
                        fill "f13" ""
                        fill "f14" ""
                        fill "f15" ""
                        waitABit
                        clickButton $ ByTag "button"
                        clickButton $ ByLinkText "Overview"
                wit "signs a child up for transport" $
                    checkpoint "transport-enrollment" mv $ do
                        clickButton $ ByLinkText "Transport"
                        fill "f1" ""
                        fill "f2" ""
                        fill "f3" ""
                        clickButton $ ByTag "button"
                wit "clicks the 'home' button" $
                    clickButton $ ByLinkText "Overview"
                wit "clicks the 'checkout' button" $
                    clickButton $ ByLinkText "Checkout"
                wit "completes the checkout form" $ do
                    clickButton $ ByTag "button"
                    focusFrame (WithName "stripe_checkout_app")
                    e1 <-
                        findElem (ByXPath "//input[@placeholder='Card number']")
                    click e1
                    sendKeys "5555555555554444" e1
                    e2 <- findElem (ByXPath "//input[@placeholder='MM / YY']")
                    click e2
                    sendKeys "08/20" e2
                    e3 <- findElem (ByXPath "//input[@placeholder='CVC']")
                    click e3
                    sendKeys "456" e3
                    clickButton $ ByXPath "//button[@type='submit']"
                    waitUntil 5.0 $ clickButton $ ByLinkText "Overview"
                    clickButton $ ByLinkText "Payments"
                    replicateM_ 10 waitABit
