{-# LANGUAGE OverloadedStrings #-}

module Regtool.HomeSpec
  ( spec
  ) where

import TestImport

import Yesod.Test

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils

spec :: RegtoolSpec
spec =
  ydescribe "Home" $ do
    yit "returns a 200 if no user is logged in" $ do
      get HomeR
      statusIs 200
    yit "redirects to the overview for an unverified user" $
      withAnyVerifiedAccountLogin_ $ do
        get HomeR
        shouldRedirectTo OverviewR
    yit "redirects to the overview for a verified user" $
      withAnyVerifiedAccountLogin_ $ do
        get HomeR
        shouldRedirectTo OverviewR
