{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool.Component.NavigationBar where

import Import

import Yesod.Core
import Yesod.Form

import Regtool.Core.Foundation

import Regtool.Utils.Readiness

makeNavigationBarWidget :: Handler RegtoolWidget
makeNavigationBarWidget = do
  admin <- isAdmin
  bar <-
    case admin of
      Just True -> pure $(widgetFile "components/navigation-bar/admin")
      Nothing -> pure $(widgetFile "components/navigation-bar/public")
      Just False -> do
        aid <- requireAuthId
        Readiness {..} <- getReadiness aid
        pure $(widgetFile "components/navigation-bar/user")
  pure $(widgetFile "components/navigation-bar")

withFormResultNavBar :: FormResult a -> Widget -> Handler Html
withFormResultNavBar fr w =
  case fr of
    FormSuccess _ -> withNavBar w
    FormFailure ts -> withFormFailureNavBar ts w
    FormMissing -> withFormFailureNavBar ["Missing data"] w

withNavBar :: Widget -> Handler Html
withNavBar = withFormFailureNavBar []

withFormFailureNavBar :: [Text] -> Widget -> Handler Html
withFormFailureNavBar msgs body = do
  let mmsg =
        Just $
        forM_ msgs $ \msg ->
          [shamlet|<div class="alert alert-danger" role="alert">#{msg}|]
  navigationBar <- makeNavigationBarWidget
  (cur, bs) <- breadcrumbs
  env <- regtoolEnvironment <$> getYesod
  defaultLayout $(widgetFile "with-nav-bar")
