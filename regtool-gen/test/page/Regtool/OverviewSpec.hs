{-# LANGUAGE OverloadedStrings #-}

module Regtool.OverviewSpec
  ( spec
  ) where

import TestImport

import Yesod.Test

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen ()
import Regtool.TestUtils

spec :: RegtoolSpec
spec =
  ydescribe "OverviewR" $ do
    needsLoginSpec GET OverviewR
    yit "returns a 200 for an unverified user" $
      withAnyVerifiedAccountLogin_ $ do
        get OverviewR
        statusIs 200
    yit "returns a 200 for a verified user" $
      withAnyVerifiedAccountLogin_ $ do
        get OverviewR
        statusIs 200
