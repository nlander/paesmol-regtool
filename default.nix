let
  pkgsv = import (import ./nix/nixpkgs.nix);
  cleanPkgs = pkgsv {};
  validity-overlay = import (
    (cleanPkgs.fetchFromGitHub (import ./nix/validity-version.nix)
    + "/nix/overlay.nix")
  );
  pkgs = pkgsv {
    overlays = [ validity-overlay (import ./nix/overlay.nix) ];
    config.allowUnfree = true;
  };
in pkgs.release-target
