{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.BusDirection where

import Import

import Regtool.Data

import Regtool.Data.Gen.Utils ()

instance GenUnchecked BusDirection

instance GenValid BusDirection where
  genValid = genValidStructurally
