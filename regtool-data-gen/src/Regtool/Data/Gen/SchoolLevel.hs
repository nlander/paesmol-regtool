{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.SchoolLevel where

import Import

import Regtool.Data

instance GenUnchecked SchoolLevel

instance GenValid SchoolLevel
