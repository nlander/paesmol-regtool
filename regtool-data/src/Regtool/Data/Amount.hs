{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- WARNING IF YOU EVER ADD A DIVISION FUNCTION THIS ALL FALLS APART
module Regtool.Data.Amount
  ( Amount
  , unAmount
  , safeAmount
  , amountCents
  , amountInCents
  , unsafeAmount
  , unsafeUnAmount
  , zeroAmount
  , addAmount
  , subAmount
  , sumAmount
  , mulAmount
  , negateAmount
  , divAmount
  , DivResult(..)
  , absAmount
  , fmtAmount
  , validatePositiveAmount
  , validateStrictlyPositiveAmount
  ) where

import Import

import Data.Aeson
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NE

import Text.Printf

import Database.Persist
import Database.Persist.Sql

newtype Amount =
  Amount
    { unAmount :: Int -- In cents
    }
  deriving (Show, Eq, Ord, Generic, FromJSON, ToJSON)

instance Validity Amount

instance ToField Amount where
  toField = toField . unsafeUnAmount

instance PersistField Amount where
  toPersistValue = toPersistValue . unAmount
  fromPersistValue = fmap Amount . fromPersistValue

instance PersistFieldSql Amount where
  sqlType Proxy = sqlType (Proxy :: Proxy Int)

safeAmount :: Double -> Maybe Amount
safeAmount d =
  let a = unsafeAmount d
   in if unsafeAmount (unsafeUnAmount a) == a
        then Just a
        else Nothing

amountCents :: Amount -> Int
amountCents (Amount a) = a

amountInCents :: Int -> Amount
amountInCents = Amount

unsafeAmount :: Double -> Amount
unsafeAmount a = Amount $ round $ a * 100 -- TODO make this safe by checking that the roundtrip works.

unsafeUnAmount :: Amount -> Double
unsafeUnAmount (Amount a) = fromIntegral a / 100

zeroAmount :: Amount
zeroAmount = Amount 0

addAmount :: Amount -> Amount -> Amount
addAmount (Amount a1) (Amount a2) = Amount $ a1 + a2

subAmount :: Amount -> Amount -> Amount
subAmount (Amount a1) (Amount a2) = Amount $ a1 - a2

sumAmount :: [Amount] -> Amount
sumAmount = Amount . sum . map unAmount

mulAmount :: Int -> Amount -> Amount
mulAmount f (Amount a) = Amount $ f * a

divAmount :: Amount -> Word -> DivResult
divAmount (Amount 0) _ = DivOfZero
divAmount (Amount a) f =
  case f of
    0 -> DivByZero
    1 -> DivSuccess $ Amount a :| []
    _ -> do
      let a' = abs a
          piece = ceiling (fromIntegral a' / fromIntegral f :: Rational)
          rest = a' - piece
      case divAmount (Amount rest) (f - 1) of
        DivOfZero -> DivSuccess $ mulAmount (signum a) (Amount piece) :| []
        DivByZero -> DivByZero
        DivSuccess ne ->
          DivSuccess $ NE.map (mulAmount (signum a)) (Amount piece NE.<| ne)

data DivResult
  = DivOfZero
  | DivByZero
  | DivSuccess (NonEmpty Amount)
  deriving (Show, Eq, Generic)

instance Validity DivResult where
  validate DivOfZero = valid
  validate DivByZero = valid
  validate (DivSuccess ne@(a :| _)) =
    decorate "DivSuccess" $
    mconcat
      [ delve "NonEmpty Amount" ne
      , decorateList (NE.toList ne) $ \a_ ->
          declare "The amount is not zero" $ a_ /= zeroAmount
      , decorateList (NE.toList ne) $ \a_ ->
          declare "The amounts all have the same sign" $
          signum (amountCents a_) == signum (amountCents a)
      ]

absAmount :: Amount -> Amount
absAmount (Amount a) = Amount $ abs a

negateAmount :: Amount -> Amount
negateAmount (Amount a) = Amount $ negate a

fmtAmount :: Amount -> String
fmtAmount a = printf "%.2f" $ unsafeUnAmount a

validatePositiveAmount :: Amount -> Validation
validatePositiveAmount a =
  declare (unwords ["the amount is positive", show a]) $ a >= zeroAmount

validateStrictlyPositiveAmount :: Amount -> Validation
validateStrictlyPositiveAmount a =
  mconcat
    [ declare "the amount is not zero" $ a /= zeroAmount
    , validatePositiveAmount a
    ]
