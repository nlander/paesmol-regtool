{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Signup.Transport
  ( signupTrips
  , getOurTransportSignups
  ) where

import Import

import Regtool.Data

import Regtool.Core.Foundation

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

signupTrips ::
     [Entity TransportSignup]
  -> [Entity Child]
  -> [Entity BusStop]
  -> [(TransportSignupId, Entity Child, Entity BusStop)]
signupTrips signups children busStops =
  flip mapMaybe signups $ \(Entity eid TransportSignup {..}) ->
    (,,) eid <$> find ((== transportSignupChild) . entityKey) children <*>
    find ((== transportSignupBusStop) . entityKey) busStops

getOurTransportSignups ::
     MonadIO m => [ChildId] -> ReaderT SqlBackend m [Entity TransportSignup]
getOurTransportSignups cids = selectList [TransportSignupChild <-. cids] []
