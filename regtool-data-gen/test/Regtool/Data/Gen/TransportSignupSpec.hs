module Regtool.Data.Gen.TransportSignupSpec where

import TestImport

import Regtool.Data.Gen.TransportSignup

spec :: Spec
spec =
  describe "transportSignupsForChildren" $
  it "generates valids on valids" $
  forAllValid $ \cids -> forAll (transportSignupsForChildren cids) shouldBeValid
