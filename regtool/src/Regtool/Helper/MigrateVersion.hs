{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Helper.MigrateVersion
  ( migrateVersion, runVersionMigration
  ) where

import Import

import Control.Monad.Logger (logInfoN, runStderrLoggingT)
import Control.Monad.Trans.Resource (runResourceT)
import qualified Data.Text as T

import Database.Persist.Sqlite

import Regtool.OptParse

import Regtool.Core.DB
import Regtool.Data

migrateVersion :: MigrateVersionSettings -> IO ()
migrateVersion MigrateVersionSettings {..} = do
  liftIO $ copyFile migrateVersionSetsFromDb migrateVersionSetsToDb
  runStderrLoggingT $
    withSqlitePool (T.pack $ toFilePath migrateVersionSetsToDb) 1 $ \pool -> do
      logInfoN "Running data migrations"
      liftIO $ runResourceT $ runSqlPool runVersionMigration pool
      logInfoN "Data migrations finished"

runVersionMigration :: MonadIO m => ReaderT SqlBackend m ()
runVersionMigration = do
  activitySignups <- selectList [] [Asc DaycareActivitySignupId]
  daycareActivities <- selectList [] [Asc DaycareActivityId]
  forM_ activitySignups $ \(Entity _ DaycareActivitySignup {..}) -> do
    case find ((== daycareActivitySignupActivity) . entityKey) daycareActivities of
      Nothing -> pure () -- Should not happen.
      Just (Entity _ DaycareActivity {..}) -> do
        now <- liftIO getCurrentTime
        msup <- getBy $ UniqueDaycareSignup daycareActivitySignupChild daycareActivityTimeslot
        case msup of
          Just _ -> pure ()
          Nothing ->
            insertValid_
              DaycareSignup
                { daycareSignupChild = daycareActivitySignupChild
                , daycareSignupTimeslot = daycareActivityTimeslot
                , daycareSignupCreationTime = now
                }
