{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.SchoolYear where

import Import

import Regtool.Data

instance GenUnchecked SchoolYear

instance GenValid SchoolYear where
  genValid = SchoolYear <$> genValid
