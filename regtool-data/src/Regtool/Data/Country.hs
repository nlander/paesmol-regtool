{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module Regtool.Data.Country where

import Import

import Data.Aeson

import Regtool.Data.StandardOrRaw

data StandardCountry
  = Belgium
  | Netherlands
  | France
  | Germany
  | Luxemburg
  | UnitedKingdom
  | Ireland
  | Portugal
  | Spain
  | Italy
  | Denmark
  | Sweden
  | Finland
  | Austria
  deriving (Show, Eq, Ord, Enum, Bounded, Generic)

instance Validity StandardCountry

instance FromJSON StandardCountry

instance ToJSON StandardCountry

standardCountryText :: StandardCountry -> Text
standardCountryText r =
  case r of
    Belgium -> "Belgium"
    Netherlands -> "Netherlands"
    France -> "France"
    Germany -> "Germany"
    Luxemburg -> "Luxemburg"
    UnitedKingdom -> "United Kingdom"
    Ireland -> "Ireland"
    Portugal -> "Portugal"
    Spain -> "Spain"
    Italy -> "Italy"
    Denmark -> "Denmark"
    Sweden -> "Sweden"
    Finland -> "Finland"
    Austria -> "Austria"

parseStandardCountry :: Text -> Maybe StandardCountry
parseStandardCountry t =
  case t of
    "Belgium" -> Just Belgium
    "Netherlands" -> Just Netherlands
    "France" -> Just France
    "Germany" -> Just Germany
    "Luxemburg" -> Just Luxemburg
    "United Kingdom" -> Just UnitedKingdom
    "Ireland" -> Just Ireland
    "Portugal" -> Just Portugal
    "Spain" -> Just Spain
    "Italy" -> Just Italy
    "Denmark" -> Just Denmark
    "Sweden" -> Just Sweden
    "Finland" -> Just Finland
    "Austria" -> Just Austria
    _ -> Nothing

instance StandardField StandardCountry where
  standardToText = standardCountryText
  standardFromText = parseStandardCountry

type Country = StandardOrRaw StandardCountry

countryText :: Country -> Text
countryText = standardOrRawText standardCountryText

parseCountry :: Text -> Country
parseCountry = parseStandardOrRaw parseStandardCountry
