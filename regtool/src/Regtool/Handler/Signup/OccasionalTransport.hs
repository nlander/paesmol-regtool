{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.Handler.Signup.OccasionalTransport
  ( SignupChildOccasionalTransport(..)
  , getSignupOccasionalTransportR
  , postSignupOccasionalTransportR
  , postSignupOccasionalTransportSignoffR
  ) where

import Import

import qualified Data.Text as T

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Utils.Children
import Regtool.Utils.Signup.OccasionalTransport

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

data SignupChildOccasionalTransport =
  SignupChildOccasionalTransport
    { signupChildOccasionalTransportChild :: ChildId
    , signupChildOccasionalTransportBusStop :: BusStopId
    , signupChildOccasionalTransportDay :: Day
    , signupChildOccasionalTransportDirection :: BusDirection
    }
  deriving (Show, Eq, Generic)

instance Validity SignupChildOccasionalTransport

signupChildOccasionalTransportForm ::
     [Entity Child] -> [Entity BusStop] -> AForm Handler SignupChildOccasionalTransport
signupChildOccasionalTransportForm cs bss =
  SignupChildOccasionalTransport <$>
  areq
    (selectFieldList $
     map (\(Entity cid Child {..}) -> (T.unwords [childFirstName, childLastName], cid)) cs)
    (bfs ("Child" :: Text))
    Nothing <*>
  areq
    (selectFieldList $ do
       Entity bsid bs <- bss
       let t = T.unlines [unTextarea $ busStopCity bs, unTextarea $ busStopLocation bs]
       pure (t, bsid))
    (bfs ("Bus Stop" :: Text))
    Nothing <*>
  areq dayField (bfs ("Day" :: Text)) Nothing <*>
  areq (selectField optionsEnum) (bfs ("Direction" :: Text)) Nothing

getSignupPage :: Maybe (FormResult a) -> Handler Html
getSignupPage mfr = do
  aid <- requireAuthId
  busStops <- runDB $ selectList [] [Asc BusStopId]
  children <- runDB $ getChildrenOf aid
  signups <- runDB $ getOurOccasionalTransportSignups $ map entityKey children
  let ets = signupTrips signups children busStops
  token <- genToken
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $ signupChildOccasionalTransportForm children busStops
  maybe withNavBar withFormResultNavBar mfr $(widgetFile "signup/occasional-transport")

getSignupOccasionalTransportR :: Handler Html
getSignupOccasionalTransportR = getSignupPage Nothing

postSignupOccasionalTransportR :: Handler Html
postSignupOccasionalTransportR = do
  aid <- requireAuthId
  busStops <- runDB $ selectList [] [Asc BusStopId]
  children <- runDB $ getChildrenOf aid
  now <- liftIO getCurrentTime
  ((result, _), _) <-
    runFormPost $
    renderCustomForm BootstrapBasicForm $ signupChildOccasionalTransportForm children busStops
  case result of
    FormSuccess SignupChildOccasionalTransport {..} -> do
      let signup =
            OccasionalTransportSignup
              { occasionalTransportSignupChild = signupChildOccasionalTransportChild
              , occasionalTransportSignupBusStop = signupChildOccasionalTransportBusStop
              , occasionalTransportSignupDay = signupChildOccasionalTransportDay
              , occasionalTransportSignupDirection = signupChildOccasionalTransportDirection
              , occasionalTransportSignupTime = now
              }
      void $ runDB $ insertValidUnique signup
      setMessage
        "Succesfully signed up for occasional transport services. Do not forget to pay for them!"
      redirect SignupOccasionalTransportR
    fr -> getSignupPage (Just fr)

postSignupOccasionalTransportSignoffR :: OccasionalTransportSignupId -> Handler Html
postSignupOccasionalTransportSignoffR sid = do
  aid <- requireAuthId
  OccasionalTransportSignup {..} <- runDB $ get404 sid
  mcof <-
    runDB $ selectFirst [ChildOfParent ==. aid, ChildOfChild ==. occasionalTransportSignupChild] []
  case mcof of
    Nothing -> invalidArgs ["This is not your child to unregister."]
    Just _ -> do
      mte <- runDB $ selectFirst [OccasionalTransportPaymentSignup ==. sid] []
      case mte of
        Just _ -> invalidArgs ["This signup has already been paid for, it cannot be unregistered."]
        Nothing -> do
          runDB $ DB.delete sid
          redirect SignupOccasionalTransportR
