{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Signup.OccasionalTransport
  ( signupTrips
  , getOurOccasionalTransportSignups
  ) where

import Import

import Regtool.Data

import Regtool.Core.Foundation

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

signupTrips ::
     [Entity OccasionalTransportSignup]
  -> [Entity Child]
  -> [Entity BusStop]
  -> [(Entity OccasionalTransportSignup, Entity Child, Entity BusStop)]
signupTrips signups children busStops =
  flip mapMaybe signups $ \eots@(Entity _ OccasionalTransportSignup {..}) ->
    (,,) eots <$> find ((== occasionalTransportSignupChild) . entityKey) children <*>
    find ((== occasionalTransportSignupBusStop) . entityKey) busStops

getOurOccasionalTransportSignups ::
     MonadIO m => [ChildId] -> ReaderT SqlBackend m [Entity OccasionalTransportSignup]
getOurOccasionalTransportSignups cids = selectList [OccasionalTransportSignupChild <-. cids] []
