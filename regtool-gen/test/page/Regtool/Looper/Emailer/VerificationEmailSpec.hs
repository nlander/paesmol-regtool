{-# LANGUAGE OverloadedStrings #-}

module Regtool.Looper.Emailer.VerificationEmailSpec
  ( spec
  ) where

import TestImport

import Data.Time

import Yesod.Test

import Regtool.Data

import Regtool.Core.Foundation.Regtool
import Regtool.Looper.Emailer.Verification

import Regtool.Looper.Emailer.TestUtils
import Regtool.TestUtils

spec :: RegtoolSpec
spec =
  ydescribe "verification email" $
  verificationGoldenTest
    "test_resources/email/verification/1"
    VerificationEmail
      { verificationEmailTo = unsafeEmailAddress "example" "domain.com"
      , verificationEmailKey = "key"
      , verificationEmailLink = "link"
      , verificationEmailTimestamp = UTCTime (fromGregorian 1970 1 1) 0
      , verificationEmailEmail = Nothing
      }

verificationGoldenTest :: String -> VerificationEmail -> YesodSpec Regtool
verificationGoldenTest path =
  emailGoldenTests
    path
    verificationEmailTextContent
    verificationEmailHtmlContent
