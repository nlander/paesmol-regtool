{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}

module Regtool where

import Import

import Control.Concurrent.Async as Async
import Control.Lens
import Control.Monad.Logger
import Control.Monad.Trans.Resource (runResourceT)
import Data.Pool

import qualified Database.Persist.Sqlite as DB

import qualified Data.Text as T

import qualified Network.HTTP.Client as Http
import qualified Network.HTTP.Types as Http
import qualified Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.Wai.Middleware.RequestLogger as Wai

import Yesod.Core

import Regtool.Application ()
import Regtool.Core.Constants
import Regtool.Core.Foundation
import Regtool.Core.Migrations
import Regtool.Data
import Regtool.Helper.MigrateVersion
import Regtool.Helper.SetupTestDatabase
import Regtool.Helper.VerifyDatabase
import Regtool.Looper
import Regtool.Looper.Types
import Regtool.OptParse

openConnectionCount :: Int
openConnectionCount = 1

regtool :: IO ()
regtool = do
  (disp, Settings) <- getInstructions
  case disp of
    DispatchRun runSets -> run runSets
    DispatchSetupTestDatabase stdbSets -> setupTestDatabase stdbSets
    DispatchMigrateVersion mvSets -> migrateVersion mvSets
    DispatchVerifyDatabase vdSets -> verifyDatabase vdSets

run :: RunSettings -> IO ()
run sets_@RunSettings {..} = do
  httpManager <- Http.newManager Http.defaultManagerSettings
  runStderrLoggingT $
    filterLogger (\_ ll -> ll >= runSetsLogLevel) $ do
      logInfoN $ T.unlines ["Running Regtool with these settings:", T.pack (ppShow sets_)]
      DB.withSqlitePoolInfo
        (DB.mkSqliteConnectionInfo (T.pack $ toFilePath runSetsDbFile) & DB.walEnabled .~ False &
         DB.fkEnabled .~ False)
        openConnectionCount $ \pool -> do
        setupDB pool sets_
        let thx = makeRegtool runSetsEnv pool httpManager runSetsStripeSettings
            resolvedRoot =
              T.concat
                [ case runSetsEnv of
                    EnvManualTesting -> "http://"
                    _ -> "https://"
                , runSetsHost
                , case runSetsEnv of
                    EnvManualTesting -> ":" <> T.pack (show runSetsPort)
                    _ -> ""
                ]
        let looperEnv =
              LooperEnv
                { looperDatabaseFilename = runSetsDbFile
                , looperConnectionPool = pool
                , looperRegtool = thx
                , looperApproot = resolvedRoot
                , looperEmailAddress = runSetsEmailAddress
                }
        let defMiddles = defaultMiddlewaresNoLogging
        let extraMiddles =
              if development
                then Wai.logStdoutDev
                else Wai.logStdout
        let middle = extraMiddles . defMiddles
        plainApp <- liftIO $ toWaiAppPlain thx
        let app_ =
              if runSetsMaintenance
                then maintenanceApp
                else middle plainApp
        logInfoN $ "Serving at " <> resolvedRoot
        liftIO $ concurrently_ (Warp.run runSetsPort app_) (runAllLoopers sets_ looperEnv)

maintenanceApp :: Wai.Application
maintenanceApp _ resp =
  resp $
  Wai.responseLBS Http.ok200 [] $
  "<html><body><p>The registration tool is in maintenance mode at the moment.</p><p>Please come back later.</p></body></html>"

setupDB ::
     (MonadIO m, MonadLoggerIO m, MonadBaseControl IO m) => Pool SqlBackend -> RunSettings -> m ()
setupDB pool RunSettings {..} =
  runResourceT $
  flip DB.runSqlPool pool $ do
    DB.runMigration migrateAll
    regtoolMigrations
    case runSetsVerification of
      RunVerificationAndFail -> do
        logInfoN "Verifying database. Will fail if verification fails."
        res <- verifyDatabaseWithResult
        failIfVerificationFailed res
      RunVerificationButDoNotFail -> do
        logInfoN "Verifying database. Will NOT fail if verification fails."
        res <- verifyDatabaseWithResult
        case res of
          VerifiedFine -> logInfoN "Verification succeeded."
          VerificationErrors es -> mapM_ logVerificationError es
      DoNotRunVerification -> logWarnN "Not running verification at all."

makeRegtool ::
     RegtoolEnvironment -> DB.ConnectionPool -> Http.Manager -> Maybe StripeSettings -> Regtool
makeRegtool e pool httpManager mss =
  Regtool
    { regtoolEnvironment = e
    , regtoolConnectionPool = pool
    , regtoolStatic = regtoolEmbeddedStatic
    , regtoolHttpManager = httpManager
    , regtoolStripeSettings = mss
    }
