{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -O0 #-}

module Regtool.Utils.Doctors
  ( canDeleteDoctor
  ) where

import Import

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Core.Foundation

canDeleteDoctor :: DoctorId -> Handler (Deletable DoctorId)
canDeleteDoctor did = do
  mts <- runDB $ selectFirst [ChildDoctor ==. did] []
  pure $
    case mts of
      Nothing -> CanDelete $ DB.delete did
      Just _ ->
        CannotDelete ["There is still a child that refers to this doctor."]
