{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.EmailAddress where

import Import

import Data.Char
import qualified Data.Text as T

import Regtool.Data

instance GenValid EmailAddress where
  genValid =
    sized $ \n -> do
      (a, b) <- genSplit n
      user <- resize a genS
      host <- resize b genS
      let eac = normalizeEmail $ T.concat [user, "@", host]
      case emailValidateFromText eac of
        Left _ -> resize (n + 1) genValid
        Right ea -> pure ea
    where
      genS = T.pack <$> ((:) <$> genChar <*> genListOf genChar)
      genChar = choose ('\0', '\127') `suchThat` (\c -> isAlphaNum c && isAscii c)
  shrinkValid = const []
