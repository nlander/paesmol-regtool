{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.StripePayment where

import Import

import Regtool.Data

import Regtool.Data.Gen.Amount ()
import Regtool.Data.Gen.Entity ()
import Regtool.Data.Gen.Utils ()

instance GenValid StripePayment where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
