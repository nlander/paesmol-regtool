{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Checkout.Utils
  ( resolveConsequences
  , amountsShouldBe
  , expectSingleAbstractCost
  , yearlyAbstractBusFeesShouldBe
  , yearlyCalculatedBusFeesShouldBe
  ) where

import TestImport

import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M
import Data.Time

import Database.Persist (Entity(..), PersistEntity)
import Database.Persist.Sqlite

import Regtool.Application ()
import Regtool.Data
import Regtool.Gen ()
import Regtool.TestUtils

import Regtool.Utils.Consequences
import Regtool.Utils.Costs.Abstract
import Regtool.Utils.Costs.Calculate
import Regtool.Utils.Costs.Choice
import Regtool.Utils.Costs.Input
import Regtool.Utils.Costs.Invoice

{-# ANN module ("HLint: ignore Use lambda-case" :: String) #-}

genEnt :: (PersistEntity a, GenValid a, ToBackendKey SqlBackend a) => a -> IO (Entity a)
genEnt v = Entity <$> generate genValid <*> pure v

-- The stripe payment entity is just a prototype
resolveConsequences :: Entity StripePayment -> CostsInput -> Choices -> IO CostsInput
resolveConsequences (Entity spid sp) ci cs = do
  now <- getCurrentTime
  let totalAmount = invoiceTotal (deriveInvoice cs)
  let mspe =
        if totalAmount > zeroAmount
          then Just
                 (Entity spid $
                  sp
                    { stripePaymentCustomer =
                        fromMaybe (stripePaymentCustomer sp) $
                        entityKey <$> costsInputStripeCustomer ci
                    , stripePaymentAmount = totalAmount
                    })
          else Nothing
  eco@(Entity cid _) <- genEnt $ choicesCheckout (costsInputAccount ci) cs (entityKey <$> mspe) now
  let pc = paymentConsequences (costsInputAccount ci) cid now cs
  ps <- mapM genEnt $ paymentConsequencesYearlyFeePayments pc
  (tppes, tpes, tees) <-
    fmap mconcat $
    forM (paymentConsequencesTransportPaymentConsequences pc) $ \tpc ->
      case tpc of
        NewPaymentsWith tps -> do
          tpes <- mapM genEnt $ NE.toList tps
          pure ([], tpes, [])
        NewEnrollmentWithPlanAndPayments cid'' tsid tpp as -> do
          cid'' `shouldBe` cid
          tppe@(Entity tppid _) <- genEnt tpp
          let SecondStageTransportPaymentConsequence tps te =
                secondStageTransportPaymentConsequence tppid cid tsid as
          tpes <- mapM genEnt $ NE.toList tps
          tee <- genEnt te
          pure ([tppe], tpes, [tee])
  otpes <- forM (paymentConsequencesOccasionalTransportPaymentConsequences pc) genEnt
  dpes <-
    forM (paymentConsequencesDaycarePaymentConsequences pc) $ \(DaycarePaymentConsequences dsid cid_ a) ->
      genEnt
        DaycarePayment
          { daycarePaymentSignup = dsid
          , daycarePaymentCheckout = cid_
          , daycarePaymentAmount = a
          , daycarePaymentTime = now
          }
  dapes <-
    forM (paymentConsequencesDaycareActivityPaymentConsequences pc) $ \(DaycareActivityPaymentConsequences dsid cid_ a) ->
      genEnt
        DaycareActivityPayment
          { daycareActivityPaymentSignup = dsid
          , daycareActivityPaymentCheckout = cid_
          , daycareActivityPaymentAmount = a
          , daycareActivityPaymentTime = now
          }
  odpes <- forM (paymentConsequencesOccasionalDaycarePaymentConsequences pc) genEnt
  let des =
        flip map (costsInputDiscounts ci) $ \de@(Entity di d) ->
          case find ((== di) . snd) $ paymentConsequencesCompleteDiscounts pc of
            Nothing -> de
            Just (checkoutId, _) -> Entity di d {discountCheckout = Just checkoutId}
  mde <- forM (paymentConsequencesNewDiscount pc) genEnt
  pure
    ci
      { costsInputStripePayments = costsInputStripePayments ci ++ maybeToList mspe
      , costsInputCheckouts = costsInputCheckouts ci ++ [eco]
      , costsInputYearlyFeePayments = costsInputYearlyFeePayments ci ++ ps
      , costsInputTransportPaymentPlans = costsInputTransportPaymentPlans ci ++ tppes
      , costsInputTransportPayments = costsInputTransportPayments ci ++ tpes
      , costsInputTransportEnrollments = costsInputTransportEnrollments ci ++ tees
      , costsInputOccasionalTransportPayments = costsInputOccasionalTransportPayments ci ++ otpes
      , costsInputDaycarePayments = costsInputDaycarePayments ci ++ dpes
      , costsInputDaycareActivityPayments = costsInputDaycareActivityPayments ci ++ dapes
      , costsInputOccasionalDaycarePayments = costsInputOccasionalDaycarePayments ci ++ odpes
      , costsInputDiscounts = des ++ maybeToList mde
      }

amountsShouldBe :: AbstractCosts -> CalculatedCosts -> Invoice -> [Int] -> [Int] -> Expectation
amountsShouldBe ac cc i cis dis = do
  let cls = invoiceCostLines i
      dls = invoiceDiscountLines i
      cas1 = sort $ map costLineAmount cls
      das1 = sort $ map discountLineAmount dls
      cas2 = sort $ map amountInCents cis
      das2 = sort $ map amountInCents dis
  unless (cas1 == cas2 && das1 == das2) $
    expectationFailure $
    unlines
      [ "These two should have been equal: (cost lines)"
      , unwords ["actual:  ", show $ map amountCents cas1]
      , unwords ["expected:", show $ map amountCents cas2]
      , "These two should have been equal: (discount lines)"
      , unwords ["actual:  ", show $ map amountCents das1]
      , unwords ["expected:", show $ map amountCents das2]
      , "These were the abstract costs:"
      , ppShow ac
      , "These were the calculated costs:"
      , ppShow cc
      , "This was the full invoice:"
      , ppShow i
      ]

expectSingleAbstractCost :: AbstractCosts -> IO (SchoolYear, AbstractYearlyCosts)
expectSingleAbstractCost ac =
  case M.toList (abstractCostsMap ac) of
    [] -> failure "Should not be an empty abstract cost map."
    [ayc] -> pure ayc
    aycs ->
      failure $
      unlines ["Should have been an abstract cost map with one year, but got this:", ppShow aycs]

yearlyAbstractBusFeesShouldBe ::
     M.Map (Entity Child) TransportFeeStatus -> M.Map (Entity Child) TransportFeeStatus -> IO ()
yearlyAbstractBusFeesShouldBe actualMap expectedMap =
  forM_ (zip (sort $ M.toList actualMap) (sort $ M.toList expectedMap)) $ \((ae, afs), (ee, efs)) -> do
    ae `shouldBe` ee
    unless (afs == efs) $
      expectationFailure $
      unlines
        [ "Abstract Transport Fee Status differs from expected value"
        , "For these entity keys:"
        , show (entityKey ae)
        , "actual:"
        , ppShow afs
        , "expected:"
        , ppShow efs
        ]

yearlyCalculatedBusFeesShouldBe ::
     M.Map (Entity Child) TransportPaymentStatus
  -> M.Map (Entity Child) TransportPaymentStatus
  -> IO ()
yearlyCalculatedBusFeesShouldBe actualMap expectedMap =
  forM_ (zip (sort $ M.toList actualMap) (sort $ M.toList expectedMap)) $ \((ae, afs), (ee, efs)) -> do
    ae `shouldBe` ee
    unless (afs == efs) $
      expectationFailure $
      unlines
        [ "Calculated Transport Fee Status differs from expected value"
        , "For these entity keys:"
        , show (entityKey ae)
        , "actual:"
        , ppShow afs
        , "expected:"
        , ppShow efs
        ]
