module Regtool.Handler.Checkout.Choose
  ( getCheckoutChooseR
  ) where

import Import

import Regtool.Core.Foundation

import Regtool.Handler.Checkout.Page

getCheckoutChooseR :: Handler Html
getCheckoutChooseR = getCheckoutChoosePage Nothing
