Doctor
  account               AccountId
  firstName             Text
  lastName              Text
  addressLine1          Text
  addressLine2          Text          Maybe
  postalCode            Text
  city                  Text
  country               Country
  phoneNumber1          Text
  phoneNumber2          Text          Maybe
  email1                EmailAddress
  email2                EmailAddress  Maybe
  comments              Textarea      Maybe
  registrationTime      UTCTime

  deriving Show
  deriving Eq
  deriving Ord
  deriving Generic
  deriving Typeable

