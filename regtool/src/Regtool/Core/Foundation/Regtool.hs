{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Regtool.Core.Foundation.Regtool where

import Import

import qualified Network.HTTP.Client as Http

import Web.Stripe as Stripe (StripeConfig)

import Database.Persist.Sqlite

import Yesod.Auth
import Yesod.Core
import Yesod.EmbeddedStatic

import Regtool.Data

type RegtoolWidget = RegtoolWidget' ()

type RegtoolWidget' = WidgetT Regtool IO

type RegtoolHandler = HandlerT Regtool IO

type RegtoolAuthHandler = HandlerT Auth RegtoolHandler

data RegtoolEnvironment
  = EnvProduction
  | EnvStaging
  | EnvManualTesting
  | EnvAutomatedTesting
  deriving (Show, Read, Eq, Enum, Bounded, Generic)

instance Validity RegtoolEnvironment

data Regtool =
  Regtool
    { regtoolEnvironment :: RegtoolEnvironment
        -- ^ The environment to testing it
    , regtoolConnectionPool :: ConnectionPool
        -- ^ The pool of database connections
    , regtoolStatic :: EmbeddedStatic
        -- ^ Settings concerning static files
    , regtoolHttpManager :: Http.Manager
        -- ^ The config to use for stripe requests
    , regtoolStripeSettings :: Maybe StripeSettings
        -- This must be 'Nothing' in automated tests, but never in any other environment.
    }

data StripeSettings =
  StripeSettings
    { stripeSetsConfig :: Stripe.StripeConfig
    , stripeSetsPublishableKey :: Text
    }
  deriving (Show, Eq)

mkYesodData "Regtool" $(parseRoutesFile "routes.txt")
