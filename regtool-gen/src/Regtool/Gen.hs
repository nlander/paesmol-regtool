{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -Wno-dodgy-exports #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}

module Regtool.Gen
  ( module Regtool.Data.Gen
  , module Regtool.Gen
  , module Regtool.Gen.Checkout
  , module Regtool.Gen.Costs
  ) where

import GenImport

import Regtool.Data

import Regtool.Data.Gen

import Regtool.Handler.Children
import Regtool.Handler.Doctors
import Regtool.Handler.Parents

import Regtool.Gen.Checkout ()
import Regtool.Gen.Costs

instance GenValid RegisterParent where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

genSimpleRegisterParent :: Gen RegisterParent
genSimpleRegisterParent =
  RegisterParent <$> genSimpleFirstName <*> genSimpleLastName <*> genSimpleAddressLine1 <*>
  genSimpleAddressLine2 <*>
  genSimplePostalCode <*>
  genSimpleCity <*>
  genSimpleCountry <*>
  genSimplePhoneNumber1 <*>
  genSimplePhoneNumber2 <*>
  genSimpleEmail1 <*>
  genSimpleEmail2 <*>
  genSimpleCompany <*>
  genSimpleRelationShip <*>
  genSimpleComments <*>
  pure Nothing

genSimpleRegisterDoctor :: Gen RegisterDoctor
genSimpleRegisterDoctor =
  RegisterDoctor <$> genSimpleFirstName <*> genSimpleLastName <*> genSimpleAddressLine1 <*>
  genSimpleAddressLine2 <*>
  genSimplePostalCode <*>
  genSimpleCity <*>
  genSimpleCountry <*>
  genSimplePhoneNumber1 <*>
  genSimplePhoneNumber2 <*>
  genSimpleEmail1 <*>
  genSimpleEmail2 <*>
  genSimpleComments <*>
  pure Nothing

genSimpleRegisterChild :: DoctorId -> LanguageSectionId -> Gen RegisterChild
genSimpleRegisterChild did lsid =
  RegisterChild <$> genSimpleFirstName <*> genSimpleLastName <*> genSimpleBirthDate <*>
  genSimpleSchoolLevel <*>
  pure lsid <*>
  genSimpleNationality <*>
  genSimpleAllergies <*>
  genSimpleHealthInfo <*>
  genSimpleMedication <*>
  genSimpleComments <*>
  genSimplePickupAllowed <*>
  genSimplePickupDisallowed <*>
  genSimplePicturePermission <*>
  genSimpleDelijnBus <*>
  pure did <*>
  pure Nothing
