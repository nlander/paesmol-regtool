{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Data
  ( module Regtool.Data
  , module Regtool.Data.Amount
  , module Regtool.Data.BusDirection
  , module Regtool.Data.Company
  , module Regtool.Data.Country
  , module Regtool.Data.EmailAddress
  , module Regtool.Data.Nationality
  , module Regtool.Data.RelationShip
  , module Regtool.Data.SchoolDay
  , module Regtool.Data.SchoolLevel
  , module Regtool.Data.SchoolYear
  , module Regtool.Data.Semestre
  , module Regtool.Data.StandardOrRaw
  , module Regtool.Data.Utils
  ) where

import Import

import Data.Aeson
import Data.Set (Set)
import Data.Time
import Data.Typeable

import Yesod.Form

import Database.Persist
import Database.Persist.Sql
import Database.Persist.TH

import Regtool.Data.Amount
import Regtool.Data.BusDirection
import Regtool.Data.Company
import Regtool.Data.Country
import Regtool.Data.EmailAddress
import Regtool.Data.Nationality
import Regtool.Data.RelationShip
import Regtool.Data.SchoolDay
import Regtool.Data.SchoolLevel
import Regtool.Data.SchoolYear
import Regtool.Data.Semestre
import Regtool.Data.StandardOrRaw
import Regtool.Data.Stripe as Stripe
import Regtool.Data.TH
import Regtool.Data.Utils

instance Validity (Key a) where
  validate = trivialValidation

instance Validity a => Validity (Entity a) where
  validate e = mconcat [delve "entityKey" $ entityKey e, delve "entityVal" $ entityVal e]

instance (PersistEntity a, FromJSON a) => FromJSON (Entity a) where
  parseJSON = withObject "Entity" $ \o -> Entity <$> o .: "key" <*> o .: "value"

instance ToJSON a => ToJSON (Entity a) where
  toJSON (Entity k v) = object ["key" .= k, "value" .= v]

instance ToJSON a => ToJSONKey (Entity a)

instance (PersistEntity a, FromJSON a) => FromJSONKey (Entity a)

type Userkey = Text

type Username = Text

type TimeStamp = UTCTime

instance Validity Textarea where
  validate = validate . unTextarea

share [mkPersist sqlSettings, mkMigrate "migrateAll"] $allModels

instance Validity Account where
  validate a@Account {..} =
    mconcat
      [ genericValidate a
      , declare "the verification key must exist if the account is not verified" $
        isJust accountVerificationKey || accountVerified
      ]

instance FromJSON Account

instance ToJSON Account

instance Validity Email

instance FromJSON Email

instance ToJSON Email

instance Validity LanguageSection

instance FromJSON LanguageSection

instance ToJSON LanguageSection

instance Validity TransportSignup where
  validate ts@TransportSignup {..} =
    mconcat
      [ genericValidate ts
      , declare
          (unwords
             ["the number of signups is strictly positive: ", show transportSignupInstalments]) $
        transportSignupInstalments >= 1
      ]

instance FromJSON TransportSignup

instance ToJSON TransportSignup

instance Validity TransportPayment where
  validate tp@TransportPayment {..} =
    mconcat [genericValidate tp, validateStrictlyPositiveAmount transportPaymentAmount]

instance FromJSON TransportPayment

instance ToJSON TransportPayment

instance Validity TransportPaymentPlan where
  validate tpp@TransportPaymentPlan {..} =
    mconcat [genericValidate tpp, validateStrictlyPositiveAmount transportPaymentPlanTotalAmount]

instance FromJSON TransportPaymentPlan

instance ToJSON TransportPaymentPlan

instance Validity TransportEnrollment

instance FromJSON TransportEnrollment

instance ToJSON TransportEnrollment

instance Validity OccasionalTransportSignup

instance FromJSON OccasionalTransportSignup

instance ToJSON OccasionalTransportSignup

instance Validity OccasionalTransportPayment where
  validate dcp@OccasionalTransportPayment {..} =
    mconcat [genericValidate dcp, validatePositiveAmount occasionalTransportPaymentAmount]

instance FromJSON OccasionalTransportPayment

instance ToJSON OccasionalTransportPayment

instance Validity YearlyFeePayment where
  validate yfp@YearlyFeePayment {..} =
    mconcat [genericValidate yfp, validatePositiveAmount yearlyFeePaymentAmount]

instance FromJSON YearlyFeePayment

instance ToJSON YearlyFeePayment

instance Validity Checkout where
  validate co@Checkout {..} =
    mconcat
      [ genericValidate co
      , declare
          "If the checkout has a payment, then the amount must have been strictly positive otherwise it must have been negative" $
        case checkoutPayment of
          Nothing -> checkoutAmount <= zeroAmount
          Just _ -> checkoutAmount > zeroAmount
      ]

instance FromJSON Checkout

instance ToJSON Checkout

instance Validity Discount where
  validate d@Discount {..} =
    mconcat [genericValidate d, validateStrictlyPositiveAmount discountAmount]

instance FromJSON Discount

instance ToJSON Discount

instance Validity Doctor

instance FromJSON Doctor

instance ToJSON Doctor

instance Validity Child

instance FromJSON Child

instance ToJSON Child

instance FromJSONKey Child

instance ToJSONKey Child

instance Validity ChildOf

instance FromJSON ChildOf

instance ToJSON ChildOf

instance Validity Parent

instance FromJSON Parent

instance ToJSON Parent

instance Validity PasswordResetEmail

instance FromJSON PasswordResetEmail

instance ToJSON PasswordResetEmail

instance Validity VerificationEmail

instance FromJSON VerificationEmail

instance ToJSON VerificationEmail

instance Validity PaymentReceivedEmail

instance FromJSON PaymentReceivedEmail

instance ToJSON PaymentReceivedEmail

instance Validity StripeCustomer

instance FromJSON StripeCustomer

instance ToJSON StripeCustomer

instance Validity StripePayment where
  validate sp@StripePayment {..} =
    mconcat [genericValidate sp, validateStrictlyPositiveAmount stripePaymentAmount]

instance FromJSON StripePayment

instance ToJSON StripePayment

instance Validity BusLine where
  validate bl@BusLine {..} =
    mconcat
      [ genericValidate bl
      , declare (unwords ["the number of seats must be strictly positive: ", show busLineSeats]) $
        busLineSeats >= 1
      ]

instance FromJSON BusLine

instance ToJSON BusLine

instance Validity BusStop

instance FromJSON BusStop

instance ToJSON BusStop

instance Validity BusLineStop

instance FromJSON BusLineStop

instance ToJSON BusLineStop

instance Validity DaycareSemestre

instance FromJSON DaycareSemestre

instance ToJSON DaycareSemestre

instance Validity DaycareTimeslot where
  validate dcts@DaycareTimeslot {..} =
    mconcat
      [ genericValidate dcts
      , declare "The end time is after the start time" $ daycareTimeslotEnd >= daycareTimeslotStart
      , validatePositiveAmount daycareTimeslotFee
      , validatePositiveAmount daycareTimeslotOccasionalFee
      ]

instance FromJSON DaycareTimeslot

instance ToJSON DaycareTimeslot

instance Validity DaycareSignup

instance FromJSON DaycareSignup

instance ToJSON DaycareSignup

instance Validity DaycarePayment where
  validate dcp@DaycarePayment {..} =
    mconcat [genericValidate dcp, validatePositiveAmount daycarePaymentAmount]

instance FromJSON DaycarePayment

instance ToJSON DaycarePayment

instance Validity DaycareActivity where
  validate dcp@DaycareActivity {..} =
    mconcat [genericValidate dcp, validatePositiveAmount daycareActivityFee]

instance FromJSON DaycareActivity

instance ToJSON DaycareActivity

instance Validity DaycareActivitySignup

instance FromJSON DaycareActivitySignup

instance ToJSON DaycareActivitySignup

instance Validity DaycareActivityPayment where
  validate dcp@DaycareActivityPayment {..} =
    mconcat [genericValidate dcp, validatePositiveAmount daycareActivityPaymentAmount]

instance FromJSON DaycareActivityPayment

instance ToJSON DaycareActivityPayment

instance Validity OccasionalDaycareSignup

instance FromJSON OccasionalDaycareSignup

instance ToJSON OccasionalDaycareSignup

instance Validity OccasionalDaycarePayment where
  validate dcp@OccasionalDaycarePayment {..} =
    mconcat [genericValidate dcp, validatePositiveAmount occasionalDaycarePaymentAmount]

instance FromJSON OccasionalDaycarePayment

instance ToJSON OccasionalDaycarePayment
