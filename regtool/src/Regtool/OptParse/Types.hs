{-# LANGUAGE DeriveGeneric #-}

module Regtool.OptParse.Types where

import Import

import Control.Monad.Logger
import Looper

import Regtool.Core.Foundation.Regtool
import Regtool.Data

type Instructions = (Dispatch, Settings)

data Dispatch
  = DispatchRun RunSettings
  | DispatchSetupTestDatabase SetupTestDatabaseSettings
  | DispatchMigrateVersion MigrateVersionSettings
  | DispatchVerifyDatabase VerifyDatabaseSettings

data RunSettings =
  RunSettings
    { runSetsEnv :: !RegtoolEnvironment
    , runSetsDbFile :: !(Path Abs File)
    , runSetsHost :: !Text
    , runSetsPort :: !Int
    , runSetsLogLevel :: !LogLevel
    , runSetsEmailAddress :: !EmailAddress
    , runSetsLoopers :: !LoopersSettings
    , runSetsStripeSettings :: !(Maybe StripeSettings)
    , runSetsVerification :: !RunVerificationSettings
    , runSetsMaintenance :: Bool
    }
  deriving (Show, Eq, Generic)

data RunVerificationSettings
  = RunVerificationAndFail
  | RunVerificationButDoNotFail
  | DoNotRunVerification
  deriving (Show, Eq, Generic)

data LoopersSettings =
  LoopersSettings
    { loopersSetsEmailer :: LooperSettings
    , loopersSetsBackuper :: LooperSettings
    }
  deriving (Show, Eq, Generic)

newtype SetupTestDatabaseSettings =
  SetupTestDatabaseSettings
    { setupTestDababaseSetsDbFile :: Path Abs File
    }
  deriving (Show, Eq, Generic)

data MigrateVersionSettings =
  MigrateVersionSettings
    { migrateVersionSetsFromDb :: Path Abs File
    , migrateVersionSetsToDb :: Path Abs File
    }
  deriving (Show, Eq, Generic)

newtype VerifyDatabaseSettings =
  VerifyDatabaseSettings
    { verifyDababaseSetsDbFile :: Path Abs File
    }
  deriving (Show, Eq, Generic)

data Settings =
  Settings
  deriving (Show, Eq, Generic)

type Arguments = (Command, Flags)

data Command
  = CommandRun RunFlags
  | CommandSetupTestDatabase SetupTestDatabaseFlags
  | CommandMigrateVersion MigrateVersionFlags
  | CommandVerifyDatabase VerifyDatabaseFlags

data RunFlags =
  RunFlags
    { runFlagsEnv :: Maybe String
    , runFlagsDbFile :: Maybe FilePath
    , runFlagsHost :: Maybe String
    , runFlagsPort :: Maybe Int
    , runFlagsLogLevel :: Maybe LogLevel
    , runFlagsEmailAddress :: Maybe EmailAddress
    , runFlagsLooperEmail :: LooperFlags
    , runFlagsLooperBackup :: LooperFlags
    , runFlagsStripeSecretKey :: Maybe String
    , runFlagsStripePublishableKey :: Maybe String
    , runFlagsRunVerificationFlags :: RunVerificationFlags
    , runFlagsMaintenance :: Maybe Bool
    }
  deriving (Show, Eq, Generic)

data RunVerificationFlags =
  RunVerificationFlags
    { doNotRunVerificationFlag :: Bool
    , runVerificationButDoNotFailFlag :: Bool
    , runVerificationAndFailFlag :: Bool
    }
  deriving (Show, Eq, Generic)

newtype SetupTestDatabaseFlags =
  SetupTestDatabaseFlags
    { setupTestDatabaseFlagsDbFile :: Maybe FilePath
    }
  deriving (Show, Eq, Generic)

data MigrateVersionFlags =
  MigrateVersionFlags
    { migrateVersionFlagsFromDb :: Maybe FilePath
    , migrateVersionFlagsToDb :: Maybe FilePath
    }
  deriving (Show, Eq, Generic)

newtype VerifyDatabaseFlags =
  VerifyDatabaseFlags
    { verifyDatabaseFlagsDbFile :: Maybe FilePath
    }
  deriving (Show, Eq, Generic)

data Environment =
  Environment
    { envEnv :: Maybe String
    , envDBFile :: Maybe FilePath
    , envHost :: Maybe String
    , envPort :: Maybe Int
    , envLogLevel :: Maybe LogLevel
    , envEmailAddress :: Maybe EmailAddress
    , envLooperEmail :: LooperEnvironment
    , envLooperBackup :: LooperEnvironment
    , envStripeSecretKey :: Maybe String
    , envStripePublishableKey :: Maybe String
    , envMaintenance :: Maybe Bool
    }
  deriving (Show, Eq, Generic)

data Flags =
  Flags
  deriving (Show, Eq, Generic)
