{-# OPTIONS_GHC -Wno-orphans #-}
{-# OPTIONS_GHC -O0 #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE UndecidableInstances #-}

module Regtool.Data.Gen.Utils where

import Import

import qualified Data.Text as T

import Web.Stripe.Types (ChargeId(..), CustomerId(..))

import Yesod.Form.Fields

import Regtool.Data

mgen :: Gen a -> Gen (Maybe a)
mgen gen = frequency [(1, pure Nothing), (4, Just <$> gen)]

genLowerAlpha :: Gen Char
genLowerAlpha = elements ['a' .. 'z']

genUpperAlpha :: Gen Char
genUpperAlpha = elements ['a' .. 'z']

genAlpha :: Gen Char
genAlpha = oneof [genLowerAlpha, genUpperAlpha]

genNum :: Gen Char
genNum = elements ['0' .. '1']

genAlphaNum :: Gen Char
genAlphaNum = oneof [genLowerAlpha, genUpperAlpha, genNum]

genLinespace :: Gen Char
genLinespace = frequency [(4, pure ' '), (1, pure '\t')]

genOverlinespace :: Gen Char
genOverlinespace = elements ['\r', '\n']

genWhitespace :: Gen Char
genWhitespace = oneof [genLinespace, genOverlinespace]

genSimpleChar :: Gen Char
genSimpleChar = oneof [genLowerAlpha, genUpperAlpha, genNum, genLinespace]

genSimpleCharOrWhitespace :: Gen Char
genSimpleCharOrWhitespace =
  oneof [genLowerAlpha, genUpperAlpha, genNum, genLinespace, genOverlinespace]

genSimpleText :: Gen Text
genSimpleText = T.pack <$> listOf1 genSimpleChar

genSimpleFirstName :: Gen Text
genSimpleFirstName = genSimpleText

genSimpleLastName :: Gen Text
genSimpleLastName = genSimpleText

genSimpleAddressLine1 :: Gen Text
genSimpleAddressLine1 = genSimpleText

genSimpleAddressLine2 :: Gen (Maybe Text)
genSimpleAddressLine2 = mgen genSimpleText

genSimplePostalCode :: Gen Text
genSimplePostalCode = genSimpleText

genSimpleCity :: Gen Text
genSimpleCity = genSimpleText

genSimplePhoneNumber1 :: Gen Text
genSimplePhoneNumber1 = genSimpleText

genSimplePhoneNumber2 :: Gen (Maybe Text)
genSimplePhoneNumber2 = mgen genSimpleText

genSimpleEmailAddress :: Gen EmailAddress
genSimpleEmailAddress = do
  user <- listOf1 genAlpha
  domain <- listOf1 genAlpha
  tld <- listOf1 genAlpha
  let t = T.pack $ concat [user, "@", domain, ".", tld]
  case emailAddressFromText t >>= constructValid of
    Nothing -> genSimpleEmailAddress
    Just ea -> pure ea

instance GenValid Textarea where
  genValid = Textarea <$> genValid
  shrinkValid (Textarea t) = Textarea <$> shrinkValid t

genSimpleTextarea :: Gen Textarea
genSimpleTextarea = (Textarea . T.pack) <$> listOf1 genSimpleChar

genSimpleComments :: Gen (Maybe Textarea)
genSimpleComments = mgen genSimpleTextarea

genSimpleEmail1 :: Gen EmailAddress
genSimpleEmail1 = genSimpleEmailAddress

genSimpleEmail2 :: Gen (Maybe EmailAddress)
genSimpleEmail2 = mgen genSimpleEmailAddress

genSimplePassword :: Gen Text
genSimplePassword = T.pack <$> listOf1 (elements $ ['a' .. 'z'] ++ ['A' .. 'Z'] ++ ['0' .. '9'])

instance GenValid ChargeId where
  genValid = ChargeId <$> genValid
  shrinkValid (ChargeId i) = ChargeId <$> shrinkValid i

instance GenValid CustomerId where
  genValid = CustomerId <$> genValid
  shrinkValid (CustomerId i) = CustomerId <$> shrinkValid i

instance GenUnchecked SchoolLevelType

instance GenValid SchoolLevelType
