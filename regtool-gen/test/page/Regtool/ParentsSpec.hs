{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Regtool.ParentsSpec
  ( spec
  ) where

import TestImport

import Database.Persist.Sqlite hiding (get)
import Network.HTTP.Types

import Yesod.Test

import Regtool.Application ()
import Regtool.Core.Foundation.Regtool
import Regtool.Gen
import Regtool.Parents.Utils
import Regtool.TestUtils

spec :: RegtoolSpec
spec = do
  ydescribe "RegisterParentR" $ do
    needsLoginSpec GET RegisterParentR
    needsLoginSpec POST RegisterParentR
    multiYit "Registers a parent succesfully" $ \i -> do
      let rp@RegisterParent {..} =
            generateDeterministicallyWithSeed i genSimpleRegisterParent
      withAnyVerifiedAccountLogin $ \(Entity aid _) -> do
        registerParent rp
        verifyParentRegistered aid rp
  ydescribe "ParentsR" $ do
    needsLoginSpec GET RegisterParentR
    yit "returns a 200 for an unverified user" $
      withAnyVerifiedAccountLogin_ $ do
        get ParentsR
        statusIs 200
    yit "returns a 200 for a verified user" $
      withAnyVerifiedAccountLogin_ $ do
        get ParentsR
        statusIs 200
  ydescribe "ParentR" $ needsLoginSpec GET $ ParentR $ toSqlKey 0
