{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Core.Foundation
  ( module Regtool.Core.Constants
  , module Regtool.Core.DB
  , module Regtool.Core.Form
  , module Regtool.Core.Foundation
  , module Regtool.Core.Foundation.Regtool
  , module Regtool.Core.Persist
  , module Regtool.Core.Static
  , module Regtool.Core.Widget
  , module Yesod.Auth
  , module Yesod.Core
  , module Yesod.Persist
  ) where

import Import

import qualified Data.Char as Char (isAlphaNum)
import qualified Data.Text as T
import Text.Hamlet
import Text.Jasmine

import qualified Crypto.Nonce as Nonce
import System.IO.Unsafe (unsafePerformIO)

import Yesod.Auth
import qualified Yesod.Auth.Email as YAE
import Yesod.Auth.Message as Msg hiding (Email)
import Yesod.Core
import Yesod.EmbeddedStatic
import Yesod.Form
import Yesod.Persist (get404)

import qualified Database.Persist as DB

import Regtool.Data

import Regtool.Core.Constants
import Regtool.Core.DB
import Regtool.Core.Form hiding (check)
import Regtool.Core.Foundation.Regtool
import Regtool.Core.Persist
import Regtool.Core.Static
import Regtool.Core.Widget

instance Yesod Regtool where
  defaultLayout widget = do
    msgs <- getMessages
    pc <-
      widgetToPageContent $ do
        addScript (StaticR static_jquery_js)
        addStylesheet (StaticR static_bootstrap_css)
        addScript (StaticR static_bootstrap_js)
        addStylesheet (StaticR static_font_awesome_css)
        toWidgetHead
          [hamlet|<link rel="icon" href=@{StaticR favicon_ico} sizes="32x32" type="image/x-icon">|]
        addStylesheet $ StaticR default_css
        addScript $ StaticR default_js
        $(widgetFile "default-body")
    withUrlRenderer $ do
      let analytics =
            if development
              then mempty
              else $(hamletFile "templates/analytics.hamlet")
      $(hamletFile "templates/default-page.hamlet")
  addStaticContent = embedStaticContent regtoolStatic StaticR mini
    where
      mini =
        if development
          then Right
          else minifym
  yesodMiddleware = defaultCsrfMiddleware . defaultYesodMiddleware
  authRoute _ = Just $ AuthR LoginR
  isAuthorized (AdminR _) _ = isAdminAuth
  isAuthorized HomeR _ = pure Authorized
  isAuthorized (AuthR _) _ = pure Authorized
  isAuthorized _ _ = isLoggedIn

isLoggedIn :: RegtoolHandler AuthResult
isLoggedIn = do
  mauth <- maybeAuthId
  pure $
    case mauth of
      Nothing -> AuthenticationRequired
      Just _ -> Authorized

isAdminAuth :: RegtoolHandler AuthResult
isAdminAuth = do
  ia <- isAdmin
  pure $
    case ia of
      Nothing -> AuthenticationRequired
      Just False -> Unauthorized "You need to be an admin."
      Just True -> Authorized

instance YesodBreadcrumbs Regtool where
  breadcrumb HomeR = pure ("Home", Nothing)
  breadcrumb OverviewR = pure ("Overview", Nothing)
  breadcrumb ProfileR = pure ("Account", Nothing)
  breadcrumb ProfileSetUserNameR = pure ("Profile: Set Username", Nothing)
    -- Parents
  breadcrumb ParentsR = pure ("Parents", Just OverviewR)
  breadcrumb RegisterParentR = pure ("Register Parent", Just ParentsR)
  breadcrumb (DeregisterParentR _) = pure ("Deregister Parent", Just ParentsR)
  breadcrumb (ParentR _) = pure ("Edit Parent", Just ParentsR)
    -- Doctors
  breadcrumb DoctorsR = pure ("Doctors", Just OverviewR)
  breadcrumb RegisterDoctorR = pure ("Register Doctor", Just DoctorsR)
  breadcrumb (DeregisterDoctorR _) = pure ("Deregister Doctor", Just ParentsR)
  breadcrumb (DoctorR _) = pure ("Edit Doctor", Just DoctorsR)
    -- Children
  breadcrumb ChildrenR = pure ("Children", Just OverviewR)
  breadcrumb RegisterChildR = pure ("Register Child", Just ChildrenR)
  breadcrumb (DeregisterChildR _) = pure ("Deregister Child", Just ChildrenR)
  breadcrumb (ChildR _) = pure ("Edit Child", Just ChildrenR)
    -- Signup
  breadcrumb SignupR = pure ("Signup", Just OverviewR)
  breadcrumb SignupTransportR = pure ("Signup for Transport", Just SignupR)
  breadcrumb (SignupTransportSignoffR _) = pure ("Signoff for transport", Just SignupTransportR)
  breadcrumb SignupOccasionalTransportR = pure ("Signup for Occasional Transport", Just SignupR)
  breadcrumb (SignupOccasionalTransportSignoffR _) =
    pure ("Signoff for occasional transport", Just SignupOccasionalTransportR)
  breadcrumb SignupDaycareR = pure ("Signup for Daycare", Just SignupR)
  breadcrumb (SignupDaycareSignoffR _) = pure ("Signoff for Daycare", Just SignupDaycareR)
  breadcrumb SignupOccasionalDaycareR = pure ("Signup for Occasional Daycare", Just SignupR)
  breadcrumb (SignupOccasionalDaycareSignoffR _) =
    pure ("Signoff for occasional daycare", Just SignupOccasionalDaycareR)
  breadcrumb SignupDaycareActivityR = pure ("Signup for Daycare Activity", Just SignupR)
  breadcrumb (SignupDaycareActivitySignoffR _) =
    pure ("Signoff for Daycare Activity", Just SignupDaycareActivityR)
    -- Checkout
  breadcrumb ChosenR = pure ("Chosen Services", Just OverviewR)
    -- Checkout
  breadcrumb CheckoutChooseR = pure ("Checkout", Just OverviewR)
  breadcrumb CheckoutPayR = pure ("Checkout", Just OverviewR)
    -- Payments
  breadcrumb PaymentsR = pure ("Payments", Just OverviewR)
    -- Admin
  breadcrumb (AdminR ar) =
    case ar of
      PanelR -> pure ("Admin Panel", Nothing)
      AdminConfigurationR -> pure ("Configuration", Just $ AdminR PanelR)
            -- Language sections
      AdminRegisterLanguageSectionR -> pure ("Register Language Section", Just $ AdminR PanelR)
      AdminLanguageSectionsR -> pure ("Language Sections", Just $ AdminR PanelR)
      (AdminLanguageSectionR _) ->
        pure ("Edit Language Section", Just $ AdminR AdminLanguageSectionsR)
      (AdminLanguageSectionDeleteR _) ->
        pure ("Delete Language Section", Just $ AdminR AdminLanguageSectionsR)
      (AdminLanguageSectionOverviewR _) ->
        pure ("Language Section Overview", Just $ AdminR AdminLanguageSectionsR)
            -- Accounts
      AdminAccountsR -> pure ("Accounts", Just $ AdminR PanelR)
      (AdminAccountR _) -> pure ("Account", Just $ AdminR AdminAccountsR)
      (AdminAccountImpersonateR _) -> pure ("Impersonate Account", Just $ AdminR AdminAccountsR)
      (AdminAccountUnregisterParentR aid _) ->
        pure ("Account: Unregister Parent", Just $ AdminR $ AdminAccountR aid)
      (AdminAccountUnregisterDoctorR aid _) ->
        pure ("Account: Unregister Doctor", Just $ AdminR $ AdminAccountR aid)
      (AdminAccountUnregisterChildR aid _) ->
        pure ("Account: Unregister Child", Just $ AdminR $ AdminAccountR aid)
            -- Bus Stops
      AdminBusStopsR -> pure ("Bus Stops", Just $ AdminR PanelR)
      (AdminBusStopR _) -> pure ("Bus Stop", Just $ AdminR AdminBusStopsR)
      (AdminBusStopArchiveR bsid) -> pure ("Archive", Just $ AdminR $ AdminBusStopR bsid)
      (AdminBusStopUnarchiveR bsid) -> pure ("Un-archive", Just $ AdminR $ AdminBusStopR bsid)
      (AdminBusStopOverviewR _) -> pure ("Bus Stop Overview", Just $ AdminR AdminBusStopsR)
      AdminRegisterBusStopR -> pure ("Register Bus Stop", Just $ AdminR AdminBusStopsR)
            -- Bus Lines
      AdminBusLinesR -> pure ("Bus Lines", Just $ AdminR PanelR)
      (AdminBusLineR _) -> pure ("Bus Line", Just $ AdminR AdminBusLinesR)
      (AdminBusLineDeleteR _) -> pure ("Delete", Just $ AdminR AdminBusLinesR)
      (AdminBusLineAttachR blid) -> pure ("Attach", Just $ AdminR $ AdminBusLineR blid)
      (AdminBusLineDetachR blid _) -> pure ("Detach", Just $ AdminR $ AdminBusLineR blid)
      (AdminBusLineOverviewR _) -> pure ("Bus Line Overview", Just $ AdminR AdminBusLinesR)
      AdminRegisterBusLineR -> pure ("Register Bus Line", Just $ AdminR AdminBusLinesR)
      AdminBusEnrollmentsR -> pure ("Bus Enrollments", Just $ AdminR PanelR)
      AdminBusEnrollmentsYearR _ -> pure ("Year", Just $ AdminR AdminBusEnrollmentsR)
      AdminBusEnrollmentsDayR -> pure ("Day", Just $ AdminR AdminBusEnrollmentsR)
      (AdminBusEnrollmentsLineR sy _) -> pure ("Line", Just $ AdminR $ AdminBusEnrollmentsYearR sy)
      (AdminBusEnrollmentsStopR sy _) -> pure ("Stop", Just $ AdminR $ AdminBusEnrollmentsYearR sy)
      (AdminAddDiscountR aid) -> pure ("Add a discount", Just $ AdminR $ AdminAccountR aid)
            -- Daycare
            -- - Semestre
      AdminRegisterDaycareSemestreR -> pure ("Register Daycare Semestre", Just $ AdminR PanelR)
      AdminDaycareSemestresR -> pure ("Daycare Semestres", Just $ AdminR PanelR)
      (AdminDaycareSemestreR _) ->
        pure ("Edit Daycare Semestre", Just $ AdminR AdminDaycareSemestresR)
      (AdminDaycareSemestreDeleteR _) ->
        pure ("Delete Daycare Semestre", Just $ AdminR AdminDaycareSemestresR)
      (AdminDaycareSemestreOverviewR _) ->
        pure ("Daycare Semestre Overview", Just $ AdminR AdminDaycareSemestresR)
      AdminDaycareEnrollmentsR -> pure ("Daycare Enrollments", Just $ AdminR PanelR)
      (AdminDaycareEnrollmentsSemestreR _) ->
        pure ("Semestre", Just $ AdminR AdminDaycareEnrollmentsR)
      (AdminDaycareEnrollmentsTimeslotR dcsid _) ->
        pure ("Timeslot", Just $ AdminR $ AdminDaycareEnrollmentsSemestreR dcsid)
      AdminDaycareEnrollmentsDayR -> pure ("Day", Just $ AdminR AdminDaycareEnrollmentsR)
      (AdminDaycareActivityEnrollmentsSemestreR _) ->
        pure ("Semestre", Just $ AdminR AdminDaycareEnrollmentsR)
      (AdminDaycareActivityEnrollmentsActivityR dcsid _) ->
        pure ("Activity", Just $ AdminR $ AdminDaycareActivityEnrollmentsSemestreR dcsid)
            -- - Timeslot
      (AdminRegisterDaycareTimeslotR sid) ->
        pure ("Register Daycare Timeslot", Just $ AdminR $ AdminDaycareSemestreOverviewR sid)
      (AdminDaycareTimeslotsR sid) ->
        pure ("Daycare Timeslots", Just $ AdminR $ AdminDaycareSemestreOverviewR sid)
      (AdminDaycareTimeslotR sid _) ->
        pure ("Edit Daycare Timeslot", Just $ AdminR $ AdminDaycareTimeslotsR sid)
      (AdminDaycareTimeslotDeleteR sid _) ->
        pure ("Delete Daycare Timeslot", Just $ AdminR $ AdminDaycareTimeslotsR sid)
      (AdminDaycareTimeslotOverviewR sid _) ->
        pure ("Daycare Timeslot Overview", Just $ AdminR $ AdminDaycareTimeslotsR sid)
            -- - Activities
      (AdminRegisterDaycareActivityR sid) ->
        pure ("Register Daycare Activity", Just $ AdminR $ AdminDaycareSemestreOverviewR sid)
      (AdminDaycareActivitiesR sid) ->
        pure ("Daycare Activities", Just $ AdminR $ AdminDaycareSemestreOverviewR sid)
      (AdminDaycareActivityR sid _) ->
        pure ("Edit Daycare Activity", Just $ AdminR (AdminDaycareActivitiesR sid))
      (AdminDaycareActivityDeleteR sid _) ->
        pure ("Delete Daycare Activity", Just $ AdminR (AdminDaycareActivitiesR sid))
      (AdminDaycareActivityOverviewR sid _) ->
        pure ("Daycare Activity Overview", Just $ AdminR $ AdminDaycareActivitiesR sid)
            -- Raw data
      AdminDataR -> pure ("Data", Just $ AdminR PanelR)
      AdminExportDataR -> pure ("Export Data", Just $ AdminR AdminDataR)
      AdminRawDataR -> pure ("Raw Data", Just $ AdminR PanelR)
      AdminExportRawDataR -> pure ("Export Raw Data", Just $ AdminR AdminRawDataR)
      (ProfileVerifyManuallyR _) -> pure ("Verify Profile Manually", Just $ AdminR PanelR)
      AdminTechnicalR -> pure ("Technical Control Panel", Just $ AdminR PanelR)
      AdminTechnicalRefreshStripeCustomersR ->
        pure ("Refresh Stripe Customers", Just $ AdminR AdminTechnicalR)
  breadcrumb (AuthR _) = pure ("Auth", Just HomeR)
  breadcrumb (StaticR _) = pure ("Static", Just HomeR)

isAdmin :: RegtoolHandler (Maybe Bool)
isAdmin = do
  mu <- maybeAuthId
  case mu of
    Nothing -> pure Nothing
    Just aid -> Just <$> accIsAdmin aid

accIsAdmin :: AccountId -> RegtoolHandler Bool
accIsAdmin aid = do
  macc <- runDB $ get aid
  pure $
    case macc of
      Nothing -> False
      Just acc -> domainPart (accountEmailAddress acc) == "paesmol.eu"

instance RenderMessage Regtool FormMessage where
  renderMessage _ _ = defaultFormMessage

instance YesodAuth Regtool where
  type AuthId Regtool = AccountId
  loginDest _ = HomeR
  logoutDest _ = HomeR
  authenticate creds = do
    let byEmail = do
          mec <- loadUser $ credsIdent creds
          pure $
            case mec of
              Nothing -> UserError InvalidEmailAddress :: AuthenticationResult Regtool
              Just (Entity aid _) -> Authenticated aid
    if credsPlugin creds == regtoolAuthPluginName
      then byEmail
      else if credsPlugin creds == impersonation
             then byEmail
             else pure $
                  ServerError $ T.unwords ["Unknown authentication plugin:", credsPlugin creds]
  authPlugins _ = [myAuthPlugin]
  authHttpManager = regtoolHttpManager
  onLogin = pure ()
  onLogout = pure ()

instance YesodAuthPersist Regtool where
  type AuthEntity Regtool = Account

instance RenderMessage Regtool AccountMsg where
  renderMessage _ _ = defaultAccountMsg

getAccount :: Handler (Entity Account)
getAccount = do
  mauth <- maybeAuthId
  aid <-
    case mauth of
      Nothing -> redirect $ AuthR LoginR
      Just aid -> pure aid
  macc <- runDB $ get aid
  acc <-
    case macc of
      Nothing -> redirect $ AuthR LoginR
      Just acc -> pure acc
  pure (Entity aid acc)

genToken :: MonadHandler m => m Html
genToken = do
  alreadyExpired
  req <- getRequest
  let tokenKey = defaultCsrfParamName
  pure $
    case reqToken req of
      Nothing -> mempty
      Just n -> [shamlet|<input type=hidden name=#{tokenKey} value=#{n}>|]

-- [ Auth ]
regtoolAuthPluginName :: Text
regtoolAuthPluginName = "regtool-auth"

myAuthPlugin :: AuthPlugin Regtool
myAuthPlugin = AuthPlugin regtoolAuthPluginName dispatch loginWidget
  where
    dispatch "POST" ["login"] = postLoginR >>= sendResponse
    dispatch "GET" ["register"] = getNewAccountR >>= sendResponse
    dispatch "POST" ["register"] = postNewAccountR >>= sendResponse
    dispatch "GET" ["reset-password"] = getResetPasswordR >>= sendResponse
    dispatch "POST" ["reset-password"] = postResetPasswordR >>= sendResponse
    dispatch "GET" ["verify", u, k] = getVerifyR u k >>= sendResponse
    dispatch "GET" ["set-password-with-key", u, k] = getSetPasswordWithKeyR u k >>= sendResponse
    dispatch "POST" ["set-password-with-key"] = postSetPasswordWithKeyR >>= sendResponse
    dispatch "GET" ["set-password-with-old"] = getSetPasswordWithOldR >>= sendResponse
    dispatch "POST" ["set-password-with-old"] = postSetPasswordWithOldR >>= sendResponse
    dispatch "POST" ["resendverifyemail"] = postResendVerifyEmailR >>= sendResponse
    dispatch _ _ = notFound
    loginWidget :: (Route Auth -> Route Regtool) -> RegtoolWidget
    loginWidget _ = do
      muk <- lookupSession "user"
      token <- genToken
      $(widgetFile "auth/login")

loadUser :: Text -> RegtoolHandler (Maybe (Entity Account))
loadUser key =
  runDB $ do
    byUser <- getBy . UniqueAccountUsername $ Just key
    case byUser of
      Just mu -> pure $ Just mu
      Nothing ->
        case emailAddressFromText key of
          Nothing -> pure Nothing
          Just ea -> getBy $ UniqueAccountEmail ea

data LoginData =
  LoginData
    { loginUserkey :: Text
    , loginPassword :: Text
    }
  deriving (Show)

loginFormPostTargetR :: AuthRoute
loginFormPostTargetR = PluginR regtoolAuthPluginName ["login"]

postLoginR :: RegtoolAuthHandler TypedContent
postLoginR = do
  let loginInputForm = LoginData <$> ireq textField "userkey" <*> ireq passwordField "passphrase"
  result <- lift $ runInputPostResult loginInputForm
  muser <-
    case result of
      FormMissing -> invalidArgs ["Form is missing"]
      FormFailure _ -> return $ Left (Msg.InvalidLogin, Nothing)
      FormSuccess (LoginData ukey pwd) -> do
        mu <- lift $ loadUser ukey
        case mu of
          Nothing -> return $ Left (Msg.InvalidUsernamePass, Just ukey)
          Just a@(Entity _ acc) ->
            return $
            if verifyPassword pwd (accountSaltedPassphraseHash acc)
              then Right a
              else Left (Msg.InvalidUsernamePass, Just ukey)
  case muser of
    Left (err, muk) -> do
      maybe (pure ()) (setSession "user") muk
      loginErrorMessageI LoginR err
    Right (Entity _ acc) ->
      if accountVerified acc
        then lift $
             setCredsRedirect $
             Creds regtoolAuthPluginName (emailAddressText $ accountEmailAddress acc) []
        else loginErrorMessageI LoginR ConfirmationEmailSentTitle

verifyPassword ::
     Text -- ^ password
  -> Text -- ^ hashed password
  -> Bool
verifyPassword = YAE.isValidPass

registerR :: AuthRoute
registerR = PluginR regtoolAuthPluginName ["register"]

getNewAccountR :: RegtoolAuthHandler Html
getNewAccountR = do
  token <- genToken
  lift $ defaultLayout $(widgetFile "auth/register")

data NewAccountData =
  NewAccountData
    { newAccountUsername :: Maybe Username
    , newAccountEmail :: EmailAddress
    , newAccountPassword1 :: Text
    , newAccountPassword2 :: Text
    }
  deriving (Show)

postNewAccountR :: RegtoolAuthHandler Html
postNewAccountR = do
  let newAccountInputForm =
        (\nad -> nad {newAccountEmail = newAccountEmail nad}) <$>
        (NewAccountData <$> iopt (checkM checkValidUsername textField) "username" <*>
         ireq emailAddressField "email" <*>
         ireq passwordField "passphrase" <*>
         ireq passwordField "passphrase-confirm")
  tm <- getRouteToParent
  mr <- lift getMessageRender
  result <- lift $ runInputPostResult newAccountInputForm
  mdata <-
    case result of
      FormMissing -> invalidArgs ["Form is missing"]
      FormFailure msg -> return $ Left msg
      FormSuccess d ->
        return $
        if newAccountPassword1 d == newAccountPassword2 d
          then Right d
          else Left [mr Msg.PassMismatch]
  case mdata of
    Left errs -> do
      setMessage $ toHtml $ T.concat errs
      redirect registerR
    Right d -> do
      void $ lift $ createNewAccount d tm
      redirect LoginR

createNewAccount ::
     NewAccountData -> (Route Auth -> Route Regtool) -> RegtoolHandler (Entity Account)
createNewAccount (NewAccountData mu email pwd _) tm = do
  case mu of
    Nothing -> pure ()
    Just u -> do
      muser <- loadUser u
      case muser of
        Just _ -> do
          setMessageI $ MsgUsernameExists u
          redirect $ tm registerR
        Nothing -> return ()
  muser' <- runDB $ getBy $ UniqueAccountEmail email
  case muser' of
    Just _ -> do
      setMessageI $ MsgEmailExists $ emailAddressText email
      redirect $ tm resetPasswordR
    Nothing -> return ()
  key <- newVerifyKey
  hashed <- hashPassword pwd
  mnew <- addNewUser mu email key hashed
  new <-
    case mnew of
      Left err -> do
        setMessage $ toHtml err
        redirect $ tm registerR
      Right x -> return x
  render <- getUrlRender
  sendVerifyEmail mu email key $ render $ tm $ verifyR email key
  setMessageI $ Msg.ConfirmationEmailSent $ emailAddressText email
  return new

hashPassword :: MonadIO m => Text -> m Text
hashPassword pwd = liftIO $ YAE.saltPass pwd

nonceGen :: Nonce.Generator
nonceGen = unsafePerformIO Nonce.new

{-# NOINLINE nonceGen #-}
newVerifyKey :: MonadIO m => m Text
newVerifyKey = Nonce.nonce128urlT nonceGen

verifyR ::
     EmailAddress -- ^ The email address
  -> Text -- ^ The verification key
  -> AuthRoute
verifyR u k = PluginR regtoolAuthPluginName ["verify", emailAddressText u, k]

addNewUser ::
     Maybe Username -> EmailAddress -> Text -> Text -> RegtoolHandler (Either Text (Entity Account))
addNewUser name email verkey pwd = do
  now <- liftIO getCurrentTime
  let account =
        Account
          { accountUsername = name
          , accountEmailAddress = email
          , accountVerified = False
          , accountVerificationKey = Just verkey
          , accountSaltedPassphraseHash = pwd
          , accountResetPassphraseKey = Nothing
          , accountCreationTime = now
          }
  ment <- runDB $ insertValidUnique account
  mr <- getMessageRender
  case ment of
    Nothing -> return $ Left $ mr $ MsgEmailExists $ emailAddressText email
    Just k -> return $ Right $ Entity k account

-- | A form validator for valid usernames during new account creation.
--
-- By default this allows usernames made up of 'isAlphaNum'.  You can also ignore
-- this validation and instead validate in 'addNewUser', but validating here
-- allows the validation to occur before database activity (checking existing
-- username) and before random salt creation (requires IO).
checkValidUsername :: Username -> RegtoolHandler (Either Text Username)
checkValidUsername u
  | T.all Char.isAlphaNum u = return $ Right u
checkValidUsername _ = do
  mr <- getMessageRender
  return $ Left $ mr MsgInvalidUsername

-- checkValidEmail :: EmailAddress -> RegtoolHandler (Either Text EmailAddress)
-- checkValidEmail u = do
--     mr <- getMessageRender
--     return .
--         either
--             (Left . (\e -> mr MsgInvalidEmail' <> ": " <> T.pack e))
--             (Right . TE.decodeUtf8 . Email.toByteString) .
--         Email.validate $
--         TE.encodeUtf8 $ emailAddressText u
-- | A form validator for valid usernames or emails during login.
--
-- By default this allows usernames made up of 'isAlphaNum', plus '@' and '.'.
-- You can also ignore this validation and instead validate in 'addNewUser',
-- but validating here allows the validation to occur before database activity
-- (checking existing username) and before random salt creation (requires IO).
checkValidLogin :: Username -> RegtoolHandler (Either Text Username)
checkValidLogin u = do
  validUser <- checkValidUsername u
  return $
    case validUser of
      Left _ ->
        case emailValidateFromText u of
          Left err -> Left $ T.pack err
          Right ea -> Right $ emailAddressText ea
      Right u_ -> Right u_

sendVerifyEmail :: Maybe Username -> EmailAddress -> Text -> Text -> RegtoolHandler ()
sendVerifyEmail _ email verKey verUrl -- TODO do something with the personal info here?
 = do
  now <- liftIO getCurrentTime
  let verEmail =
        VerificationEmail
          { verificationEmailTo = email
          , verificationEmailKey = verKey
          , verificationEmailLink = verUrl
          , verificationEmailTimestamp = now
          , verificationEmailEmail = Nothing
          }
  runDB $ insertValid_ verEmail

resetPasswordR :: AuthRoute
resetPasswordR = PluginR regtoolAuthPluginName ["reset-password"]

getResetPasswordR :: RegtoolAuthHandler Html
getResetPasswordR = do
  toParent <- getRouteToParent
  token <- genToken
  lift $ defaultLayout $(widgetFile "auth/reset-password")

postResetPasswordR :: RegtoolAuthHandler TypedContent
postResetPasswordR = do
  let resetPasswordInputForm = ireq emailField "userkey"
  result <- lift $ runInputPostResult resetPasswordInputForm
  mdata <-
    case result of
      FormMissing -> invalidArgs ["Form is missing"]
      FormFailure msg -> return $ Left msg
      FormSuccess ukey -> Right <$> lift (loadUser ukey)
  case mdata of
    Left errs -> do
      setMessage $ toHtml $ T.concat errs
      redirect LoginR
    Right Nothing -> do
      lift $ setMessageI MsgInvalidUsername
      redirect resetPasswordR
    Right (Just a@(Entity _ acc)) -> do
      key <- newVerifyKey
      lift $ setNewPasswordKey a key
      render <- getUrlRender
      lift $
        sendPasswordResetEmail (accountUsername acc) (accountEmailAddress acc) key $
        render $ setPasswordWithKeyR (emailAddressText $ accountEmailAddress acc) key
            -- Don't display the email in the message since anybody can request the resend.
      lift $ setMessageI MsgResetPwdEmailSent
      redirect LoginR

setNewPasswordKey :: Entity Account -> Text -> RegtoolHandler ()
setNewPasswordKey (Entity aid _) pkey =
  runDB $
  DB.update
    aid
    [AccountResetPassphraseKey DB.=. Just pkey] -- TODO also reset password to 'Nothing'?

setNewPassword :: Entity Account -> Text -> RegtoolHandler ()
setNewPassword (Entity aid _) saltedPass =
  runDB $ DB.update aid [AccountSaltedPassphraseHash DB.=. saltedPass]

sendPasswordResetEmail :: Maybe Username -> EmailAddress -> Text -> Text -> RegtoolHandler ()
sendPasswordResetEmail _ email resetKey resetUrl = do
  now <- liftIO getCurrentTime
  let verEmail =
        PasswordResetEmail
          { passwordResetEmailTo = email
          , passwordResetEmailKey = resetKey
          , passwordResetEmailLink = resetUrl
          , passwordResetEmailTimestamp = now
          , passwordResetEmailEmail = Nothing
          }
  runDB $ insertValid_ verEmail

getVerifyR :: Userkey -> Text -> RegtoolAuthHandler ()
getVerifyR uname k = do
  muser <- lift $ loadUser uname
  case muser of
    Nothing -> do
      lift $ setMessageI Msg.InvalidKey
      redirect LoginR
    Just a@(Entity _ acc) -> do
      when
        (isNothing (accountVerificationKey acc) ||
         accountVerificationKey acc /= Just k || accountVerified acc) $ do
        lift $ setMessageI Msg.InvalidKey
        redirect LoginR
      lift $ verifyAccount a
      lift $ setMessageI MsgEmailVerified
      lift $
        setCreds True $ Creds regtoolAuthPluginName (emailAddressText $ accountEmailAddress acc) []

verifyAccount :: Entity Account -> RegtoolHandler ()
verifyAccount (Entity aid _) =
  runDB $ DB.update aid [AccountVerified DB.=. True, AccountVerificationKey DB.=. Nothing]

setVerifyKey :: Entity Account -> Text -> RegtoolHandler ()
setVerifyKey (Entity aid _) verkey =
  runDB $ DB.update aid [AccountVerified DB.=. False, AccountVerificationKey DB.=. Just verkey]

setPasswordWithKeyR ::
     Text -- ^ The email address or username
  -> Text -- ^ The reset key
  -> AuthRoute
setPasswordWithKeyR u k = PluginR regtoolAuthPluginName ["set-password-with-key", u, k]

getSetPasswordWithKeyR :: Userkey -> Text -> RegtoolAuthHandler TypedContent
getSetPasswordWithKeyR uname resetkey = do
  muser <- lift $ loadUser uname
  case muser of
    Just (Entity _ acc)
      | isJust (accountResetPassphraseKey acc) && accountResetPassphraseKey acc == Just resetkey -> do
        token <- lift genToken
        messageRender <- lift getMessageRender
        selectRep $ do
          provideJsonMessage $ messageRender Msg.SetPass
          provideRep $ lift $ authLayout $(widgetFile "auth/set-password-with-key")
    _ -> do
      lift $ setMessageI Msg.InvalidKey
      redirect LoginR

setPasswordWithKeyTargetR :: AuthRoute
setPasswordWithKeyTargetR = PluginR regtoolAuthPluginName ["set-password-with-key"]

-- | The data for setting a new password with a reset key
data NewPasswordWithKeyData =
  NewPasswordWithKeyData
    { newPasswordWithKeyUserKey :: Text -- ^ The email address of the user
    , newPasswordWithKeyKey :: Text -- ^ Holds the reset key sent by email
    , newPasswordWithKeyPwd :: Text
    , newPasswordWithKeyConfirm :: Text
    }
  deriving (Show)

postSetPasswordWithKeyR :: RegtoolAuthHandler ()
postSetPasswordWithKeyR = do
  let newPasswordWithKeyForm =
        NewPasswordWithKeyData <$> ireq textField "userkey" <*> ireq hiddenField "resetkey" <*>
        ireq passwordField "passphrase" <*>
        ireq passwordField "passphrase-confirm"
  result <- lift $ runInputPostResult newPasswordWithKeyForm
  mnew <-
    case result of
      FormMissing -> invalidArgs ["Form is missing"]
      FormFailure msg -> return $ Left msg
      FormSuccess d
        | newPasswordWithKeyPwd d == newPasswordWithKeyConfirm d -> return $ Right d
        | otherwise -> do
          lift $ setMessageI Msg.PassMismatch
          redirect $ setPasswordWithKeyR (newPasswordWithKeyUserKey d) (newPasswordWithKeyKey d)
  case mnew of
    Left errs -> do
      setMessage $ toHtml $ T.unwords errs
      redirect LoginR
    Right d -> do
      muser <- lift $ loadUser (newPasswordWithKeyUserKey d)
      case muser of
        Nothing -> permissionDenied "Invalid username"
        Just a@(Entity _ acc) -> do
          unless (Just (newPasswordWithKeyKey d) == accountResetPassphraseKey acc) $
            permissionDenied "Invalid key"
          hashed <- hashPassword (newPasswordWithKeyPwd d)
          lift $ setNewPassword a hashed
          lift $ setMessageI Msg.PassUpdated
          lift $ setCreds True $ Creds regtoolAuthPluginName (newPasswordWithKeyUserKey d) []

setPasswordWithOldR :: AuthRoute
setPasswordWithOldR = PluginR regtoolAuthPluginName ["set-password-with-old"]

-- | Configure a new password while logged in
getSetPasswordWithOldR :: RegtoolAuthHandler TypedContent
getSetPasswordWithOldR = do
  (Entity _ acc) <- loggedInUser
  token <- lift genToken
  messageRender <- lift getMessageRender
  selectRep $ do
    provideJsonMessage $ messageRender Msg.SetPass
    provideRep $ lift $ authLayout $(widgetFile "auth/set-password-with-old")

-- | The data for setting a new password with the previous password
data NewPasswordWithOldData =
  NewPasswordWithOldData
    { newPasswordWithOldUserKey :: Text -- ^ The email address of the user
    , newPasswordWithOldOldPwd :: Text -- ^ The old password
    , newPasswordWithOldPwd :: Text
    , newPasswordWithOldConfirm :: Text
    }
  deriving (Show)

postSetPasswordWithOldR :: RegtoolAuthHandler ()
postSetPasswordWithOldR = do
  let newPasswordWithKeyForm =
        NewPasswordWithOldData <$> ireq textField "userkey" <*> ireq hiddenField "old-passphrase" <*>
        ireq passwordField "new-passphrase" <*>
        ireq passwordField "new-passphrase-confirm"
  result <- lift $ runInputPostResult newPasswordWithKeyForm
  mnew <-
    case result of
      FormMissing -> invalidArgs ["Form is missing"]
      FormFailure msg -> return $ Left msg
      FormSuccess d
        | newPasswordWithOldPwd d == newPasswordWithOldConfirm d -> return $ Right d
        | otherwise -> do
          lift $ setMessageI Msg.PassMismatch
          redirect setPasswordWithOldR
  case mnew of
    Left errs -> do
      setMessage $ toHtml $ T.unwords errs
      redirect LoginR
    Right d -> do
      muser <- lift $ loadUser (newPasswordWithOldUserKey d)
      case muser of
        Nothing -> permissionDenied "Invalid username"
        Just a@(Entity _ acc) -> do
          unless (verifyPassword (newPasswordWithOldOldPwd d) (accountSaltedPassphraseHash acc)) $ do
            lift (setMessageI MsgInvalidPassword)
            redirect setPasswordWithOldR
          hashed <- hashPassword (newPasswordWithOldPwd d)
          lift $ setNewPassword a hashed
          lift $ setMessageI Msg.PassUpdated
          lift $ setCreds True $ Creds regtoolAuthPluginName (newPasswordWithOldUserKey d) []

loggedInUser :: RegtoolAuthHandler (Entity Account)
loggedInUser = lift requireAuth

resendVerifyEmailForm :: Userkey -> AForm RegtoolHandler Userkey
resendVerifyEmailForm u = areq hiddenField "" $ Just u

postResendVerifyEmailR :: RegtoolAuthHandler ()
postResendVerifyEmailR = do
  ((result, _), _) <- lift $ runFormPost $ renderDivs $ resendVerifyEmailForm ""
  muser <-
    case result of
      FormMissing -> invalidArgs ["Form is missing"]
      FormFailure msg -> invalidArgs msg
      FormSuccess uname -> lift $ loadUser uname
  case muser
        -- The username is a hidden field so it should be correct.  No need to set a message or redirect.
        of
    Nothing -> invalidArgs ["Invalid username or email address"]
    Just a@(Entity _ acc) -> do
      key <- newVerifyKey
      lift $ setVerifyKey a key
      render <- getUrlRender
      lift $
        sendVerifyEmail (accountUsername acc) (accountEmailAddress acc) key $
        render $ verifyR (accountEmailAddress acc) key
      lift $ setMessageI $ Msg.ConfirmationEmailSent $ emailAddressText $ accountEmailAddress acc
      redirect LoginR

-- | Messages specific to yesod-auth-account-fork.  We also use messages from "Yesod.Auth.Message".
data AccountMsg
  = MsgUsername
  | MsgLoginName
  | MsgForgotPassword
  | MsgInvalidUsername
  | MsgInvalidPassword
  | MsgInvalidEmail'
  | MsgUsernameExists T.Text
  | MsgEmailExists T.Text
  | MsgResendVerifyEmail
  | MsgResetPwdEmailSent
  | MsgEmailVerified
  | MsgEmailUnverified
  | MsgCurrentPassword

-- | Defaults to 'englishAccountMsg'
defaultAccountMsg :: AccountMsg -> T.Text
defaultAccountMsg = englishAccountMsg

englishAccountMsg :: AccountMsg -> T.Text
englishAccountMsg MsgUsername = "Username"
englishAccountMsg MsgLoginName = "Username or email"
englishAccountMsg MsgForgotPassword = "Forgot password?"
englishAccountMsg MsgInvalidUsername = "Invalid username"
englishAccountMsg MsgInvalidPassword = "You must provide your current password"
englishAccountMsg MsgInvalidEmail' = "Invalid email"
englishAccountMsg (MsgUsernameExists u) =
  T.concat ["The username ", u, " already exists.  Please choose an alternate username."]
englishAccountMsg (MsgEmailExists u) =
  T.concat ["The email ", u, " already exists.  Please consider a password reset."]
englishAccountMsg MsgResendVerifyEmail = "Resend verification email"
englishAccountMsg MsgResetPwdEmailSent =
  "A password reset email has been sent to your email address."
englishAccountMsg MsgEmailVerified = "Your email has been verified."
englishAccountMsg MsgEmailUnverified = "Your email has not yet been verified."
englishAccountMsg MsgCurrentPassword = "Please fill in your current password"

impersonation :: Text
impersonation = "impersonation"

impersonateAccount :: AccountId -> Handler TypedContent
impersonateAccount aid = do
  ea <- accountEmailAddress <$> runDB (get404 aid)
  setCredsRedirect $ Creds impersonation (emailAddressText ea) []

defaultTimeFormat :: String
defaultTimeFormat = "%F"

defaultFormatDay :: Day -> String
defaultFormatDay = formatTime defaultTimeLocale defaultTimeFormat

prettyTimeFormat :: String
prettyTimeFormat = "%A %d %B %Y"

prettyFormatDay :: Day -> String
prettyFormatDay = formatTime defaultTimeLocale prettyTimeFormat

defaultFormatUTCTime :: UTCTime -> String
defaultFormatUTCTime = formatTime defaultTimeLocale "%F %T"

data Deletable a
  = CanDelete (ReaderT SqlBackend Handler ())
  | CannotDelete [Text]
  deriving (Generic)

deleteOrError :: Deletable a -> Handler ()
deleteOrError (CanDelete doDeletion) = runDB doDeletion
deleteOrError (CannotDelete ers) = invalidArgs ers
