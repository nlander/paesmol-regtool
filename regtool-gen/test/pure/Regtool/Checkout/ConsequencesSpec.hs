{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Regtool.Checkout.ConsequencesSpec
  ( spec
  ) where

import TestImport

import Database.Persist

import qualified Data.List.NonEmpty as NE

import Regtool.Application ()
import Regtool.Data
import Regtool.Data.Gen
import Regtool.Gen ()

import Regtool.Utils.Consequences
import Regtool.Utils.Costs.Abstract
import Regtool.Utils.Costs.Calculate
import Regtool.Utils.Costs.Choice
import Regtool.Utils.Costs.Input
import Regtool.Utils.Costs.Invoice

import Regtool.Checkout.Utils

{-# ANN module ("HLint: ignore Reduce duplication" :: String) #-}

spec :: Spec
spec = do
  eqSpecOnValid @PaymentConsequences
  genValidSpec @PaymentConsequences
  -- shrinkValidSpec @PaymentConsequences
  eqSpecOnValid @TransportPaymentConsequences
  genValidSpec @TransportPaymentConsequences
  -- shrinkValidSpec @TransportPaymentConsequences
  describe "yearlyFeePayment" $
    it "produces valid yearly fee payments" $
    forAllValid $ \(aid, sy, cid, yc) ->
      shouldBeValid $ yearlyFeePayments aid sy cid (yearlyChoicesYearlyFee yc)
  describe "transportPaymentConsequences" $
    it "produces valid transport payment consequences" $
    producesValidsOnValids2 transportPaymentConsequences
  describe "occasionalTransportPaymentConsequences" $ do
    it "produces valid occasional transport payment consequences" $
      producesValidsOnValids2 occasionalTransportPaymentConsequences
    it "Has one payment per choice" $
      forAllValid $ \(cid, otcs) ->
        length (occasionalTransportPaymentConsequences cid otcs) `shouldBe` length otcs
  describe "semestrelyPaymentConsequences" $
    it "produces valid payment consequences" $ producesValidsOnValids2 semestrelyPaymentConsequences
  describe "newDiscount" $ it "produces valid discounts" $ producesValidsOnValids3 newDiscount
  describe "secondStageTransportPaymentConsequence" $
    it "produces valid consequences" $
    forAllValid $ \(tppid, spid, tsid) ->
      forAll (suchThatMap (genListOf strictlyPositiveValidAmount) NE.nonEmpty) $ \as ->
        shouldBeValid $ secondStageTransportPaymentConsequence tppid spid tsid as
  describe "paymentConsequences" $ do
    it "produces valid consequences" $ forAllValid $ producesValidsOnValids3 . paymentConsequences
    monoidSpecOnValid @PaymentConsequences
    it "has one occasional transport payment per occasional transport choice" $
      forAllValid $ \aid ->
        forAllValid $ \eid ->
          forAllValid $ \now ->
            forAllValid $ \cs ->
              length
                (paymentConsequencesOccasionalTransportPaymentConsequences $
                 paymentConsequences aid eid now cs) `shouldBe`
              length (choicesOccasionalTransport cs)
    it "produces consequences that leave the costs input valid" $
      forAllValid $ \ci ->
        forAllValid $ \conf ->
          forAll (genValid `suchThat` (`notElem` map entityKey (costsInputStripePayments ci))) $ \spid ->
            forAll
              (genValid `suchThat`
               ((`notElem` map (stripePaymentCharge . entityVal) (costsInputStripePayments ci)) .
                stripePaymentCharge) `suchThat`
               ((`notElem` map (stripePaymentTimestamp . entityVal) (costsInputStripePayments ci)) .
                stripePaymentTimestamp)) $ \sp -> do
              let ac = makeAbstractCosts ci
                  cc = calculateCosts conf ac
                  cs = deriveChoices cc
                  spe = Entity spid sp
              ci' <- resolveConsequences spe ci cs
              shouldBeValid ci'
    it
      "produces consequences that leave nothing unpaid if everything is to be paid at once for a positive amount" $
      forAllValid $ \ci ->
        forAllValid $ \conf ->
          forAll (genValid `suchThat` (`notElem` map entityKey (costsInputStripePayments ci))) $ \spid ->
            forAll
              (genValid `suchThat`
               ((`notElem` map (stripePaymentCharge . entityVal) (costsInputStripePayments ci)) .
                stripePaymentCharge) `suchThat`
               ((`notElem` map (stripePaymentTimestamp . entityVal) (costsInputStripePayments ci)) .
                stripePaymentTimestamp)) $ \sp ->
              forAllValid $ \now ->
                forAllValid $ \ce' -> do
                  let ac = makeAbstractCosts ci
                      cc = calculateCosts conf ac
                      cs = deriveChoices cc
                      i = deriveInvoice cs
                      spe = Entity spid sp
                  unless (invoiceTotal i <= zeroAmount) $ -- TODO make the other test too
                   do
                    let aid = costsInputAccount ci
                    ci' <- resolveConsequences spe ci cs
                    shouldBeValid ci'
                    let ac' = makeAbstractCosts ci'
                        cc' = calculateCosts conf ac'
                        cs' = deriveChoices cc'
                        i' = deriveInvoice cs'
                        pc' = paymentConsequences aid ce' now cs'
                    shouldBeValid ac'
                    shouldBeValid cc'
                    shouldBeValid cs'
                    shouldBeValid i'
                    shouldBeValid pc'
                    unless (invoiceTotal i' == zeroAmount) $
                      expectationFailure $
                      unlines
                        [ "Everything should have been paid"
                        , "This was the input:"
                        , ppShow ci
                        , "These were the abstract costs:"
                        , ppShow ac
                        , "These were the calculated costs:"
                        , ppShow cc
                        , "These were the choices costs:"
                        , ppShow cs
                        , "This was the invoice"
                        , ppShow i
                        , "This was the second input:"
                        , ppShow ci'
                        , "but got this invoice instead:"
                        , ppShow i'
                        , "for this config"
                        , ppShow conf
                        ]
                    invoiceTotal i' `shouldBe` zeroAmount
                    pc' `shouldBe` mempty
