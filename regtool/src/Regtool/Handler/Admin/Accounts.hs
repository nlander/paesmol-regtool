{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Accounts
  ( getAdminAccountsR
  , getAdminAccountR
  , postAdminAccountImpersonateR
  , postAdminAccountUnregisterParentR
  , postAdminAccountUnregisterDoctorR
  , postAdminAccountUnregisterChildR
  ) where

import Import

import qualified Data.Text as T

import Yesod.Core

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Utils.Children
import Regtool.Utils.Doctors
import Regtool.Utils.Parents
import Regtool.Utils.Signup.Daycare
import Regtool.Utils.Signup.DaycareActivity
import Regtool.Utils.Signup.OccasionalDaycare
import Regtool.Utils.Signup.OccasionalTransport
import Regtool.Utils.Signup.Transport

import Regtool.Component.NavigationBar

getAdminAccountsR :: Handler Html
getAdminAccountsR = do
  accounts <- runDB $ selectList [] [Asc AccountId]
  token <- genToken
  withNavBar $(widgetFile "admin/accounts")

getAdminAccountR :: AccountId -> Handler Html
getAdminAccountR aid = do
  account <- runDB $ get404 aid
  token <- genToken
  parents <- runDB $ selectList [ParentAccount ==. aid] []
  doctors <- runDB $ selectList [DoctorAccount ==. aid] []
  let lookupDoctorName did =
        case find (\(Entity i _) -> did == i) doctors of
          Nothing -> "Unknown Doctor"
          Just (Entity _ d) -> T.unwords [doctorFirstName d, doctorLastName d]
  languageSections <- runDB $ selectList [] [Asc LanguageSectionId]
  let lookupLanguageSectionName lsid =
        case find (\(Entity i _) -> lsid == i) languageSections of
          Nothing -> "Unknown Language Section"
          Just (Entity _ ls) -> languageSectionName ls
  children <- runDB $ getChildrenOf aid
  let lookupChildName cid =
        case find (\(Entity i _) -> cid == i) children of
          Nothing -> "Unknown Child"
          Just (Entity _ c) -> T.unwords [childFirstName c, childLastName c]
  busStops <- runDB $ selectList [] [Asc BusStopId]
  let lookupBusStopName bid =
        case find (\(Entity i _) -> bid == i) busStops of
          Nothing -> "Unknown BusStops"
          Just (Entity _ b) ->
            T.unwords [unTextarea $ busStopCity b, unTextarea $ busStopLocation b]
  transportSignups <- runDB $ getOurTransportSignups $ map entityKey children
  occasionalTransportSignups <- runDB $ getOurOccasionalTransportSignups $ map entityKey children
  semestres <- runDB $ selectList [] [Asc DaycareSemestreId]
  timeslots <- runDB $ selectList [] [Asc DaycareTimeslotId]
  let lookupTimeslotDescription tsid =
        case find (\(Entity i _) -> tsid == i) timeslots of
          Nothing -> "Unknown Daycare Timeslot"
          Just (Entity _ ts) ->
            case find (\(Entity i _) -> daycareTimeslotSemestre ts == i) semestres of
              Nothing -> "Unknown Daycare Semestres"
              Just (Entity _ dcs) -> timeslotDescription dcs ts
  daycareSignups <- runDB $ getOurDaycareSignups $ map entityKey children
  let lookupOccasionalTimeslotDescription tsid =
        case find (\(Entity i _) -> tsid == i) timeslots of
          Nothing -> "Unknown Daycare Timeslot"
          Just (Entity _ ts) ->
            case find (\(Entity i _) -> daycareTimeslotSemestre ts == i) semestres of
              Nothing -> "Unknown Daycare Semestres"
              Just (Entity _ dcs) -> occasionalTimeslotDescription dcs ts
  occasionalDaycareSignups <- runDB $ getOurOccasionalDaycareSignups $ map entityKey children
  daycareActivities <- runDB $ selectList [] [Asc DaycareActivityId]
  let lookupDaycareSemestre dsid = find (\(Entity i _) -> dsid == i) semestres
  let lookupDaycareActivity daid = find (\(Entity i _) -> daid == i) daycareActivities
  daycareActivitySignups <-
    runDB $ getOurDaycareActivitySignups $  map entityKey children
  mc <- runDB $ getBy $ UniqueAccountCustomer aid
  payments <-
    case mc of
      Nothing -> pure []
      Just (Entity scid _) -> runDB $ selectList [StripePaymentCustomer ==. scid] []
  discounts <- runDB $ selectList [DiscountAccount ==. aid] []
  withNavBar $(widgetFile "admin/account")

postAdminAccountImpersonateR :: AccountId -> Handler TypedContent
postAdminAccountImpersonateR = impersonateAccount

postAdminAccountUnregisterParentR :: AccountId -> ParentId -> Handler Html
postAdminAccountUnregisterParentR aid pid = do
  canDeleteParent pid >>= deleteOrError
  redirect $ AdminR $ AdminAccountR aid

postAdminAccountUnregisterDoctorR :: AccountId -> DoctorId -> Handler Html
postAdminAccountUnregisterDoctorR aid did = do
  canDeleteDoctor did >>= deleteOrError
  redirect $ AdminR $ AdminAccountR aid

postAdminAccountUnregisterChildR :: AccountId -> ChildId -> Handler Html
postAdminAccountUnregisterChildR aid cid = do
  canDeleteChild cid >>= deleteOrError
  redirect $ AdminR $ AdminAccountR aid
