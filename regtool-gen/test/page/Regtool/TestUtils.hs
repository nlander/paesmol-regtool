{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Regtool.TestUtils
  ( RegtoolSpec
  , RegtoolExample
  , regtoolSpec
  , runDB
  , failure
  , genEmailAndPass
  , withVerifiedAccount
  , withVerifiedAccount_
  , withVerifiedAccountLogin
  , withVerifiedAccountLogin_
  , withAnyVerifiedAccount_
  , withAnyVerifiedAccountLogin
  , withAnyVerifiedAccountLogin_
  , lookupExistingByEmail
  , loginTo
  , needsLogin
  , needsLoginSpec
  , addPostParamMaybe
  , addPostParamStandardOrOther
  , debugBody
  , multiYit
  , multiYitN
  , StdMethod(..)
  , boolText
  , sqlKeyText
  , enumText
  , dayText
  , generateDeterministicallyWithSeed
  , shouldRedirectTo
  ) where

import TestImport

import Test.QuickCheck.Gen
import Test.QuickCheck.Random

import qualified Data.ByteString.Lazy.Char8 as LB8
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import Data.Time

import Control.Monad.Logger (runNoLoggingT)
import Control.Monad.Trans.Resource (runResourceT)
import Database.Persist.Sqlite hiding (get)

import qualified Network.HTTP.Client as Http
import Network.HTTP.Types
import Network.URI (URI(uriPath), parseURI)
import Network.Wai.Test (SResponse(..))

import Yesod.Test

import Regtool
import Regtool.Application ()
import Regtool.Core.Foundation hiding (get, runDB)
import Regtool.Data
import Regtool.Gen

type RegtoolSpec = YesodSpec Regtool

type RegtoolExample = YesodExample Regtool

regtoolSpec :: RegtoolSpec -> Spec
regtoolSpec = yesodSpecWithSiteGenerator generateSite

runDB :: SqlPersistM a -> YesodExample Regtool a
runDB query = do
  app <- getTestYesod
  liftIO $ runDBWithRegtool app query

runDBWithRegtool :: Regtool -> SqlPersistM a -> IO a
runDBWithRegtool app query = runSqlPersistMPool query (regtoolConnectionPool app)

generateSite :: IO Regtool
generateSite = do
  httpManager <- Http.newManager Http.defaultManagerSettings
  withSystemTempFile "regtool-test-database.db" $ \dbfile _ -> do
    pool <- runNoLoggingT $ createSqlitePool (T.pack $ toFilePath dbfile) 1
    void $ runResourceT $ flip runSqlPool pool $ runMigrationSilent migrateAll
    pure $ makeRegtool EnvAutomatedTesting pool httpManager Nothing

failure :: MonadIO m => String -> m a
failure msg = do
  liftIO $ expectationFailure msg
  undefined

withUnverifiedAccount ::
     EmailAddress -> Text -> (Entity Account -> RegtoolExample a) -> RegtoolExample a
withUnverifiedAccount e p func = do
  get $ AuthR registerR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl $ AuthR registerR
    addTokenFromCookie
    addPostParam "email" $ emailAddressText e
    addPostParam "passphrase" p
    addPostParam "passphrase-confirm" p
  loc <- followRedirect
  liftIO $ loc `shouldBe` Right "/auth/login"
  ae <- lookupExistingByEmail e
  func ae


genEmailAndPass :: RegtoolExample (EmailAddress, Text)
genEmailAndPass = liftIO $ generate $ (,) <$> genSimpleEmailAddress <*> genSimplePassword

withAnyEmailAndPass ::
     (EmailAddress -> Text -> RegtoolExample a -> RegtoolExample a)
  -> (RegtoolExample a -> RegtoolExample a)
withAnyEmailAndPass func1 func2 = do
  (e, p) <- genEmailAndPass
  func1 e p func2

withAnyEmailAndPass1 ::
     (EmailAddress -> Text -> (arg -> RegtoolExample a) -> RegtoolExample a)
  -> ((arg -> RegtoolExample a) -> RegtoolExample a)
withAnyEmailAndPass1 func1 func2 = do
  (e, p) <- genEmailAndPass
  func1 e p func2

withVerifiedAccount ::
     EmailAddress -> Text -> (Entity Account -> RegtoolExample a) -> RegtoolExample a
withVerifiedAccount e p func =
  withUnverifiedAccount e p $ \ent@(Entity _ acc) -> do
    verkey <-
      case accountVerificationKey acc of
        Just key -> pure key
        _ -> failure "Account should have been unverified and have a verkey"
    request $ do
      setMethod methodGet
      setUrl $ AuthR $ verifyR e verkey
      addTokenFromCookie
    statusIs 303
    func ent

withVerifiedAccount_ :: EmailAddress -> Text -> RegtoolExample a -> RegtoolExample a
withVerifiedAccount_ e p func = withVerifiedAccount e p $ const func

withVerifiedAccountLogin ::
     EmailAddress -> Text -> (Entity Account -> RegtoolExample a) -> RegtoolExample a
withVerifiedAccountLogin e p func =
  withVerifiedAccount e p $ \ent@(Entity _ acc) -> do
    loginTo (emailAddressText $ accountEmailAddress acc) p
    func ent

withVerifiedAccountLogin_ :: EmailAddress -> Text -> RegtoolExample a -> RegtoolExample a
withVerifiedAccountLogin_ e p func = withVerifiedAccountLogin e p $ const func

withAnyVerifiedAccount_ :: RegtoolExample a -> RegtoolExample a
withAnyVerifiedAccount_ = withAnyEmailAndPass withVerifiedAccount_

withAnyVerifiedAccountLogin :: (Entity Account -> RegtoolExample a) -> RegtoolExample a
withAnyVerifiedAccountLogin = withAnyEmailAndPass1 withVerifiedAccountLogin

withAnyVerifiedAccountLogin_ :: RegtoolExample a -> RegtoolExample a
withAnyVerifiedAccountLogin_ = withAnyEmailAndPass withVerifiedAccountLogin_

lookupExistingByEmail :: EmailAddress -> RegtoolExample (Entity Account)
lookupExistingByEmail email = do
  ment <- runDB $ getBy $ UniqueAccountEmail email
  case ment of
    Nothing -> failure $ unwords ["Should have existed:", show email]
    Just ent -> pure ent

loginTo :: Userkey -> Text -> RegtoolExample ()
loginTo ukey passphrase = do
  get $ AuthR LoginR
  statusIs 200
  request $ do
    setMethod methodPost
    setUrl $ AuthR loginFormPostTargetR
    addTokenFromCookie
    addPostParam "userkey" ukey
    addPostParam "passphrase" passphrase
  statusIs 303
  loc <- followRedirect
  liftIO $ loc `shouldBe` Right "/"

needsLogin :: RedirectUrl Regtool url => StdMethod -> url -> RegtoolExample ()
needsLogin method url = do
  mbloc <- firstRedirect method url
  maybe (failure "Should have location header") assertLoginPage mbloc

needsLoginSpec :: RedirectUrl Regtool url => StdMethod -> url -> RegtoolSpec
needsLoginSpec method route =
  yit "requires the user to be logged in by redirecting to the login page" $ do
    let isPost = renderStdMethod method == methodPost
    when isPost $ get $ AuthR LoginR -- To make sure there's a CSRF cookie available
    needsLogin method route

addPostParamMaybe :: Text -> Maybe Text -> RequestBuilder site ()
addPostParamMaybe f mt =
  case mt of
    Nothing -> pure ()
    Just t -> addPostParam f t

addPostParamStandardOrOther ::
     forall a site. (Bounded a, Enum a, Show a)
  => Text
  -> Text
  -> StandardOrRaw a
  -> RequestBuilder site ()
addPostParamStandardOrOther f1 f2 sor = do
  addPostParam f1 $ T.pack $ show $ standardOrOtherIndex sor
  case sor of
    Standard _ -> pure ()
    Raw t -> addPostParam f2 t

assertLoginPage :: ByteString -> RegtoolExample ()
assertLoginPage loc = do
  assertEq "correct login redirection location" "/auth/login" loc
  get $ urlPathB loc
  statusIs 200
  bodyContains "Login"

-- Stages in login process, used below
firstRedirect :: RedirectUrl Regtool url => StdMethod -> url -> RegtoolExample (Maybe ByteString)
firstRedirect method url = do
  request $ do
    let isPost = renderStdMethod method == methodPost
    setMethod $ renderStdMethod method
    when isPost addTokenFromCookie
    setUrl url
  extractLocation -- We should get redirected to the login page

extractLocation :: RegtoolExample (Maybe ByteString)
extractLocation =
  withResponse
    (\SResponse {simpleStatus = s, simpleHeaders = h} -> do
       let code = statusCode s
       assertEq
         ("Expected a 302 or 303 redirection status " ++ "but received " ++ show code)
         (code == 302 || code == 303)
         True
       return $ lookup "Location" h)

-- Convert an absolute URL (eg extracted from responses) to just the path
-- for use in test requests.
urlPath :: Text -> Text
urlPath = T.pack . maybe "" uriPath . parseURI . T.unpack

-- Internal use only - actual urls are ascii, so exact encoding is irrelevant
urlPathB :: ByteString -> Text
urlPathB = urlPath . T.decodeUtf8

debugBody :: RegtoolExample ()
debugBody = do
  mr <- getResponse
  case mr of
    Nothing -> pure ()
    Just r -> liftIO . LB8.putStrLn . simpleBody $ r

multiYitN :: Int -> String -> (Int -> RegtoolExample ()) -> RegtoolSpec
multiYitN n name func =
  ydescribe name $ forM_ [1 .. n] $ \i -> yit (concat ["[", show i, "/", show n, "]"]) $ func i

multiYit :: String -> (Int -> RegtoolExample ()) -> RegtoolSpec
multiYit = multiYitN 5

boolText :: Bool -> Text
boolText True = "yes"
boolText False = "no"

sqlKeyText :: ToBackendKey SqlBackend record => Key record -> Text
sqlKeyText = T.pack . show . fromSqlKey

enumText :: Enum a => a -> Text
enumText a = T.pack $ show $ fromEnum a + 1

dayText :: Day -> Text
dayText = T.pack . show

generateDeterministicallyWithSeed :: Int -> Gen a -> a
generateDeterministicallyWithSeed i gen = unGen gen qcgen 30
  where
    qcgen = mkQCGen i

shouldRedirectTo :: Route Regtool -> RegtoolExample ()
shouldRedirectTo route =
  withResponse $ \sresp ->
    if statusCode (simpleStatus sresp) == 303
      then do
        loc <- getLocation
        liftIO $
          case loc of
            Left _ -> expectationFailure "Did not find a location."
            Right r ->
              unless (r == route) $
              expectationFailure $
              unlines
                [ "The status code was 303, but got redirected to"
                , show loc
                , "instead of"
                , show route
                , "if it helps, the following was the status:"
                , ppShow sresp
                ]
      else liftIO $
           expectationFailure $
           unlines
             [ "Expected status code 303, and to be redirected to"
             , show route
             , "but instead, the status was"
             , show $ statusCode $ simpleStatus sresp
             , "if it helps, the following was the status:"
             , ppShow sresp
             ]
