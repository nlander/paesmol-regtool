{-# OPTIONS_GHC -Wno-orphans #-}

module Regtool.Data.Gen.TransportPaymentPlan where

import Import

import Regtool.Data

import Regtool.Data.Gen.Amount ()

instance GenUnchecked TransportPaymentPlan

instance GenValid TransportPaymentPlan where
  genValid = genValidStructurally
