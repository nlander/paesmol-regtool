{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.LanguageSections where

import Import

import qualified Database.Persist as DB
import Yesod.Core
import Yesod.Form.Bootstrap3

import Regtool.Data

import Regtool.Component.NavigationBar
import Regtool.Core.Form
import Regtool.Core.Foundation

getAdminLanguageSectionsR :: Handler Html
getAdminLanguageSectionsR = do
  languageSections <- runDB $ selectList [] [Asc LanguageSectionId]
  token <- genToken
  withNavBar $(widgetFile "admin/language-sections")

data RegisterLanguageSection =
  RegisterLanguageSection
    { registerLanguageSectionName :: Text
    , registerLanguageSectionId :: Maybe LanguageSectionId
    }
  deriving (Show, Eq)

registerLanguageSectionForm ::
     Maybe (Entity LanguageSection) -> AForm Handler RegisterLanguageSection
registerLanguageSectionForm mce =
  RegisterLanguageSection <$>
  areq textField (bfs ("Name" :: Text)) (dv languageSectionName) <*>
  aopt hiddenField ("Id" {fsLabel = ""}) (Just $ entityKey <$> mce)
  where
    dv :: (LanguageSection -> a) -> Maybe a
    dv func = (func . entityVal) <$> mce

getAdminRegisterLanguageSectionR :: Handler Html
getAdminRegisterLanguageSectionR = do
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $ registerLanguageSectionForm Nothing
  withNavBar $(widgetFile "admin/register-language-section")

postAdminRegisterLanguageSectionR :: Handler Html
postAdminRegisterLanguageSectionR = do
  ((result, formWidget), formEnctype) <-
    runFormPost $
    renderCustomForm BootstrapBasicForm $ registerLanguageSectionForm Nothing
  case result of
    FormSuccess RegisterLanguageSection {..} -> do
      now <- liftIO getCurrentTime
      let languageSection_ =
            LanguageSection
              { languageSectionName = registerLanguageSectionName
              , languageSectionCreationTime = now
              }
      case registerLanguageSectionId of
        Just i -> do
          runDB $ replaceValid i languageSection_
          redirect $ AdminR AdminLanguageSectionsR
        Nothing -> do
          did <- runDB $ insertValid languageSection_
          redirect $ AdminR $ AdminLanguageSectionR did
    _ -> withNavBar $(widgetFile "admin/register-language-section")

getAdminLanguageSectionR :: LanguageSectionId -> Handler Html
getAdminLanguageSectionR bid = do
  languageSection <- runDB $ get404 bid
  (formWidget, formEnctype) <-
    generateFormPost $
    renderCustomForm BootstrapBasicForm $
    registerLanguageSectionForm $ Just $ Entity bid languageSection
  withNavBar $(widgetFile "admin/language-section")

getAdminLanguageSectionOverviewR :: LanguageSectionId -> Handler Html
getAdminLanguageSectionOverviewR bid = do
  languageSection <- runDB $ get404 bid
  enrolledChildren <- getChildrenInLanguageSection bid
  withNavBar $(widgetFile "admin/language-section-overview")

postAdminLanguageSectionDeleteR :: LanguageSectionId -> Handler Html
postAdminLanguageSectionDeleteR lsid = do
  canDeleteLanguageSection lsid >>= deleteOrError
  redirect $ AdminR AdminLanguageSectionsR

canDeleteLanguageSection ::
     LanguageSectionId -> Handler (Deletable LanguageSectionId)
canDeleteLanguageSection lsid = do
  mc <- runDB $ selectFirst [ChildLanguageSection ==. lsid] []
  pure $
    case mc of
      Nothing -> CanDelete $ DB.delete lsid
      Just _ ->
        CannotDelete ["There is still a child in this language section."]

getChildrenInLanguageSection :: LanguageSectionId -> Handler [Entity Child]
getChildrenInLanguageSection bid =
  runDB $ selectList [ChildLanguageSection ==. bid] []
