{-# LANGUAGE RecordWildCards #-}

module Regtool.Utils.Readiness
  ( Readiness(..)
  , getReadiness
  ) where

import Import

import Regtool.Data

import Regtool.Core.Foundation
import Regtool.Utils.Children
import Regtool.Utils.Signup.Daycare
import Regtool.Utils.Signup.DaycareActivity
import Regtool.Utils.Signup.OccasionalTransport
import Regtool.Utils.Signup.Transport

data Readiness =
  Readiness
    { readyToFillInDoctors :: Bool
    , readyToFillInChildren :: Bool
    , readyToSignupChildren :: Bool
    , readyToViewChosenServices :: Bool
    , readyToViewCosts :: Bool
    , readyToViewPayments :: Bool
    }
  deriving (Show, Eq)

getReadiness :: AccountId -> Handler Readiness
getReadiness aid = do
  parents <- runDB $ selectList [ParentAccount ==. aid] []
  doctors <- runDB $ selectList [DoctorAccount ==. aid] []
  children <- runDB $ getChildrenOf aid
  let cids = map entityKey children
  transportSignups <- runDB $ getOurTransportSignups cids
  occasionalTransportSignups <- runDB $ getOurOccasionalTransportSignups cids
  daycareSignups <- runDB $ getOurDaycareSignups cids
  daycareActivitySignups <- runDB $ getOurDaycareActivitySignups cids
  mcid <- runDB $ getBy $ UniqueAccountCustomer aid
  let readyToFillInDoctors = not (null parents)
  let readyToFillInChildren = readyToFillInDoctors && not (null doctors)
  let readyToSignupChildren = readyToFillInChildren && not (null children)
  let readyToViewChosenServices =
        readyToFillInChildren &&
        (not (null transportSignups) ||
         not (null occasionalTransportSignups) ||
         not (null daycareSignups) || not (null daycareActivitySignups))
  let readyToViewCosts = readyToViewChosenServices
  let readyToViewPayments = readyToViewCosts && isJust mcid
  pure Readiness {..}
