{-# LANGUAGE TypeApplications #-}

module Regtool.Costs.InvoiceSpec
  ( spec
  ) where

import TestImport

import Regtool.Gen ()

import Regtool.Utils.Costs.Invoice

spec :: Spec
spec = do
  genValidSpec @Invoice
  -- shrinkValidSpec @Invoice
  -- doesNotShrinkToItselfSpec @Invoice
  genValidSpec @CostLine
  -- shrinkValidSpec @CostLine
  -- doesNotShrinkToItselfSpec @CostLine
  describe "deriveInvoice" $ it "produces valid invoices" $ producesValidsOnValids deriveInvoice
