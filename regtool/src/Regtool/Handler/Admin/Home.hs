{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Admin.Home where

import Yesod.Core

import Regtool.Component.NavigationBar
import Regtool.Core.Foundation

getPanelR :: Handler Html
getPanelR = withNavBar $(widgetFile "admin/panel")
