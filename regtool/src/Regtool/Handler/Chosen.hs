{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Regtool.Handler.Chosen
  ( getChosenR
  ) where

import Import

import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NE
import qualified Data.Map as M
import qualified Data.Text as T

import Regtool.Data

import Regtool.Core.Foundation

import Regtool.Component.NavigationBar

import Regtool.Utils.Children

import Regtool.Handler.Admin.Config

import Regtool.Utils.Costs.Abstract
import Regtool.Utils.Costs.Calculate
import Regtool.Utils.Time
import Regtool.Utils.Costs.Input

getChosenR :: Handler Html
getChosenR = do
  ci <- getCostsInput
  let ac = makeAbstractCosts ci
  env <- regtoolEnvironment <$> getYesod
  conf <- confCostsConfig <$> getConfiguration
  let cc = calculateCosts conf ac
  let errors =
        let e :: (Show a, Validity a) => String -> a -> Maybe (String, String, String)
            e n v =
              case prettyValidate v of
                Left err -> Just (n, ppShow v, err)
                Right _ -> Nothing
         in catMaybes
              [ e "Costs Input" ci
              , e "Abstract Costs" ac
              , e "Environment" env
              , e "Costs Config" conf
              , e "Calculated Costs" cc
              ]
  withNavBar $
    case errors of
      [] -> mconcat [$(widgetFile "chosen"), $(widgetFile "chosen-debug")]
      _ -> $(widgetFile "chosen-error")

transportServiceOverview :: Child -> TransportPaymentStatus -> Widget
transportServiceOverview c TransportNotSignedUp =
  [whamlet|#{childName c} has not been signed up for transport services|]
transportServiceOverview c (TransportPaymentNotStartedButNecessary _ _ (a :| [])) =
  [whamlet|Transport services for #{childName c} cost #{fmtAmount a} and will be paid all at once.|]
transportServiceOverview c (TransportPaymentNotStartedButNecessary (Entity _ ts) tpp as) =
  [whamlet|
        <p>
            Transport services for #{childName c} cost #{fmtAmount $ transportPaymentPlanTotalAmount tpp}
            and will be paid in #{transportSignupInstalments ts} instalments.
        <p>
          This next instalments are:
          <ul>
            $forall a <- NE.toList as
              <li>
                #{fmtAmount a}.
        |]
transportServiceOverview c (TransportPaymentStartedButNotDone (Entity _ ts) _ (Entity _ tpp) _ sps as) =
  case length sps of
    1 ->
      [whamlet|
              <p>
                The first installment (of #{transportSignupInstalments ts})
                of the transport services for #{childName c}
                that total #{fmtAmount $ transportPaymentPlanTotalAmount tpp}
                has been paid ^{paymentTimestamps sps}
              <p>
                This next instalments are:
                <ul>
                  $forall a <- NE.toList as
                    <li>
                      #{fmtAmount a}.
            |]
    l ->
      [whamlet|
              <p>
                The first #{l} instalments (of #{transportSignupInstalments ts})
                of the transport services for #{childName c}
                that total #{fmtAmount $ transportPaymentPlanTotalAmount tpp}
                have been paid ^{paymentTimestamps sps}
              <p>
                This next instalments are:
                <ul>
                  $forall a <- NE.toList as
                    <li>
                      #{fmtAmount a}.
            |]
transportServiceOverview c (TransportPaymentEntirelyDone _ _ (Entity _ tpp) _ sps) =
  [whamlet|
        Transport services for #{childName c}
        that total #{fmtAmount $ transportPaymentPlanTotalAmount tpp}
        have been paid ^{paymentTimestamps sps}|]
transportServiceOverview c (TransportPaymentNotRequiredBecauseOfAmount _) =
  [whamlet|
        Transport services for #{childName c} are free.|]
transportServiceOverview c (TransportPaymentNotRequiredBecauseOfParents _) =
  [whamlet|
        Transport services for #{childName c} are free because of parents' status.|]

occasionalTransportServiceOverview :: OccasionalTransportPaymentStatus -> Widget
occasionalTransportServiceOverview otps =
  case otps of
    OccasionalTransportPaymentUnpaid (Entity _ c) (Entity _ ots) a ->
      [whamlet|
                Occasional transport services for #{childName c} on #{prettyFormatDay $ occasionalTransportSignupDay ots} (#{prettyBusDirection $ occasionalTransportSignupDirection ots}) cost #{fmtAmount a}.|]
    OccasionalTransportPaymentPaid (Entity _ c) (Entity _ ots) _ ->
      [whamlet|
                Occasional Transport services for #{childName c} on #{prettyFormatDay $ occasionalTransportSignupDay ots} (#{prettyBusDirection $ occasionalTransportSignupDirection ots}) have been paid.|]

paymentTimestamps :: [Entity Checkout] -> Widget
paymentTimestamps cs =
  case cs of
    [] -> [whamlet|for.|]
    [Entity _ c] -> [whamlet|for on #{defaultFormatUTCTime $ checkoutTimestamp c}.|]
    _ ->
      [whamlet|
              for:
              <ul>
                $forall Entity _ c <- cs
                  <li>
                    on #{defaultFormatUTCTime $ checkoutTimestamp c}|]

daycareServiceOverview :: DaycarePaymentStatus -> Widget
daycareServiceOverview dps =
  case dps of
    DaycarePaymentUnpaid (Entity _ ec) (Entity _ dts) _ a ->
      [whamlet|
                #{childName ec} has been signed up for dacare on
                #{timeslotDescription dts}
                at a price of
                #{fmtAmount a}
                and have not been paid for.
                    |]
    DaycarePaymentPaid (Entity _ ec) (Entity _ dts) _ (Entity _ dp) ->
      [whamlet|
                #{childName ec} has been signed up for dacare on
                #{timeslotDescription dts}
                and its fee has been paid on
                #{defaultFormatUTCTime $ daycarePaymentTime dp}.
                    |]

timeslotDescription :: DaycareTimeslot -> Text
timeslotDescription DaycareTimeslot {..} =
  T.unwords
    [ schoolDayText daycareTimeslotSchoolDay
    , T.pack $ formatTimestamp daycareTimeslotStart
    , "-"
    , T.pack $ formatTimestamp daycareTimeslotEnd
    ]

daycareActivityOverview :: DaycareActivityPaymentStatus -> Widget
daycareActivityOverview dps =
  case dps of
    DaycareActivityPaymentUnpaid (Entity _ ec) (Entity _ dts) _ a ->
      [whamlet|
                #{childName ec} has been signed up for
                #{activityDescription dts}
                at a price of
                #{fmtAmount a}
                and have not been paid for.
                    |]
    DaycareActivityPaymentPaid (Entity _ ec) (Entity _ dts) _ (Entity _ dp) ->
      [whamlet|
                #{childName ec} has been signed up for
                #{activityDescription dts}
                and its fee has been paid on
                #{defaultFormatUTCTime $ daycareActivityPaymentTime dp}.
                    |]

activityDescription :: DaycareActivity -> Text
activityDescription DaycareActivity {..} =
  T.unwords
    [ daycareActivityName
    , "on"
    , schoolDayText daycareActivitySchoolDay
    , "at"
    , T.pack $ show daycareActivityStart
    , "-"
    , T.pack $ show daycareActivityEnd
    ]

occasionalDaycareServiceOverview :: OccasionalDaycarePaymentStatus -> Widget
occasionalDaycareServiceOverview otps =
  case otps of
    OccasionalDaycarePaymentUnpaid (Entity _ c) (Entity _ ots) a ->
      [whamlet|
                Occasional daycare services for #{childName c} on #{prettyFormatDay $ occasionalDaycareSignupDay ots} cost #{fmtAmount a}.|]
    OccasionalDaycarePaymentPaid (Entity _ c) (Entity _ ots) _ ->
      [whamlet|
                Occasional Daycare services for #{childName c} on #{prettyFormatDay $ occasionalDaycareSignupDay ots} have been paid.|]
