#!/bin/bash

set -e
set -x

nice -n 19 stack install :regtool --file-watch --exec="./scripts/redo-regtool.sh" --ghc-options="-freverse-errors -DDEVELOPMENT -O0" --fast --jobs 8 --ghc-options='-fobject-code' $@
